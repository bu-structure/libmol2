libmol API Reference
====================

The basis of libmol is to provide efficient data structures for
molecular data and their parameters. It also provides functions for
reading in this data from files in common formats, and performing
common transformations and computing useful values from this data.

General principles for functions is to have "output" parameters first,
then input data, then input parameters.

Modules
---------------

.. toctree::
   :maxdepth: 3

   api/vector
   api/atom_group
   api/quaternion


Parameters
----------

.. toctree::
   :maxdepth: 2

   charmm
   mol_prms

Library Documentation
---------------------

.. toctree::
   :maxdepth: 2

   api/transform
   api/utils
   api/rmsd
