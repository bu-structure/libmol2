.. include:: ../global.rst

atom_group
==============
.. toctree::
   :maxdepth: 2

   atom_group/atom_group_contents

The ``atom_group`` module is designed to allow basic operations with molecular data.

At its core lies the :c:type:`mol_atom_group <mol_atom_group>` data structure,
which was designed to represent a molecular system as a collection of atoms. 
This data structure stores typical atomic properties
(those present in PDB files, widespread forcefield parameters, etc.),
molecular geometries, residue-level representation of the system, and some other
things. Detailed description of the atom group structure can be found in the 
:c:type:`dedicated section of documentation <mol_atom_group>`.

At this point, atom groups can be initialized from standard PDB files or libmol-
specific MS and JSON files. The relevant functions are found in the ``pdb``, ``ms``,
and ``json`` modules.


Basic operations
----------------
The ``atom_group`` module allows for basic operations on the atom group:

* :c:func:`mol_atom_group_create`
* :c:func:`mol_atom_group_join`
* :c:func:`mol_atom_group_copy`
* :c:func:`copy_atom`
* :c:func:`mol_atom_group_destroy`
* :c:func:`mol_atom_group_free`


Residue-level operations
----------------------------
As mentioned above, the atom group data structure supports residue-level
representation. The main goal of the residue-level representation layer is to 
allow fast and convenient access to the atoms belonging to a particular residue.

The central point here is the :c:type:`mol_residue` data structure,
which holds the range of atom indices belonging to a given residue, as well as 
the residue index stored as an instance of :c:type:`mol_residue_id`.

The set of :c:type:`mol_residue`'s of the given atom group is
stored in a hash table which lives in the :c:member:`mol_atom_group::residues` 
field of the atom group data structure. Every instance of :c:type:`mol_residue`
can be accessed from the hash table using the :c:func:`find_residue`
function with residue index as key. To make things easier, the atom group
:c:type:`mol_residue`'s can also be accessed through the 
:c:member:`mol_atom_group::residue_list` array.

The :c:member:`mol_atom_group::residues`  hash table is initialized when the 
atom group is read from PDB, MS, or JSON files.
The :c:member:`mol_atom_group::residue_list` array is only initialized when
reading from PDB or MS files. Both fields can also be initialized manually
(see below). Both fields are rebuilt during the atom group merging/copying.

A somewhat related entity is the :c:member:`mol_atom_group::seqres` field of 
the atom group. It is simply a string containing SEQRES entries which is
initialized when the atom group is read from a PDB file.
The :c:member:`mol_atom_group::seqres` string contains the SEQRES sequence in 
single-letter representation (e.g., ALA=A, ARG=R, and so on). The functions for
dealing with :c:member:`mol_atom_group::seqres` are located in the ``sequence`` 
module. 

Functions:
 
* :c:func:`mol_atom_group_create_residue_hash`
* :c:func:`mol_atom_group_create_residue_list`
* :c:func:`find_residue`
* :c:func:`mol_find_atom`


Metadata
--------
The concept of metadata was introduced to prevent the number of atom group
fields from growing any further while preserving the users' ability to store 
arbitrary data in the atom group instance.

In practice, metadata can be an arbitrary pointer identified by a string key
known at compile time which can be "attached" to the atom group and then
retrieved at any later point.

From the technical standpoint, metadata (or, more precisely, the pointers to it)
is stored in a hash table which lives in the :c:member:`mol_atom_group::metadata`
field of the atom group, so accessing it is slightly slower than accessing normal
atom group fields.

Note that metadata is not managed by the standard atom group
join/copy/destroy/free functions, so all the responsibility of taking care of
it lies with the user.

The ``atom_group`` module supports some standard operations with metadata:

* :c:func:`mol_atom_group_add_metadata`
* :c:func:`mol_atom_group_fetch_metadata`
* :c:func:`mol_atom_group_has_metadata`
* :c:func:`mol_atom_group_delete_metadata`
* :c:func:`mol_atom_group_join_atomic_metadata`


Selections
----------
In the most general form, selections in libmol are implemented as the following
select/filter functions:

* :c:func:`mol_atom_group_select`
* :c:func:`mol_atom_group_filter`
* :c:func:`mol_atom_group_test_foreach`

These functions can select the atoms using arbitrary user-specified criteria
and return the selection as a new atom group. The criteria themselves have to
be passed as a callback atomic checker function which tests whether a given 
atom matches the target criterium. This allows great flexibility.

At this point, there are not many premade atomic checkers, so writing an atomic
checker for any custom selection is usually the users responsibility. Still, as
an example, the module provides the following commonly used atomic checkers:

* :c:func:`mol_is_backbone`
* :c:func:`mol_is_sidechain`

The module also provides some task-specific selectors:

* :c:func:`mol_remove_atom_type`
* :c:func:`mol_extract_atom_type`
* :c:func:`mol_extract_atom_types`
* :c:func:`mol_extract_atom_type_prefix`
* :c:func:`mol_extract_chain`

Note that currently the atom groups created by all selector functions only contain
atomic fields. Geometry, metadata, residue hashes, and so on are not carried over.


Fixed/active degrees of freedom. Energy evaluation.
---------------------------------------------------
The atom group data structure, and the library in general were designed to allow
for energy evaluation and energy minimization of molecular systems in which
some atoms (often the majority of them) are "fixed" in 3D space. 
For example, one could be interested in refining the interface of the protein 
complex without affecting the non-interfacial portions of the subunits.
From the computational standpoint, it is convenient to exclude the pairs of
fixed atoms from energy calculation and various minimization procedures,
as it allows to speed-up the calculations.

The storage of info about "fixed" and "active" degrees of freedom in the 
atom group is done by the :c:member:`mol_atom_group::fixed` atomic field, and the
complementary 
:c:member:`mol_atom_group::active_atoms`,
:c:member:`mol_atom_group::active_bonds`, ... set of fields,
with the latter being constructed based on the former.

Before any energy evaluation is done, the fixed/active fields should be 
intialized with the :c:func:`mol_fixed_init` function
(it also ininializes the :c:member:`mol_atom_group::gradients` field).
Fixed atoms are specified and the complementary active lists are built
using the :c:func:`mol_fixed_update_with_list`
function (although there is a couple alternatives which do essentially
the same thing).

During the energy evaluation, only active atoms are perturbed and only the
energy terms corresponding to active-active and active-fixed atom 
interactions are computed.

Note that, in most cases, energy evaluation also requires an initialized
neighbor list for non-bonded interactions (see the documentation for
the ``nblist`` module). These neighbor lists treat fixed atoms similarly.

* :c:func:`mol_fixed_init`
* :c:func:`mol_fixed_update_active_lists`
* :c:func:`mol_fixed_update`
* :c:func:`mol_fixed_update_with_list`
* :c:func:`mol_atom_group_get_actives`
* :c:func:`mol_atom_group_set_actives`
* :c:func:`mol_zero_gradients`

Geometric measurements
----------------------
Various functions for measuring the geometric properties of the atom group.

* :c:func:`centroid`  
* :c:func:`center_of_extrema`  
* :c:func:`mol_atom_group_diameter`  
* :c:func:`mol_atom_group_bounding_box`  
* :c:func:`mol_atom_group_bounding_sphere`  


Basic transformations
---------------------
Some basic transformations of atomic coordinates for the atom group as a whole.

* :c:func:`mol_atom_group_translate`  
* :c:func:`mol_atom_group_rotate`  
* :c:func:`mol_atom_group_move`  
* :c:func:`mol_atom_group_move_in_copy`  

Other
-----
* :c:func:`mol_gaps <mol_gaps>`  


