# Contribution guidelines

## Version Control

After cloning this repository, please configure your name and commit template.
Your email must match your BitBucket account:

    git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com

We use [Git Flow](https://guides.github.com/introduction/flow/) to organize development.
In short, you create separate branch, do whatever you need there, and then create pull
request on BitBucket.

Recommendations about commits:

- Commits should be atomic: deal with one thing at a time, no more no less.
- Commit message must be meaningful, but brief.

Recommendations about pull requests:

- Pull request must deal with one issue; if work could be split into two PRs — consider doing it.
- Try to limit pull requests to 500 added lines.
- Check that all tests pass.
- Check that all commits have decent descriptions and correct author.
- Create good and unambiguous description of pull request.
- Don't forget to assign a reviewer.

## Coding style

Major points:

**General**

- We use C11 standard here.
- Indentation is done with tabs. Tab is assumed to be 8 spaces wide.
- Curly braces use mostly K&R style, but function definitions use Allman style (see example below).

**Naming**

- All function and variable names should be in lowercase. Use underscores. Do not use camelCase.
- All interfaced functions should start with `mol_`, e.g: `void mol_object_action()`.
- Functions should be preferentially named using the **prefix\_object\_action()** template,
  e.g., ``mol_matrix3_multiply()``.

**Qualifiers**

- In function definitions, all non-modified arguments should have a ``const`` qualifier,
  even for non-pointer types.
- Modified arguments should be defined before const arguments, e.g.
  ```void smp_my_func(float* modified, const int not_modified)```
- All functions not intended to be in the user interface should have a ``static`` qualifier, e.g.,
    ```static void _auxiliary_function(void)```

**Includes**

- All headers should have `#ifndef`-guards.
- Use ``""`` when referring to local (in-library) headers (e.g ``#include "atom_group.h"``)
- Use ``<>`` when referring to standard library headers (e.g ``#include <math.h>``)

**Documentation**

- All interfaced functions and structures should have a Doxygen-style comment.
- See code example below for preferred comment style.

**Testing**

- All interfaced functions should have unit tests.
  We use [Check](https://libcheck.github.io/check/) as a testing framework.
- Tests are located in the ``/tests`` directory.
  All testing-related data files are located in ``/tests/data``
- Please be reasonable with the amount of time your tests take to run.

**Misc**

- If a function has many arguments (typically >2), each argument should be on a new line. Indent with tabs. Ex.:

         void mol_my_func_w_many_arguments(
                                float* modified,
                                const int* not_modified_1,
                                const int not_modified_2)

- Each non-trivial `struct` should have associated `_create`, `_init`, `_destroy`, and `_free` functions.
- If you leave TODO/FIXME in code, create corresponding BitBucket issue, and write its ID in said TODO.
- Don't leave commented-out code: move it to tests, or delete it for good.


### Coding style example:

`file.h`:

    // Include guards:
    #ifndef _MOL_MYFILE_H_
    #define _MOL_MYFILE_H_

    // Standard library and project includes:
    #include <stdio.h>
    #include "atom_group.h"

    // Doxygen-styled comment documenting a struct.
    /**
     * Structure description.
     */
    struct mol_my_object {
        int element_1; /**< Element description */
        int element_2; /**< Element description */
    }

    // Doxygen-styled comment documenting a function.
    /**
     * \brief Short function description.
     *
     * Detailed function description.
     * (Here, returns the number of atoms in the atom group)
     *
     * \param ag	Atom group.
     * \return	Number of atoms, or \f$42\f$ if apocalypse is upon us.
     */
    int mol_object_action2(const struct mol_atom_group* ag);

`file.c`:

    #include "file.h"

    // Non-interface functions are defined as static.
    // Documenting such functions is not mandatory,
    // but highly advised if the function is not trivial.
    /**
     * Do almost nothing.
     */
    static void _do_nothing(void)
    {
        printf("Doing nothing here.\n");
    }

    int mol_object_action2(const struct mol_atom_group* ag)
    {
            if (1) {
                    return ag->natoms;
            } else {
                    _do_nothing();
                    return 42;
            }
    }

Please, follow the style strictly.
