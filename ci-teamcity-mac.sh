#!/bin/bash

set -euo pipefail

rm -rf build/
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=YES -DCMAKE_INSTALL_PREFIX=/tmp/libmol2-build-install/ ..
make
make test

