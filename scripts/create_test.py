#!/usr/bin/env python

TEMPLATE = """#include <stdio.h>
#include <check.h>
#include <errno.h>

// Test specific includes here
// #include ""

// Test cases
START_TEST(test_X)
{
  // test code here
}
END_TEST

Suite *SUITE_NAME_suite(void)
{
	Suite *suite = suite_create("SUITE_NAME");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_X);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = SUITE_NAME_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
"""


def create_test(suite_name):
    return TEMPLATE.replace("SUITE_NAME", suite_name)


if __name__ == "__main__":
    import sys

    if len(sys.argv) < 2:
        print("Usage: create_test.py TEST_NAME");
        print("    Run from the libmol root directory")
        sys.exit(1)

    suite_name = sys.argv[1]

    with open("tests/test_{0}.c".format(suite_name), "w") as f:
        f.write(create_test(suite_name))

    print("Test {0} created in file tests/test_{0}.c".format(suite_name))
    print("  Add the source file to the list of tests in tests/CMakeLists.txt")
    print("  then write the test.")
