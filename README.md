libmol2
-------

Will contain code described in the [library proposal](https://bitbucket.org/bu-structure/library-proposal).

## Dependencies

* CMake and C compiler
* Jansson, gperf
* [Check](http://check.sourceforge.net/) for running tests
* Lcov for generating code coverage on tests

**Ubuntu 16.04 / 18.04**

    sudo apt install build-essential git cmake libjansson4 libjansson-dev gperf check lcov pkg-config

**Mac OS**

Install [Homebrew](https://brew.sh/), then run:

    brew install cmake gperf jansson check git lcov pkg-config

## Getting started

First, install dependencies. Then download this repository and go to its root:

    git clone https://bitbucket.org/bu-structure/libmol2.git && cd libmol2

Now, create separate build directory and run CMake:

    mkdir build && cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=YES -DCMAKE_INSTALL_PREFIX=$HOME ..

If it does not complain about anything, you can finally compile the library and run tests:

    make && make test

The tests should all pass.

**Note**: just running `make test` will _not_ rebuild code; after changing anything, you should first run `make`.

If you want to install the library to your home directory (`~/lib`, `~/include`, etc), just run

    make install

### Customizing your build

If you want to customize your build, run `ccmake ..` on the build directory and
change the variables in the nice console interface. Alternatively, you can just pass
them as arguments to the `cmake` command — like we did with `CMAKE_BUILD_TYPE` and
`BUILD_TESTS` earlier.

The most common variables you may want to change are:

* `CMAKE_BUILD_TYPE` — we recommend `Release` for normal use, and `Debug` for development.
`RelWithDebInfo` is a nice middle ground, that works almost as fast as release version, but
still contains debug symbols.

* `CMAKE_INSTALL_PREFIX` — choose installation directory (for `make install`).

* `BUILD_TESTS` — set to `YES` to enable tests, and to `NO` if you don't need them.

* `USE_COVERAGE` — set up test coverage measurement. See below.

* `USE_SANITIZER` — enable AddressSanitizer. Not recommended for release builds, since it complicates linking to other projects. Strongly recommended for development, since it helps detect memory access errors.

* `BUILD_NOE` — set to `YES` to add NOE (NMR) calculation capability. This feature requires 
   GSL library. To install it run `sudo apt install libgsl-dev` on Ubuntu or `brew install gsl`
   on Mac OS.

### Measuring test coverage

Build code with `USE_COVERAGE=YES`. Then to run tests and simultaneously generate code coverage
run `make && make coverage && make test` on the build directory. The Lcov tool generates HTML
files showing coverage statistics.
Open `./code_coverage/index.html` in your browser to view the coverage report.

### Debugging

When running tests, typically, each test is run in a separate process, which allows testing exit codes and timeouts.
However, that makes using a debugger problematic.

As a solution, set the [`CK_FORK=no` environment variable](https://libcheck.github.io/check/doc/check_html/check_4.html#No-Fork-Mode).
E.g., run `CK_FORK=no gdb ./test_prms`, or set this environment variable in your IDE settings for the test of interest.
It breaks tests expecting a failure but is a recommended solution otherwise.

### Valgrind issues

Some of the tests that deal with `long double` may fail when run under Valgrind.
This is unfortunate but expected behavior and don't indicate any actual bugs in the code.

From the [Valgrind manual](http://valgrind.org/docs/manual/manual-core.html#manual-core.limits):

> Precision: There is no support for 80 bit arithmetic. 
> Internally, Valgrind represents all such "long double" numbers in 64 bits, and so there may be some differences in results. 
> Whether or not this is critical remains to be seen.


### Contributing

The description of the code style is available in `CONTRIBUTING.md`.
