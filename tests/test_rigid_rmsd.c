#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>

#include "pdb.h"
#include "rigidrmsd.h"
#include "vector.h"

#define TOLERANCE 5e-3


START_TEST(test_rmsd_pairwise)
{
	struct mol_matrix3_list *rots = mol_matrix3_list_from_file("rot.prm");
	struct mol_matrix3 eye = {1, 0, 0, 0, 1, 0, 0, 0, 1};
	struct mol_vector3 tv0 = {0.0, 0.0, 0.0};
	struct mol_vector3 tv1 = {1.0, 1.0, 1.0};
	struct mol_vector3 tv2 = {2.0, 2.0, 2.0};
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");

	struct mol_vector3 center;
	centroid(&center, ag1);
	MOL_VEC_MULT_SCALAR(center, center, -1);

	mol_atom_group_translate(ag1, &center);

	struct mol_rrmsd_atom_group *rag = mol_rrmsd_atom_group_create(ag1, NULL);

	double r;

	// Both are not rotated at all
	r = mol_rrmsd_rmsd_pairwise(rag, &eye, &tv0, &eye, &tv0);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	// Both are rotated the same
	r = mol_rrmsd_rmsd_pairwise(rag, &rots->members[0], &tv1, &rots->members[0], &tv1);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	r = mol_rrmsd_rmsd_pairwise(rag, &rots->members[0], &tv1, &rots->members[0], &tv0);
	ck_assert(fabs(r - sqrtf(3)) <= TOLERANCE);

	r = mol_rrmsd_rmsd_pairwise(rag, &rots->members[0], &tv1, &rots->members[1], &tv2);
	ck_assert(fabs(r - 30.855179599) <= TOLERANCE);

	mol_list_free(rots);
	mol_atom_group_free(ag1);
	mol_rrmsd_atom_group_free(rag);
}
END_TEST


START_TEST(test_rmsd_pairwise_q)
{
	struct mol_quaternion eye = {1, 0, 0, 0};
	struct mol_quaternion rot0 = {.W = 0.183012905584506668, .X = -0.683012647313002871, .Y = -0.183012905584506668, .Z = 0.683012647313002871};
	struct mol_quaternion rot1 = {.W = 0.683011840447561443, .X = 0.183013094636726485, .Y = -0.683013352864650855, .Z = -0.183013094636726485};
	struct mol_vector3 tv0 = {0.0, 0.0, 0.0};
	struct mol_vector3 tv1 = {1.0, 1.0, 1.0};
	struct mol_vector3 tv2 = {2.0, 2.0, 2.0};
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");

	struct mol_vector3 center;
	centroid(&center, ag1);
	MOL_VEC_MULT_SCALAR(center, center, -1);

	mol_atom_group_translate(ag1, &center);

	struct mol_rrmsd_atom_group *rag = mol_rrmsd_atom_group_create(ag1, NULL);

	double r;

	// Both are not rotated at all
	r = mol_rrmsd_rmsd_pairwise_q(rag, &eye, &tv0, &eye, &tv0);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	// Both are rotated the same
	r = mol_rrmsd_rmsd_pairwise_q(rag, &rot0, &tv1, &rot0, &tv1);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	r = mol_rrmsd_rmsd_pairwise_q(rag, &rot0, &tv1, &rot0, &tv0);
	ck_assert(fabs(r - sqrtf(3)) <= TOLERANCE);

	r = mol_rrmsd_rmsd_pairwise_q(rag, &rot0, &tv1, &rot1, &tv2);
	ck_assert(fabs(r - 30.855179599) <= TOLERANCE);

	mol_atom_group_free(ag1);
	mol_rrmsd_atom_group_free(rag);
}
END_TEST


START_TEST(test_rmsd)
{
	struct mol_matrix3_list *rots = mol_matrix3_list_from_file("rot.prm");
	struct mol_vector3 tv0 = {0.0, 0.0, 0.0};
	struct mol_vector3 tv1 = {1.0, 0.0, 0.0};
	struct mol_vector3 tvdiag = {-1.0, 1.0, 1.0};
	struct mol_matrix3 eye = {1, 0, 0, 0, 1, 0, 0, 0, 1};
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");
	struct mol_rrmsd_atom_group *rag = mol_rrmsd_atom_group_create(ag1, NULL);
	double r;

	r = mol_rrmsd_rmsd(rag, &eye, &tv0);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	r = mol_rrmsd_rmsd(rag, &eye, &tv1);
	ck_assert(fabs(r - 1) <= TOLERANCE);

	r = mol_rrmsd_rmsd(rag, &eye, &tvdiag);
	ck_assert(fabs(r - sqrtf(3)) <= TOLERANCE);

	/*
	 * VMD Script to replicate reference value.
	 * VMD uses 4*4 transformation matrix to express rotation+translation.
	 * Here we have no translation, so we set additional row and column to identity.
	 * % set M {{-0.000001033  0.000000000   -1.000001033 0.0}  {0.500001033   -0.866027033  0.000000000 0.0}   {-0.866026000  -0.500001033  -0.000001033 0.0} {0.0 0.0 0.0 1.0}}
	 * % [atomselect top all] move $M
	 * Then use Extensions>Analysis>RMSD Calculator to get RMSD.
	 */
	r = mol_rrmsd_rmsd(rag, &rots->members[0], &tv0);
	ck_assert(fabs(r - 88.58240132216405) <= TOLERANCE);

	// set M {{-0.000001033  0.0 -1.000001033 1.0}  {0.500001033 -0.866027033 0.0 0.0} {-0.866026000 -0.500001033 -0.000001033 0.0} {0.0 0.0 0.0 1.0}}
	r = mol_rrmsd_rmsd(rag, &rots->members[0], &tv1);
	ck_assert(fabs(r - 88.49562199340687) <= TOLERANCE);


	// set M {{-0.000001033  0.0 -1.000001033 -1.0}  {0.500001033 -0.866027033 0.0 1.0} {-0.866026000 -0.500001033 -0.000001033 1.0} {0.0 0.0 0.0 1.0}}
	r = mol_rrmsd_rmsd(rag, &rots->members[0], &tvdiag);
	ck_assert(fabs(r - 87.46092156019047) <= TOLERANCE);

	mol_list_free(rots);
	mol_atom_group_free(ag1);
	mol_rrmsd_atom_group_free(rag);
}
END_TEST


START_TEST(test_rmsd_q)
{
	// We use the same translations/rotations as in test_rrmsd_one_transform, so we have the same reference values
	struct mol_vector3 tv0 = {0.0, 0.0, 0.0};
	struct mol_vector3 tv1 = {1.0, 0.0, 0.0};
	struct mol_vector3 tvdiag = {-1.0, 1.0, 1.0};
	struct mol_quaternion eye = {1, 0, 0, 0};
	struct mol_quaternion rot = {.W = 0.183012905584506668, .X = -0.683012647313002871, .Y = -0.183012905584506668, .Z = 0.683012647313002871};
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");
	struct mol_rrmsd_atom_group *rag = mol_rrmsd_atom_group_create(ag1, NULL);
	double r;

	r = mol_rrmsd_rmsd_q(rag, &eye, &tv0);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &eye, &tv1);
	ck_assert(fabs(r - 1) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &eye, &tvdiag);
	ck_assert(fabs(r - sqrtf(3)) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &rot, &tv0);
	ck_assert(fabs(r - 88.58240132216405) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &rot, &tv1);
	ck_assert(fabs(r - 88.49562199340687) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &rot, &tvdiag);
	ck_assert(fabs(r - 87.46092156019047) <= TOLERANCE);

	mol_atom_group_free(ag1);
	mol_rrmsd_atom_group_free(rag);
}
END_TEST


START_TEST(test_rmsd_with_interface)
{
	struct mol_matrix3_list *rots = mol_matrix3_list_from_file("rot.prm");
	struct mol_vector3 tv0 = {0.0, 0.0, 0.0};
	struct mol_vector3 tv1 = {1.0, 0.0, 0.0};
	struct mol_vector3 tvdiag = {-1.0, 1.0, 1.0};
	struct mol_matrix3 eye = {1, 0, 0, 0, 1, 0, 0, 0, 1};
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");
	double r;

	// Some randomly chosen interface
	// Appropriate VMD selection for RMSD calculator: chain A and resid 17 to 21 and name CA
	struct mol_index_list interface;
	interface.size = 5;
	interface.members = calloc(5, sizeof(size_t));
	interface.members[0] = 111; // chain A, residue ASP17, name CA
	interface.members[1] = 119; // chain A, residue ARG18, name CA
	interface.members[2] = 130; // chain A, residue VAL19, name CA
	interface.members[3] = 137; // chain A, residue THR20, name CA
	interface.members[4] = 144; // chain A, residue ILE21, name CA

	struct mol_rrmsd_atom_group *rag = mol_rrmsd_atom_group_create(ag1, &interface);

	r = mol_rrmsd_rmsd(rag, &eye, &tv0);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	r = mol_rrmsd_rmsd(rag, &eye, &tv1);
	ck_assert(fabs(r - 1) <= TOLERANCE);

	r = mol_rrmsd_rmsd(rag, &eye, &tvdiag);
	ck_assert(fabs(r - sqrtf(3)) <= TOLERANCE);

	// See test_rrmsd_one_transform for details of how reference values were obtained
	r = mol_rrmsd_rmsd(rag, &rots->members[0], &tv0);
	ck_assert(fabs(r - 84.59973260024795) <= TOLERANCE);

	r = mol_rrmsd_rmsd(rag, &rots->members[0], &tv1);
	ck_assert(fabs(r - 84.28772560335484) <= TOLERANCE);

	r = mol_rrmsd_rmsd(rag, &rots->members[0], &tvdiag);
	ck_assert(fabs(r - 83.62118450305834) <= TOLERANCE);

	mol_list_destroy(&interface);
	mol_list_free(rots);
	mol_atom_group_free(ag1);
	mol_rrmsd_atom_group_free(rag);
}
END_TEST


START_TEST(test_rmsd_q_with_interface)
{
	struct mol_vector3 tv0 = {0.0, 0.0, 0.0};
	struct mol_vector3 tv1 = {1.0, 0.0, 0.0};
	struct mol_vector3 tvdiag = {-1.0, 1.0, 1.0};
	struct mol_quaternion eye = {1, 0, 0, 0};
	struct mol_quaternion rot = {.W = 0.183012905584506668, .X = -0.683012647313002871, .Y = -0.183012905584506668, .Z = 0.683012647313002871};
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");
	double r;

	// Some randomly chosen interface
	// Appropriate VMD selection for RMSD calculator: chain A and resid 17 to 21 and name CA
	struct mol_index_list interface;
	interface.size = 5;
	interface.members = calloc(5, sizeof(size_t));
	interface.members[0] = 111; // chain A, residue ASP17, name CA
	interface.members[1] = 119; // chain A, residue ARG18, name CA
	interface.members[2] = 130; // chain A, residue VAL19, name CA
	interface.members[3] = 137; // chain A, residue THR20, name CA
	interface.members[4] = 144; // chain A, residue ILE21, name CA

	struct mol_rrmsd_atom_group *rag = mol_rrmsd_atom_group_create(ag1, &interface);

	r = mol_rrmsd_rmsd_q(rag, &eye, &tv0);
	ck_assert(fabs(r - 0) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &eye, &tv1);
	ck_assert(fabs(r - 1) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &eye, &tvdiag);
	ck_assert(fabs(r - sqrtf(3)) <= TOLERANCE);

	// See test_rrmsd_one_transform for details of how reference values were obtained
	r = mol_rrmsd_rmsd_q(rag, &rot, &tv0);
	ck_assert(fabs(r - 84.59973260024795) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &rot, &tv1);
	ck_assert(fabs(r - 84.28772560335484) <= TOLERANCE);

	r = mol_rrmsd_rmsd_q(rag, &rot, &tvdiag);
	ck_assert(fabs(r - 83.62118450305834) <= TOLERANCE);

	mol_list_destroy(&interface);
	mol_atom_group_free(ag1);
	mol_rrmsd_atom_group_free(rag);
}
END_TEST


START_TEST(test_atom_group_update)
{
	struct mol_atom_group *ag1 = mol_read_pdb("1rei.pdb");

	// Some randomly chosen interface
	// Appropriate VMD selection for RMSD calculator: chain A and resid 17 to 21 and name CA
	struct mol_index_list interface;
	interface.size = 5;
	interface.members = calloc(5, sizeof(size_t));
	interface.members[0] = 111; // chain A, residue ASP17, name CA
	interface.members[1] = 119; // chain A, residue ARG18, name CA
	interface.members[2] = 130; // chain A, residue VAL19, name CA
	interface.members[3] = 137; // chain A, residue THR20, name CA
	interface.members[4] = 144; // chain A, residue ILE21, name CA

	struct mol_rrmsd_atom_group *rag = mol_rrmsd_atom_group_create(ag1, NULL);
	// CoM of the whole molecule
	ck_assert(fabs(rag->centroid.X + 8.81273755) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Y - 39.99422027) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Z - 16.99608436) <= TOLERANCE);

	mol_rrmsd_atom_group_update(rag, ag1, NULL); // Nothing is updated
	ck_assert(fabs(rag->centroid.X + 8.81273755) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Y - 39.99422027) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Z - 16.99608436) <= TOLERANCE);

	mol_rrmsd_atom_group_update(rag, ag1, &interface);
	// CoM of the interface
	ck_assert(fabs(rag->centroid.X - 12.7748) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Y - 38.8436) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Z - 14.0722) <= TOLERANCE);

	// Move atomgroup so it's centroid is at origin
	struct mol_vector3 tv;
	centroid(&tv, ag1);
	MOL_VEC_MULT_SCALAR(tv, tv, -1);
	mol_atom_group_translate(ag1, &tv);

	mol_rrmsd_atom_group_update(rag, ag1, NULL);
	ck_assert(fabs(rag->centroid.X) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Y) <= TOLERANCE);
	ck_assert(fabs(rag->centroid.Z) <= TOLERANCE);

	mol_list_destroy(&interface);
	mol_atom_group_free(ag1);
	mol_rrmsd_atom_group_free(rag);
}
END_TEST



Suite *rmsd_suite(void)
{
	Suite *suite = suite_create("rigid_rmsd");

	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_rmsd);
	tcase_add_test(tcase, test_rmsd_q);
	tcase_add_test(tcase, test_rmsd_pairwise);
	tcase_add_test(tcase, test_rmsd_pairwise_q);
	tcase_add_test(tcase, test_rmsd_with_interface);
	tcase_add_test(tcase, test_rmsd_q_with_interface);
	tcase_add_test(tcase, test_atom_group_update);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = rmsd_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
