#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "nbenergy.h"
#include "pdb.h"
#include "json.h"
#include "icharmm.h"

struct mol_atom_group *test_ag;
struct agsetup test_ags;

static const double delta = 1e-6;
static const double tolerance = 1e-5;

#define _ALMOST_ZERO(x) (fabs((x)) < 1e-6)

double _vdwengs03_wrapper(void)
{
	double energy = 0.;
	vdwengs03(0.5, test_ags.nblst->nbcof, test_ag, &energy, test_ags.nf03, test_ags.listf03);
	return energy;
}

double _vdweng_wrapper(void)
{
	double energy = 0.;
	vdweng(test_ag, &energy, test_ags.nblst);
	return energy;
}
double _elengs03_wrapper(void)
{
	double energy = 0.;
	elengs03(0.5, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
	return energy;
}

double _eleng_wrapper(void)
{
	double energy = 0.;
	eleng(test_ag, 1.0, &energy, test_ags.nblst);
	return energy;
}


void test_grads(const double d, double (*energy_func)(void))
{
	double en, en1, t;
	struct mol_vector3 *fs = malloc(test_ag->natoms * sizeof(struct mol_vector3));

	en = energy_func();

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		t = test_ag->coords[i].X;
		test_ag->coords[i].X = d + t;
		en1 = energy_func();
		test_ag->coords[i].X = t;
		fs[i].X = (en - en1) / d;

		t = test_ag->coords[i].Y;
		test_ag->coords[i].Y = d + t;
		en1 = energy_func();
		test_ag->coords[i].Y = t;
		fs[i].Y = (en - en1) / d;

		t = test_ag->coords[i].Z;
		test_ag->coords[i].Z = d + t;
		en1 = energy_func();
		test_ag->coords[i].Z = t;
		fs[i].Z = (en - en1) / d;
	}
	mol_zero_gradients(test_ag);
	energy_func();
	char msg[256];

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		sprintf(msg,
			"\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
			i,
			test_ag->gradients[i].X, test_ag->gradients[i].Y, test_ag->gradients[i].Z,
			fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(fabs(test_ag->gradients[i].X - fs[i].X) < tolerance, "%s", msg);
		ck_assert_msg(fabs(test_ag->gradients[i].Y - fs[i].Y) < tolerance, "%s", msg);
		ck_assert_msg(fabs(test_ag->gradients[i].Z - fs[i].Z) < tolerance, "%s", msg);

	}
	free(fs);
}

void setup_phenol(void)
{
	test_ag = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(test_ag, "phenol.psf", "small.prm", "small.rtf");

	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_generic(void)
{
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
}

void setup_three(void)
{
	test_ag = mol_read_json("propylene_oxide.json"); // Allocates gradients

	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void setup_tip3p(void)
{
	test_ag = mol_read_pdb("water.pdb");
	mol_atom_group_read_geometry(test_ag, "water.psf", "small.prm", "small.rtf");

	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void setup_oxetane(void)
{
	test_ag = mol_read_json("oxetane.json");

	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void setup_pair(void)
{
	test_ag = mol_read_pdb("two_atoms.pdb");
	mol_atom_group_read_geometry(test_ag, "two_atoms.psf", "small.prm", "small.rtf");

	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}


START_TEST(test_pair_init_nblst)
{
	ck_assert_int_eq(test_ags.n02, 0);
	ck_assert_int_eq(test_ags.n03, 0);
	ck_assert_int_eq(test_ags.nf03, 0);
	// We only have one pair, between atoms #0 and #1. The pairs are ordered, so the "first" atom is #0.
	ck_assert_int_eq(test_ags.nblst->nfat, 1);
	ck_assert_int_eq(test_ags.nblst->ifat[0], 0); // First atom is #0
	ck_assert_int_eq(test_ags.nblst->nsat[0], 1); // It has one neighbor
	ck_assert_int_eq(test_ags.nblst->isat[0][0], 1); // This neighbor is #1
}
END_TEST

START_TEST(test_pair_eleng)
{
	// Shifted electrostatics.
	// E = C_elec * q1 * q2 * (1 - r/rc)^2 / r
	// G = - C_elec * q1 * q2 * (1/r^2 - 1/rc^2) * (r / |r|)
	// Move second atom along OX axis and check energies and gradients against the manually computed values
	const double distances[] = {2, 0.001, 12, 200};
	const double energy_ref[] = {115.3026388888889, 332016.25703938614, 0, 0};
	const double force_ref[] = {80.71184722222222, 332071597.6939472, 0, 0};
	for (int i = 0; i < 4; i++) {
		test_ag->coords[1].X = distances[i];
		double energy = 0.0;
		mol_zero_gradients(test_ag);
		eleng(test_ag, 1.0, &energy, test_ags.nblst);
		ck_assert(_ALMOST_ZERO(energy - energy_ref[i]));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].X - (-force_ref[i])));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Y - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Z - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].X - (force_ref[i])));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Y - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Z - 0.0));
	}
}
END_TEST

START_TEST(test_pair_eleng_eps)
{
	// Use permittivity of 80.1
	const double distance = 2;
	const double energy_ref = 1.4394836315716466;
	const double force_ref = 1.0076385421001526;
	test_ag->coords[1].X = distance;
	double energy = 0.0;
	mol_zero_gradients(test_ag);
	eleng(test_ag, 80.1, &energy, test_ags.nblst);
	ck_assert(_ALMOST_ZERO(energy - energy_ref));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].X - (-force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Z - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].X - (force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Z - 0.0));
}
END_TEST

START_TEST(test_pair_eleng_not_in_list)
{
	// Remove the atoms from non-bonded list
	test_ags.nblst->nfat = 0;
	double energy = 0.0;
	mol_zero_gradients(test_ag);
	eleng(test_ag, 1.0, &energy, test_ags.nblst);
	ck_assert(_ALMOST_ZERO(energy));
	for (int i = 0; i < 2; i++) {
		ck_assert(_ALMOST_ZERO(test_ag->gradients[i].X));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[i].Y));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[i].Z));
	}
	test_ags.nblst->nfat = 1; // Revert it back so the list can be properly free'd
}
END_TEST

START_TEST(test_pair_elengs03)
{
	// Add manually to 03 list
	test_ags.nf03 = 1;
	test_ags.listf03 = calloc(2, sizeof(int));
	test_ags.listf03[0] = 0;
	test_ags.listf03[1] = 1;

	// Shifted electrostatics.
	// E = C_elec * q1 * q2 * (1 - r/rc)^2 / r
	// G = - C_elec * q1 * q2 * (1/r^2 - 1/rc^2) * (r / |r|)
	const double distances[] = {2, 0.001, 12, 200};
	const double energy_ref[] = {115.3026388888889, 332016.25703938614, 0, 0};
	const double force_ref[] = {80.71184722222222, 332071597.6939472, 0, 0};
	for (int i = 0; i < 4; i++) {
		test_ag->coords[1].X = distances[i];
		double energy = 0.0;
		mol_zero_gradients(test_ag);
		elengs03(1.0, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
		ck_assert(_ALMOST_ZERO(energy - energy_ref[i]));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].X - (-force_ref[i])));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Y - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Z - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].X - (force_ref[i])));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Y - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Z - 0.0));
	}
}
END_TEST

START_TEST(test_pair_elengs03_eps)
{
	// Add manually to 03 list
	test_ags.nf03 = 1;
	test_ags.listf03 = calloc(2, sizeof(int));
	test_ags.listf03[0] = 0;
	test_ags.listf03[1] = 1;

	// Use permittivity of 80.1
	const double distance = 2;
	const double energy_ref = 1.4394836315716466;
	const double force_ref = 1.0076385421001526;
	test_ag->coords[1].X = distance;
	double energy = 0.0;
	mol_zero_gradients(test_ag);
	elengs03(1.0, test_ags.nblst->nbcof, test_ag, 80.1, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(_ALMOST_ZERO(energy - energy_ref));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].X - (-force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Z - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].X - (force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Z - 0.0));
}
END_TEST

START_TEST(test_pair_elengs03_reverse_order)
{
	// Add manually to 03 list, but use reverse order. It should not matter
	test_ags.nf03 = 1;
	test_ags.listf03 = calloc(2, sizeof(int));
	test_ags.listf03[0] = 1;
	test_ags.listf03[1] = 0;

	const double distance = 2;
	const double energy_ref = 115.3026388888889;
	const double force_ref = 80.71184722222222;
	test_ag->coords[1].X = distance;
	double energy = 0.0;
	mol_zero_gradients(test_ag);
	elengs03(1.0, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(_ALMOST_ZERO(energy - energy_ref));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].X - (-force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Z - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].X - (force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Z - 0.0));
}
END_TEST

START_TEST(test_pair_eleng03)
{
	// Add manually to 03 list
	test_ags.nf03 = 1;
	test_ags.listf03 = calloc(2, sizeof(int));
	test_ags.listf03[0] = 0;
	test_ags.listf03[1] = 1;

	// Plain, non-shifted electrostatics.
	// E = C_elec * q1 * q2 / r
	// G = - C_elec * q1 * q2 / r^2 * (r / |r|)
	const double distances[] = {2, 0.001, 12, 200};
	const double energy_ref[] = {166.0358, 332071.6, 27.672633333333334, 1.660358};
	const double force_ref[] = {83.0179, 332071600.0, 2.3060527777777775, 0.00830179};
	for (int i = 0; i < 4; i++) {
		test_ag->coords[1].X = distances[i];
		double energy = 0.0;
		mol_zero_gradients(test_ag);
		eleng03(1.0, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
		ck_assert(_ALMOST_ZERO(energy - energy_ref[i]));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].X - (-force_ref[i])));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Y - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Z - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].X - (force_ref[i])));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Y - 0.0));
		ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Z - 0.0));
	}
}
END_TEST

START_TEST(test_pair_eleng03_eps)
{
	// Add manually to 03 list
	test_ags.nf03 = 1;
	test_ags.listf03 = calloc(2, sizeof(int));
	test_ags.listf03[0] = 0;
	test_ags.listf03[1] = 1;

	// Use permittivity of 80.1
	const double distance = 2;
	const double energy_ref = 2.072856429463171;
	const double force_ref = 1.0364282147315855;
	test_ag->coords[1].X = distance;
	double energy = 0.0;
	mol_zero_gradients(test_ag);
	eleng03(1.0, test_ag, 80.1, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(_ALMOST_ZERO(energy - energy_ref));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].X - (-force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[0].Z - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].X - (force_ref)));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Y - 0.0));
	ck_assert(_ALMOST_ZERO(test_ag->gradients[1].Z - 0.0));
}
END_TEST


START_TEST(test_phenol_vdwengs03)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _vdwengs03_wrapper);
	double energy = 0.0;
	vdwengs03(1.0, test_ags.nblst->nbcof, test_ag, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_phenol_vdweng)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _vdweng_wrapper);
	double energy = 0.0;
	vdweng(test_ag, &energy, test_ags.nblst);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_phenol_elengs03)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _elengs03_wrapper);
	double energy = 0.0;
	elengs03(1.0, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_phenol_eleng)
{
	mol_zero_gradients(test_ag);
	test_grads(delta, _eleng_wrapper);
	double energy = 0.0;
	eleng(test_ag, 1.0, &energy, test_ags.nblst);
	ck_assert(isfinite(energy));
	ck_assert(!_ALMOST_ZERO(energy));
}
END_TEST


START_TEST(test_phenol_init_nblist)
{
	static const int list02_ref[] = {0,2, 0,4, 1,3, 1,5, 2,4, 2,6, 3,5, 3,7, 4,6};
	static const int list03_ref[] = {0,3, 1,4, 1,6, 2,5, 2,7, 4,7, 5,6};
	ck_assert_int_eq(test_ags.n02, 9);
	for (int i = 0; i < test_ags.n02*2; i++)
		ck_assert_int_eq(test_ags.list02[i], list02_ref[i]);
	ck_assert_int_eq(test_ags.n03, 7);
	ck_assert_int_eq(test_ags.nf03, 7);
	for (int i = 0; i < test_ags.n03*2; i++) {
		ck_assert_int_eq(test_ags.list03[i], list03_ref[i]);
		ck_assert_int_eq(test_ags.listf03[i], list03_ref[i]);
	}
	ck_assert_int_eq(test_ags.nblst->nfat, 3);
	ck_assert_int_eq(test_ags.nblst->ifat[0], 0); // C1 [0], with O1 [6] and H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[0], 2);
	ck_assert_int_eq(test_ags.nblst->isat[0][0], 6);
	ck_assert_int_eq(test_ags.nblst->isat[0][1], 7);
	ck_assert_int_eq(test_ags.nblst->ifat[1], 1); // C2 [1], with H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[1], 1);
	ck_assert_int_eq(test_ags.nblst->isat[1][0], 7);
	ck_assert_int_eq(test_ags.nblst->ifat[2], 5); // C6 [5], with H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[2], 1);
	ck_assert_int_eq(test_ags.nblst->isat[2][0], 7);
}
END_TEST


START_TEST(test_phenol_update_nblist_fixed)
{
	test_ag->fixed[1] = true;
	test_ag->fixed[4] = true;
	test_ag->fixed[7] = true;
	mol_fixed_update_active_lists(test_ag);
	mol_nblst_update_fixed(test_ag, &test_ags); // calling update_nblst is not enough, because it does not update listf03
	static const int list02_ref[] = {0,2, 0,4, 1,3, 1,5, 2,4, 2,6, 3,5, 3,7, 4,6};
	static const int list03_ref[] = {0,3, 1,4, 1,6, 2,5, 2,7, 4,7, 5,6};
	static const int list03f_ref[] = {0,3, 1,6, 2,5, 2,7, 5,6}; // We leave out pairs 1-4 and 4-7, because both are fixed
	ck_assert_int_eq(test_ags.n02, 9);
	for (int i = 0; i < test_ags.n02*2; i++)
		ck_assert_int_eq(test_ags.list02[i], list02_ref[i]);
	ck_assert_int_eq(test_ags.n03, 7);
	for (int i = 0; i < test_ags.n03*2; i++)
		ck_assert_int_eq(test_ags.list03[i], list03_ref[i]);
	ck_assert_int_eq(test_ags.nf03, 5);
	for (int i = 0; i < test_ags.nf03*2; i++)
		ck_assert_int_eq(test_ags.listf03[i], list03f_ref[i]);

	// We also don't have pair 1-7 in the nblist anymore, because both are fixed. Pair 5-7 is left intact
	ck_assert_int_eq(test_ags.nblst->nfat, 2);
	ck_assert_int_eq(test_ags.nblst->ifat[0], 0); // C1 [0], with O1 [6] and H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[0], 2);
	ck_assert_int_eq(test_ags.nblst->isat[0][0], 6);
	ck_assert_int_eq(test_ags.nblst->isat[0][1], 7);
	ck_assert_int_eq(test_ags.nblst->ifat[1], 5); // C6 [5], with H6 [7]
	ck_assert_int_eq(test_ags.nblst->nsat[1], 1);
	ck_assert_int_eq(test_ags.nblst->isat[1][0], 7);
}
END_TEST

START_TEST(test_three_init_nblst)
{
	// 02 list is [(0,2), (0,3)]
	ck_assert_int_eq(test_ags.n02, 2);
	ck_assert_int_eq(test_ags.list02[0], 0);
	ck_assert_int_eq(test_ags.list02[1], 2);
	ck_assert_int_eq(test_ags.list02[2], 0);
	ck_assert_int_eq(test_ags.list02[3], 3);
	// 03 list is empty
	ck_assert_int_eq(test_ags.n03, 0);
	ck_assert_int_eq(test_ags.nf03, 0);
	// nblist is empty
	ck_assert_int_eq(test_ags.nblst->nfat, 0);
}
END_TEST

START_TEST(test_tip3p_init_nblst)
{
	// TIP3P water has bonds between all atoms (yes, even H-H, although it's somewhat virtual).
	// Therefore, it should have no 02 and 03 lists, as all atoms are 01
	// Coincidentally, we test how well the code works in edge-case of empty lists
	ck_assert_int_eq(test_ags.n02, 0);
	ck_assert_int_eq(test_ags.n03, 0);
	ck_assert_int_eq(test_ags.nf03, 0);
	ck_assert_int_eq(test_ags.nblst->nfat, 0);
}
END_TEST

START_TEST(test_oxetane_init_nblst)
{
	// Oxetane has (well, IS) a 4-atom ring, so it's possible to introduce redundant 0-2 bonds.
	// 02 list is [(0,2), (1,3)]
	ck_assert_int_eq(test_ags.n02, 2);
	ck_assert_int_eq(test_ags.list02[0], 0);
	ck_assert_int_eq(test_ags.list02[1], 2);
	ck_assert_int_eq(test_ags.list02[2], 1);
	ck_assert_int_eq(test_ags.list02[3], 3);
	ck_assert_int_eq(test_ags.n03, 0);
	ck_assert_int_eq(test_ags.nf03, 0);
	ck_assert_int_eq(test_ags.nblst->nfat, 0);
}
END_TEST

// These four test functions are used for both three_membered_ring and tip3p test cases.
START_TEST(test_vdwengs03_zero)
{
	double energy = 0.0;
	vdwengs03(1.0, test_ags.nblst->nbcof, test_ag, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_vdweng_zero)
{
	double energy = 0.0;
	vdweng(test_ag, &energy, test_ags.nblst);
	ck_assert(_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_elengs03_zero)
{
	double energy = 0.0;
	elengs03(0.5, test_ags.nblst->nbcof, test_ag, 1.0, &energy, test_ags.nf03, test_ags.listf03);
	ck_assert(_ALMOST_ZERO(energy));
}
END_TEST

START_TEST(test_eleng_zero)
{
	double energy = 0.0;
	eleng(test_ag, 1.0, &energy, test_ags.nblst);
	ck_assert(_ALMOST_ZERO(energy));
}
END_TEST

Suite *nbenergy_suite(void)
{
	Suite *suite = suite_create("nbenergy");

	TCase *tcase_pair = tcase_create("pair");
	tcase_add_checked_fixture(tcase_pair, setup_pair, teardown_generic);
	tcase_add_test(tcase_pair, test_pair_init_nblst);
	tcase_add_test(tcase_pair, test_pair_eleng);
	tcase_add_test(tcase_pair, test_pair_eleng_eps);
	tcase_add_test(tcase_pair, test_pair_eleng_not_in_list);
	tcase_add_test(tcase_pair, test_pair_elengs03);
	tcase_add_test(tcase_pair, test_pair_elengs03_eps);
	tcase_add_test(tcase_pair, test_pair_elengs03_reverse_order);
	tcase_add_test(tcase_pair, test_pair_eleng03);
	tcase_add_test(tcase_pair, test_pair_eleng03_eps);

	suite_add_tcase(suite, tcase_pair);

	TCase *tcase = tcase_create("test");
	tcase_set_timeout(tcase, 20);
	tcase_add_checked_fixture(tcase, setup_phenol, teardown_generic);
	tcase_add_test(tcase, test_phenol_init_nblist);
	tcase_add_test(tcase, test_phenol_update_nblist_fixed);
	tcase_add_test(tcase, test_phenol_vdwengs03);
	tcase_add_test(tcase, test_phenol_vdweng);
	tcase_add_test(tcase, test_phenol_elengs03);
	tcase_add_test(tcase, test_phenol_eleng);

	suite_add_tcase(suite, tcase);

	TCase *tcase_three = tcase_create("three_membered_ring");
	tcase_add_checked_fixture(tcase_three, setup_three, teardown_generic);
	tcase_add_test(tcase_three, test_three_init_nblst);
	tcase_add_test(tcase_three, test_vdwengs03_zero);
	tcase_add_test(tcase_three, test_vdweng_zero);
	tcase_add_test(tcase_three, test_elengs03_zero);
	tcase_add_test(tcase_three, test_eleng_zero);

	suite_add_tcase(suite, tcase_three);

	TCase *tcase_tip3p = tcase_create("tip3p");
	tcase_add_checked_fixture(tcase_tip3p, setup_tip3p, teardown_generic);
	tcase_add_test(tcase_tip3p, test_tip3p_init_nblst);
	tcase_add_test(tcase_tip3p, test_vdwengs03_zero);
	tcase_add_test(tcase_tip3p, test_vdweng_zero);
	tcase_add_test(tcase_tip3p, test_elengs03_zero);
	tcase_add_test(tcase_tip3p, test_eleng_zero);

	suite_add_tcase(suite, tcase_tip3p);

	TCase *tcase_oxetane = tcase_create("oxetane");
	tcase_add_checked_fixture(tcase_oxetane, setup_oxetane, teardown_generic);
	tcase_add_test(tcase_oxetane, test_oxetane_init_nblst);

	suite_add_tcase(suite, tcase_oxetane);

	return suite;
}

int main(void)
{
	Suite *suite = nbenergy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
