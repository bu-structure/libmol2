#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "benergy.h"
#include "pdb.h"
#include "icharmm.h"

struct mol_atom_group *test_ag;
const double delta = 0.000001;
const double tolerance = 1e-3;
const double tolerance_strict = 1e-9;

void check_b_grads(struct mol_atom_group *ag, double d,
		   void (*efun) (struct mol_atom_group *, double *))
{
	double en, en1, t;
	struct mol_vector3 *fs = calloc(ag->natoms, sizeof(struct mol_vector3));
//en0
	en = 0;
	(*efun) (ag, &en);

	for (size_t i = 0; i < ag->natoms; i++) {
//x
		en1 = 0;
		t = ag->coords[i].X;
		ag->coords[i].X = d + t;
		(*efun) (ag, &en1);
		ag->coords[i].X = t;
		fs[i].X = (en - en1) / d;
//y
		en1 = 0;
		t = ag->coords[i].Y;
		ag->coords[i].Y = d + t;
		(*efun) (ag, &en1);
		ag->coords[i].Y = t;
		fs[i].Y = (en - en1) / d;
//z
		en1 = 0;
		t = ag->coords[i].Z;
		ag->coords[i].Z = d + t;
		(*efun) (ag, &en1);
		ag->coords[i].Z = t;
		fs[i].Z = (en - en1) / d;
	}
	en = 0;
	mol_zero_gradients(ag);
	(*efun) (ag, &en);
	char msg[256];

	for (size_t i = 0; i < ag->natoms; i++) {
		sprintf(msg,
			"\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
			i,
			ag->gradients[i].X, ag->gradients[i].Y, ag->gradients[i].Z,
			fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(fabs(ag->gradients[i].X - fs[i].X) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Y - fs[i].Y) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Z - fs[i].Z) < tolerance, "%s", msg);
	}
	free(fs);
}

void setup(void)
{
	test_ag = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(test_ag, "phenol.psf", "small.prm", "small.rtf");

	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
}

void setup_gaff(void)
{
	test_ag = mol_read_pdb("phenol_gaff.pdb");
	mol_atom_group_read_geometry(test_ag, "phenol_gaff.psf", "small_gaff.prm", "small_gaff.rtf");

	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
}

void teardown(void)
{
	mol_atom_group_free(test_ag);
}

// Test cases
START_TEST(test_beng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, beng);
}
END_TEST

START_TEST(test_aeng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, aeng);
}
END_TEST

START_TEST(test_ieng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, ieng);
}
END_TEST

START_TEST(test_teng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, teng);
}
END_TEST

//c-c extended by 0.1A along x-axis
//c-o extended by 0.1A along y-axis (in negative direction)
//other c-o extended by 0.1A along z-axis
START_TEST(acetate_sprung)
{
	test_ag = mol_read_pdb("acetate_sprung.pdb");
	mol_atom_group_read_geometry(test_ag, "acetate.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed

	double energy = 0;
	mol_zero_gradients(test_ag);
	beng(test_ag, &energy);
	double predicted_energy = (328.30+648.00+648.00)*(0.1*0.1); //based on small.prm
	char msg[256];
	sprintf(msg, "acetate energy: calculated: %lf predicted: %lf\n", energy,
			predicted_energy);
	ck_assert_msg(fabs(predicted_energy - energy) < tolerance_strict, "%s", msg);
	double predicted_0_gX = -2.0*328.30*0.1; //based on small.prm
	double predicted_1_gX = 2.0*328.30*0.1; //based on small.prm
	double predicted_1_gY = -2.0*648.00*0.1; //based on small.prm
	double predicted_1_gZ = 2.0*648.00*0.1; //based on small.prm
	double predicted_2_gY = 2.0*648.00*0.1; //based on small.prm
	double predicted_3_gZ = -2.0*648.00*0.1; //based on small.prm

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			0, 'X', test_ag->gradients[0].X, predicted_0_gX);
	ck_assert_msg(fabs(predicted_0_gX - test_ag->gradients[0].X) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'X', test_ag->gradients[1].X, predicted_1_gX);
	ck_assert_msg(fabs(predicted_1_gX - test_ag->gradients[1].X) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Y', test_ag->gradients[1].Y, predicted_1_gY);
	ck_assert_msg(fabs(predicted_1_gY - test_ag->gradients[1].Y) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Z', test_ag->gradients[1].Z, predicted_1_gZ);
	ck_assert_msg(fabs(predicted_1_gZ - test_ag->gradients[1].Z) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			2, 'Y', test_ag->gradients[2].Y, predicted_2_gY);
	ck_assert_msg(fabs(predicted_2_gY - test_ag->gradients[2].Y) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			3, 'Z', test_ag->gradients[3].Z, predicted_3_gZ);
	ck_assert_msg(fabs(predicted_3_gZ - test_ag->gradients[3].Z) < tolerance_strict, "%s", msg);

	mol_atom_group_free(test_ag);
}
END_TEST

//c-c shortened by 0.1A along x-axis
//c-o shortened by 0.1A along y-axis (in negative direction)
//other c-o shortened by 0.1A along z-axis
START_TEST(acetate_sprang)
{
	test_ag = mol_read_pdb("acetate_sprang.pdb");
	mol_atom_group_read_geometry(test_ag, "acetate.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed

	double energy = 0;
	mol_zero_gradients(test_ag);
	beng(test_ag, &energy);
	double predicted_energy = (328.30+648.00+648.00)*(0.1*0.1); //based on small.prm
	char msg[256];
	sprintf(msg, "acetate energy: calculated: %lf predicted: %lf\n", energy,
			predicted_energy);
	ck_assert_msg(fabs(predicted_energy - energy) < tolerance_strict, "%s", msg);
	double predicted_0_gX = 2.0*328.30*0.1; //based on small.prm
	double predicted_1_gX = -2.0*328.30*0.1; //based on small.prm
	double predicted_1_gY = 2.0*648.00*0.1; //based on small.prm
	double predicted_1_gZ = -2.0*648.00*0.1; //based on small.prm
	double predicted_2_gY = -2.0*648.00*0.1; //based on small.prm
	double predicted_3_gZ = 2.0*648.00*0.1; //based on small.prm

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			0, 'X', test_ag->gradients[0].X, predicted_0_gX);
	ck_assert_msg(fabs(predicted_0_gX - test_ag->gradients[0].X) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'X', test_ag->gradients[1].X, predicted_1_gX);
	ck_assert_msg(fabs(predicted_1_gX - test_ag->gradients[1].X) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Y', test_ag->gradients[1].Y, predicted_1_gY);
	ck_assert_msg(fabs(predicted_1_gY - test_ag->gradients[1].Y) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Z', test_ag->gradients[1].Z, predicted_1_gZ);
	ck_assert_msg(fabs(predicted_1_gZ - test_ag->gradients[1].Z) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			2, 'Y', test_ag->gradients[2].Y, predicted_2_gY);
	ck_assert_msg(fabs(predicted_2_gY - test_ag->gradients[2].Y) < tolerance_strict, "%s", msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			3, 'Z', test_ag->gradients[3].Z, predicted_3_gZ);
	ck_assert_msg(fabs(predicted_3_gZ - test_ag->gradients[3].Z) < tolerance_strict, "%s", msg);

	mol_atom_group_free(test_ag);
}
END_TEST


START_TEST(test_phenol_gaff_sander)
{
	/* Sander 17.0 output
	 * NSTEP =        0   TIME(PS) =       0.000  TEMP(K) =     0.00  PRESS =     0.0
	 * Etot   =         8.7436  EKtot   =         0.0000  EPtot      =         8.7436
	 * BOND   =         5.1107  ANGLE   =         0.1766  DIHED      =         0.0001
	 * 1-4 NB =         3.9229  1-4 EEL =        -0.3175  VDWAALS    =        -0.2709
	 * EELEC  =         0.1215  EHBOND  =         0.0000  RESTRAINT  =         0.0000
	 * EKCMT  =         0.0000  VIRIAL  =         0.0000  VOLUME     =    252957.4112
	 */
	static const double tolerance_amber = 1e-4;
	double energy;
	mol_zero_gradients(test_ag);

	energy = 0;
	beng(test_ag, &energy);
	ck_assert(fabs(5.1107 - energy) < tolerance_amber);

	energy = 0;
	aeng(test_ag, &energy);
	ck_assert(fabs(0.1766 - energy) < tolerance_amber);

	energy = 0;
	teng(test_ag, &energy);
	ieng(test_ag, &energy);
	ck_assert(fabs(0.0001 - energy) < tolerance_amber);
}
END_TEST



START_TEST(test_phenol_gaff_sander_distorted)
{
	/*
	 * Sander 17.0 output
	 * NSTEP =        0   TIME(PS) =       0.000  TEMP(K) =     0.00  PRESS =     0.0
	 * Etot   =        25.5118  EKtot   =         0.0000  EPtot      =        25.5118
	 * BOND   =        13.5975  ANGLE   =         2.7837  DIHED      =         5.7602
	 * 1-4 NB =         3.8288  1-4 EEL =        -0.3147  VDWAALS    =        -0.2657
	 * EELEC  =         0.1221  EHBOND  =         0.0000  RESTRAINT  =         0.0000
	 * EKCMT  =         0.0000  VIRIAL  =         0.0000  VOLUME     =    255015.2414
	 */
	// Move Oxygen a little
	test_ag->coords[6].Z -= 0.5;

	static const double tolerance_amber = 1e-4;
	double energy;
	mol_zero_gradients(test_ag);

	energy = 0;
	beng(test_ag, &energy);
	ck_assert(fabs(13.5975 - energy) < tolerance_amber);

	energy = 0;
	aeng(test_ag, &energy);
	ck_assert(fabs(2.7837 - energy) < tolerance_amber);

	energy = 0;
	teng(test_ag, &energy);
	ieng(test_ag, &energy);
	ck_assert(fabs(5.7602 - energy) < tolerance_amber);

	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, beng);
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, aeng);
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, teng);
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, ieng);
}
END_TEST


Suite *benergy_suite(void)
{
	Suite *suite = suite_create("benergy");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_set_timeout(tcase, 20);
        tcase_add_checked_fixture(tcase, setup, teardown);
	tcase_add_test(tcase, test_beng);
	tcase_add_test(tcase, test_aeng);
	tcase_add_test(tcase, test_ieng);
	tcase_add_test(tcase, test_teng);

	suite_add_tcase(suite, tcase);

	TCase *tcase_gaff = tcase_create("test_gaff");
	tcase_add_checked_fixture(tcase_gaff, setup_gaff, teardown);
	// Sanity tests
	tcase_add_test(tcase_gaff, test_beng);
	tcase_add_test(tcase_gaff, test_aeng);
	tcase_add_test(tcase_gaff, test_ieng);
	tcase_add_test(tcase_gaff, test_teng);
	// Comparing energies with sander
	tcase_add_test(tcase_gaff, test_phenol_gaff_sander);
	tcase_add_test(tcase_gaff, test_phenol_gaff_sander_distorted);

	suite_add_tcase(suite, tcase_gaff);


	TCase *bonds = tcase_create("bond_acetate");
	tcase_add_test(bonds, acetate_sprung);
	tcase_add_test(bonds, acetate_sprang);

	suite_add_tcase(suite, bonds);

	return suite;
}

int main(void)
{
	Suite *suite = benergy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

