#include <stdio.h>
#include <check.h>
#include <math.h>
#include "atom_group.h"
#include "pdb.h"
#include "icharmm.h"
#include "pwpot.h"

struct mol_atom_group *test_ag, *test_ag2;
struct agsetup test_ags;
struct mol_prms *params, *params_mb;

#define _ALMOST_EQ(x, y) (fabs((x) - (y)) < 1e-6)
#define _ALMOST_EQ_EPS(x, y, eps) (fabs((x) - (y)) < (eps))


/**
 * Calculate numerical gradient of pwpot for single coordinate.
 * \param atom_num Atom number
 * \param crd_id Coordinate ID (0 means X, 1 means Y, 2 means Z)
 * \param d Displacement of atom
 * \param weight Weight of pairwise term in total energy function
 * \param en0 Energy in unperturbed state
 * \return Gradient
 */
static double _calc_pwpot_grad(
		const size_t atom_num,
		const int crd_id,
		const double d,
		const double weight,
		const double en0)
{
	double *crd = NULL;
	double en = 0;

	switch (crd_id) {
		case 0:
			crd = &test_ag->coords[atom_num].X;
			break;
		case 1:
			crd = &test_ag->coords[atom_num].Y;
			break;
		case 2:
			crd = &test_ag->coords[atom_num].Z;
			break;
	}
	double tmp = *crd;
	*crd += d;
	mol_pwpot_eng(test_ag, &en, params, test_ags.nblst, weight);
	*crd = tmp;
	return (en0 - en) / d;
}


// Function for testing calculated numerical gradient of pwpot
static void _test_pwpot_grads(const double d)
{
	double en, weight = 0.5;
	double tolerance = 1e-6;
	struct mol_vector3 *fs = calloc(test_ag->natoms, sizeof(struct mol_vector3));

	en = 0;
	mol_pwpot_eng(test_ag, &en, params, test_ags.nblst, weight);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		fs[i].X = _calc_pwpot_grad(i, 0, d, weight, en);
		fs[i].Y = _calc_pwpot_grad(i, 1, d, weight, en);
		fs[i].Z = _calc_pwpot_grad(i, 2, d, weight, en);
	}

	en = 0;
	// Since mol_pwpot_eng overwrites gradients in atom group, we must recalculate them.
	mol_zero_gradients(test_ag);
	mol_pwpot_eng(test_ag, &en, params, test_ags.nblst, weight);
	char msg[256];

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		sprintf(msg,
			"\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
			i,
			test_ag->gradients[i].X, test_ag->gradients[i].Y, test_ag->gradients[i].Z,
			fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(_ALMOST_EQ_EPS(test_ag->gradients[i].X, fs[i].X, tolerance), "%s", msg);
		ck_assert_msg(_ALMOST_EQ_EPS(test_ag->gradients[i].Y, fs[i].Y, tolerance), "%s", msg);
		ck_assert_msg(_ALMOST_EQ_EPS(test_ag->gradients[i].Z, fs[i].Z, tolerance), "%s", msg);

	}
	free(fs);
}


// Regression testing
START_TEST(test_pwpot_old_libmol)
{
	double energy = 0.0;
	const double energy_ref = -0.041265252902; // Reference value from old libmol
	mol_pwpot_eng(test_ag, &energy, params, test_ags.nblst, 0.05);
	ck_assert(_ALMOST_EQ(energy, energy_ref));
	_test_pwpot_grads(1e-6);
}
END_TEST


// Simple test for table values
START_TEST(test_pwpot_eng_pair)
{
	// DARS energy for O-O pair is 0.12 (Table S1 in Ref. [1]).
	// ACP energy for O-O pair is -0.016 (Table 1 in Ref. [2]).
	// Total energy here is 0.25 * (3*DARS + 0.5*ACP) (page 397 in Ref. [2]).
	const double distances[] = {2.0, 6.0, 6.5, 7.0, 200.0};
	const double energy_ref[] = {0.088, 0.088, 0.044, 0, 0};
	const double force_ref[] = {0, 0, 1.875 * 0.088, 0, 0};
	for (int i = 0; i < 5; i++) {
		test_ag->coords[1].X = distances[i];
		double energy = 0.0;
		mol_zero_gradients(test_ag);
		mol_pwpot_eng(test_ag, &energy, params, test_ags.nblst, 1.00);

		ck_assert(_ALMOST_EQ(energy, energy_ref[i]));
		ck_assert(_ALMOST_EQ(test_ag->gradients[0].X, -force_ref[i]));
		ck_assert(_ALMOST_EQ(test_ag->gradients[0].Y, 0.0));
		ck_assert(_ALMOST_EQ(test_ag->gradients[0].Z, 0.0));
		ck_assert(_ALMOST_EQ(test_ag->gradients[1].X, force_ref[i]));
		ck_assert(_ALMOST_EQ(test_ag->gradients[1].Y, 0.0));
		ck_assert(_ALMOST_EQ(test_ag->gradients[1].Z, 0.0));
	}
}
END_TEST


START_TEST(test_pwpot_score_pair)
{
	// DARS energy for O-O pair is 0.12 (Table S1 in Ref. [1]).
	// ACP energy for O-O pair is -0.016 (Table 1 in Ref. [2]).
	// Total energy here is 0.25 * (3*DARS + 0.5*ACP) (page 397 in Ref. [2]).
	const double distances[] = {2.0, 6.0, 6.5, 7.0, 200.0};
	const double energy_ref[] = {0.088, 0.088, 0, 0, 0}; // No smooth switch to zero
	// Each atom group contains two atoms. We want to test pairwise interaction, so we move one atom from each far away.
	MOL_VEC_SET_SCALAR(test_ag->coords[0], 9999);
	MOL_VEC_SET_SCALAR(test_ag2->coords[0], -9999);
	for (int i = 0; i < 5; i++) {
		test_ag->coords[1].X = distances[i];
		double energy = mol_pwpot_score(test_ag, test_ag2, params);
		ck_assert(_ALMOST_EQ(energy, energy_ref[i]));
	}
}
END_TEST


START_TEST(test_pwpot_score_pair_multibin)
{
	// DARS energy for O-O pair is 0.12 (Table S1 in Ref. [1]).
	// ACP energy for O-O pair is -0.016 (Table 1 in Ref. [2]).
	// Total energy here is 0.25 * (3*DARS + 0.5*ACP) (page 397 in Ref. [2]).
	const double distances[] = {0.0, 3.0, 3.5, 4.0, 6.0, 6.5, 7.0, 200.0};
	const double energy_ref[] = {0.88, 0.88, 0.088, 0.088, 0.088, 0, 0, 0}; // No smooth switch to zero
	// Each atom group contains two atoms. We want to test pairwise interaction, so we move one atom from each far away.
	// After that, only test_ag[1] and test_ag2[0] are interacting
	MOL_VEC_SET_SCALAR(test_ag->coords[0], 9999);
	MOL_VEC_SET_SCALAR(test_ag2->coords[1], -9999);
	for (int i = 0; i < 8; i++) {
		test_ag->coords[1].X = distances[i];
		double energy = mol_pwpot_score(test_ag, test_ag2, params_mb);
		// We have 10x scaled values in pwpot table for short-distance bin, so we use 10x less precision
		ck_assert_msg(_ALMOST_EQ_EPS(energy, energy_ref[i], 1e-5),
			"Energy at distance %.1f expected %.6f, got %.6f\n",
			distances[i], energy_ref[i], energy);
	}
}
END_TEST

START_TEST(test_pwpot_eng_empty)
{
	// No atom ids in atom_group: should exit.
	free(test_ag->pwpot_id);
	test_ag->pwpot_id = NULL;
	double energy;
	mol_pwpot_eng(test_ag, &energy, params, test_ags.nblst, 1.00);
}
END_TEST


START_TEST(test_pwpot_eng_multibin)
{
	// No multibin potential for energy: should exit.
	double energy;
	mol_pwpot_eng(test_ag, &energy, params_mb, test_ags.nblst, 1.00);
}
END_TEST

START_TEST(test_pwpot_score_empty)
{
	// No atom ids in atom_group: should exit.
	free(test_ag->pwpot_id);
	test_ag->pwpot_id = NULL;
	mol_pwpot_score(test_ag, test_ag, params);
}
END_TEST


void setup_ala(void)
{
	test_ag = mol_read_pdb("ala.pdb");
	mol_atom_group_read_geometry(test_ag, "ala.psf", "charmm_19_stripped.prm", "charmm_19_stripped.rtf");
	params = mol_prms_read("test.prm");
	mol_atom_group_add_prms(test_ag, params);
	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_ala(void)
{
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
	mol_prms_destroy(params);
}


void setup_pair(void)
{
	test_ag = mol_read_pdb("two_atoms.pdb");
	mol_atom_group_read_geometry(test_ag, "two_atoms.psf", "small.prm", "small.rtf");
	params = mol_prms_read("two_atoms.prm");
	params_mb = mol_prms_read("test_2pwpot.prm");
	mol_atom_group_add_prms(test_ag, params);

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);

	test_ag2 = mol_atom_group_copy(test_ag);  // Used for mol_pwpot_score
}

void teardown_pair(void)
{
	destroy_agsetup(&test_ags);
	mol_atom_group_free(test_ag);
	mol_atom_group_free(test_ag2);
	mol_prms_destroy(params);
}

Suite *energy_suite(void)
{
	Suite *suite = suite_create("energy_pwpot");

	TCase *tcase_pair = tcase_create("test_pwpot_pair");
	tcase_add_checked_fixture(tcase_pair, setup_pair, teardown_pair);
	tcase_add_test(tcase_pair, test_pwpot_eng_pair);
	tcase_add_test(tcase_pair, test_pwpot_score_pair);
	tcase_add_test(tcase_pair, test_pwpot_score_pair_multibin);

#ifndef _WIN32  // Windows don't support tcase_add_exit_test
	tcase_add_exit_test(tcase_pair, test_pwpot_eng_empty, EXIT_FAILURE);
	tcase_add_exit_test(tcase_pair, test_pwpot_eng_multibin, EXIT_FAILURE);
	tcase_add_exit_test(tcase_pair, test_pwpot_score_empty, EXIT_FAILURE);
#endif /* _WIN32 */

	suite_add_tcase(suite, tcase_pair);

	TCase *tcase_ala = tcase_create("test_pwpot_ala");
	tcase_add_checked_fixture(tcase_ala, setup_ala, teardown_ala);
	tcase_add_test(tcase_ala, test_pwpot_old_libmol);
	suite_add_tcase(suite, tcase_ala);

	return suite;
}

int main(void)
{
	Suite *suite = energy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
