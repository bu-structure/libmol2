#include <stdio.h>
#include <check.h>
#include <math.h>
#include <string.h>

#include "genborn.h"
#include "pdb.h"
#include "atom_group.h"
#include "utils.h"
#include "icharmm.h"

static const double delta = 1e-7;
static const double tolerance = 1e-4;
struct agsetup test_ags;

void test_gb_grad(double d, double energy, struct mol_atom_group *ag)
{
	double moved_energy = 0.0;
	double temp = 0.0;
	size_t n_atoms = ag->natoms;

	struct mol_vector3 *moved_grad = (struct mol_vector3 *) calloc(n_atoms, sizeof(struct mol_vector3));
	for (size_t i = 0; i < n_atoms; i++) {
		temp = ag->coords[i].X;
		ag->coords[i].X += d;
		moved_energy = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].X = temp;
		moved_grad[i].X = (moved_energy - energy) / d;

		temp = ag->coords[i].Y;
		ag->coords[i].Y += d;
		moved_energy = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].Y = temp;
		moved_grad[i].Y = (moved_energy - energy) / d;

		temp = ag->coords[i].Z;
		ag->coords[i].Z += d;
		moved_energy = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].Z = temp;
		moved_grad[i].Z = (moved_energy - energy) / d;
	}
	mol_zero_gradients(ag);
	mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	char msg[256];
	for (size_t i = 0; i < n_atoms; i++) {
		sprintf(msg,
		        "\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        i,
		        ag->gradients[i].X, ag->gradients[i].Y, ag->gradients[i].Z,
		        moved_grad[i].X, moved_grad[i].Y, moved_grad[i].Z);
		ck_assert_msg(fabs(ag->gradients[i].X - moved_grad[i].X) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Y - moved_grad[i].Y) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Z - moved_grad[i].Z) < tolerance, "%s", msg);

	}
	free_if_not_null(moved_grad);
}

void test_sa_grad(double d, double energy, struct mol_atom_group *ag)
{
	double moved_energy = 0.0;
	double temp = 0.0;
	size_t n_atoms = ag->natoms;

	struct mol_vector3 *moved_grad = (struct mol_vector3 *) calloc(n_atoms, sizeof(struct mol_vector3));
	for (size_t i = 0; i < n_atoms; i++) {
		temp = ag->coords[i].X;
		ag->coords[i].X += d;
		moved_energy = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].X = temp;
		moved_grad[i].X = (moved_energy - energy) / d;

		temp = ag->coords[i].Y;
		ag->coords[i].Y += d;
		moved_energy = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].Y = temp;
		moved_grad[i].Y = (moved_energy - energy) / d;

		temp = ag->coords[i].Z;
		ag->coords[i].Z += d;
		moved_energy = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].Z = temp;
		moved_grad[i].Z = (moved_energy - energy) / d;
	}
	mol_zero_gradients(ag);
	mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	char msg[256];
	for (size_t i = 0; i < n_atoms; i++) {
		sprintf(msg,
		        "\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        i,
		        ag->gradients[i].X, ag->gradients[i].Y, ag->gradients[i].Z,
		        moved_grad[i].X, moved_grad[i].Y, moved_grad[i].Z);
		ck_assert_msg(fabs(ag->gradients[i].X - moved_grad[i].X) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Y - moved_grad[i].Y) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Z - moved_grad[i].Z) < tolerance, "%s", msg);

	}
	free_if_not_null(moved_grad);
}

void test_gbsa_grad(double d, double energy, struct mol_atom_group *ag)
{
	double moved_energy = 0.0;
	double temp = 0.0;
	size_t n_atoms = ag->natoms;

	struct mol_vector3 *moved_grad = (struct mol_vector3 *) calloc(n_atoms, sizeof(struct mol_vector3));
	for (size_t i = 0; i < n_atoms; i++) {
		temp = ag->coords[i].X;
		ag->coords[i].X += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].X = temp;
		moved_grad[i].X = (moved_energy - energy) / d;

		temp = ag->coords[i].Y;
		ag->coords[i].Y += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].Y = temp;
		moved_grad[i].Y = (moved_energy - energy) / d;

		temp = ag->coords[i].Z;
		ag->coords[i].Z += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[i].Z = temp;
		moved_grad[i].Z = (moved_energy - energy) / d;
	}
	mol_zero_gradients(ag);
	mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	char msg[256];
	for (size_t i = 0; i < n_atoms; i++) {
		sprintf(msg,
		        "\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        i,
		        ag->gradients[i].X, ag->gradients[i].Y, ag->gradients[i].Z,
		        moved_grad[i].X, moved_grad[i].Y, moved_grad[i].Z);
		ck_assert_msg(fabs(ag->gradients[i].X - moved_grad[i].X) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Y - moved_grad[i].Y) < tolerance, "%s", msg);
		ck_assert_msg(fabs(ag->gradients[i].Z - moved_grad[i].Z) < tolerance, "%s", msg);

	}
	free_if_not_null(moved_grad);
}

void test_gb_grad_ala(double d, double energy, struct mol_atom_group *ag)
{
	double moved_energy = 0.0;
	double temp = 0.0;
	size_t n_atoms = ag->natoms;
	//A subset of atoms was chosen arbitrarily to for testing effective radius
	int indx[] = {0, 1, 2, 19, 20, 21, 37, 38, 39};
	struct mol_vector3 *moved_grad = calloc(n_atoms, sizeof(struct mol_vector3));

	for (size_t i = 0; i < 9; i++) {
		size_t ind = indx[i];
		temp = ag->coords[ind].X;
		ag->coords[ind].X += d;
		moved_energy = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].X = temp;
		moved_grad[ind].X = (moved_energy - energy) / d;

		temp = ag->coords[ind].Y;
		ag->coords[ind].Y += d;
		moved_energy = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Y = temp;
		moved_grad[ind].Y = (moved_energy - energy) / d;

		temp = ag->coords[ind].Z;
		ag->coords[ind].Z += d;
		moved_energy = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Z = temp;
		moved_grad[ind].Z = (moved_energy - energy) / d;
	}
	mol_zero_gradients(ag);
	mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	char msg[256];
	for (size_t i = 0; i < 9; i++) {
		size_t ind = indx[i];
		sprintf(msg,
		        "\n(atom: %zu) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        ind,
		        ag->gradients[ind].X, ag->gradients[ind].Y, ag->gradients[ind].Z,
		        moved_grad[ind].X, moved_grad[ind].Y, moved_grad[ind].Z);
		ck_assert_msg(fabs(ag->gradients[ind].X - moved_grad[ind].X) < 0.01 * fabs(moved_grad[ind].X), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Y - moved_grad[ind].Y) < 0.01 * fabs(moved_grad[ind].Y), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Z - moved_grad[ind].Z) < 0.01 * fabs(moved_grad[ind].Z), "%s", msg);

	}
	free_if_not_null(moved_grad);
}

void test_sa_grad_ala(double d, double energy, struct mol_atom_group *ag)
{
	double moved_energy = 0.0;
	double temp = 0.0;
	size_t n_atoms = ag->natoms;
	//A subset of atoms was chosen arbitrarily to for testing effective radius
	int indx[] = {0, 1, 2, 19, 20, 21, 37, 38, 39};
	struct mol_vector3 *moved_grad = calloc(n_atoms, sizeof(struct mol_vector3));

	for (size_t i = 0; i < 9; i++) {
		size_t ind = indx[i];
		temp = ag->coords[ind].X;
		ag->coords[ind].X += d;
		moved_energy = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].X = temp;
		moved_grad[ind].X = (moved_energy - energy) / d;

		temp = ag->coords[ind].Y;
		ag->coords[ind].Y += d;
		moved_energy = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Y = temp;
		moved_grad[ind].Y = (moved_energy - energy) / d;

		temp = ag->coords[ind].Z;
		ag->coords[ind].Z += d;
		moved_energy = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Z = temp;
		moved_grad[ind].Z = (moved_energy - energy) / d;
	}
	mol_zero_gradients(ag);
	mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	char msg[256];
	for (size_t i = 0; i < 9; i++) {
		size_t ind = indx[i];
		sprintf(msg,
		        "\n(atom: %zu) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        ind,
		        ag->gradients[ind].X, ag->gradients[ind].Y, ag->gradients[ind].Z,
		        moved_grad[ind].X, moved_grad[ind].Y, moved_grad[ind].Z);
		ck_assert_msg(fabs(ag->gradients[ind].X - moved_grad[ind].X) < 0.01 * fabs(moved_grad[ind].X), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Y - moved_grad[ind].Y) < 0.01 * fabs(moved_grad[ind].Y), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Z - moved_grad[ind].Z) < 0.01 * fabs(moved_grad[ind].Z), "%s", msg);

	}
	free_if_not_null(moved_grad);
}

void test_gbsa_grad_ala(double d, double energy, struct mol_atom_group *ag)
{
	double moved_energy = 0.0;
	double temp = 0.0;
	size_t n_atoms = ag->natoms;
	//A subset of atoms was chosen arbitrarily to for testing effective radius
	int indx[] = {0, 1, 2, 19, 20, 21, 37, 38, 39};
	struct mol_vector3 *moved_grad = calloc(n_atoms, sizeof(struct mol_vector3));

	for (size_t i = 0; i < 9; i++) {
		size_t ind = indx[i];
		temp = ag->coords[ind].X;
		ag->coords[ind].X += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].X = temp;
		moved_grad[ind].X = (moved_energy - energy) / d;

		temp = ag->coords[ind].Y;
		ag->coords[ind].Y += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Y = temp;
		moved_grad[ind].Y = (moved_energy - energy) / d;

		temp = ag->coords[ind].Z;
		ag->coords[ind].Z += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Z = temp;
		moved_grad[ind].Z = (moved_energy - energy) / d;
	}
	mol_zero_gradients(ag);
	mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	char msg[256];
	for (size_t i = 0; i < 9; i++) {
		size_t ind = indx[i];
		sprintf(msg,
		        "\n(atom: %zu) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        ind,
		        ag->gradients[ind].X, ag->gradients[ind].Y, ag->gradients[ind].Z,
		        moved_grad[ind].X, moved_grad[ind].Y, moved_grad[ind].Z);
		ck_assert_msg(fabs(ag->gradients[ind].X - moved_grad[ind].X) < 0.01 * fabs(moved_grad[ind].X), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Y - moved_grad[ind].Y) < 0.01 * fabs(moved_grad[ind].Y), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Z - moved_grad[ind].Z) < 0.01 * fabs(moved_grad[ind].Z), "%s", msg);

	}
	free_if_not_null(moved_grad);
}

void test_gbsa_grad_1reiA(double d, double energy, struct mol_atom_group *ag)
{
	double moved_energy = 0.0;
	double temp = 0.0;
	size_t n_atoms = ag->natoms;
	//A subset of atoms was chosen arbitrarily to speed testing up
	const size_t check_indx[] = {0, 1, 2, 500, 501, 502, 1000, 1001, 1002};
	struct mol_vector3 *moved_grad = calloc(n_atoms, sizeof(struct mol_vector3));

	for (size_t i = 0; i < 9; i++) {
		size_t ind = check_indx[i];
		temp = ag->coords[ind].X;
		ag->coords[ind].X += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].X = temp;
		moved_grad[ind].X = (moved_energy - energy) / d;

		temp = ag->coords[ind].Y;
		ag->coords[ind].Y += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Y = temp;
		moved_grad[ind].Y = (moved_energy - energy) / d;

		temp = ag->coords[ind].Z;
		ag->coords[ind].Z += d;
		moved_energy = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		ag->coords[ind].Z = temp;
		moved_grad[ind].Z = (moved_energy - energy) / d;
	}
	mol_zero_gradients(ag);
	mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	char msg[256];
	for (size_t i = 0; i < 9; i++) {
		size_t ind = check_indx[i];
		sprintf(msg,
		        "\n(atom: %zu) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        ind,
		        ag->gradients[ind].X, ag->gradients[ind].Y, ag->gradients[ind].Z,
		        moved_grad[ind].X, moved_grad[ind].Y, moved_grad[ind].Z);

		ck_assert_msg(fabs(ag->gradients[ind].X - moved_grad[ind].X) < 0.01 * fabs(moved_grad[ind].X), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Y - moved_grad[ind].Y) < 0.01 * fabs(moved_grad[ind].Y), "%s", msg);
		ck_assert_msg(fabs(ag->gradients[ind].Z - moved_grad[ind].Z) < 0.01 * fabs(moved_grad[ind].Z), "%s", msg);

	}
	free_if_not_null(moved_grad);
}

START_TEST(test_obc_gb)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei_A_libmol.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "1rei_A_libmol.psf", "parm14sb.prm", "parm14sb_notip3.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	//Set the nb list cut to the cut value in amber's sander script
	test_ags.nblst->nbcut = 13.0;
	update_nblst(ag, &test_ags);
	bool init = mol_init_gb(ag);
	ck_assert(init);
	mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
//	test_gb_grad_1reiA(delta, gb, ag);
	bool del = mol_gb_delete_metadata(ag);
	ck_assert(del == true);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_sa)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei_A_libmol.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "1rei_A_libmol.psf", "parm14sb.prm", "parm14sb_notip3.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	//Set the nb list cut to the cut value in amber's sander script
	test_ags.nblst->nbcut = 13.0;
	update_nblst(ag, &test_ags);
	bool init = mol_init_gb(ag);
	ck_assert(init);
	mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
//	test_sa_grad_1reiA(delta, ener, ag);
	bool del = mol_gb_delete_metadata(ag);
	ck_assert(del == true);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_gbsa)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei_A_libmol.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "1rei_A_libmol.psf", "parm14sb.prm", "parm14sb_notip3.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	//Set the nb list cut to the cut value in amber's sander script
	test_ags.nblst->nbcut = 13.0;
	update_nblst(ag, &test_ags);
	bool init = mol_init_gb(ag);
	ck_assert(init);
	double ener = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	test_gbsa_grad_1reiA(delta, ener, ag);
	bool del = mol_gb_delete_metadata(ag);
	ck_assert(del == true);
	mol_atom_group_free(ag);
}

END_TEST

START_TEST(test_obc_gb_ala)
{
	//check alpha of some atoms with amber's values
	struct mol_atom_group *ag = mol_read_pdb("ala_libmol.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "ala_libmol.psf", "parm14sb.prm", "parm14sb_notip3.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	test_ags.nblst->nbcut = 13.0;
	update_nblst(ag, &test_ags);
	bool init = mol_init_gb(ag);
	ck_assert(init);
	double gb = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	test_gb_grad_ala(delta, gb, ag);
	bool del = mol_gb_delete_metadata(ag);
	ck_assert(del == true);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_sa_ala)
{
	//check alpha of some atoms with amber's values
	struct mol_atom_group *ag = mol_read_pdb("ala_libmol.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "ala_libmol.psf", "parm14sb.prm", "parm14sb_notip3.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	test_ags.nblst->nbcut = 13.0;
	update_nblst(ag, &test_ags);
	bool init = mol_init_gb(ag);
	ck_assert(init);
	double ener = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	test_sa_grad_ala(delta, ener, ag);
	bool del = mol_gb_delete_metadata(ag);
	ck_assert(del == true);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_gbsa_ala)
{
	//check alpha of some atoms with amber's values
	struct mol_atom_group *ag = mol_read_pdb("ala_libmol.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "ala_libmol.psf", "parm14sb.prm", "parm14sb_notip3.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	test_ags.nblst->nbcut = 13.0;
	update_nblst(ag, &test_ags);
	bool init = mol_init_gb(ag);
	ck_assert(init);
	double ener = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
	test_gbsa_grad_ala(delta, ener, ag);
	bool del = mol_gb_delete_metadata(ag);
	ck_assert(del == true);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_gb_two_atoms)
{
	struct mol_atom_group *ag = mol_read_pdb("two_atoms.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "two_atoms.psf", "two_atoms_charmm.prm",
	                                        "two_atoms_charmm.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	update_nblst(ag, &test_ags);
	const double coordinate[] = {14.5, 14, 11.5, 11, 8, 7, 4, 3, 2.5, 2};
	test_ags.nblst->nbcut = 13.0;
	for (int i = 0; i < 1; i++) {
		mol_zero_gradients(ag);
		ag->coords[1].X = coordinate[i];
		bool init = mol_init_gb(ag);
		ck_assert(init);
		double gb = mol_gb_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		test_gb_grad(delta, gb, ag);
		bool del = mol_gb_delete_metadata(ag);
		ck_assert(del == true);
	}
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_sa_two_atoms)
{
	struct mol_atom_group *ag = mol_read_pdb("two_atoms.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "two_atoms.psf", "two_atoms_charmm.prm",
	                                        "two_atoms_charmm.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	update_nblst(ag, &test_ags);
	const double coordinate[] = {14.5, 14, 11.5, 11, 8, 7, 4, 3, 2.5, 2};
	test_ags.nblst->nbcut = 13.0;
	for (int i = 0; i < 1; i++) {
		mol_zero_gradients(ag);
		ag->coords[1].X = coordinate[i];
		bool init = mol_init_gb(ag);
		ck_assert(init);
		double gb = mol_sa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		test_sa_grad(delta, gb, ag);
		bool del = mol_gb_delete_metadata(ag);
		ck_assert(del == true);
	}
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_gbsa_two_atoms)
{
	struct mol_atom_group *ag = mol_read_pdb("two_atoms.pdb");
	bool geo = mol_atom_group_read_geometry(ag, "two_atoms.psf", "two_atoms_charmm.prm",
	                                        "two_atoms_charmm.rtf");
	ck_assert(geo == true);
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);
	init_nblst(ag, &test_ags);
	update_nblst(ag, &test_ags);
	const double coordinate[] = {14.5, 14, 11.5, 11, 8, 7, 4, 3, 2.5, 2};
	test_ags.nblst->nbcut = 13.0;
	for (int i = 0; i < 1; i++) {
		mol_zero_gradients(ag);
		ag->coords[1].X = coordinate[i];
		bool init = mol_init_gb(ag);
		ck_assert(init);
		double gb = mol_gbsa_energy(ag, 25.0, &test_ags, MOL_GENBORN_OBC_2);
		test_gbsa_grad(delta, gb, ag);
		bool del = mol_gb_delete_metadata(ag);
		ck_assert(del == true);
	}
	mol_atom_group_free(ag);
}
END_TEST

Suite *gb_suite(void)
{
	Suite *suite = suite_create("genborn");
	TCase *tcase = tcase_create("test_genborn");
	tcase_set_timeout(tcase, 600);
	tcase_add_test(tcase, test_obc_gb);
	tcase_add_test(tcase, test_sa);
	tcase_add_test(tcase, test_gbsa);
	tcase_add_test(tcase, test_obc_gb_ala);
	tcase_add_test(tcase, test_sa_ala);
	tcase_add_test(tcase, test_gbsa_ala);
	tcase_add_test(tcase, test_gb_two_atoms);
	tcase_add_test(tcase, test_sa_two_atoms);
	tcase_add_test(tcase, test_gbsa_two_atoms);
	suite_add_tcase(suite, tcase);
	return suite;
}

int main(void)
{
	Suite *suite = gb_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

