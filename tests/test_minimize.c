#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "benergy.h"
#include "gbsa.h"
#include "icharmm.h"
#include "minimize.h"
#include "nbenergy.h"
#include "pdb.h"

#define __TOL__ 5E-4

struct energy_prm {
	struct mol_atom_group *ag;
	struct agsetup *ag_setup;
	struct acesetup *ace_setup;
};

static lbfgsfloatval_t energy_func(
	void* restrict prm,
	const double* restrict array,
	double* restrict gradient,
	const int array_size,
	const lbfgsfloatval_t step)
{
	lbfgsfloatval_t energy = 0.0;
	struct energy_prm* energy_prm = (struct energy_prm*) prm;
	if (array != NULL) {
		ck_assert((size_t) array_size == energy_prm->ag->active_atoms->size * 3);
		mol_atom_group_set_actives(energy_prm->ag, array);
	}
	bool updated = check_clusterupdate(energy_prm->ag, energy_prm->ag_setup);
	if (updated) {
		ace_updatenblst(energy_prm->ag_setup, energy_prm->ace_setup);
	}

	//reset energy
	mol_zero_gradients(energy_prm->ag);

	//energy calculations
	aceeng(energy_prm->ag, &energy, energy_prm->ace_setup, energy_prm->ag_setup);
	vdweng(energy_prm->ag, &energy, energy_prm->ag_setup->nblst);
	vdwengs03(1.0, energy_prm->ag_setup->nblst->nbcof, energy_prm->ag, &energy,
		  energy_prm->ag_setup->nf03, energy_prm->ag_setup->listf03);
	beng(energy_prm->ag, &energy);
	aeng(energy_prm->ag, &energy);
	teng(energy_prm->ag, &energy);
	ieng(energy_prm->ag, &energy);

	if (gradient != NULL) {
		for (int i = 0; i < array_size / 3; i++ ) {
			int atom_i = energy_prm->ag->active_atoms->members[i];
			gradient[3*i]     = -energy_prm->ag->gradients[atom_i].X;
			gradient[(3*i)+1] = -energy_prm->ag->gradients[atom_i].Y;
			gradient[(3*i)+2] = -energy_prm->ag->gradients[atom_i].Z;
		}
	}

	return energy;
}

// Test cases
START_TEST(test_minimize_lbfgs)
{
	struct mol_atom_group *ag = mol_read_pdb("phenol.pdb");
	ck_assert(ag != NULL);
	mol_atom_group_read_geometry(ag, "phenol.psf", "small.prm", "small.rtf");
	ag->gradients = calloc(ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(ag);
	mol_fixed_update(ag, 0, NULL);

	struct mol_vector3 *orig_coords = calloc(ag->natoms, sizeof(struct mol_vector3));
	memcpy(orig_coords, ag->coords, ag->natoms*sizeof(struct mol_vector3));

	struct agsetup ags;
	init_nblst(ag, &ags);
	update_nblst(ag, &ags);
	struct acesetup ace_setup;
	ace_setup.efac = 0.5;
	ace_ini(ag, &ace_setup);
	ace_fixedupdate(ag, &ags, &ace_setup);
	ace_updatenblst(&ags, &ace_setup);

	struct energy_prm engpar;
	engpar.ag = ag;
	engpar.ag_setup = &ags;
	engpar.ace_setup = &ace_setup;

	mol_minimize_ag(MOL_LBFGS, 1000, 1E-6, ag, (void *)(&engpar), energy_func);

	struct mol_atom_group *expected = mol_read_pdb("phenol_min_lbfgs.pdb");

	ck_assert(ag->natoms == expected->natoms);
	for(size_t i = 0; i < ag->natoms; i++) {
		ck_assert(fabs(ag->coords[i].X - expected->coords[i].X) < __TOL__);
		ck_assert(fabs(ag->coords[i].Y - expected->coords[i].Y) < __TOL__);
		ck_assert(fabs(ag->coords[i].Z - expected->coords[i].Z) < __TOL__);
	}
	free(orig_coords);
	mol_atom_group_free(expected);
	mol_atom_group_free(ag);
	destroy_acesetup(&ace_setup);
	destroy_agsetup(&ags);
}
END_TEST

Suite *minimize_suite(void)
{
	Suite *suite = suite_create("minimize");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_minimize_lbfgs);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = minimize_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
