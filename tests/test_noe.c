#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>
#include <time.h>

#include "pdb.h"
#include "noe.h"

// These functions are defined only in newer versions of check
#ifndef ck_assert_double_eq_tol
#define ck_assert_double_eq_tol(x, y, tol) do { \
	sprintf(msg, "Calculated: %lf, Reference: %lf, Tolerance %lf (line %i)\n", x, y, tol, __LINE__); \
	ck_assert_msg(fabs((x) - (y)) <= (tol), msg); \
	} while(0);
#endif

#ifndef ck_assert_ptr_nonnull
#define ck_assert_ptr_nonnull(x) do { \
	sprintf(msg, "Pointer is NULL (line %i)\n", __LINE__); \
	ck_assert_msg((x) != NULL, msg); \
	} while(0);
#endif

#ifndef ck_assert_ptr_null
#define ck_assert_ptr_null(x) do { \
	sprintf(msg, "Pointer is not NULL (line %i)\n", __LINE__); \
	ck_assert_msg((x) == NULL, msg); \
	} while(0);
#endif

char msg[512];

static void compare_two_matrices(const double *comp, const double *real, const size_t size)
{
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			sprintf(msg, "entry (%zu, %zu) - calculated: %lf, reference: %lf\n",
			        i, j, comp[i * size + j], real[i * size + j]);
			ck_assert_msg(fabs(comp[i * size + j] - real[i * size + j]) <= 0.00001, "%s", msg);
		}
	}
}


static void compare_two_matrices_upper_triangle(const double *comp, const double *real, const size_t size)
{
	for (size_t i = 0; i < size; i++) {
		for (size_t j = i; j < size; j++) {
			sprintf(msg, "entry (%zu, %zu) - calculated: %lf, reference: %lf\n",
			        i, j, comp[i * size + j], real[i * size + j]);
			ck_assert_msg(fabs(comp[i * size + j] - real[i * size + j]) <= 0.00001, "%s", msg);
		}
	}
}


static void compare_gradients(const struct mol_noe_grad *num, const struct mol_noe_grad *ana)
{
	ck_assert_int_eq(num->natoms, ana->natoms);
	ck_assert_int_eq(num->npeaks, ana->npeaks);

	for (size_t i = 0; i < num->natoms * num->npeaks * num->npeaks; i++) {
		ck_assert_double_eq_tol(num->grad[i].X, ana->grad[i].X, 0.0001);
		ck_assert_double_eq_tol(num->grad[i].Y, ana->grad[i].Y, 0.0001);
		ck_assert_double_eq_tol(num->grad[i].Z, ana->grad[i].Z, 0.0001);
	}
}


/**
 * Check analytically computed gradient vs finite differences
 */
static void test_peaks_gradient(const char *groups, const char *pdb)
{
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt(groups);
	struct mol_atom_group *ag = mol_read_pdb(pdb);
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.1, 1000);

	struct mol_noe_grad *in_ana = mol_noe_grad_create(ag->natoms, g->ngroups);
	struct mol_noe_grad *rx_ana = mol_noe_grad_create(ag->natoms, g->ngroups);
	spec->rx_grad = rx_ana;
	spec->in_grad = in_ana;
	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);

	struct mol_noe_grad *in_num = mol_noe_grad_create(ag->natoms, g->ngroups);
	struct mol_noe_grad *rx_num = mol_noe_grad_create(ag->natoms, g->ngroups);
	spec->rx_grad = rx_num;
	spec->in_grad = in_num;
	ck_assert_int_eq(mol_noe_calc_peaks_grad_numeric(spec, ag, 0.0000001), true);

	compare_gradients(in_num, in_ana);

	mol_noe_free(spec);
	mol_atom_group_free(ag);
	mol_noe_grad_free(in_ana);
	mol_noe_grad_free(rx_ana);
}

/**
 * Check analytically computed energy gradient vs finite differences
 */
static void test_energy_gradient_from_txt(
	const char *groups,
	const char *pdb,
	const char *exp_path,
	const double weight,
	const double power)
{
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt(groups);
	struct mol_atom_group *ag = mol_read_pdb(pdb);
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.2, 1000.);

	struct mol_noe_grad *in_ana = mol_noe_grad_create(ag->natoms, g->ngroups);
	struct mol_noe_grad *rx_ana = mol_noe_grad_create(ag->natoms, g->ngroups);
	spec->rx_grad = rx_ana;
	spec->in_grad = in_ana;
	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);

	spec->exp = mol_noe_experiment_from_txt_stacked(exp_path, g->ngroups, NULL);
	ck_assert_ptr_nonnull(spec->exp);

	struct mol_vector3 *grad_ana = calloc(ag->natoms, sizeof(struct mol_vector3));
	ck_assert_int_eq(mol_noe_calc_energy(spec, grad_ana, weight, power), true);

	struct mol_vector3 *grad_num = calloc(ag->natoms, sizeof(struct mol_vector3));
	ag->gradients = grad_num;
	ck_assert_int_eq(mol_noe_calc_energy_grad_numeric(spec, ag, 0.000001, weight, power), true);

	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_double_eq_tol(grad_ana[i].X, -grad_num[i].X, 0.0001);
		ck_assert_double_eq_tol(grad_ana[i].Y, -grad_num[i].Y, 0.0001);
		ck_assert_double_eq_tol(grad_ana[i].Z, -grad_num[i].Z, 0.0001);
	}

	mol_noe_free(spec);
	mol_atom_group_free(ag);
	free(grad_ana);
}


/**
 * Check analytically computed energy gradient vs finite differences
 */
static void test_energy_gradient_from_json(
	const char *json,
	const char *pdb,
	const double weight,
	const double power)
{
	struct mol_atom_group *ag = mol_read_pdb(pdb);
	struct mol_noe *spec = mol_noe_read_json(json);
	ck_assert_ptr_nonnull(spec->exp);

	mol_noe_alloc_grad(spec);
	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);

	struct mol_vector3 *grad_ana = calloc(ag->natoms, sizeof(struct mol_vector3));
	ck_assert_int_eq(mol_noe_calc_energy(spec, grad_ana, weight, power), true);

	struct mol_vector3 *grad_num = calloc(ag->natoms, sizeof(struct mol_vector3));
	ag->gradients = grad_num;
	ck_assert_int_eq(mol_noe_calc_energy_grad_numeric(spec, ag, 0.000001, weight, power), true);

	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_double_eq_tol(grad_ana[i].X, -grad_num[i].X, 0.0001);
		ck_assert_double_eq_tol(grad_ana[i].Y, -grad_num[i].Y, 0.0001);
		ck_assert_double_eq_tol(grad_ana[i].Z, -grad_num[i].Z, 0.0001);
	}

	mol_noe_free(spec);
	mol_atom_group_free(ag);
	free(grad_ana);
}


/* *******************
 * Peaks calculation *
 *********************/

// In this section the computed peaks are compared to the ones produced by Xplor

START_TEST(nmr_test_2h_peaks)
{
	// Two protons
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt("2h.groups");
	struct mol_atom_group *ag = mol_read_pdb("2h.pdb");
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.2, 10);

	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);
	double *real = mol_noe_matrix_read_txt_stacked("2h.mat", spec->size, NULL);
	ck_assert_ptr_nonnull(real);

	compare_two_matrices(spec->in, real, spec->size);

	mol_noe_free(spec);
	mol_atom_group_free(ag);
	free(real);
}

END_TEST


START_TEST(nmr_test_3h_peaks)
{
	// Three protons
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt("3h.groups");
	struct mol_atom_group *ag = mol_read_pdb("3h.pdb");
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.2, 10);
	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);

	double *real = mol_noe_matrix_read_txt_stacked("3h.mat", spec->size, NULL);
	ck_assert_ptr_nonnull(real);

	compare_two_matrices(spec->in, real, spec->size);

	mol_noe_free(spec);
	mol_atom_group_free(ag);
	free(real);
}

END_TEST


START_TEST(nmr_test_6h_peaks)
{
	// 6 protons split into 3 groups
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt("6h.groups");
	struct mol_atom_group *ag = mol_read_pdb("6h.pdb");
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.2, 100);

	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);
	double *real = mol_noe_matrix_read_txt_stacked("6h.mat", spec->size, NULL);
	ck_assert_ptr_nonnull(real);

	compare_two_matrices(spec->in, real, spec->size);

	mol_noe_free(spec);
	mol_atom_group_free(ag);
	free(real);
}

END_TEST


START_TEST(nmr_test_6h_peaks_mask)
{
	// Test masking. Select 3 cross-peaks and verify the only those
	// three are calculated and the rest are zero
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt("6h.groups");
	struct mol_atom_group *ag = mol_read_pdb("6h.pdb");
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.2, 100);
	spec->_mask = calloc(9, sizeof(bool));
	spec->_mask[0] = true;
	spec->_mask[1] = true;
	spec->_mask[4] = true;

	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);
	double *real = mol_noe_matrix_read_txt_stacked("6h.mat", spec->size, NULL);
	ck_assert_ptr_nonnull(real);

	real[2] = real[5] = real[6] = real[7] = real[8] = 0;
	compare_two_matrices(spec->in, real, spec->size);

	spec->_mask = NULL;
	mol_noe_free(spec);
	mol_atom_group_free(ag);
	free(real);
}

END_TEST


/* ********************
 * Energy calculation *
 **********************/


START_TEST(nmr_test_3h_energy)
{
	// Test energy (fitting score) calculation for predicted cross-peaks
	// vs Xplor peaks. Since they almost the same the energy is zero and
	// the scaling factor is 1.0
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt("3h.groups");
	struct mol_atom_group *ag = mol_read_pdb("3h.pdb");
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.2, sqrt(10));
	spec->exp = mol_noe_experiment_from_txt_stacked("3h.mat", spec->size, NULL);
	ck_assert_ptr_nonnull(spec->exp);

	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, true), true);
	ck_assert_int_eq(mol_noe_calc_energy(spec, NULL, 1.0, 1.0 / 3.0), true);
	ck_assert_double_eq_tol(spec->energy, 0.0, 10E-4);
	ck_assert_double_eq_tol(spec->scale, 1.0, 10E-4);

	// Translate a proton and recompute fitting score and scaling factor
	struct mol_vector3f tr = {3.0, 3.0, 3.0};
	MOL_VEC_ADD(ag->coords[0], ag->coords[0], tr);
	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, true), true);
	ck_assert_int_eq(mol_noe_calc_energy(spec, NULL, 1.0, 1.0 / 3.0), true);
	ck_assert_double_eq_tol(spec->energy, 0.186389, 10E-4);
	ck_assert_double_eq_tol(spec->scale, 0.543524, 10E-4);

	mol_noe_free(spec);
	mol_atom_group_free(ag);
}

END_TEST


START_TEST(nmr_test_6h_energy)
{
	// Same as above for 6 protons
	struct mol_noe_group_list *g = mol_noe_group_list_read_txt("6h.groups");
	struct mol_atom_group *ag = mol_read_pdb("6h.pdb");
	struct mol_noe *spec = mol_noe_create(g, 0.00006, 0.1, 0.2, sqrt(10));
	spec->exp = mol_noe_experiment_from_txt_stacked("6h.mat", spec->size, NULL);
	ck_assert_ptr_nonnull(spec->exp);

	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);
	ck_assert_int_eq(mol_noe_calc_energy(spec, NULL, 1.0, 1.0 / 6.0), true);
	ck_assert_double_eq_tol(spec->energy, 0.0, 10E-4);
	ck_assert_double_eq_tol(spec->scale, 1.0, 10E-4);

	struct mol_vector3f tr = {3.0, -5.0, 2.0};
	MOL_VEC_ADD(ag->coords[0], ag->coords[0], tr);
	ck_assert_int_eq(mol_noe_calc_peaks(spec, ag, true, 0), true);
	ck_assert_int_eq(mol_noe_calc_energy(spec, NULL, 1.0, 1.0 / 6.0), true);
	ck_assert_double_eq_tol(spec->energy, 0.019955, 10E-4);
	ck_assert_double_eq_tol(spec->scale, 0.085339, 10E-4);

	mol_noe_free(spec);
	mol_atom_group_free(ag);
}

END_TEST


/* **********************
 * Gradient calculation *
 ************************/

// Compute gradients for peaks / fit energy wrt. to proton coordinates
// and compare with finite differences gradients


START_TEST(nmr_test_3h_gradient)
{
	test_peaks_gradient("3h.groups", "3h.pdb");
}

END_TEST


START_TEST(nmr_test_6h_gradient)
{
	test_peaks_gradient("6h.groups", "6h.pdb");
}

END_TEST


START_TEST(nmr_test_mol_gradient)
{
	test_peaks_gradient("mol.groups", "noe_mol.pdb");
}

END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_self)
{
	test_energy_gradient_from_txt("6h.groups", "6h.pdb", "6h.mat", 1.0, 1.0 / 6.0);
}

END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_perturbed)
{
	test_energy_gradient_from_txt("6h.groups", "6h.pdb", "6h_perturbed.mat", 1.0, 1.0 / 6.0);
}

END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_perturbed_from_json)
{
	test_energy_gradient_from_json("6hgroups_plus_errors.json", "6h.pdb", 1.0, 1.0 / 6.0);
}

END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_perturbed_from_json_peak_overlap)
{
	test_energy_gradient_from_json("6hgroups_plus_errors_plus_peak_overlap.json", "6h.pdb", 1.0, 1.0 / 6.0);
}

END_TEST



START_TEST(nmr_test_6h_noe_energy_gradient_cil)
{
	test_energy_gradient_from_txt("cil_md_ch22.groups", "cil_md_ch22.pdb", "cil_md_ch22.mat", 1.0, 1.0 / 6.0);
}

END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_cil_diff_weight)
{
	test_energy_gradient_from_txt("cil_md_ch22.groups", "cil_md_ch22.pdb", "cil_md_ch22.mat", 5.0, 1.0 / 6.0);
}

END_TEST


START_TEST(nmr_test_6h_noe_energy_gradient_cil_diff_power)
{
	test_energy_gradient_from_txt("cil_md_ch22.groups", "cil_md_ch22.pdb", "cil_md_ch22.mat", 1.0, 1.0 / 3.0);
}

END_TEST


/* ************
 * Json input *
 **************/

// Test json input / output for correctness

START_TEST(nmr_test_parse_2hgroups_json)
{
	// Test invalid input files
	ck_assert_ptr_null(mol_noe_read_json("noe_error1.json"));
	ck_assert_ptr_null(mol_noe_read_json("noe_error2.json"));
	ck_assert_ptr_null(mol_noe_read_json("noe_error3.json"));
	ck_assert_ptr_null(mol_noe_read_json("noe_error5.json"));
	ck_assert_ptr_null(mol_noe_read_json("noe_error6.json"));

	// Test valid input files
	struct mol_noe *noe;
	ck_assert_ptr_nonnull(noe = mol_noe_read_json("noe_valid1.json"));
	mol_noe_free(noe);
	ck_assert_ptr_nonnull(noe = mol_noe_read_json("noe_valid2.json"));
	mol_noe_free(noe);
	ck_assert_ptr_nonnull(noe = mol_noe_read_json("noe_valid3.json"));
	mol_noe_free(noe);
	ck_assert_ptr_nonnull(noe = mol_noe_read_json("noe_valid4.json"));
	mol_noe_free(noe);
}

END_TEST


double* _noe_exp_to_matrix(struct mol_noe_experiment* exp, size_t size)
{
	double* m = calloc(size * size, sizeof(double));
	for (size_t p = 0; p < exp->npeaks; p++) {
		size_t i = exp->peaks[p].group1[0];
		size_t j = exp->peaks[p].group2[0];
		m[i * size + j] = exp->peaks[p].volume;
	}
	return m;
}


START_TEST(nmr_test_2h_peaks_json)
{
	struct mol_noe *noe = mol_noe_read_json("2hgroups.json");
	ck_assert_int_eq(noe->size, 2);
	ck_assert_int_eq(noe->groups->ngroups, 2);
	ck_assert_int_eq(noe->groups->natoms, 2);
	ck_assert_int_eq(noe->groups->groups[0].atoms[0], 0);
	ck_assert_int_eq(noe->groups->groups[0].size, 1);
	ck_assert_int_eq(noe->groups->groups[0].id, 0);
	ck_assert_int_eq(noe->groups->groups[1].atoms[0], 1);
	ck_assert_int_eq(noe->groups->groups[1].size, 1);
	ck_assert_int_eq(noe->groups->groups[1].id, 1);
	ck_assert_double_eq_tol(noe->omega, 0.00006, 10E-9);
	ck_assert_double_eq_tol(noe->t_cor, 0.1, 10E-9);
	ck_assert_double_eq_tol(noe->t_mix, 0.2, 10E-9);
	ck_assert_double_eq_tol(noe->cutoff, 10.0, 10E-9);
	ck_assert_ptr_null(noe->_mask);
	double* noe_mat = _noe_exp_to_matrix(noe->exp, 2);
	compare_two_matrices((double[4]) {0.840252, -0.074589, -0.074589, 0.840252}, noe_mat, 2);

	struct mol_atom_group *ag = mol_read_pdb("2h.pdb");
	ck_assert_int_eq(mol_noe_calc_peaks(noe, ag, true, 0), true);
	compare_two_matrices(noe->in, noe_mat, noe->size);
	mol_noe_free(noe);
	mol_atom_group_free(ag);
	free(noe_mat);
}

END_TEST


START_TEST(nmr_test_6h_peaks_json)
{
	struct mol_noe *noe = mol_noe_read_json("6hgroups.json");
	struct mol_atom_group *ag = mol_read_pdb("6h.pdb");
	ck_assert_int_eq(mol_noe_calc_peaks(noe, ag, true, 0), true);
	double* noe_mat = _noe_exp_to_matrix(noe->exp, noe->size);
	compare_two_matrices_upper_triangle(noe->in, noe_mat, noe->size);
	mol_noe_free(noe);
	mol_atom_group_free(ag);
	free(noe_mat);
}

END_TEST


START_TEST(nmr_test_6h_peaks_swapped_json)
{
	struct mol_noe *noe = mol_noe_read_json("6hgroups_swapped.json");
	struct mol_atom_group *ag = mol_read_pdb("6h.pdb");
	ck_assert_int_eq(mol_noe_calc_peaks(noe, ag, true, 0), true);
	double* noe_mat = _noe_exp_to_matrix(noe->exp, noe->size);
	compare_two_matrices_upper_triangle(noe->in, noe_mat, noe->size);
	mol_noe_free(noe);
	mol_atom_group_free(ag);
	free(noe_mat);
}

END_TEST


START_TEST(nmr_test_6h_forgot_to_calc_distances)
{
	struct mol_noe *noe = mol_noe_read_json("6hgroups.json");
	struct mol_atom_group *ag = mol_read_pdb("6h.pdb");
	ck_assert_int_eq(mol_noe_calc_peaks(noe, ag, false, 0), false);
	mol_noe_free(noe);
	mol_atom_group_free(ag);
}

END_TEST


Suite *lists_suite(void)
{
	Suite *suite = suite_create("mol_noe_grad");

	TCase *tcase_peaks = tcase_create("peaks");
	tcase_add_test(tcase_peaks, nmr_test_2h_peaks);
	tcase_add_test(tcase_peaks, nmr_test_3h_peaks);
	tcase_add_test(tcase_peaks, nmr_test_6h_peaks);
	tcase_add_test(tcase_peaks, nmr_test_6h_peaks_mask);
	tcase_add_test(tcase_peaks, nmr_test_6h_forgot_to_calc_distances);
	suite_add_tcase(suite, tcase_peaks);

	TCase *tcase_energy = tcase_create("energy");
	tcase_add_test(tcase_energy, nmr_test_3h_energy);
	tcase_add_test(tcase_energy, nmr_test_6h_energy);
	suite_add_tcase(suite, tcase_energy);

	TCase *tcase_grads = tcase_create("gradients");
	tcase_add_test(tcase_grads, nmr_test_3h_gradient);
	tcase_add_test(tcase_grads, nmr_test_6h_gradient);
	tcase_add_test(tcase_grads, nmr_test_mol_gradient);
	tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_self);
	tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_perturbed);
	tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_perturbed_from_json);
	tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_perturbed_from_json_peak_overlap);
	tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_cil);
	tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_cil_diff_weight);
	tcase_add_test(tcase_grads, nmr_test_6h_noe_energy_gradient_cil_diff_power);
	suite_add_tcase(suite, tcase_grads);

	TCase *tcase_json = tcase_create("json");
	tcase_add_test(tcase_json, nmr_test_parse_2hgroups_json);
	tcase_add_test(tcase_json, nmr_test_2h_peaks_json);
	tcase_add_test(tcase_json, nmr_test_6h_peaks_json);
	tcase_add_test(tcase_json, nmr_test_6h_peaks_swapped_json);
	suite_add_tcase(suite, tcase_json);

	return suite;
}


int main(void)
{
	Suite *suite = lists_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}