#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#define _POSIX_C_SOURCE 200809L
#define _USE_MATH_DEFINES

#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>
#include <stdbool.h>

#include "quaternion.h"


START_TEST(test_structs)
{
	struct mol_quaternion q = {1.9, 2.3, 2.1, 3.3};

	ck_assert(fabs(q.W - 1.9) <= DBL_EPSILON);
	ck_assert(fabs(q.X - 2.3) <= DBL_EPSILON);
	ck_assert(fabs(q.Y - 2.1) <= DBL_EPSILON);
	ck_assert(fabs(q.Z - 3.3) <= DBL_EPSILON);

	struct mol_quaternion *p = mol_quaternion_create();
	mol_quaternion_free(p);
}
END_TEST


START_TEST(test_macros)
{
	struct mol_quaternion qa = {3.0, 6.0, 3.0, 9.0};
	struct mol_quaternion qb = {1.0, 3.0, 5.0, 7.0};
	struct mol_quaternion q;

	MOL_QUATERNION_ADD(q, qa, qb);
	ck_assert(fabs(q.W - 4) <= DBL_EPSILON);
	ck_assert(fabs(q.X - 9) <= DBL_EPSILON);
	ck_assert(fabs(q.Y - 8) <= DBL_EPSILON);
	ck_assert(fabs(q.Z - 16) <= DBL_EPSILON);

	MOL_QUATERNION_SUB(q, qa, qb);
	ck_assert(fabs(q.W - 2) <= DBL_EPSILON);
	ck_assert(fabs(q.X - 3) <= DBL_EPSILON);
	ck_assert(fabs(q.Y + 2) <= DBL_EPSILON);
	ck_assert(fabs(q.Z - 2) <= DBL_EPSILON);

	MOL_QUATERNION_MULT_SCALAR(q, qa, 0.5);
	ck_assert(fabs(q.W - 1.5) <= DBL_EPSILON);
	ck_assert(fabs(q.X - 3.0) <= DBL_EPSILON);
	ck_assert(fabs(q.Y - 1.5) <= DBL_EPSILON);
	ck_assert(fabs(q.Z - 4.5) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_normalize)
{
	struct mol_quaternion q = {1.0, 1.0, 1.0, 1.0};

	struct mol_quaternion q_norm = mol_quaternion_normalize(q);
	ck_assert(fabs(q_norm.W - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.X - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.Y - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.Z - 0.5) <= DBL_EPSILON);

	q = (struct mol_quaternion) {1.0, -1.0, -1.0, 1.0};
	q_norm = mol_quaternion_normalize(q);

	ck_assert(fabs(q_norm.W - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.X - (-0.5)) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.Y - (-0.5)) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.Z - 0.5) <= DBL_EPSILON);

	q = (struct mol_quaternion) {-4.0, 0.0, 0.0, -3.0};
	q_norm = mol_quaternion_normalize(q);

	ck_assert(fabs(q_norm.W - (-0.8)) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.X - 0.0) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.Y - 0.0) <= DBL_EPSILON);
	ck_assert(fabs(q_norm.Z - (-0.6)) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_length)
{
	struct mol_quaternion q = {1.0, 1.0, 1.0, 1.0};
	double len = mol_quaternion_length(q);
	ck_assert(fabs(len - 2.0) <= DBL_EPSILON);

	q = (struct mol_quaternion) {-3.0, 4.0, -12.0, 0.0};
	len = mol_quaternion_length(q);
	ck_assert(fabs(len - 13.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_length_sq)
{
	struct mol_quaternion q = {1.0, 1.0, 1.0, 1.0};
	double len_sq = mol_quaternion_length_sq(q);
	ck_assert(fabs(len_sq - 4.0) <= DBL_EPSILON);

	q = (struct mol_quaternion) {-3.0, 4.0, -12.0, 0.0};
	len_sq = mol_quaternion_length_sq(q);
	ck_assert(fabs(len_sq - 169.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_read)
{
	struct mol_quaternion_list *rots = mol_quaternion_list_from_file_unnormalized("base.qua");

	ck_assert(rots != NULL);
	ck_assert_int_eq(rots->size, 72);
	//0.236268	0.881766	-0.204124	0.353553
	ck_assert(fabs(rots->members[2].W - 0.236268) <= DBL_EPSILON);
	ck_assert(fabs(rots->members[2].X - 0.881766) <= DBL_EPSILON);
	ck_assert(fabs(rots->members[2].Y - (-0.204124)) <= DBL_EPSILON);
	ck_assert(fabs(rots->members[2].Z - 0.353553) <= DBL_EPSILON);
	mol_list_free(rots);
}
END_TEST


START_TEST(test_read_broken)
{
	struct mol_quaternion_list *rots = mol_quaternion_list_from_file("base_broken.qua");
	ck_assert(rots == NULL);
}
END_TEST


static bool _quaternion_approx_equal(const struct mol_quaternion qa, const struct mol_quaternion qb)
{
	return (
		((fabs(qa.W - qb.W) <= 2.0 * DBL_EPSILON) &&
		 (fabs(qa.X - qb.X) <= 2.0 * DBL_EPSILON) &&
		 (fabs(qa.Y - qb.Y) <= 2.0 * DBL_EPSILON) &&
		 (fabs(qa.Z - qb.Z) <= 2.0 * DBL_EPSILON))
		|| //quaternion * -1 = quaternion
		((fabs(qa.W + qb.W) <= 2.0 * DBL_EPSILON) &&
		 (fabs(qa.X + qb.X) <= 2.0 * DBL_EPSILON) &&
		 (fabs(qa.Y + qb.Y) <= 2.0 * DBL_EPSILON) &&
		 (fabs(qa.Z + qb.Z) <= 2.0 * DBL_EPSILON))
	);
}


START_TEST(test_roundtrip_identity)
{
	struct mol_quaternion q = {1.0, 0.0, 0.0, 0.0};
	struct mol_matrix3 m;
	mol_matrix3_from_quaternion(&m, q);
	struct mol_quaternion q_restored;
	mol_quaternion_from_matrix3(&q_restored, m);
	ck_assert(_quaternion_approx_equal(q, q_restored));
}
END_TEST


START_TEST(test_roundtrip)
{
	struct mol_quaternion_list *rots = mol_quaternion_list_from_file("base.qua");
	ck_assert_int_eq(rots->size, 72);

	for (size_t i = 0; i < rots->size; i++) {
		struct mol_matrix3 m;
		mol_matrix3_from_quaternion(&m, rots->members[i]);
		struct mol_quaternion q;
		mol_quaternion_from_matrix3(&q, m);
		ck_assert(_quaternion_approx_equal(rots->members[i], q));
	}

	mol_list_free(rots);
}
END_TEST


START_TEST(test_from_axis_angle)
{
	const struct mol_vector3 axis = {.X = 1 / sqrt(14), .Y = 2 / sqrt(14), .Z = 3 / sqrt(14)};
	const struct mol_quaternion q0 = {.W = 1, .X = 0, .Y = 0, .Z = 0};
	const struct mol_quaternion q60 = {.W = sqrt(3) / 2, .X = 0.5 / sqrt(14), .Y = 1 / sqrt(14), .Z = 1.5 / sqrt(14)};
	const struct mol_quaternion q60i = {.W = sqrt(3) / 2, .X = -0.5 / sqrt(14), .Y = -1 / sqrt(14), .Z = -1.5 / sqrt(14)};
	const struct mol_quaternion q420 = {.W = sqrt(3) / 2, .X = 0.5 / sqrt(14), .Y = 1 / sqrt(14), .Z = 1.5 / sqrt(14)};

	struct mol_quaternion q;

	mol_quaternion_from_axis_angle(&q, &axis, 0);
	ck_assert(_quaternion_approx_equal(q, q0));

	mol_quaternion_from_axis_angle(&q, &axis, M_PI / 3);
	ck_assert(_quaternion_approx_equal(q, q60));

	mol_quaternion_from_axis_angle(&q, &axis, -M_PI / 3);
	ck_assert(_quaternion_approx_equal(q, q60i));

	// This one is tricky, as there are actually more than one quaternion for each rotation
	mol_quaternion_from_axis_angle(&q, &axis, 7 * M_PI / 3);
	if (q.W < 0) {
		q = mol_quaternion_scale(q, -1);
	}
	ck_assert(_quaternion_approx_equal(q, q420));
}
END_TEST


START_TEST(test_to_axis_angle)
{
	const struct mol_quaternion q0 = {.W = 1, .X = 0, .Y = 0, .Z = 0};
	const struct mol_quaternion q60 = {.W = sqrt(3) / 2, .X = 0.5 / sqrt(14), .Y = 1 / sqrt(14), .Z = 1.5 / sqrt(14)};
	const struct mol_quaternion q60i = {.W = -sqrt(3) / 2, .X = -0.5 / sqrt(14), .Y = -1 / sqrt(14), .Z = -1.5 / sqrt(14)};
	const double eps = 2 * DBL_EPSILON; // Same as _quaternion_approx_equal uses
	struct mol_vector3 v;
	double a;

	// For this case, we don't have a well-defined rotation axis, so we just check that it has length of 1.
	mol_quaternion_to_axis_angle(&v, &a, &q0);
	ck_assert(fabs(a - 0) < eps);
	double len = MOL_VEC_SQ_NORM(v);
	ck_assert(fabs(len - 1) < eps);

	mol_quaternion_to_axis_angle(&v, &a, &q60);
	ck_assert(fabs(a - M_PI / 3) < eps);
	ck_assert(fabs(v.X - 1 / sqrt(14)) < eps);
	ck_assert(fabs(v.Y - 2 / sqrt(14)) < eps);
	ck_assert(fabs(v.Z - 3 / sqrt(14)) < eps);

	mol_quaternion_to_axis_angle(&v, &a, &q60i);
	// The rotation is the same, but we use the opposing axis in quaternion. Therefore, the opposing axis will be returned.
	ck_assert(fabs(a + M_PI / 3) < eps);
	ck_assert(fabs(v.X + 1 / sqrt(14)) < eps);
	ck_assert(fabs(v.Y + 2 / sqrt(14)) < eps);
	ck_assert(fabs(v.Z + 3 / sqrt(14)) < eps);
}
END_TEST


START_TEST(test_scale)
{
	struct mol_quaternion q = {3.0, 6.0, 3.0, 9.0};
	double a = 4.0;

	struct mol_quaternion q_scaled = mol_quaternion_scale(q, a);

	ck_assert(fabs(q_scaled.W - 12) <= DBL_EPSILON);
	ck_assert(fabs(q_scaled.X - 24) <= DBL_EPSILON);
	ck_assert(fabs(q_scaled.Y - 12) <= DBL_EPSILON);
	ck_assert(fabs(q_scaled.Z - 36) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_prod)
{
	struct mol_quaternion qa = {3.0, 6.0, 3.0, 9.0};
	struct mol_quaternion qb = {1.0, 2.0, 3.0, 4.0};

	struct mol_quaternion q_prod = mol_quaternion_prod(qa, qb);

	ck_assert(fabs(q_prod.W - (-54.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_prod.X - (-3.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_prod.Y - 6.0) <= DBL_EPSILON);
	ck_assert(fabs(q_prod.Z - 33.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_conj)
{
	struct mol_quaternion q = {3.0, -6.0, -3.0, -9.0};

	struct mol_quaternion q_conj = mol_quaternion_conj(q);

	ck_assert(fabs(q_conj.W - 3.0) <= DBL_EPSILON);
	ck_assert(fabs(q_conj.X - 6.0) <= DBL_EPSILON);
	ck_assert(fabs(q_conj.Y - 3.0) <= DBL_EPSILON);
	ck_assert(fabs(q_conj.Z - 9.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_inv)
{
	struct mol_quaternion q = {3.0, 6.0, 3.0, 9.0};
	struct mol_quaternion q_inv = mol_quaternion_inv(q);

	// Conjugate outcome will be: 3.0, -6.0, -3.0, -9.0
	// Product outcome will be: 135.0, 0, 0 ,0
	// Inverse outcome SHOULD be: 3/135, -6/135. -3/135, -9/135

	ck_assert(fabs(q_inv.W - (3.0 / 135.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_inv.X - (-6.0 / 135.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_inv.Y - (-3.0 / 135.0)) <= DBL_EPSILON);
	ck_assert(fabs(q_inv.Z - (-9.0 / 135.0)) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_negtrace)
{
	// Matrix diagonals add up to a negative number
	// This does not have any specific physical meaning, but affects internal workings of the algorithm

	// m11 biggest, rotation by pi around (1, 0, 0)
	struct mol_quaternion q1 = {3.0, 6.0, 3.0, 9.0};
	struct mol_matrix3 trace_neg_mat = {1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -1.0};

	mol_quaternion_from_matrix3(&q1, trace_neg_mat);

	ck_assert(fabs(q1.W) <= DBL_EPSILON);
	ck_assert(fabs(q1.X - 1.0) <= DBL_EPSILON);
	ck_assert(fabs(q1.Y) <= DBL_EPSILON);
	ck_assert(fabs(q1.Z) <= DBL_EPSILON);

	// m22 biggest, rotation by pi around (0, 1, 0)
	struct mol_quaternion q2 = {3.0, 6.0, 3.0, 9.0};
	struct mol_matrix3 trace_neg_mat2 = {-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0};

	mol_quaternion_from_matrix3(&q2, trace_neg_mat2);

	ck_assert(fabs(q2.W) <= DBL_EPSILON);
	ck_assert(fabs(q2.X) <= DBL_EPSILON);
	ck_assert(fabs(q2.Y - 1.0) <= DBL_EPSILON);
	ck_assert(fabs(q2.Z) <= DBL_EPSILON);

	// m33 biggest, rotation by pi around (0, 0, 1)
	struct mol_quaternion q3 = {3.0, 6.0, 3.0, 9.0};
	struct mol_matrix3 trace_neg_mat3 = {-1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0};

	mol_quaternion_from_matrix3(&q3, trace_neg_mat3);

	ck_assert(fabs(q3.W) <= 2*DBL_EPSILON);
	ck_assert(fabs(q3.X) <= 2*DBL_EPSILON);
	ck_assert(fabs(q3.Y) <= 2*DBL_EPSILON);
	ck_assert(fabs(q3.Z - 1.0) <= 2*DBL_EPSILON);
}
END_TEST


Suite *quaternion_suite(void)
{
	Suite *suite = suite_create("quaternion");

	TCase *tcase_quaternion = tcase_create("test_quaternion");
	tcase_add_test(tcase_quaternion, test_structs);
	tcase_add_test(tcase_quaternion, test_macros);
	tcase_add_test(tcase_quaternion, test_normalize);
	tcase_add_test(tcase_quaternion, test_length);
	tcase_add_test(tcase_quaternion, test_length_sq);
	tcase_add_test(tcase_quaternion, test_scale);
	tcase_add_test(tcase_quaternion, test_prod);
	tcase_add_test(tcase_quaternion, test_conj);
	tcase_add_test(tcase_quaternion, test_inv);
	tcase_add_test(tcase_quaternion, test_from_axis_angle);
	tcase_add_test(tcase_quaternion, test_to_axis_angle);

	TCase *tcase_reading = tcase_create("test_quaternion_reading");
	tcase_add_test(tcase_reading, test_read);
	tcase_add_test(tcase_reading, test_read_broken);

	TCase *tcase_roundtrip = tcase_create("test_quaternion_roundtrip");
	tcase_add_test(tcase_roundtrip, test_roundtrip_identity);
	tcase_add_test(tcase_roundtrip, test_roundtrip);

	TCase *tcase_negtrace = tcase_create("test_quaternion_negtrace");
	tcase_add_test(tcase_negtrace, test_negtrace);

	suite_add_tcase(suite, tcase_quaternion);
	suite_add_tcase(suite, tcase_reading);
	suite_add_tcase(suite, tcase_roundtrip);
	suite_add_tcase(suite, tcase_negtrace);

	return suite;
}

int main(void)
{
	Suite *suite = quaternion_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
