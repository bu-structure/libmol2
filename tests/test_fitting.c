#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "fitting.h"
#include "atom_group.h"
#include "json.h"
#include "pdb.h"
#include "vector.h"

struct mol_atom_group *test_ref_lig1, *test_ref_lig2, *test_mob_lig, *test_1atom1, *test_1atom2, *test_2atoms;
struct mol_atom_group **test_ref_lig_list, **test_ref_2atoms;

const double tolerance_strict = 1e-9;

#ifndef ck_assert_double_eq_tol
#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < (tol))
#endif

#ifndef ck_assert_double_eq_def
#define ck_assert_double_eq_def(val, ref) \
    ck_assert_double_eq_tol(val, ref, tolerance_strict)
#endif

static void test_fitting_gradients(
	struct mol_atom_group *mob_ag,
	const struct mol_atom_group *ref_ag,
	const struct mol_fitting_params prms,
	const double scale,
	const double delta,
	const double tol)
{
	struct mol_vector3 *num_grad = calloc(mob_ag->natoms, sizeof(struct mol_vector3));

	mol_zero_gradients(mob_ag);
	const double score_orig = mol_fitting_score(mob_ag, ref_ag, &prms, scale);

	struct mol_vector3 *old_grad_ptr = mob_ag->gradients;
	mob_ag->gradients = NULL;

	double score, oldcrd;
	for (size_t i = 0; i < mob_ag->natoms; i++) {
		oldcrd = mob_ag->coords[i].X;
		mob_ag->coords[i].X = oldcrd + delta;
		score = mol_fitting_score(mob_ag, ref_ag, &prms, scale);
		num_grad[i].X = (score_orig - score) / delta;
		mob_ag->coords[i].X = oldcrd;

		oldcrd = mob_ag->coords[i].Y;
		mob_ag->coords[i].Y = oldcrd + delta;
		score = mol_fitting_score(mob_ag, ref_ag, &prms, scale);
		num_grad[i].Y = (score_orig - score) / delta;
		mob_ag->coords[i].Y = oldcrd;

		oldcrd = mob_ag->coords[i].Z;
		mob_ag->coords[i].Z = oldcrd + delta;
		score = mol_fitting_score(mob_ag, ref_ag, &prms, scale);
		num_grad[i].Z = (score_orig - score) / delta;
		mob_ag->coords[i].Z = oldcrd;
	}

	mob_ag->gradients = old_grad_ptr;

	char msg[256];
	for (size_t i = 0; i < mob_ag->natoms; i++) {
		sprintf(msg,
		        "\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        i,
		        mob_ag->gradients[i].X, mob_ag->gradients[i].Y, mob_ag->gradients[i].Z,
		        num_grad[i].X, num_grad[i].Y, num_grad[i].Z);

		ck_assert_msg(fabs(mob_ag->gradients[i].X - num_grad[i].X) < tol, "%s", msg);
		ck_assert_msg(fabs(mob_ag->gradients[i].Y - num_grad[i].Y) < tol, "%s", msg);
		ck_assert_msg(fabs(mob_ag->gradients[i].Z - num_grad[i].Z) < tol, "%s", msg);
	}
	free(num_grad);
}

static void test_fitting_list_gradients(
	struct mol_atom_group *mob_ag,
	struct mol_atom_group **ref_ag_list,
	struct mol_fitting_params prms,
	const size_t size,
	const double stride,
	const double tol)
{
	struct mol_vector3 *num_grad = calloc(mob_ag->natoms, sizeof(struct mol_vector3));

	mol_zero_gradients(mob_ag);
	const double score_orig = mol_fitting_score_aglist(mob_ag, ref_ag_list, size, &prms, 1.0);

	struct mol_vector3 *old_grad_ptr = mob_ag->gradients;
	mob_ag->gradients = NULL;

	double score, oldcrd;
	for (size_t i = 0; i < mob_ag->natoms; i++) {
		oldcrd = mob_ag->coords[i].X;
		mob_ag->coords[i].X = oldcrd + stride;
		score = mol_fitting_score_aglist(mob_ag, ref_ag_list, size, &prms, 1.0);
		num_grad[i].X = (score_orig - score) / stride;
		mob_ag->coords[i].X = oldcrd;

		oldcrd = mob_ag->coords[i].Y;
		mob_ag->coords[i].Y = oldcrd + stride;
		score = mol_fitting_score_aglist(mob_ag, ref_ag_list, size, &prms, 1.0);
		num_grad[i].Y = (score_orig - score) / stride;
		mob_ag->coords[i].Y = oldcrd;

		oldcrd = mob_ag->coords[i].Z;
		mob_ag->coords[i].Z = oldcrd + stride;
		score = mol_fitting_score_aglist(mob_ag, ref_ag_list, size, &prms, 1.0);
		num_grad[i].Z = (score_orig - score) / stride;
		mob_ag->coords[i].Z = oldcrd;
	}

	mob_ag->gradients = old_grad_ptr;

	char msg[256];
	for (size_t i = 0; i < mob_ag->natoms; i++) {
		sprintf(msg,
		        "\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
		        i,
		        mob_ag->gradients[i].X, mob_ag->gradients[i].Y, mob_ag->gradients[i].Z,
		        num_grad[i].X, num_grad[i].Y, num_grad[i].Z);

		ck_assert_msg(fabs(mob_ag->gradients[i].X - num_grad[i].X) < tol, "%s", msg);
		ck_assert_msg(fabs(mob_ag->gradients[i].Y - num_grad[i].Y) < tol, "%s", msg);
		ck_assert_msg(fabs(mob_ag->gradients[i].Z - num_grad[i].Z) < tol, "%s", msg);
	}
	free(num_grad);
}

START_TEST(test_fitting_fit_atom_group)
{
	// Score real molecule against a translated copy of itself
	struct mol_fitting_params prms = {.radius = 2.0, .mask = NULL, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score(test_mob_lig, test_ref_lig1, &prms, 1.0),
	                        -34.083017347);

	mol_zero_gradients(test_mob_lig);
	test_fitting_gradients(test_mob_lig, test_ref_lig1, prms, 1.0, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_to_list)
{
	// Score real molecule against a set of two real molecules (actually, two copies of the same molecule, but translated)
	struct mol_fitting_params prms = {.radius = 2.0, .mask = NULL, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score_aglist(test_mob_lig,
	                                                 test_ref_lig_list,
	                                                 2, &prms, 1.0), -63.063680134);

	mol_zero_gradients(test_mob_lig);
	test_fitting_list_gradients(test_mob_lig, test_ref_lig_list, prms, 2, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_1atom_self)
{
	// Score single atom against itself. Should be -exp(0) = -1
	struct mol_fitting_params prms = {.radius = 2.0, .mask = NULL, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score(test_1atom1, test_1atom1, &prms, 1.0), -1.0);

	mol_zero_gradients(test_1atom1);
	test_fitting_gradients(test_1atom1, test_1atom1, prms, 1.0, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_1atom_change_radius)
{
	// Score single atom against another atom. Should be -exp(-2.312742 / 3^2)
	struct mol_fitting_params prms = {.radius = 3.0, .mask = NULL, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score(test_1atom1, test_1atom2, &prms, 1.0), -0.773390384);

	mol_zero_gradients(test_1atom1);
	test_fitting_gradients(test_1atom1, test_1atom2, prms, 1.0, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_1atom_change_scale)
{
	// Score single atom against another atom. Should be -2*exp(-2.312742 / 2^2)
	struct mol_fitting_params prms = {.radius = 2.0, .mask = NULL, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score(test_1atom1, test_1atom2, &prms, 2.0), -1.121830449);

	mol_zero_gradients(test_1atom2);
	test_fitting_gradients(test_1atom1, test_1atom2, prms, 2.0, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_1atom_on_2atoms)
{
	// Score single atom against itself and another atom. Should be -exp(0) -exp(-2.312742 / 2^2)
	struct mol_fitting_params prms = {.radius = 2.0, .mask = NULL, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score_aglist(test_1atom1, test_ref_2atoms, 2, &prms, 1.0),
	                        -1.560915224);

	mol_zero_gradients(test_1atom1);
	test_fitting_list_gradients(test_1atom1, test_ref_2atoms, prms, 2, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_1atom_on_2atoms_normalized)
{
	// Score single atom against itself and another atom normalized. Should be (-exp(0) -exp(-2.312742 / 2^2)) / 2
	struct mol_fitting_params prms = {.radius = 2.0, .mask = NULL, .normalize = true};
	ck_assert_double_eq_def(mol_fitting_score(test_1atom1, test_2atoms, &prms, 1.0),
	                        -1.560915224 / 2);

	mol_zero_gradients(test_1atom1);
	test_fitting_gradients(test_1atom1, test_2atoms, prms, 1, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_1atom1_on_2atoms_masked)
{
	// Score two atoms against one atom (the second atom is masked). Should be -exp(0) = -1
	bool mask[2] = { true, false };
	struct mol_fitting_params prms = {.radius = 2.0, .mask = mask, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score(test_2atoms, test_1atom1, &prms, 1.0), -1.0);

	mol_zero_gradients(test_2atoms);
	test_fitting_gradients(test_2atoms, test_1atom1, prms, 1.0, 1e-6, 1e-4);
}
END_TEST

START_TEST(test_fitting_fit_atom_group_1atom2_on_2atoms_masked)
{
	// Score two atoms against one atom (the first atom is masked). Should be -exp(0) = -1
	bool mask[2] = { false, true };
	struct mol_fitting_params prms = {.radius = 2.0, .mask = mask, .normalize = false};
	ck_assert_double_eq_def(mol_fitting_score(test_2atoms, test_1atom2, &prms, 1.0), -1.0);

	mol_zero_gradients(test_2atoms);
	test_fitting_gradients(test_2atoms, test_1atom2, prms, 1.0, 1e-6, 1e-4);
}
END_TEST

void setup_real(void)
{
	test_ref_lig1 = mol_read_pdb("fitting_ref.pdb");
	test_ref_lig2 = mol_read_pdb("fitting_ref.pdb");
	test_1atom1 = mol_read_pdb("fitting_1atom1.pdb");
	test_1atom2 = mol_read_pdb("fitting_1atom2.pdb");
	test_2atoms = mol_atom_group_join(test_1atom1, test_1atom2);

	// Translate fitting_ref.pdb to get a different atom group
	const struct mol_vector3 tr = {1., 2., 0.};
	mol_atom_group_translate(test_ref_lig2, &tr);

	test_ref_lig_list = calloc(2, sizeof(struct mol_atom_group *));
	test_ref_lig_list[0] = test_ref_lig1;
	test_ref_lig_list[1] = test_ref_lig2;

	test_ref_2atoms = calloc(2, sizeof(struct mol_atom_group *));
	test_ref_2atoms[0] = test_1atom1;
	test_ref_2atoms[1] = test_1atom2;

	test_mob_lig = mol_read_json("fitting_mob.json");
	test_1atom1->gradients = calloc(test_1atom1->natoms, sizeof(struct mol_vector3));
	test_1atom2->gradients = calloc(test_1atom2->natoms, sizeof(struct mol_vector3));
	test_2atoms->gradients = calloc(test_2atoms->natoms, sizeof(struct mol_vector3));
}

void teardown_real(void)
{
	mol_atom_group_free(test_ref_lig1);
	mol_atom_group_free(test_ref_lig2);
	mol_atom_group_free(test_mob_lig);
	mol_atom_group_free(test_1atom1);
	mol_atom_group_free(test_1atom2);
	mol_atom_group_free(test_2atoms);

	free(test_ref_lig_list);
	free(test_ref_2atoms);
}

Suite *lists_suite(void)
{
	Suite *suite = suite_create("fitting");

	TCase *tcase_real = tcase_create("real");
	tcase_add_checked_fixture(tcase_real, setup_real, teardown_real);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_to_list);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_1atom_self);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_1atom_change_radius);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_1atom_change_scale);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_1atom_on_2atoms);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_1atom_on_2atoms_normalized);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_1atom1_on_2atoms_masked);
	tcase_add_test(tcase_real, test_fitting_fit_atom_group_1atom2_on_2atoms_masked);
	suite_add_tcase(suite, tcase_real);

	return suite;
}

int main(void)
{
	Suite *suite = lists_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
