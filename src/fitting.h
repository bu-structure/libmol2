/*
 * Routines for shape matching between 2 atom groups.
 *
 * The fit score between pair of atoms takes the form of exp(-d_ij^2/rad^2), and the fit score between 2 atom groups
 * is a sum over i and j.
 */

#ifndef _MOL_FITTING_H
#define _MOL_FITTING_H

#include <math.h>
#include "atom_group.h"

/**
 * Shape fitting parameters.
 */
struct mol_fitting_params {
	double radius; ///< Gaussian standard deviation (Angstroms). Recommended value is 2.0
	bool *mask; ///< Which atoms of mobile atom group will be affected ("true" to include). If NULL, all atoms will be affected
	bool normalize; ///< If "true", divide the score by the number of used atom pairs (masked atoms are excluded)
};

/**
 * Compute the fit score of a mobile molecule to the reference molecule using the equation:
 *
 * -1. * Σ exp[-d_ij^2 / r^2]
 *
 * where d_ij is distance between atom i in mobile group and j in the reference group
 * and r is the Gaussian standard deviation.
 *
 * If gradient is not NULL, computes the gradient for mobile atom group and adds it to the existing gradient.
 *
 * \param mob_ag Mobile atom group.
 * \param ref_ag Reference atom group.
 * \param prms Fitting parameters.
 * \param scale Scale the final score and gradient by this factor (final_score = score * scale).
 * \return Fitting score. Lower values correspond to better overlap.
 */
double mol_fitting_score(struct mol_atom_group *mob_ag,
                         const struct mol_atom_group *ref_ag,
                         const struct mol_fitting_params* prms,
                         const double scale);

/**
 * Uses \ref mol_fitting_fit_atom_group to compute fit score of a mobile atom group to the array of atom groups by
 * accumulating separate fit scores and gradients.
 *
 * Note: Although ref_ag_list doesn't have a const qualifier (due to C rules), it does not change within the function
 *
 * \param mob_ag Mobile atom group atom group.
 * \param ref_ag_list Reference atom group list.
 * \param size Reference list size.
 * \param prms Fitting parameters.
 * \param scale Scale the final score and gradient by this factor (final_score = score * scale).
 * \return Fitting score. Lower values correspond to better overlap.
 */
double mol_fitting_score_aglist(struct mol_atom_group *mob_ag,
                                struct mol_atom_group **ref_ag_list,
                                const size_t size,
                                const struct mol_fitting_params* prms,
                                const double scale);

#endif /* _MOL_FITTING_H */
