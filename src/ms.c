#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include "ms.h"

#include "vector.h"
#include "utils.h"

#include <stdio.h>
#ifndef _WIN32
#include <unistd.h>
#endif
#include <stdbool.h>
#include <string.h>
#include "private_utils.h"


static const char *ATOM_RECORD_FORMAT =
	"%6s%5d %4s%c%-4.4s%c%4hd%c   %8.3f%8.3f%8.3f%6.2f%6.2f\n";

static void parse_ms_atom_line(struct mol_atom_group *ag, const char *line,
                                const size_t index, const ssize_t line_length)
{
	/* MS modification to PDB format
	Record Format

	COLUMNS        DATA  TYPE    FIELD        DEFINITION
	-------------------------------------------------------------------------------------
	 1 -  6        Record name   "ATOM  "
	 7 - 11        Integer       serial       Atom  serial number.
	13 - 16        Atom          name         Atom name.
	17             Character     altLoc       Alternate location indicator.
	18 - 20        Residue name  resName      Residue name.
	21             Residue suffix             Libmol extension adding a suffix to residue name.
	22             Character     chainID      Chain identifier.
	23 - 26        Integer       resSeq       Residue sequence number.
	27             AChar         iCode        Code for insertion of residues.
	31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
	39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
	47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
	55 - 60        Real(6.2)     attraction   Attraction
	61 - 66        Real(6.2)     surface      Surface accessibility
	*/
	if (index >= ag->natoms) {
		_PRINTF_ERROR("Unable to read atom #%zu into atomgroup with %zu atoms", index, ag->natoms);
		return;
	}
	if (line_length < 54) { // We need at least x, y, z coordinates
		_PRINTF_ERROR("Line for atom #%zu is too short", index);
		return;
	}

	ag->atom_name[index] = strndup(line+12, 4);
	ag->alternate_location[index] = line[16];

	ag->residue_name[index] = strndup(line+17, 4);
	if (ag->residue_name[index][3] == ' ') {
		ag->residue_name[index][3] = '\0';
	}

	sscanf(line+21, "%c%4hd%c",
		&ag->residue_id[index].chain,
		&ag->residue_id[index].residue_seq,
		&ag->residue_id[index].insertion);

	sscanf(line+30, "%8lf%8lf%8lf", &ag->coords[index].X, &ag->coords[index].Y,
		&ag->coords[index].Z);

	sscanf(line+54, "%6lf", &ag->attraction_level[index]);

	if (line_length >= 66) {
		double surface;
		sscanf(line+60, "%6lf", &surface);

		ag->surface[index] = surface > 0.0;
	}
}

static void initialize_ms_atom_group(struct mol_atom_group *ag, const size_t natoms)
{
	ag->natoms = natoms;
	ag->coords = malloc(sizeof(struct mol_vector3) * natoms);
	ag->atom_name = malloc(sizeof(char*) * natoms);
	ag->alternate_location = malloc(sizeof(char) * natoms);
	ag->residue_name = malloc(sizeof(char*) * natoms);
	ag->residue_id = malloc(sizeof(struct mol_residue_id) * natoms);
	ag->attraction_level = malloc(sizeof(double) * natoms);
	ag->surface = malloc(sizeof(bool) * natoms);
}

struct mol_atom_group *mol_read_ms(const char *ms_filename)
{
	FILE *fp = fopen(ms_filename, "r");

	if (fp == NULL) {
		perror(__func__);
		_PRINTF_ERROR("Unable to open %s", ms_filename);
		return NULL;
	}

	struct mol_atom_group *ag = mol_fread_ms(fp);

	fclose(fp);
	return ag;
}

struct mol_atom_group *mol_fread_ms(FILE *fp)
{
	struct mol_atom_group *ag = NULL;

	if (fp == NULL) {
		return NULL;
	}

	char *line = NULL;
	size_t len = 0;
	ssize_t read;

	size_t natoms = 0;

	while (getline(&line, &len, fp) != -1) {
		if ( (strncmp("ATOM  ", line, 6) == 0) ||
		     (strncmp("HETATM", line, 6) == 0)) {
			natoms++;
		}
	}

	if (natoms == 0) {
		_PRINTF_ERROR("No atoms found in file");
		free_if_not_null(line);
		return NULL;
	}

	rewind(fp);

	ag = mol_atom_group_create();
	initialize_ms_atom_group(ag, natoms);

	size_t i = 0;
	while ((read = getline(&line, &len, fp)) != -1) {
		if ( (strncmp("ATOM  ", line, 6) == 0) ||
		     (strncmp("HETATM", line, 6) == 0)) {
			parse_ms_atom_line(ag, line, i, read);
			i++;
		}
	}

	mol_atom_group_create_residue_hash(ag);
	mol_atom_group_create_residue_list(ag);

	free_if_not_null(line);
	return ag;
}

struct mol_atom_group_list *mol_read_ms_models(const char *ms_filename)
{
	struct mol_atom_group_list *list = NULL;

	FILE *fp = fopen(ms_filename, "r");

	if (fp == NULL) {
		perror(__func__);
		_PRINTF_ERROR("Unable to open %s", ms_filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len=0;
	ssize_t read;

	size_t nmodels = 0;

	while (getline(&line, &len, fp) != -1) {
		if (strncmp("MODEL ", line, 6) == 0) {
			nmodels++;
		}
	}

	if (nmodels == 0) {
		_PRINTF_ERROR("No models found in file %s", ms_filename);
		goto reading_out;
	}

	rewind(fp);

	size_t natoms = 0;

	while (getline(&line, &len, fp) != -1) {
		if ( (strncmp("ATOM  ", line, 6) == 0) ||
		     (strncmp("HETATM", line, 6) == 0)) {
			natoms++;
		}
		if (strncmp("ENDMDL", line, 6) == 0) {
			break;
		}
	}

	if (natoms == 0) {
		_PRINTF_ERROR("No atoms found in first model of %s", ms_filename);
		goto reading_out;
	}

	rewind(fp);

	list = mol_atom_group_list_create(nmodels);
	for (size_t i=0; i<nmodels; i++) {
		initialize_ms_atom_group(&(list->members[i]), natoms);
	}

	size_t atom_i = 0;
	size_t model_i = 0;
	while ((read = getline(&line, &len, fp)) != -1) {
		if ( (strncmp("ATOM  ", line, 6) == 0) ||
		     (strncmp("HETATM", line, 6) == 0)) {
			parse_ms_atom_line(&(list->members[model_i]), line, atom_i, read);
			atom_i++;
		}
		if (strncmp("ENDMDL", line, 6) == 0) {
			atom_i = 0;
			model_i++;
		}
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return list;

}

bool mol_write_ms(const char *ms_filename, const struct mol_atom_group *ag)
{
	if (ag->attraction_level == NULL && ag->surface == NULL) {
		_PRINTF_ERROR("Cannot write ms file without either attraction or surface accesibility");
		return false;
	}
	bool close_fp = false;
	FILE *ofp = stdout;
	if (strcmp(ms_filename, "-")) {
		close_fp = true;
		ofp = fopen(ms_filename, "w");
		if (ofp == NULL) {
			return false;
		}
	}

	mol_fwrite_ms(ofp, ag);

	if (close_fp) {
		fclose(ofp);
	}

	return true;
}

void mol_fwrite_ms(FILE *ofp, const struct mol_atom_group *ag)
{
	const char *atom_name;
	char alternate_location;
	const char *residue_name;
	char chain;
	int16_t residue_seq = 0;
	char insertion;
	struct mol_vector3 *coords;
	double attraction_level;

	for (size_t i = 0; i < ag->natoms; ++i) {
		atom_name = ag->atom_name == NULL ? "    " : ag->atom_name[i];
		alternate_location =
			ag->alternate_location == NULL ? ' ' : ag->alternate_location[i];
		residue_name =
			ag->residue_name == NULL ? "UNK" : ag->residue_name[i];
		if (ag->residue_id == NULL) {
			chain = 'A';
			residue_seq++;
			insertion = ' ';
		} else {
			chain = ag->residue_id[i].chain;
			residue_seq = ag->residue_id[i].residue_seq;
			insertion = ag->residue_id[i].insertion;
		}
		coords = &ag->coords[i]; // Assume there are always coords

		attraction_level = ag->attraction_level == NULL ?
			0.0 : ag->attraction_level[i];

		fprintf(ofp, ATOM_RECORD_FORMAT,
			"ATOM  ", i+1, atom_name, alternate_location,
			residue_name, chain, residue_seq, insertion,
			coords->X, coords->Y, coords->Z,
			attraction_level, (double) ag->surface[i]);
	}
}
