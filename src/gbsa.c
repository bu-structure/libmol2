#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#define _POSIX_C_SOURCE 200809L
#define _USE_MATH_DEFINES
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "gbsa.h"
#include "geometry.h"
#include "private_utils.h"

#define ACE_NONPOLAR 1		// 01
#define ACE_POLAR    2		// 10
#define ACE_ALL   (ACE_NONPOLAR | ACE_POLAR)	// 11

typedef uint_fast8_t ACE_ENERGY_TYPE;

//Init ACE
void ace_ini(struct mol_atom_group *ag, struct acesetup *ac_s)
{
	const double minr = 0.6;
	const double sqrtpi = sqrt(M_PI);
	const double c1 = 4.0 / (3.0 * M_PI);
	const double c2 = 77.0 * M_PI * M_SQRT2 / 512.0;
	const double c3 = 2.0 * M_PI * sqrtpi;
	const double pi2 = M_1_PI * M_1_PI;
	const double width = 1.2;
	double ri, ri2, rk, vk, rk2, alpha, alpha2, alpha4, prod2, prod4, ratio,
		tik2, qik, qterm, temp, fik, omik, s2ik, s3ik, uik, uik2, omgik;
	int nbsize;

	if (ag->ace_volume == NULL) {
		_PRINTF_ERROR("Unable to initialize ACE potential: no atom volumes. Does your PRM file contain VOLUME section?");
		exit(EXIT_FAILURE);
	}

	ac_s->ntypes = ag->num_atom_types + 1;

	// ACE code assumes atom types #1 and #2 are "H" and "HC", respectively.
	// This is not a hard requirement, but that's how the code is written now.
	// Hopefully, we'll have a GB-OBC implementation soon, so this one is not worth fixing, but worth adding guardrails.
	if (ac_s->ntypes < 3) {
		_PRINTF_ERROR("Unable to initialize ACE potential: must have at least two atom types."); // because ac_s->ntypes = ag->num_atom_types + 1;
		exit(EXIT_FAILURE);
	}
	for (size_t i = 0; i < ag->natoms; i++) {
		if (ag->ftypen[i] == 1) {
			if (strcmp("H", ag->ftype_name[i]) != 0) {
				_PRINTF_ERROR("Unable to initialize ACE potential: atom type #1 should be \"H\".");
				exit(EXIT_FAILURE);
			}
		}
		if (ag->ftypen[i] == 2) {
			if (strcmp("HC", ag->ftype_name[i]) != 0) {
				_PRINTF_ERROR("Unable to initialize ACE potential: atom type #2 should be \"HC\".");
				exit(EXIT_FAILURE);
			}
		}
	}

	ac_s->rsolv = malloc(sizeof(double) * ac_s->ntypes);
	ac_s->vsolv = malloc(sizeof(double) * ac_s->ntypes);
	ac_s->lwace = malloc(sizeof(double) * ac_s->ntypes);
	ac_s->s2ace = malloc(sizeof(double) * ac_s->ntypes * ac_s->ntypes);
	ac_s->u4ace = malloc(sizeof(double) * ac_s->ntypes * ac_s->ntypes);
	ac_s->wace = malloc(sizeof(double) * ac_s->ntypes * ac_s->ntypes);
	ac_s->hydr = malloc(sizeof(double) * ac_s->ntypes);
	for (size_t i = 0; i < ac_s->ntypes; i++) {
		ac_s->vsolv[i] = -1.0;
		ac_s->rsolv[i] = 0.0;
	}

	struct mol_bond *b;
	for (size_t i = 0; i < ag->natoms; i++) {
		if (ac_s->vsolv[ag->ftypen[i]] <= 0.0) {
			ac_s->vsolv[ag->ftypen[i]] = ag->ace_volume[i];
			ac_s->rsolv[ag->ftypen[i]] =
				fmax(ag->vdw_radius[i], minr);

			//Hydrogen correction
			if (strcmp("H", ag->ftype_name[i]) == 0) {
				ac_s->rsolv[ag->ftypen[i]] =
					fmax(0.85, ac_s->rsolv[ag->ftypen[i]]);
			} else if (strcmp("HC", ag->ftype_name[i]) == 0) {
				ac_s->rsolv[ag->ftypen[i]] =
					fmax(0.83, ac_s->rsolv[ag->ftypen[i]]);
			}

			for (size_t j = 0; j < ag->nbonds; j++) {
				b = &ag->bonds[j];

				if (ag->ftypen[b->ai] == ag->ftypen[i]) {
					ac_s->rsolv[ag->ftypen[i]] =
						fmax(ac_s->rsolv[ag->ftypen[i]],
							ag->vdw_radius[b->aj] - b->l0);
				} else if (ag->ftypen[b->aj] == ag->ftypen[i]) {
					ac_s->rsolv[ag->ftypen[i]] =
						fmax(ac_s->rsolv[ag->ftypen[i]],
							ag->vdw_radius[b->ai] - b->l0);
				}
			}
		}
	}

	//Hydrogen correction
	if (ac_s->rsolv[1] < 0.85)
		ac_s->rsolv[1] = 0.85;
	if (ac_s->rsolv[2] < 0.83)
		ac_s->rsolv[2] = 0.83;

	for (size_t i = 0; i < ac_s->ntypes; i++) {
		ac_s->hydr[i] =
			-4 * M_PI * 0.003 * ac_s->rsolv[i] * (ac_s->rsolv[i] +
							1.4) *
			(ac_s->rsolv[i] + 1.4);
		ri = ac_s->rsolv[i];
		ri2 = ri * ri;
		if (ri > 0) {
			for (size_t k = 0; k < ac_s->ntypes; k++) {
				rk = ac_s->rsolv[k];
				vk = ac_s->vsolv[k];
				if (rk > 0) {
					rk2 = rk * rk;
					alpha = fmax(width, ri / rk);
					alpha2 = alpha * alpha;
					alpha4 = alpha2 * alpha2;
					prod2 = alpha2 * rk2;
					prod4 = prod2 * prod2;
					ratio = alpha2 * rk2 / ri2;
					tik2 = 0.5 * M_PI * ratio;
					temp = 1.0 / (1.0 + 2.0 * tik2);
					fik = 2.0 / (1.0 + tik2) - temp;
					qik = tik2 * sqrt(temp);
					qterm = qik - atan(qik);
					if (k == i) {
						omik = -((vk * pi2) - c1*(ri2 * ri)) *
							qterm/(alpha4 * ri2 * ri2);
						ac_s->lwace[i] = omik;
					}
					if (vk > 1.0e-8) {
						omgik = vk * qterm * pi2 / prod4;
						s2ik = 3.0 * qterm * prod2 /
							((3.0 + fik) * qik - 4.0 * atan(qik));
						s3ik = s2ik * sqrt(s2ik);
						uik = c2 * ri / (1.0 - (c3 * s3ik * ri * omgik / vk));
						ac_s->wace[i * ac_s->ntypes + k] = omgik;
						ac_s->s2ace[i * ac_s->ntypes + k] = s2ik;
						uik2 = uik * uik;
						ac_s->u4ace[i * ac_s->ntypes + k] = uik2 * uik2;
					} else {
						ac_s->wace[i * ac_s->ntypes + k] = 0.0;
						ac_s->s2ace[i * ac_s->ntypes + k] = 1.0e-6;
						ac_s->u4ace[i * ac_s->ntypes + k] = 1.0e+6;
					}
				}
			}
		}
	}

	ac_s->nbsize = 1;
	nbsize = 1;
	ac_s->eself = calloc(ag->natoms, sizeof(double));
	ac_s->rborn = calloc(ag->natoms, sizeof(double));
	ac_s->swarr = calloc(nbsize, sizeof(double));
	ac_s->dswarr = calloc(nbsize, sizeof(double));
	ac_s->darr = calloc(nbsize, sizeof(double));

	//d(rb)/d(eself)
	ac_s->dbrdes = calloc(ag->natoms, sizeof(double));
	ac_s->vsf = calloc(2 * nbsize, sizeof(struct mol_vector3));
	ac_s->vf = calloc(ag->natoms, sizeof(struct mol_vector3));
	ac_s->diarr = calloc(ag->natoms, sizeof(double));
	ac_s->list0123 = calloc(1, sizeof(int));
}

static int *compute_0123_list(struct mol_atom_group *ag, int *n0123,
			int *list03, int n03, int *list02, int n02,
			int *na01, int **pna01)
{
	int n2, ind;
	int *p;
	*n0123 = (ag->nbonds + n03 + n02);
	int *list0123 = malloc(*n0123 * 2 * sizeof(int));
	ind = 0;
	//list03 goes first
	for (int i = 0; i < n03; i++) {
		list0123[ind++] = list03[2 * i];
		list0123[ind++] = list03[2 * i + 1];
	}
	for (size_t i = 0; i < ag->natoms; i++) {
		n2 = na01[i];
		p = pna01[i];
		for (int j = 0; j < n2; j++) {
			if (p[j] > (int) i) {
				list0123[ind++] = i;
				list0123[ind++] = p[j];
			}
		}
	}
	for (int i = 0; i < n02; i++) {
		list0123[ind++] = list02[2 * i];
		list0123[ind++] = list02[2 * i + 1];
	}

	return list0123;
}

static inline double _pow4(const double x) {
	const double x2 = x * x;
	return x2 * x2;
}

static void ace_eselfupdate(const int i1, const int i2, const int it,
			const int kt, const int ij, const struct mol_vector3 dv,
			const struct acesetup *const ac_s,
			const double rul3,
			const double rul12, const double nb2cot,
			const double nb2cof, const int nbsize)
{
	double sw, dsw, expterm, temp, rmu, term, rl, ru, u4ace, ffk;
	const double r2 = MOL_VEC_SQ_NORM(dv);
	ac_s->darr[ij] = -1;

	if (r2 < nb2cof) {
		sw = 1.0;
		dsw = 0;
		if (r2 > nb2cot) {
			rl = (nb2cot) - r2;
			ru = (nb2cof) - r2;
			sw = ru * ru * (ru - 3 * rl) * rul3;
			dsw = rl * ru * rul12;
		}

		ac_s->swarr[ij] = sw;
		ac_s->dswarr[ij] = dsw;
		const double r = sqrt(r2);
		ac_s->darr[ij] = r;
		const double r3 = r2 * r;
		const double r4 = r2 * r2;
		if (ac_s->vsolv[kt] > 0) {
			expterm =
				ac_s->wace[it * ac_s->ntypes + kt] *
				exp(-r2 / ac_s->s2ace[it * ac_s->ntypes + kt]);
			u4ace = ac_s->u4ace[it * ac_s->ntypes + kt];
			rmu = r4 + u4ace;
			term = (ac_s->vsolv[kt] / (8.0 * M_PI)) * _pow4(r3 / rmu);

			temp = 2.0 * (expterm + term);
			ac_s->eself[i1] -= temp * sw;
			ffk = ((-8 * term * (3 * u4ace - r4) / (r2 * rmu)) +
				(4 * expterm / ac_s->s2ace[it * ac_s->ntypes + kt])) * sw -
				temp * dsw;

			MOL_VEC_MULT_SCALAR(ac_s->vsf[ij], dv, ffk);

			MOL_VEC_SUB(ac_s->vf[i1], ac_s->vf[i1], ac_s->vsf[ij]);
		} else {
			MOL_VEC_SET_SCALAR(ac_s->vsf[ij], 0.0);
		}
		if (ac_s->vsolv[it] > 0) {
			int ind;
			expterm =
				ac_s->wace[kt * ac_s->ntypes + it] *
				exp(-r2 / ac_s->s2ace[kt * ac_s-> ntypes + it]);
			u4ace = ac_s->u4ace[kt * ac_s->ntypes + it];
			rmu = r4 + u4ace;
			term = (ac_s->vsolv[it] / (8.0 * M_PI)) * _pow4(r3 / rmu);
			temp = 2.0 * (expterm + term);
			ac_s->eself[i2] -= temp * sw;
			ffk = ((-8 * term * (3 * u4ace - r4) / (r2 * rmu)) +
				(4 * expterm / ac_s->s2ace[kt * ac_s->ntypes + it])) * sw -
				temp * dsw;

			ind = ij + nbsize;
			MOL_VEC_MULT_SCALAR(ac_s->vsf[ind], dv, -ffk);

			MOL_VEC_SUB(ac_s->vf[i2], ac_s->vf[i2], ac_s->vsf[ind]);
		} else {
			MOL_VEC_SET_SCALAR(ac_s->vsf[ij + nbsize], 0.0);
		}
	}
}

//Fixed update
void ace_fixedupdate(struct mol_atom_group *ag, struct agsetup *ags,
		struct acesetup *ac_s)
{
	int *list01 = malloc(2 * (ag->nbonds) * sizeof(int));
	int *na01 = malloc((ag->natoms) * sizeof(int));
	int **pna01 = malloc((ag->natoms) * sizeof(int *));
	int *list0123;
	int n0123;
	comp_list01(ag, list01, na01, pna01);
	if (ac_s->list0123 != NULL)
		free(ac_s->list0123);
	list0123 =
		compute_0123_list(ag, &n0123, ags->listf03, ags->nf03, ags->list02,
				ags->n02, na01, pna01);
	ac_s->list0123 = list0123;
	ac_s->n0123 = n0123;
	free(pna01);
	free(na01);
	free(list01);
}

void ace_updatenblst(const struct agsetup *const restrict ags,
		struct acesetup *const restrict ac_s)
{
	int nbsize = 0;
	for (int i = 0; i < ags->nblst->nfat; i++) {
		nbsize += ags->nblst->nsat[i];
	}
	nbsize += ac_s->n0123;
	ac_s->nbsize = nbsize;
	ac_s->swarr = realloc(ac_s->swarr, nbsize * sizeof(double));
	ac_s->dswarr = realloc(ac_s->dswarr, nbsize * sizeof(double));
	ac_s->darr = realloc(ac_s->darr, nbsize * sizeof(double));
	ac_s->vsf = realloc(ac_s->vsf, 2 * nbsize * sizeof(struct mol_vector3));
}

//Free ace
void destroy_acesetup(struct acesetup *ac_s)
{
	free(ac_s->list0123);
	free(ac_s->eself);
	free(ac_s->rborn);
	free(ac_s->swarr);
	free(ac_s->dswarr);
	free(ac_s->darr);
	free(ac_s->dbrdes);
	free(ac_s->vsf);
	free(ac_s->vf);
	free(ac_s->diarr);
	free(ac_s->lwace);
	free(ac_s->rsolv);
	free(ac_s->vsolv);
	free(ac_s->s2ace);
	free(ac_s->u4ace);
	free(ac_s->wace);
	free(ac_s->hydr);
}

void free_acesetup(struct acesetup *ac_s)
{
	destroy_acesetup(ac_s);
	free(ac_s);
}


static void aceeng_internal(const struct mol_atom_group *const ag,
			double *const restrict en,
			const struct acesetup *const ac_s,
			const struct agsetup *const ags,
			const ACE_ENERGY_TYPE ace_energy_type)
{
	int i1, i2, it, n2, kt, ij = 0;
	int *p;
	double b0 = 0;
	static const double nb2cot = 8.0 * 8.0;	//Switching start
	const double nb2cof = ags->nblst->nbcof * ags->nblst->nbcof;
	const int nbsize = ac_s->nbsize;
	double etotal = 0;
	double ecoul = 0;
	int *list0123 = ac_s->list0123;
	int n0123 = ac_s->n0123;
	double *eself = ac_s->eself;
	double *rborn = ac_s->rborn;
	double *swarr = ac_s->swarr;
	double *dswarr = ac_s->dswarr;
	double *darr = ac_s->darr;
	//d(rb)/d(eself)
	double *dbrdes = ac_s->dbrdes;
	struct mol_vector3 *vsf = ac_s->vsf;
	struct mol_vector3 *vf = ac_s->vf;
	double *diarr = ac_s->diarr;
	static const double cutoff_zero = 0.5; // Angstroms. Chosen arbitrary to avoid singularities

	const double rul3 = 1.0 / pow((nb2cof - nb2cot), 3.0);
	const double rul12 = 12.0 * rul3;
	double ehydr = 0;

	struct mol_vector3 dist;

	//NBLST RBORN
	for (size_t i = 0; i < ag->natoms; i++) {
		double ri;
		it = ag->ftypen[i];
		ri = ac_s->rsolv[it];
		eself[i] = 1.0 / ri + 2.0 * ac_s->lwace[it];
		b0 = b0 + ag->ace_volume[i];
		MOL_VEC_SET_SCALAR(vf[i], 0);
	}
	b0 = pow((0.75 * b0 / M_PI), 1.0 / 3.0);
	ij = 0;
	//Loop through non bonded atoms
	for (int i = 0; i < ags->nblst->nfat; i++) {
		i1 = ags->nblst->ifat[i];
		n2 = ags->nblst->nsat[i];

		p = ags->nblst->isat[i];
		it = ag->ftypen[i1];
		for (int j = 0; j < n2; j++) {
			i2 = p[j];
			MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);

			kt = ag->ftypen[i2];
			ace_eselfupdate(i1, i2, it, kt, ij++, dist, ac_s,
					rul3, rul12, nb2cot,
					nb2cof, nbsize);
		}
	}

	//Loop through 1-2-3-4 list
	for (int i = 0; i < n0123; i++) {
		i1 = list0123[2 * i];
		i2 = list0123[2 * i + 1];
		it = ag->ftypen[i1];
		kt = ag->ftypen[i2];

		MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);

		ace_eselfupdate(i1, i2, it, kt, ij++, dist, ac_s,
				rul3, rul12, nb2cot, nb2cof, nbsize);
	}

	//Electrostatic constant need to carry over to constants
	static const double kelec = 332.0716;
	static const double factor_E = -332.0716 / 2.0;
	//change to epsilons;
	static const double tau = ((1 / 4.0) - (1 / 78.0));

	for (size_t i = 0; i < ag->natoms; i++) {
		double c2;
		diarr[i] = 0;
		if (ace_energy_type & ACE_NONPOLAR) {
			it = ag->ftypen[i];
			ehydr += -ac_s->hydr[it] * eself[i];
			diarr[i] += -(ac_s->hydr[it] / factor_E);
		}
		if (ace_energy_type & ACE_POLAR) {
			if (eself[i] >= 1.0 / b0) {
				rborn[i] = 1.0 / eself[i];
				dbrdes[i] = rborn[i] / (factor_E * eself[i]);
			} else {
				rborn[i] = 1.0 * b0 * (2.0 - b0 * eself[i]);
				dbrdes[i] = b0 * b0 / factor_E;
			}
			c2 = ag->charge[i] * ag->charge[i] * tau;
			eself[i] = factor_E * eself[i] * c2;
			etotal += eself[i];
			diarr[i] += c2;
		}
	}
	if (ace_energy_type & ACE_POLAR) {
		//Second loop calculating energy nb
		ij = 0;
		const double fac1 = -kelec * tau;
		const double facc1 = kelec / (4.0);

		struct mol_vector3 fv;
		for (int i = 0; i < ags->nblst->nfat; i++) {
			i1 = ags->nblst->ifat[i];
			n2 = ags->nblst->nsat[i];
			p = ags->nblst->isat[i];
			if (ag->charge[i1] != 0) {
				for (int j = 0; j < n2; j++) {
					double s, s2, brij, expo, fexp, rij2,
						rij, cij, fac2, sw, dsw, fac3, fac4,
						dij, dji, facc2, fac5;

					i2 = p[j];
					if ((ag->charge[i2] != 0) && (darr[ij] > 0)) {
						MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);
						s = darr[ij];
						s2 = s * s;
						brij = rborn[i1] * rborn[i2];
						expo = s2 / (4.0 * brij);
						fexp = exp(-expo);
						rij2 = s2 + brij * fexp;
						rij = sqrt(rij2);
						cij = ag->charge[i1] * ag->charge[i2];
						fac2 = fac1 * cij / rij;
						sw = swarr[ij];
						dsw = dswarr[ij];
						// For etotal we should not have any singularities, as we always add Born radii
						etotal += fac2 * sw;
						fac3 = fac2 / rij2;
						fac4 = 0.5 * fac3 * (1 + expo) * fexp;
						fac4 = fac4 * sw;
						dij = fac4 * rborn[i2] * dbrdes[i1];
						dji = fac4 * rborn[i1] * dbrdes[i2];
						diarr[i1] += dij;
						diarr[i2] += dji;
						//full field force (incl. coulomb) for non-bonded pairs
						if (s > cutoff_zero) {
							facc2 = facc1 * cij / s;
							fac5 =
								fac3 * (0.25 * fexp - 1.0) -
								facc2 / s2;
							ecoul += facc2 * sw;
							fac5 = sw * fac5 + (fac2 + facc2) * dsw;
							MOL_VEC_MULT_SCALAR(fv, dist, fac5);
							MOL_VEC_SUB(ag->gradients[i1], ag->gradients[i1], fv);
							MOL_VEC_ADD(ag->gradients[i2], ag->gradients[i2], fv);
						} else { // Here comes some homebrew smoothing.
							// Since we should be way below non-bonded cutoff, we have sw = 1, dsw = 0
							// We use quadratic function f(t) = at^2 + bt + c, such that:
							// f'(0) = 0; f'(x) = F'(x); f(x) = F(x),
							// where x is cutoff radii, F is original function
							// This leads to a = F'(x) / (2x); b = 0; c = F(x) - ax^2
							// For historic reasons, we're also adding part of GB gradient here that does not suffer singularity and should not be capped.
							// Therefore, we split fac5 calculation in two parts: capped and normal.
							facc2 = facc1 * cij / cutoff_zero; // At threshold
							// Only ES part that should be capped
							fac5 = (- facc2 / (cutoff_zero * cutoff_zero));
							// Now, facc2 and fac5 have their values as if we were right at the cutoff.
							double a = fac5 / 2 / cutoff_zero;
							double c = facc2 - a * cutoff_zero * cutoff_zero;
							ecoul += a * s * s + c;
							fac5 = 2 * a * s;
							fac5 += fac3 * (0.25 * fexp - 1.0); // GB part that should not be capped
							MOL_VEC_MULT_SCALAR(fv, dist, fac5);
							MOL_VEC_SUB(ag->gradients[i1], ag->gradients[i1], fv);
							MOL_VEC_ADD(ag->gradients[i2], ag->gradients[i2], fv);
						}
					}
					ij++;
				}
			} else {
				ij += n2;
			}
		}
		//1-2-3-4 second loop
		for (int i = 0; i < n0123; i++) {
			double s, s2, brij, expo, fexp, rij2, rij, cij, fac2,
				sw, dsw, fac3, fac4, dij, dji, facc2, fac5;
			i1 = list0123[2 * i];
			i2 = list0123[2 * i + 1];
			MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);

			if ((darr[ij] > 0) && (ag->charge[i1] != 0) && (ag->charge[i2] != 0)) {
				s = darr[ij];
				s2 = s * s;
				brij = rborn[i1] * rborn[i2];
				expo = s2 / (4.0 * brij);
				fexp = exp(-expo);
				rij2 = s2 + brij * fexp;
				rij = sqrt(rij2);
				cij = ag->charge[i1] * ag->charge[i2];
				fac2 = fac1 * cij / rij;
				sw = swarr[ij];
				dsw = dswarr[ij];
				etotal += fac2 * sw;
				fac3 = fac2 / rij2;
				fac4 = 0.5 * fac3 * (1 + expo) * fexp;
				fac4 = fac4 * sw;
				dij = fac4 * rborn[i2] * dbrdes[i1];
				dji = fac4 * rborn[i1] * dbrdes[i2];
				diarr[i1] += dij;
				diarr[i2] += dji;
				//1-4 switching
				if (i < ags->nf03) {
					facc2 = ac_s->efac * facc1 * cij / s;
					fac5 = fac3 * (0.25 * fexp - 1.0) - facc2 / s2;
					ecoul += facc2 * sw;
					fac5 = sw * fac5 + (fac2 + facc2) * dsw;
				} else {
					//1-3 1-2 interactions
					fac5 = fac3 * (0.25 * fexp - 1.0);
					fac5 = sw * fac5 + (fac2) * dsw;
				}

				MOL_VEC_MULT_SCALAR(fv, dist, fac5);
				MOL_VEC_SUB(ag->gradients[i1], ag->gradients[i1], fv);
				MOL_VEC_ADD(ag->gradients[i2], ag->gradients[i2], fv);
			}
			ij++;
		}
	}
	*en += ecoul + etotal + ehydr;

	for (size_t i = 0; i < ag->natoms; i++) {
		double fdiarr = factor_E * diarr[i];
		struct mol_vector3 fv;
		MOL_VEC_MULT_SCALAR(fv, vf[i], fdiarr);
		MOL_VEC_ADD(ag->gradients[i], ag->gradients[i], fv);
	}
	ij = 0;
	for (int i = 0; i < ags->nblst->nfat; i++) {
		i1 = ags->nblst->ifat[i];
		n2 = ags->nblst->nsat[i];
		p = ags->nblst->isat[i];
		for (int j = 0; j < n2; j++) {
			if (darr[ij] > 0) {
				double fdiarr1;
				double fdiarr2;
				struct mol_vector3 fv;
				i2 = p[j];
				fdiarr1 = factor_E * diarr[i1];
				fdiarr2 = factor_E * diarr[i2];
				MOL_VEC_MULT_SCALAR(fv, vsf[ij], fdiarr1);
				MOL_VEC_ADD(ag->gradients[i2], ag->gradients[i2], fv);

				MOL_VEC_MULT_SCALAR(fv, vsf[ij + nbsize], fdiarr2);
				MOL_VEC_ADD(ag->gradients[i1], ag->gradients[i1], fv);
			}
			ij++;
		}
	}
	//Loop through 1-2-3-4 list
	for (int i = 0; i < n0123; i++) {
		i1 = list0123[2 * i];
		i2 = list0123[2 * i + 1];
		if (darr[ij] > 0) {
			double fdiarr1 = factor_E * diarr[i1];
			double fdiarr2 = factor_E * diarr[i2];
			struct mol_vector3 fv;
			MOL_VEC_MULT_SCALAR(fv, vsf[ij], fdiarr1);
			MOL_VEC_ADD(ag->gradients[i2], ag->gradients[i2], fv);

			MOL_VEC_MULT_SCALAR(fv, vsf[ij + nbsize], fdiarr2);
			MOL_VEC_ADD(ag->gradients[i1], ag->gradients[i1], fv);
		}
		ij++;
	}
}

void aceeng(struct mol_atom_group *ag, double *en, struct acesetup *ac_s,
	struct agsetup *ags)
{
	aceeng_internal(ag, en, ac_s, ags, ACE_ALL);
}

void aceeng_nonpolar(struct mol_atom_group *ag, double *en,
		struct acesetup *ac_s, struct agsetup *ags)
{
	aceeng_internal(ag, en, ac_s, ags, ACE_NONPOLAR);
}

void aceeng_polar(struct mol_atom_group *ag, double *en, struct acesetup *ac_s,
		struct agsetup *ags)
{
	aceeng_internal(ag, en, ac_s, ags, ACE_POLAR);
}
