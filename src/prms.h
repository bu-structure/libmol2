#ifndef _MOL_PRMS_H_
#define _MOL_PRMS_H_

#include <stdlib.h>
#include <stdio.h>

#include "atom_group.h"
#include "utils.h"

typedef char* mol_version;

struct mol_prms {
	// atom
	size_t natoms;
	struct mol_atom_prm *atoms;

	int nsubatoms;

	size_t num_pwpot;
	struct mol_pwpot_prm *pwpot;

	mol_version version;
};

struct mol_atom_prm {
	int id;
	int subid;

	char typemaj[8];
	char typemin[8];

	double r; // vdw radius
	double q; // partial charge
};

struct mol_pwpot_prm {
	float r1, r2;
	size_t k; // number of eigenvalues
	float *lambdas; // eigenvalues
	float *Xs; // eigenvectors (k by nsubatoms)
	float **eng; // eps_ij
	float *eng_data;
};


/**
 * Read parameter file with parameters of atoms and knowledge-based potential.
 *
 * See the file format description at
 * https://bitbucket.org/bu-structure/mol-prms/src/master/atom/README.md
 *
 * \param path Path to the libmol2 .prm file.
 * \return Newly allocated \ref mol_prms structure, or \c NULL on failure.
 */
struct mol_prms *mol_prms_read(const char *path);

void mol_prms_destroy(struct mol_prms *prms);
void mol_prms_free(struct mol_prms *prms);

void mol_atom_group_add_prms(struct mol_atom_group *ag, const struct mol_prms *prms);

/* DEPRECATED - use functions above */
void destroy_prms(struct mol_prms *prms) __attribute__ ((deprecated));
void free_prms(struct mol_prms *prms) __attribute__ ((deprecated));

struct mol_prms *read_prms(const char *path) __attribute__ ((deprecated));

void print_prms(const struct mol_prms *prms) __attribute__ ((deprecated));;
void fprint_prms(FILE *fp, const struct mol_prms *prms) __attribute__ ((deprecated));;

#endif /* _MOL_PRMS_H_ */
