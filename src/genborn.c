#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 700
#endif
#define _POSIX_C_SOURCE 200809L
#define _USE_MATH_DEFINES
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "private_utils.h"
#include "utils.h"
#include "genborn.h"

//Dij equation 13  in [2] with k = 0, e_p=1
const double dielec_ij = 1.0 - 1.0 / 78.5;

//Coulomb's constant.
const double ke = 332.06371; // kcal Å / e^2

static double _gb_borndii(const struct mol_atom_group *ag, const size_t index);

static double _gb_fs(const struct mol_atom_group *ag, const size_t index);

//Equation 18 and 29 in [2]
static double _gb_descreen(
	const size_t i,
	const size_t j,
	const double ro_i0,
	const double dist_ij_sq,
	const double dist_ij,
	const double *borndii,
	const double *factor,
	double **dhij_drij,
	const double r_cut);

//Equation 27 in [2]
static double _gb_deij_ai(
	const struct mol_atom_group *ag,
	const size_t i,
	const size_t j,
	const double *eff_dist,
	double **deij_rij,
	double **eij);

//Equation 26 in [2]
static double _gb_dai_rij(
	const size_t i,
	const size_t j,
	const double *eff_dist,
	const double *bonrndii,
	const double *tanh_dai,
	const double *const *dhij_drij);

//Equation 10 in [2] for a pair of atoms
static void _gb_gradient(
	struct mol_atom_group *ag,
	const size_t fatom_indx,
	const size_t satom_indx,
	const double *eff_dist,
	const double *det,
	const double *borndii,
	const double *tanh_dai,
	const double *const *dhij_drij,
	const double deij_rij);
static bool _gb_has_meta(const struct mol_atom_group *ag);

//Amber's "born" radii are used, as extracted from probing PRMTOP file
static double _gb_borndii(const struct mol_atom_group *ag, const size_t index)
{
	if (strcmp(ag->element[index], " C") == 0 || ag->element[index][0] == 'C') {
		return 1.7;
	} else if (strcmp(ag->element[index], " H") == 0 || ag->element[index][0] == 'H') {
		return 1.2;
	} else if (strcmp(ag->element[index], " N") == 0 || ag->element[index][0] == 'N') {
		return 1.55;
	} else if (strcmp(ag->element[index], " S") == 0 || ag->element[index][0] == 'S') {
		return 1.8;
	} else
		return 1.5;
}

//Amber's  atom radius scaling factors are used, as extracted from probing PRMTOP file
static double _gb_fs(const struct mol_atom_group *ag, const size_t index)
{
	if (strcmp(ag->element[index], " C") == 0 || ag->element[index][0] == 'C') {
		return 0.72;
	} else if (strcmp(ag->element[index], " N") == 0 || ag->element[index][0] == 'N') {
		return 0.79;
	} else if (strcmp(ag->element[index], " S") == 0 || ag->element[index][0] == 'S') {
		return 0.96;
	} else
		return 0.85;
}

static double _gb_descreen(
	const size_t i,
	const size_t j,
	const double ro_i0,
	const double dist_ij_sq,
	const double dist_ij,
	const double *borndii,
	const double *factor,
	double **dhij_drij, // 1/dist_ij * dhij/drij
	const double r_cut)
{
	double radius_is = (borndii[j] - 0.09) * factor[j];
	double h_ij = 0.0;
	if (dist_ij > r_cut + radius_is) {
		h_ij = 0.0;
		dhij_drij[i][j] = 0.0;
	} else if (dist_ij > r_cut - radius_is) {
		double rad_sq = radius_is * radius_is;
		double cut_sq = r_cut * r_cut;
		double dist_radius = dist_ij - radius_is;
		double log_tmp = log(dist_radius / r_cut);
		h_ij = (0.125 / dist_ij) * (1 + 2 * dist_ij / dist_radius +
			(1 / cut_sq) * (dist_ij_sq - 4 * r_cut * dist_ij - rad_sq) + 2 * log_tmp);
		dhij_drij[i][j] = (1 / dist_ij) * ((-1 / (8 * cut_sq * dist_ij_sq * dist_radius * dist_radius))
			* (r_cut - dist_radius) * (r_cut + dist_radius) * (rad_sq + dist_ij_sq) - (0.25 / dist_ij_sq) * log_tmp);
	} else if (dist_ij > 4 * radius_is) {
		double tmp = (radius_is * radius_is / dist_ij_sq);
		h_ij = (tmp * radius_is / dist_ij_sq) *
		       (1.0 / 3.0 + tmp * (0.4 + tmp * (3.0 / 7.0 + tmp * (4.0 / 9.0 + (5.0 / 11.0 * tmp)))));
		dhij_drij[i][j] = -tmp * (1 / dist_ij) * (radius_is / (dist_ij_sq * dist_ij)) * (4.0 / 3.0 + tmp *
			(2.4 + tmp * (24.0 / 7.0 + tmp * (40.0 / 9.0 + (60.0 / 11.0 * tmp)))));
	} else if (dist_ij > ro_i0 + radius_is) {
		double rad_tmp = radius_is * radius_is;
		double log_tmp = (log((dist_ij - radius_is) / (dist_ij + radius_is)));
		h_ij = 0.5 * ((radius_is / (dist_ij_sq - rad_tmp)) + 0.5 / dist_ij * log_tmp);
		dhij_drij[i][j] = (1 / dist_ij) * (-0.5 * (radius_is * (dist_ij_sq + rad_tmp)) /
			(dist_ij * (dist_ij_sq - rad_tmp) * (dist_ij_sq - rad_tmp)) - (0.25 / dist_ij_sq) * log_tmp);
	} else if (dist_ij > fabs(ro_i0 - radius_is)) {
		double rad_sq = radius_is * radius_is;
		double ro_sq = ro_i0 * ro_i0;
		double log_tmp = log((ro_i0) / (dist_ij + radius_is));
		h_ij = 0.25 * ((1 / (ro_i0)) * (2 - (0.5 / (dist_ij * (ro_i0))) * (dist_ij_sq + ro_sq - rad_sq)) -
			1 / (dist_ij + radius_is) + 1 / dist_ij * log_tmp);
		dhij_drij[i][j] = (1 / dist_ij) * 0.25 * ((-0.5 / ro_sq) + (((dist_ij_sq + rad_sq) * (ro_sq - rad_sq) -
			2 * dist_ij * rad_sq * radius_is) / (2 * dist_ij_sq * ro_sq * (dist_ij + radius_is) * (dist_ij + radius_is))) - (1 / (dist_ij_sq)) * log_tmp);
	} else if (ro_i0 < radius_is) {
		double rad_sq = radius_is * radius_is;
		double log_tmp = (log((radius_is - dist_ij) / (dist_ij + radius_is)));
		h_ij = 0.5 * (radius_is / (dist_ij_sq - rad_sq) + 2 / (ro_i0) + 0.5 / dist_ij * log_tmp);
		dhij_drij[i][j] = (1 / dist_ij) * (-0.5 * ((radius_is * (dist_ij_sq + rad_sq) /
			(dist_ij * (dist_ij_sq - rad_sq) * (dist_ij_sq - rad_sq))) + (0.5 / dist_ij_sq) * log_tmp));
	} else {
		h_ij = 0;
		dhij_drij[i][j] = 0.0;
	}
	return h_ij;
}

static double _gb_deij_ai(
	const struct mol_atom_group *ag,
	const size_t i,
	const size_t j,
	const double *eff_dist,
	double **deij_rij,
	double **eij)
{
	double dist_ij_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[i], ag->coords[j]);
	double effij = eff_dist[i] * eff_dist[j];
	double e = exp(-dist_ij_sq / (4 * effij));
	double f_ij = dist_ij_sq + effij * e;
	double fij_half = pow(f_ij, 1.5);
	double qij = ag->charge[i] * ag->charge[j];
	double qiqj = dielec_ij * ke * qij;
	double deij_ai = (qiqj / eff_dist[i]) * (e / (2 * fij_half)) * (effij + dist_ij_sq / 4);
	deij_rij[i][j] = (qiqj / fij_half) * (1 - 0.25 * e);
	deij_rij[j][i] = deij_rij[i][j];
	eij[i][j] = -qiqj / sqrt(f_ij);
	eij[j][i] = eij[i][j];
	return deij_ai;
}

static double _gb_dai_rij(
	const size_t i,
	const size_t j,
	const double *eff_dist,
	const double *borndii,
	const double *tanh_dai,
	const double *const *dhij_drij)
{
	return (eff_dist[i] * eff_dist[i] * (borndii[i] - 0.09) / borndii[i]) * tanh_dai[i] * dhij_drij[i][j];
}

static void _gb_gradient(
	struct mol_atom_group *ag,
	const size_t fatom_indx,
	const size_t satom_indx,
	const double *eff_dist,
	const double *det,
	const double *borndii,
	const double *tanh_dai,
	const double *const *dhij_drij,
	const double deij_rij)
{
	struct mol_vector3 temp;
	MOL_VEC_SUB(temp, ag->coords[fatom_indx], ag->coords[satom_indx]);
	double dE_fatom = (det[fatom_indx]) * _gb_dai_rij(fatom_indx, satom_indx, eff_dist, borndii, tanh_dai, dhij_drij);
	double dE_satom = (det[satom_indx]) * _gb_dai_rij(satom_indx, fatom_indx, eff_dist, borndii, tanh_dai, dhij_drij);
	MOL_VEC_MULT_SCALAR(temp, temp, deij_rij + dE_fatom + dE_satom);
	MOL_VEC_ADD(ag->gradients[fatom_indx], ag->gradients[fatom_indx], temp);
	MOL_VEC_SUB(ag->gradients[satom_indx], ag->gradients[satom_indx], temp);
}

bool mol_init_gb(struct mol_atom_group *ag)
{
	size_t natoms = ag->natoms;
	if (natoms == 0) {
		_PRINTF_ERROR("Atom group is empty");
		return false;
	}
	if (!ag->element) {
		_PRINTF_ERROR("Atom group element does not exist");
		return false;
	}
	double *borndii = calloc(natoms, sizeof(double));
	double *factor = calloc(natoms, sizeof(double));
	double *eff_dist = calloc(natoms, sizeof(double)); //alpha_i equation 15 in [2]
	double *sumdet = calloc(natoms, sizeof(double)); //dE/dai
	double *tanh_dai = calloc(natoms, sizeof(double));
	double *sum_descreen = calloc(natoms, sizeof(double)); // sum(H_kl)
	double **dhij_drij = calloc(natoms, sizeof(double *));
	double **deij_rij = calloc(natoms, sizeof(double *));
	double **eij = calloc(natoms, sizeof(double *));
	for (size_t i = 0; i < natoms; i++) {
		borndii[i] = _gb_borndii(ag, i);
		factor[i] = _gb_fs(ag, i);
		dhij_drij[i] = calloc(natoms, sizeof(double));
		deij_rij[i] = calloc(natoms, sizeof(natoms));
		eij[i] = calloc(natoms, sizeof(natoms));
	}
	mol_atom_group_add_metadata(ag, "gb_borndii", borndii);
	mol_atom_group_add_metadata(ag, "gb_factor", factor);
	mol_atom_group_add_metadata(ag, "gb_eff_dist", eff_dist);
	mol_atom_group_add_metadata(ag, "gb_sumdet", sumdet);
	mol_atom_group_add_metadata(ag, "gb_tanh_dai", tanh_dai);
	mol_atom_group_add_metadata(ag, "gb_dhij_drij", dhij_drij);
	mol_atom_group_add_metadata(ag, "gb_sum_descreen", sum_descreen);
	mol_atom_group_add_metadata(ag, "gb_deij_drij", deij_rij);
	mol_atom_group_add_metadata(ag, "gb_eij", eij);

	return true;
}

double mol_gb_energy(
	struct mol_atom_group *ag,
	const double r_cut,
	struct agsetup *ags,
	const struct mol_genborn_obc_type type)
{
	size_t natoms = ag->natoms;
	double *borndii = mol_atom_group_fetch_metadata(ag, "gb_borndii");
	double *factor = mol_atom_group_fetch_metadata(ag, "gb_factor");
	double *eff_dist = mol_atom_group_fetch_metadata(ag, "gb_eff_dist");
	double *sumdet = mol_atom_group_fetch_metadata(ag, "gb_sumdet");
	memset(sumdet, 0, natoms * sizeof(double));
	double *tanh_dai = mol_atom_group_fetch_metadata(ag, "gb_tanh_dai");
	double **dhij_rij = mol_atom_group_fetch_metadata(ag, "gb_dhij_drij");
	double *sum_descreen = mol_atom_group_fetch_metadata(ag, "gb_sum_descreen");
	memset(sum_descreen, 0, natoms * sizeof(double));
	double **deij_rij = mol_atom_group_fetch_metadata(ag, "gb_deij_drij");
	double **eij = mol_atom_group_fetch_metadata(ag, "gb_eij");

	int n_fat = ags->nblst->nfat;
	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		const double ro_i0 = borndii[fatom_indx] - 0.09;
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			double dist_ij_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[fatom_indx], ag->coords[satom_indx]);
			double dist_ij = sqrt(dist_ij_sq);
			sum_descreen[fatom_indx] += _gb_descreen(fatom_indx, satom_indx, ro_i0, dist_ij_sq, dist_ij, borndii, factor, dhij_rij,
			                                         r_cut);
			const double ro_k0 = borndii[satom_indx] - 0.09;
			sum_descreen[satom_indx] += _gb_descreen(satom_indx, fatom_indx, ro_k0, dist_ij_sq, dist_ij, borndii, factor, dhij_rij,
			                                         r_cut);
		}
	}
	for (size_t i = 0; i < natoms; i++) {
		const double ro_i0 = borndii[i] - 0.09;
		sum_descreen[i] += _gb_descreen(i, i, ro_i0, 0.0, 0.0, borndii, factor, dhij_rij, r_cut);
		double radius_i = borndii[i];
		double phi = ro_i0 * sum_descreen[i]; //equation 16 in [2]
		double phi_sq = phi * phi;
		double tanhi = tanh(type.alpha * phi - type.beta * phi_sq + type.gamma * phi * phi_sq); //tanh of equation 15 in [2]
		tanh_dai[i] = (1 - (tanhi * tanhi)) *
		              (type.alpha - 2 * type.beta * phi + 3 * type.gamma * phi_sq); //tanh of equation 26 in [2]
		eff_dist[i] = 1 / (1 / (radius_i - 0.09) -
		                   (1 / (radius_i)) * tanhi);
		sumdet[i] += 0.5 * ke * ag->charge[i] * ag->charge[i] * dielec_ij / (eff_dist[i] * eff_dist[i]); //deii/dai
	}

	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			double tmp = _gb_deij_ai(ag, fatom_indx, satom_indx, eff_dist, deij_rij, eij); //deij/dai
			sumdet[fatom_indx] += tmp;
			sumdet[satom_indx] += tmp * eff_dist[fatom_indx] / eff_dist[satom_indx]; //deij/daj
		}
	}
	double gb_ex = 0.0;
	//obc gb
	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			_gb_gradient(ag, fatom_indx, satom_indx, eff_dist, sumdet, borndii, tanh_dai,
				(const double *const *) dhij_rij, deij_rij[fatom_indx][satom_indx]);
			gb_ex += eij[fatom_indx][satom_indx];
		}
	}
	for (size_t i = 0; i < natoms; i++) {
		gb_ex += -0.5 * ke * ag->charge[i] * ag->charge[i] * (dielec_ij) / eff_dist[i];
	}
	return gb_ex;
}

double mol_sa_energy(
	struct mol_atom_group *ag,
	const double r_cut,
	struct agsetup *ags,
	const struct mol_genborn_obc_type type)
{
	size_t natoms = ag->natoms;
	double *borndii = mol_atom_group_fetch_metadata(ag, "gb_borndii");
	double *factor = mol_atom_group_fetch_metadata(ag, "gb_factor");
	double *eff_dist = mol_atom_group_fetch_metadata(ag, "gb_eff_dist");
	double *sumdet = mol_atom_group_fetch_metadata(ag, "gb_sumdet");
	memset(sumdet, 0, natoms * sizeof(double));
	double *tanh_dai = mol_atom_group_fetch_metadata(ag, "gb_tanh_dai");
	double **dhij_rij = mol_atom_group_fetch_metadata(ag, "gb_dhij_drij");
	double *sum_descreen = mol_atom_group_fetch_metadata(ag, "gb_sum_descreen");
	memset(sum_descreen, 0, natoms * sizeof(double));
	double **deij_rij = mol_atom_group_fetch_metadata(ag, "gb_deij_drij");
	double **eij = mol_atom_group_fetch_metadata(ag, "gb_eij");

	int n_fat = ags->nblst->nfat;
	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		const double ro_i0 = borndii[fatom_indx] - 0.09;
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			double dist_ij_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[fatom_indx], ag->coords[satom_indx]);
			double dist_ij = sqrt(dist_ij_sq);
			sum_descreen[fatom_indx] += _gb_descreen(fatom_indx, satom_indx, ro_i0, dist_ij_sq, dist_ij, borndii, factor, dhij_rij,
			                                         r_cut);
			const double ro_k0 = borndii[satom_indx] - 0.09;
			sum_descreen[satom_indx] += _gb_descreen(satom_indx, fatom_indx, ro_k0, dist_ij_sq, dist_ij, borndii, factor, dhij_rij,
			                                         r_cut);
		}
	}
	for (size_t i = 0; i < natoms; i++) {
		const double ro_i0 = borndii[i] - 0.09;
		sum_descreen[i] += _gb_descreen(i, i, ro_i0, 0.0, 0.0, borndii, factor, dhij_rij, r_cut);
		double radius_i = borndii[i];
		double phi = ro_i0 * sum_descreen[i]; //equation 16 in [2]
		double phi_sq = phi * phi;
		double tanhi = tanh(type.alpha * phi - type.beta * phi_sq + type.gamma * phi * phi_sq); //tanh of equation 15 in [2]
		tanh_dai[i] = (1 - (tanhi *tanhi)) *
		              (type.alpha - 2 * type.beta * phi + 3 * type.gamma * phi_sq); //tanh of equation 26 in [2]
		eff_dist[i] = 1 / (1 / (radius_i - 0.09) -
		                   (1 / (radius_i)) * tanhi);
		sumdet[i] += 0.5 * ke * ag->charge[i] * ag->charge[i] * dielec_ij / (eff_dist[i] * eff_dist[i]); //deii/dai
	}

	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			double tmp = _gb_deij_ai(ag, fatom_indx, satom_indx, eff_dist, deij_rij, eij); //deij/dai
			sumdet[fatom_indx] += tmp;
			sumdet[satom_indx] += tmp * eff_dist[fatom_indx] / eff_dist[satom_indx]; //deij/daj
		}
	}
	double gb_ex = 0.0;
	struct mol_vector3 temp;
	//sa
	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			MOL_VEC_SUB(temp, ag->coords[fatom_indx], ag->coords[satom_indx]);
			double dE_fatom = (4 * M_PI * 0.03 * (ag->vdw_radius[fatom_indx] + 1.4) * (ag->vdw_radius[fatom_indx] + 1.4) *
			                   ag->vdw_radius[fatom_indx] / (eff_dist[fatom_indx] * eff_dist[fatom_indx]))
			                  * _gb_dai_rij(fatom_indx, satom_indx, eff_dist, borndii, tanh_dai, (const double *const *) dhij_rij);
			double dE_satom = (4 * M_PI * 0.03 * (ag->vdw_radius[satom_indx] + 1.4) * (ag->vdw_radius[satom_indx] + 1.4) *
			                   ag->vdw_radius[satom_indx] / (eff_dist[satom_indx] * eff_dist[satom_indx]))
			                  * _gb_dai_rij(satom_indx, fatom_indx, eff_dist, borndii, tanh_dai, (const double *const *) dhij_rij);
			MOL_VEC_MULT_SCALAR(temp, temp, dE_fatom + dE_satom);
			MOL_VEC_ADD(ag->gradients[fatom_indx], ag->gradients[fatom_indx], temp);
			MOL_VEC_SUB(ag->gradients[satom_indx], ag->gradients[satom_indx], temp);
		}
	}

	for (size_t i = 0; i < natoms; i++) {
		gb_ex += -4 * M_PI * 0.03 * (ag->vdw_radius[i] + 1.4) * (ag->vdw_radius[i] + 1.4) * ag->vdw_radius[i] / eff_dist[i];
	}
	return gb_ex;
}

double mol_gbsa_energy(
	struct mol_atom_group *ag,
	const double r_cut,
	struct agsetup *ags,
	const struct mol_genborn_obc_type type)
{
	size_t natoms = ag->natoms;
	double *borndii = mol_atom_group_fetch_metadata(ag, "gb_borndii");
	double *factor = mol_atom_group_fetch_metadata(ag, "gb_factor");
	double *eff_dist = mol_atom_group_fetch_metadata(ag, "gb_eff_dist");
	double *sumdet = mol_atom_group_fetch_metadata(ag, "gb_sumdet");
	memset(sumdet, 0, natoms * sizeof(double));
	double *tanh_dai = mol_atom_group_fetch_metadata(ag, "gb_tanh_dai");
	double **dhij_rij = mol_atom_group_fetch_metadata(ag, "gb_dhij_drij");
	double *sum_descreen = mol_atom_group_fetch_metadata(ag, "gb_sum_descreen");
	memset(sum_descreen, 0, natoms * sizeof(double));
	double **deij_rij = mol_atom_group_fetch_metadata(ag, "gb_deij_drij");
	double **eij = mol_atom_group_fetch_metadata(ag, "gb_eij");

	int n_fat = ags->nblst->nfat;
	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		const double ro_i0 = borndii[fatom_indx] - 0.09;
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			double dist_ij_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[fatom_indx], ag->coords[satom_indx]);
			double dist_ij = sqrt(dist_ij_sq);
			sum_descreen[fatom_indx] += _gb_descreen(fatom_indx, satom_indx, ro_i0, dist_ij_sq, dist_ij, borndii, factor, dhij_rij,
			                                         r_cut);
			const double ro_k0 = borndii[satom_indx] - 0.09;
			sum_descreen[satom_indx] += _gb_descreen(satom_indx, fatom_indx, ro_k0, dist_ij_sq, dist_ij, borndii, factor, dhij_rij,
			                                         r_cut);
		}
	}
	for (size_t i = 0; i < natoms; i++) {
		const double ro_i0 = borndii[i] - 0.09;
		sum_descreen[i] += _gb_descreen(i, i, ro_i0, 0.0, 0.0, borndii, factor, dhij_rij, r_cut);
		double radius_i = borndii[i];
		double phi = ro_i0 * sum_descreen[i]; //equation 16 in [2]
		double phi_sq = phi * phi;
		double tanhi = tanh(type.alpha * phi - type.beta * phi_sq + type.gamma * phi * phi_sq); //tanh of equation 15 in [2]
		tanh_dai[i] = (1 - (tanhi * tanhi)) *
		              (type.alpha - 2 * type.beta * phi + 3 * type.gamma * phi_sq); //tanh of equation 26 in [2]
		eff_dist[i] = 1 / (1 / (radius_i - 0.09) -
		                   (1 / (radius_i)) * tanhi);
		sumdet[i] += 0.5 * ke * ag->charge[i] * ag->charge[i] * dielec_ij / (eff_dist[i] * eff_dist[i]); //deii/dai
	}

	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			double tmp = _gb_deij_ai(ag, fatom_indx, satom_indx, eff_dist, deij_rij, eij); //deij/dai
			sumdet[fatom_indx] += tmp;
			sumdet[satom_indx] += tmp * eff_dist[fatom_indx] / eff_dist[satom_indx]; //deij/daj
		}
	}
	double gb_ex = 0.0;
	struct mol_vector3 temp;
	//gbsa
	for (int i = 0; i < n_fat; i++) {
		size_t fatom_indx = ags->nblst->ifat[i];
		for (int j = 0; j < ags->nblst->nsat[i]; j++) {
			size_t satom_indx = ags->nblst->isat[i][j];
			_gb_gradient(ag, fatom_indx, satom_indx, eff_dist, sumdet, borndii, tanh_dai,
			             (const double *const *) dhij_rij, deij_rij[fatom_indx][satom_indx]);
			gb_ex += eij[fatom_indx][satom_indx];
			MOL_VEC_SUB(temp, ag->coords[fatom_indx], ag->coords[satom_indx]);
			double dE_fatom = (4 * M_PI * 0.03 * (ag->vdw_radius[fatom_indx] + 1.4) * (ag->vdw_radius[fatom_indx] + 1.4) *
			                   ag->vdw_radius[fatom_indx] / (eff_dist[fatom_indx] * eff_dist[fatom_indx]))
			                  * _gb_dai_rij(fatom_indx, satom_indx, eff_dist, borndii, tanh_dai, (const double *const *) dhij_rij);
			double dE_satom = (4 * M_PI * 0.03 * (ag->vdw_radius[satom_indx] + 1.4) * (ag->vdw_radius[satom_indx] + 1.4) *
			                   ag->vdw_radius[satom_indx] / (eff_dist[satom_indx] * eff_dist[satom_indx]))
			                  * _gb_dai_rij(satom_indx, fatom_indx, eff_dist, borndii, tanh_dai, (const double *const *) dhij_rij);
			MOL_VEC_MULT_SCALAR(temp, temp, dE_fatom + dE_satom);
			MOL_VEC_ADD(ag->gradients[fatom_indx], ag->gradients[fatom_indx], temp);
			MOL_VEC_SUB(ag->gradients[satom_indx], ag->gradients[satom_indx], temp);

		}
	}

	for (size_t i = 0; i < natoms; i++) {
		gb_ex += -0.5 * ke * ag->charge[i] * ag->charge[i] * (dielec_ij) / eff_dist[i];
		gb_ex += -4 * M_PI * 0.03 * (ag->vdw_radius[i] + 1.4) * (ag->vdw_radius[i] + 1.4) * ag->vdw_radius[i] / eff_dist[i];
	}
	return gb_ex;
}

static bool _gb_has_meta(const struct mol_atom_group *ag)
{
	if (!mol_atom_group_has_metadata(ag, "gb_borndii")) {
		_PRINTF_ERROR("Metadata does not have borndii");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_factor")) {
		_PRINTF_ERROR("Metadata does not have scale factor");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_eff_dist")) {
		_PRINTF_ERROR("Metadata does not have effective distance");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_sumdet")) {
		_PRINTF_ERROR("Metadata does not have sum determinants");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_tanh_dai")) {
		_PRINTF_ERROR("Metadata does not have tanh");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_sum_descreen")) {
		_PRINTF_ERROR("Metadata does not have descreen");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_dhij_drij")) {
		_PRINTF_ERROR("Metadata does not have differentiation of energy");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_deij_drij")) {
		_PRINTF_ERROR("Metadata does not have differentiation of energy over distance");
		return false;
	}
	if (!mol_atom_group_has_metadata(ag, "gb_eij")) {
		_PRINTF_ERROR("Metadata does not have differentiation of pairwise energy");
		return false;
	}
	return true;

}

bool mol_gb_delete_metadata(struct mol_atom_group *ag)
{
	if (_gb_has_meta(ag)) {
		double *borndii = mol_atom_group_fetch_metadata(ag, "gb_borndii");
		free_if_not_null(borndii);
		mol_atom_group_delete_metadata(ag, "gb_borndii");
		double *factor = mol_atom_group_fetch_metadata(ag, "gb_factor");
		free_if_not_null(factor);
		mol_atom_group_delete_metadata(ag, "gb_factor");
		double *eff_dist = mol_atom_group_fetch_metadata(ag, "gb_eff_dist");
		free_if_not_null(eff_dist);
		mol_atom_group_delete_metadata(ag, "gb_eff_dist");
		double *sumdet = mol_atom_group_fetch_metadata(ag, "gb_sumdet");
		free_if_not_null(sumdet);
		mol_atom_group_delete_metadata(ag, "gb_sumdet");
		double *tanh_dai = mol_atom_group_fetch_metadata(ag, "gb_tanh_dai");
		free_if_not_null(tanh_dai);
		mol_atom_group_delete_metadata(ag, "gb_tanh_dai");
		double *sum_descreen = mol_atom_group_fetch_metadata(ag, "gb_sum_descreen");
		free_if_not_null(sum_descreen);
		mol_atom_group_delete_metadata(ag, "gb_sum_descreen");
		double **dhij_rij = mol_atom_group_fetch_metadata(ag, "gb_dhij_drij");
		double **deij_rij = mol_atom_group_fetch_metadata(ag, "gb_deij_drij");
		double **eij = mol_atom_group_fetch_metadata(ag, "gb_eij");
		for (size_t i = 0; i < ag->natoms; i++) {
			free_if_not_null(dhij_rij[i]);
			free_if_not_null(deij_rij[i]);
			free_if_not_null(eij[i]);
		}
		free_if_not_null(dhij_rij);
		free_if_not_null(deij_rij);
		free_if_not_null(eij);
		mol_atom_group_delete_metadata(ag, "gb_dhij_drij");
		mol_atom_group_delete_metadata(ag, "gb_deij_drij");
		mol_atom_group_delete_metadata(ag, "gb_eij");
	}
	return true;
}
