#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>

#include "nbenergy.h"
#include "benergy.h"
#include "vector.h"
#include "private_utils.h"


#define MAXPAIR 36
#define CCELEC 332.0716


static void addpair(int i, int k, size_t *npair, size_t *pairs);
static int natpair(const struct mol_atom_group *ag, const size_t *pair, const int *clust);
static void addclust(const struct mol_atom_group *ag, const size_t *pair, int *clust, int nclust);

void vdweng03(double f, struct mol_atom_group *ag, double *ven, int n03, int *list03)
{
	int i, i1, i2;
	double ei, ej, eij, ri, rj, rij;
	double dx, dy, dz, d2;
	double Rd6, Rd12, dven, g;
	for (i = 0; i < n03; ++i) {
		i1 = list03[2 * i];
		ei = f * (ag->eps03[i1]);
		ri = ag->vdw_radius03[i1];

		i2 = list03[2 * i + 1];
		ej = ag->eps03[i2];
		rj = ag->vdw_radius03[i2];

		eij = ei * ej;
		rij = ri + rj;
		rij *= rij;

		dx = ag->coords[i1].X - ag->coords[i2].X;
		dy = ag->coords[i1].Y - ag->coords[i2].Y;
		dz = ag->coords[i1].Z - ag->coords[i2].Z;
		d2 = dx * dx + dy * dy + dz * dz;
		Rd6 = rij / d2;
		Rd6 = Rd6 * Rd6 * Rd6;
		Rd12 = Rd6 * Rd6;
		(*ven) += eij * (Rd12 - 2 * Rd6);
		dven = -eij * 12 * (-Rd12 + Rd6) / d2;
		g = dven * dx;
		(ag->gradients[i1].X) += g;
		(ag->gradients[i2].X) -= g;
		g = dven * dy;
		(ag->gradients[i1].Y) += g;
		(ag->gradients[i2].Y) -= g;
		g = dven * dz;
		(ag->gradients[i1].Z) += g;
		(ag->gradients[i2].Z) -= g;
	}
}

void vdwengs03(double f, double rc, struct mol_atom_group *ag, double *ven,
	       int n03, int *list03)
{
	int i1, i2;

	double ei, ri, ej, rj, eij;
	double d2, Rd12, Rd6, Rr12, Rr6, dr6;
	double rij, dven;
	double rc2 = rc * rc;

	struct mol_vector3 g, dist;

	for (int i = 0; i < n03; ++i) {
		i1 = list03[2 * i];
		i2 = list03[2 * i + 1];

		ei = f * (ag->eps03[i1]);
		ri = ag->vdw_radius03[i1];

		ej = ag->eps03[i2];
		rj = ag->vdw_radius03[i2];

		eij = ei * ej;
		rij = ri + rj;
		rij *= rij;

		MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);
		d2 = MOL_VEC_SQ_NORM(dist);

		Rd6 = rij / d2;
		Rd6 = Rd6 * Rd6 * Rd6;
		Rd12 = Rd6 * Rd6;
		Rr6 = rij / rc2;
		Rr6 = Rr6 * Rr6 * Rr6;
		Rr12 = Rr6 * Rr6;
		dr6 = d2 / rc2;
		dr6 = dr6 * dr6 * dr6;

		(*ven) +=
		    eij * (Rd12 - 2 * Rd6 + Rr6 * (4.0 - 2 * dr6) +
			   Rr12 * (2 * dr6 - 3.0));
		dven = -eij * 12 * (-Rd12 + Rd6 + dr6 * (Rr12 - Rr6)) / d2;

		MOL_VEC_MULT_SCALAR(g, dist, dven);

		MOL_VEC_ADD(ag->gradients[i1], ag->gradients[i1], g);
		MOL_VEC_SUB(ag->gradients[i2], ag->gradients[i2], g);
	}
}

void vdweng(struct mol_atom_group *ag, double *ven, struct nblist *nblst)
{
	int i1, n2, *p, j, i2;
	double ei, ri, ej, rj, eij;
	double d2, Rd12, Rd6, Rr12, Rr6, dr6;
	double rij, dven;
	double rc = nblst->nbcof;
	double rc2 = rc * rc;

	struct mol_vector3 g, dist;

	for (int i = 0; i < nblst->nfat; ++i) {
		i1 = nblst->ifat[i];

		ei = ag->eps[i1];
		ri = ag->vdw_radius[i1];
		n2 = nblst->nsat[i];
		p = nblst->isat[i];
		for (j = 0; j < n2; ++j) {
			i2 = p[j];

			ej = ag->eps[i2];
			rj = ag->vdw_radius[i2];

			MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);
			d2 = MOL_VEC_SQ_NORM(dist);
			if (d2 < rc2) {
				eij = ei * ej;
				rij = ri + rj;
				rij *= rij;

				Rd6 = rij / d2;
				Rd6 = Rd6 * Rd6 * Rd6;
				Rd12 = Rd6 * Rd6;
				Rr6 = rij / rc2;
				Rr6 = Rr6 * Rr6 * Rr6;
				Rr12 = Rr6 * Rr6;
				dr6 = d2 / rc2;
				dr6 = dr6 * dr6 * dr6;

				(*ven) +=
				    eij * (Rd12 - 2 * Rd6 +
					   Rr6 * (4.0 - 2 * dr6) +
					   Rr12 * (2 * dr6 - 3.0));
				dven =
				    -eij * 12 * (-Rd12 + Rd6 +
						 dr6 * (Rr12 - Rr6)) / d2;

				MOL_VEC_MULT_SCALAR(g, dist, dven);

				MOL_VEC_ADD(ag->gradients[i1], ag->gradients[i1], g);
				MOL_VEC_SUB(ag->gradients[i2], ag->gradients[i2], g);
			}
		}
	}
}

void eleng03(double f, struct mol_atom_group *ag, double eps, double *een,
	     int n03, int *list03)
{
	int i, i1, i2;
	struct mol_vector3 g, dist;
	double chi, chj, chij, ee, deen;
	double d, d2;
	for (i = 0; i < n03; ++i) {
		i1 = list03[2 * i];
		chi = ag->charge[i1];

		i2 = list03[2 * i + 1];
		chj = ag->charge[i2];

		chij = f * CCELEC * chi * chj / eps;

		MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);
		d2 = dist.X*dist.X + dist.Y*dist.Y + dist.Z*dist.Z;
		d = sqrt(d2);
		ee = chij / d;
		(*een) += ee;
		deen = ee / d2;

		MOL_VEC_MULT_SCALAR(g, dist, deen);

		MOL_VEC_ADD(ag->gradients[i1], ag->gradients[i1], g);
		MOL_VEC_SUB(ag->gradients[i2], ag->gradients[i2], g);
	}
}

void elengs03(double f, double rc, struct mol_atom_group *ag, double eps, double *een,
	      int n03, int *list03)
{
	int i, i1, i2;
	struct mol_vector3 g, dist;

	double d2, d1, ch1, ch2;
	double esh, desh;

	double pf = f * CCELEC / eps;
	double rc2 = 1.0 / (rc * rc);
	for (i = 0; i < n03; ++i) {
		i1 = list03[2 * i];
		ch1 = pf * (ag->charge[i1]);

		i2 = list03[2 * i + 1];
		ch2 = ag->charge[i2];

		MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);
		d2 = dist.X*dist.X + dist.Y*dist.Y + dist.Z*dist.Z;
		d1 = sqrt(d2);
		if (d1 < rc) {
			esh = 1.0 - d1 / rc;
			esh *= esh / d1;
			desh = (1.0 / d2 - rc2) / d1;
			ch2 = ch2 * ch1;
			(*een) += ch2 * esh;

			MOL_VEC_MULT_SCALAR(g, dist, ch2*desh);
			MOL_VEC_ADD(ag->gradients[i1], ag->gradients[i1], g);
			MOL_VEC_SUB(ag->gradients[i2], ag->gradients[i2], g);
		}
	}
}

void eleng(struct mol_atom_group *ag, double eps, double *een, struct nblist *nblst)
{
	int i, j, i1, n2, i2, *p;
	struct mol_vector3 g, dist;

	double d2, d1, ch1, ch2;
	double esh, desh;

	double pf = CCELEC / eps;
	double rc = nblst->nbcof;
	double rc2 = 1.0 / (rc * rc);
	for (i = 0; i < nblst->nfat; ++i) {
		i1 = nblst->ifat[i];

		ch1 = pf * (ag->charge[i1]);

		n2 = nblst->nsat[i];
		p = nblst->isat[i];
		for (j = 0; j < n2; ++j) {
			i2 = p[j];

			ch2 = ag->charge[i2];
			MOL_VEC_SUB(dist, ag->coords[i1], ag->coords[i2]);
			d2 = dist.X*dist.X + dist.Y*dist.Y + dist.Z*dist.Z;
			d1 = sqrt(d2);

			if (d1 < rc) {
				esh = 1.0 - d1 / rc;
				esh *= esh / d1;
				desh = (1.0 / d2 - rc2) / d1;
				ch2 = ch2 * ch1;
				(*een) += ch2 * esh;

				MOL_VEC_MULT_SCALAR(g, dist, ch2*desh);
				MOL_VEC_ADD(ag->gradients[i1], ag->gradients[i1], g);
				MOL_VEC_SUB(ag->gradients[i2], ag->gradients[i2], g);
			}
		}
	}
}

void destroy_nblist(struct nblist *nblst)
{
	for (int i = 0; i < nblst->nfat; ++i) {
		if (nblst->nsat[i] > 0)
			free(nblst->isat[i]);
	}
	free(nblst->isat);
	free(nblst->nsat);
	free(nblst->ifat);
	free(nblst->crds);
}

void free_nblist(struct nblist *nblst)
{
	destroy_nblist(nblst);
	free(nblst);
}

//! Generate a nonbonded list nblst from cluster and cube subdivisions and exclusion list arrays.
/*! To generate nblist
    1. Loop over the filled cubes
    2. Loop over linked clusters in the first cube
    3. Loop over neighboring cubes including first cube.
    4. Loop through the linked list clusters of the second cube.
    5. Go only for pairs of clusters which are closer than an nbcut plus lagest distance from any atom to the
       geometry center of cluster 1 plus the largest distance of the cluster 2
    6. For the pair of clusters icl1 and icl2 go through all pairs of their atoms
    7. Write the pair to the list if the distance between clusters is less than nbcut minus largest dist of
       cluster one minus largest dist of cluster two.
    8. Check the atomic pair distance  if the condition in the step 7 is not true
    9. Write the pair if distance less than nonbonded cutoff.

    The structure of the nblist is:
    number of first atoms,
    array of first atom indices,
    array of numbers of second atoms for each first one,
    array of pointers to arrays of all second atoms,
    arrays of all second atoms. */

void gen_nblist(struct mol_atom_group *ag, struct cubeset *cust,
		struct clusterset *clst, int *excl_list, int **pd1, int **pd2,
		int ndm, struct nblist *nblst)
{
	int ic1, ic2, icl1, icl2;
	int k1, k2, ak1, ak2, ka1, ka2, ka3, ip;
	double mdc1, mdc2, dna, dda, dx, dy, dz, d;
	double x1, x2, y1, y2, z1, z2, nbcut;
	double dista;

	struct mol_vector3 dd;

	size_t natoms = ag->natoms;

/* prepare nonbond list arrays. */
	int *p;
	for (int i = 0; i < nblst->nfat; ++i) {
		if ((nblst->nsat[i]) > 0)
			free(nblst->isat[i]);
	}
	nblst->nfat = 0;
	nblst->ifat = realloc(nblst->ifat, natoms * sizeof(int));
	nblst->nsat = realloc(nblst->nsat, natoms * sizeof(int));
	nblst->isat = realloc(nblst->isat, natoms * sizeof(int *));
//ifat is temporal
	int *ifat = malloc(natoms * sizeof(int));
	for (size_t i = 0; i < natoms; ++i) {
		nblst->nsat[i] = 0;
		nblst->ifat[i] = -1;
		ifat[i] = -1;
	}

	nbcut = nblst->nbcut;
	double nbcuts = nbcut * nbcut;

/* loop over the filled cubes. */
	for (int i = 0; i < cust->nfcubes; ++i) {
		ic1 = cust->ifcubes[i];
		icl1 = cust->cubes[ic1].hstincube;
/* loop over all clusters in cube1. */
		while (icl1 >= 0) {
			mdc1 = clst->clusters[icl1].mdc;
			x1 = clst->clusters[icl1].gcent[0];
			y1 = clst->clusters[icl1].gcent[1];
			z1 = clst->clusters[icl1].gcent[2];
/* loop over neighboring cubes including cube1. */
			for (int j = -1; j < cust->cubes[ic1].nncubes; ++j) {
				if (j == -1)	// cube-self part
					icl2 = clst->clusters[icl1].nextincube;
				else	// cube-neighbor cube part
				{
					ic2 = cust->cubes[ic1].icubes[j];
					icl2 = cust->cubes[ic2].hstincube;
				}
/* second loop over clusters. */
				while (icl2 >= 0) {
					mdc2 = clst->clusters[icl2].mdc;
					x2 = clst->clusters[icl2].gcent[0];
					y2 = clst->clusters[icl2].gcent[1];
					z2 = clst->clusters[icl2].gcent[2];
					dx = x1 - x2;
					dx *= dx;
					dy = y1 - y2;
					dy *= dy;
					dz = z1 - z2;
					dz *= dz;
					d = dx + dy + dz;
					dna = nbcut - mdc1 - mdc2;
					dna *= dna;
					dda = nbcut + mdc1 + mdc2;
					dda *= dda;
					if (d <= dda) {
/* for clusters icl1 and icl2 go through all pairs of their atoms. */
						for (k1 = 0; k1 < clst->clusters[icl1].natoms; ++k1) {
							ak1 = clst->clusters[icl1].iatom[k1];
							for (k2 = 0; k2 < clst->clusters[icl2].natoms; ++k2) {
								ak2 =
								    clst->clusters[icl2].iatom[k2];
								if (ag->fixed[ak1] && ag->fixed[ak2])
									continue;
								if (ak1 < ak2) {
									ka1 = ak1;
									ka2 = ak2;
								} else {
									ka1 = ak2;
									ka2 = ak1;
								}
/* check the exclusion table. */
								ka3 =
								    exta(ka1, ka2, excl_list,
									 pd1, pd2, ndm);
								if (ka3 > 0)
									continue;
/* check distances of distant atom pairs (d>dna). */
								if (d > dna) {
									MOL_VEC_SUB(dd, ag->coords[ka1], ag->coords[ka2]);
									dista = MOL_VEC_SQ_NORM(dd);

									if (dista > nbcuts)
										continue;
								}
/* write a successful atom pair to a nonbond list structure. */
								if (ifat[ka1] ==
								    -1) {
									nblst->ifat
									    [nblst->nfat]
									    =
									    ka1;
									ifat[ka1] = nblst->nfat;
									nblst->isat
									    [nblst->nfat]
									    =
									    malloc
									    ((natoms - ka1 - 1) * sizeof(int));
									(nblst->nfat)++;
								}
								ip = ifat[ka1];
								p = nblst->isat
								    [ip];
								p[nblst->nsat
								  [ip]] = ka2;
								(nblst->nsat
								 [ip])++;
							}
						}
					}
					icl2 = clst->clusters[icl2].nextincube;
				}
			}
			icl1 = clst->clusters[icl1].nextincube;
		}
	}
/* cleanup, total pair calculation. */
	int ntotp = 0;
	free(ifat);
	if (nblst->nfat > 0) {
		nblst->ifat = realloc(nblst->ifat, (nblst->nfat) * sizeof(int));
		nblst->nsat = realloc(nblst->nsat, (nblst->nfat) * sizeof(int));
		nblst->isat = realloc(nblst->isat, (nblst->nfat) * sizeof(int *));
	}

	for (int i = 0; i < nblst->nfat; ++i) {
		ntotp += nblst->nsat[i];
		nblst->isat[i] = realloc(nblst->isat[i], (nblst->nsat[i]) * sizeof(int));
	}
	nblst->npairs = ntotp;
}

void gen_nblist_new(struct mol_atom_group *ag, struct cubeset *cust,
		    struct clusterset *clst, int *excl_list, int **pd1,
		    int **pd2, int ndm, struct nblist *nblst)
{
	int ic1, ic2, icl1, icl2;
	int ak1, ak2, ka1, ka2, ka3, ip;
	double mdc1, mdc2, dna, dda, dx, dy, dz, d;
	double x1, x2, y1, y2, z1, z2, nbcut;
	double dista;

	size_t natoms = ag->natoms;
	struct mol_vector3 dd;

//prepare nonbond list arrays
	int *p;
	for (int i = 0; i < nblst->nfat; ++i) {
		if ((nblst->nsat[i]) > 0)
			free(nblst->isat[i]);
	}
	nblst->nfat = 0;
	nblst->ifat = realloc(nblst->ifat, natoms * sizeof(int));
	nblst->nsat = realloc(nblst->nsat, natoms * sizeof(int));
	nblst->isat = realloc(nblst->isat, natoms * sizeof(int *));
//ifat is temporal
	int *ifat = malloc(natoms * sizeof(int));
	for (size_t i = 0; i < natoms; ++i) {
		nblst->nsat[i] = 0;
		nblst->ifat[i] = -1;
		ifat[i] = -1;
	}

	nbcut = nblst->nbcut;
	double nbcuts = nbcut * nbcut;

//loop over the filled cubes
	for (int i = 0; i < cust->nfcubes; ++i) {
		ic1 = cust->ifcubes[i];
		icl1 = cust->cubes[ic1].hstincube;
//loop over all clusters in cube1
		while (icl1 >= 0) {
			mdc1 = clst->clusters[icl1].mdc;

			x1 = clst->clusters[icl1].gcent[0];
			y1 = clst->clusters[icl1].gcent[1];
			z1 = clst->clusters[icl1].gcent[2];
//loop over neighboring cubes including cube1
			for (int j = -1; j < cust->cubes[ic1].nncubes; ++j) {
				if (j == -1)	// cube-self part
					icl2 = clst->clusters[icl1].nextincube;
				else	// cube-neighbor cube part
				{
					ic2 = cust->cubes[ic1].icubes[j];
					icl2 = cust->cubes[ic2].hstincube;
				}
//second loop over clusters
				while (icl2 >= 0) {
					mdc2 = clst->clusters[icl2].mdc;
					x2 = clst->clusters[icl2].gcent[0];
					y2 = clst->clusters[icl2].gcent[1];
					z2 = clst->clusters[icl2].gcent[2];
					dx = x1 - x2;
					dx *= dx;
					dy = y1 - y2;
					dy *= dy;
					dz = z1 - z2;
					dz *= dz;
					d = dx + dy + dz;
					dna = nbcut - mdc1 - mdc2;
					dna *= dna;
					dda = nbcut + mdc1 + mdc2;
					dda *= dda;

					if (d <= dda) {
//for clusters icl1 and icl2 go through all pairs of their atoms
						for (int k1 = 0; k1 < clst->clusters[icl1].natoms; k1++) {
							ak1 = clst->clusters[icl1].iatom[k1];
							for (int k2 = 0; k2 < clst->clusters[icl2].natoms; k2++) {
								ak2 = clst->clusters[icl2].iatom[k2];

								if (ag->fixed[ak1] && ag->fixed[ak2])
									continue;
								if (ak1 < ak2) {
									ka1 = ak1;
									ka2 = ak2;
								} else {
									ka1 = ak2;
									ka2 = ak1;
								}
//check the exclusion table
								ka3 =
									exta(ka1, ka2, excl_list,
										pd1, pd2, ndm);
								if (ka3 > 0)
									continue;
//check distances of distant atom pairs (d>dna)
								if (d > dna) {
									MOL_VEC_SUB(dd, ag->coords[ka1], ag->coords[ka2]);
									dista = MOL_VEC_SQ_NORM(dd);
									if (dista > nbcuts)
										continue;
								}
//write a successful atom pair to a nonbond list structure
								if (ifat[ka1] ==
								    -1) {
//                                nblst->ifat[nblst->nfat]=ka1;
									ifat[ka1] = nblst->nfat;
//                                nblst->isat[nblst->nfat]=malloc((natoms-ka1-1)*sizeof(int));
									(nblst->nfat)++;
								}
								ip = ifat[ka1];
//                             p=nblst->isat[ip];
//                             p[nblst->nsat[ip]]=ka2;
								(nblst->nsat
								 [ip])++;
							}
						}
					}
					icl2 = clst->clusters[icl2].nextincube;
				}
			}
			icl1 = clst->clusters[icl1].nextincube;
		}
	}

	for (size_t i = 0; i < natoms; ++i) {
		ip = ifat[i];
		if (ip > -1) {
			if (nblst->nsat[ip] > 0)
				nblst->isat[ip] =
				    malloc(nblst->nsat[ip] * sizeof(int));
			nblst->nsat[ip] = 0;
		}
		nblst->ifat[i] = -1;
		ifat[i] = -1;
	}
	nblst->nfat = 0;

//loop over the filled cubes
	for (int i = 0; i < cust->nfcubes; ++i) {
		ic1 = cust->ifcubes[i];
		icl1 = cust->cubes[ic1].hstincube;
//loop over all clusters in cube1
		while (icl1 >= 0) {
			mdc1 = clst->clusters[icl1].mdc;
			x1 = clst->clusters[icl1].gcent[0];
			y1 = clst->clusters[icl1].gcent[1];
			z1 = clst->clusters[icl1].gcent[2];
//loop over neighboring cubes including cube1
			for (int j = -1; j < cust->cubes[ic1].nncubes; ++j) {
				if (j == -1)	// cube-self part
					icl2 = clst->clusters[icl1].nextincube;
				else	// cube-neighbor cube part
				{
					ic2 = cust->cubes[ic1].icubes[j];
					icl2 = cust->cubes[ic2].hstincube;
				}
//second loop over clusters
				while (icl2 >= 0) {
					mdc2 = clst->clusters[icl2].mdc;
					x2 = clst->clusters[icl2].gcent[0];
					y2 = clst->clusters[icl2].gcent[1];
					z2 = clst->clusters[icl2].gcent[2];
					dx = x1 - x2;
					dx *= dx;
					dy = y1 - y2;
					dy *= dy;
					dz = z1 - z2;
					dz *= dz;
					d = dx + dy + dz;
					dna = nbcut - mdc1 - mdc2;
					dna *= dna;
					dda = nbcut + mdc1 + mdc2;
					dda *= dda;
					if (d <= dda) {
//for clusters icl1 and icl2 go through all pairs of their atoms
						for (int k1 = 0; k1 < clst->clusters[icl1].natoms; ++k1) {
							ak1 = clst->clusters[icl1].iatom[k1];
							for (int k2 = 0; k2 < clst->clusters[icl2].natoms; ++k2) {
								ak2 =
								    clst->clusters[icl2].iatom[k2];
								if (ag->fixed[ak1] && ag->fixed[ak2])
									continue;
								if (ak1 < ak2) {
									ka1 = ak1;
									ka2 = ak2;
								} else {
									ka1 = ak2;
									ka2 = ak1;
								}
//check the exclusion table
								ka3 =
									exta(ka1, ka2, excl_list,
										pd1, pd2, ndm);
								if (ka3 > 0)
									continue;
//check distances of distant atom pairs (d>dna)
								if (d > dna) {
									MOL_VEC_SUB(dd, ag->coords[ka1], ag->coords[ka2]);
									dista = MOL_VEC_SQ_NORM(dd);
									if (dista > nbcuts)
										continue;
								}
//write a successful atom pair to a nonbond list structure
								if (ifat[ka1] == -1) {
									nblst->ifat[nblst->nfat] = ka1;
									ifat[ka1] = nblst->nfat;
//                                nblst->isat[nblst->nfat]=malloc((natoms-ka1-1)*sizeof(int));
									(nblst->nfat)++;
								}
								ip = ifat[ka1];
								p = nblst->isat[ip];
								p[nblst->nsat[ip]] = ka2;
								(nblst->nsat[ip])++;
							}
						}
					}
					icl2 = clst->clusters[icl2].nextincube;
				}
			}
			icl1 = clst->clusters[icl1].nextincube;
		}
	}
//cleanup, total pair calculation
	int ntotp = 0;
	free(ifat);
	if (nblst->nfat > 0) {
		nblst->ifat = realloc(nblst->ifat, (nblst->nfat) * sizeof(int));
		nblst->nsat = realloc(nblst->nsat, (nblst->nfat) * sizeof(int));
		nblst->isat = realloc(nblst->isat, (nblst->nfat) * sizeof(int *));
	}

	for (int i = 0; i < nblst->nfat; ++i) {
		ntotp += nblst->nsat[i];
//           nblst->isat[i]=realloc(nblst->isat[i],(nblst->nsat[i])*sizeof(int));
	}
	nblst->npairs = ntotp;
}

void free_cubeset(struct cubeset *cust)
{
	int i, ic;

	for (i = 0; i < (cust->nfcubes); ++i) {
		ic = cust->ifcubes[i];
		if (cust->cubes[ic].nncubes > 0)
			free(cust->cubes[ic].icubes);
	}
	free(cust->cubes);
	free(cust->ifcubes);
}

//! Generate a set of cubes cust based on the clusterset clst.
/*! Called in update_nblst. */
	/*! 0. Sum of nblist cutoff and margin size gives the cubesize.
	   1. Calculate coordinate span of cluster centers
	   2. Number of cubes in each direction is span over cubesize plus 1
	   3. For each cluster find a cube it belongs to.
	   4. Each cube knows last cluster contained in it.
	   5. Each cluster knows another cluster belongin to the same cube (linked list)
	   6. Each cube knows its occupied neighbouring cubes. */

void gen_cubeset(double nbcut, struct clusterset *clst, struct cubeset *cust)
{
	double xmin, xmax, ymin, ymax, zmin, zmax, cubel;
	int i, nx, ny, nz, nxy, n, j, k, l;
	int ix, iy, iz, ic, ic1;
	int ix1, ix2, iy1, iy2, iz1, iz2;
	int nnbr, temp[27];
	/* sum of nblist cutoff and margin size gives the cubesize cubel. */
	cubel = nbcut + clst->marg;
	cust->cubel = cubel;
	/* calculate coordinate span of cluster centers. */
	xmin = clst->clusters[0].gcent[0];
	xmax = xmin;
	ymin = clst->clusters[0].gcent[1];
	ymax = ymin;
	zmin = clst->clusters[0].gcent[2];
	zmax = zmin;
	for (i = 1; i < clst->nclusters; ++i) {
		if (xmin > clst->clusters[i].gcent[0])
			xmin = clst->clusters[i].gcent[0];
		if (xmax < clst->clusters[i].gcent[0])
			xmax = clst->clusters[i].gcent[0];
		if (ymin > clst->clusters[i].gcent[1])
			ymin = clst->clusters[i].gcent[1];
		if (ymax < clst->clusters[i].gcent[1])
			ymax = clst->clusters[i].gcent[1];
		if (zmin > clst->clusters[i].gcent[2])
			zmin = clst->clusters[i].gcent[2];
		if (zmax < clst->clusters[i].gcent[2])
			zmax = clst->clusters[i].gcent[2];
	}
	/* number of cubes in each direction is span over cubesize plus 1. */
	nx = 1 + (int)((xmax - xmin) / cubel);
	ny = 1 + (int)((ymax - ymin) / cubel);
	nz = 1 + (int)((zmax - zmin) / cubel);
	nxy = nx * ny;
	n = nxy * nz;
	cust->ncubes = n;
	cust->nfcubes = 0;
	cust->cubes = malloc(n * sizeof(struct cube));
	if (cust->cubes == NULL) {
		_PRINTF_ERROR("Can not allocate memory for %d non-bonded cubes", n);
		exit(EXIT_FAILURE);
	}
	cust->ifcubes = malloc(n * sizeof(int));
	if (cust->ifcubes == NULL) {
		_PRINTF_ERROR("Can not allocate memory for %d non-bonded cube indices", n);
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < n; ++i) {
		cust->cubes[i].hstincube = -1;
		cust->cubes[i].nncubes = 0;
	}
	/* for each cluster find a cube it belongs. */
	for (i = 0; i < clst->nclusters; ++i) {
		ix = (int)((clst->clusters[i].gcent[0] - xmin) / cubel);
		iy = (int)((clst->clusters[i].gcent[1] - ymin) / cubel);
		iz = (int)((clst->clusters[i].gcent[2] - zmin) / cubel);
		ic = ix + iy * nx + iz * nxy;
		if (ic < 0 || ic >= n) {
			_PRINTF_ERROR("Error generating nblist for cluster #%d. Bad coordinates?", i);
			exit(EXIT_FAILURE);
		}
		if (cust->cubes[ic].hstincube == -1) {
			cust->cubes[ic].ix = ix;
			cust->cubes[ic].iy = iy;
			cust->cubes[ic].iz = iz;
			cust->ifcubes[(cust->nfcubes)++] = ic;
		}
		/* each cluster knows another cluster belonging to the same cube (linked list). */
		clst->clusters[i].nextincube = cust->cubes[ic].hstincube;
		/* each cube knows last cluster contained in it. */
		cust->cubes[ic].hstincube = i;
	}
	/* loop over occupied cubes. */
	for (i = 0; i < cust->nfcubes; ++i) {
		ic = cust->ifcubes[i];
		if (cust->cubes[ic].hstincube == -1)
			continue;
		ix = cust->cubes[ic].ix;
		ix1 = ix - 1;
		if (ix1 < 0)
			ix1 = 0;
		ix2 = ix + 1;
		if (ix2 > nx - 1)
			ix2 = nx - 1;
		iy = cust->cubes[ic].iy;
		iy1 = iy - 1;
		if (iy1 < 0)
			iy1 = 0;
		iy2 = iy + 1;
		if (iy2 > ny - 1)
			iy2 = ny - 1;
		iz = cust->cubes[ic].iz;
		iz1 = iz - 1;
		if (iz1 < 0)
			iz1 = 0;
		iz2 = iz + 1;
		if (iz2 > nz - 1)
			iz2 = nz - 1;
		nnbr = 0;
		for (j = iz1; j <= iz2; ++j) {
			for (k = iy1; k <= iy2; k++) {
				for (l = ix1; l <= ix2; l++) {
					ic1 = l + k * nx + j * nxy;
					if (ic1 < 0 || ic1 >= n) {
						_PRINTF_ERROR("Error generating nblist for cube #%d. Bad coordinates?", i);
						exit(EXIT_FAILURE);
					}
					if (ic1 <= ic)
						continue;
					if (cust->cubes[ic1].hstincube == -1)
						continue;
					temp[nnbr++] = ic1;
				}
			}
		}
		cust->cubes[ic].nncubes = nnbr;
		/* each cube knows its occupied neighbouring cubes. */
		if (nnbr > 0) {
			cust->cubes[ic].icubes = malloc(nnbr * sizeof(int));
			for (j = 0; j < nnbr; ++j)
				cust->cubes[ic].icubes[j] = temp[j];
		}
	}
}

//! Access margin size of future cubes.
/*! Calculate geometrical center of each cluster and margine size which is a sum of
    2 longest distances from
    the geometrical center of any cluster to one of its member atoms. */
void findmarg(struct mol_atom_group *ag, struct clusterset *clst)
{
	int k, a;
	double dist1 = 0, dist2 = 0;
	double d, md;
	struct mol_vector3 gc, tmp, dist;
	for (int i = 0; i < clst->nclusters; ++i) {
		md = 0.0;
		MOL_VEC_SET_SCALAR(gc, 0.0);
		a = clst->clusters[i].natoms;
		for (int j = 0; j < a; ++j) {
			k = clst->clusters[i].iatom[j];
			MOL_VEC_DIV_SCALAR(tmp, ag->coords[k], a);
			MOL_VEC_ADD(gc, gc, tmp);
		}
		for (int j = 0; j < a; ++j) {
			k = clst->clusters[i].iatom[j];
			MOL_VEC_SUB(dist, ag->coords[k], gc);
			d = sqrt(MOL_VEC_SQ_NORM(dist));
			if (d > md)
				md = d;	/* maximum distance for a given cluster */
			if (d > dist1) {
				dist2 = dist1;	/* dist1 and dist2 are two longest distances among the set of clusters. */
				dist1 = d;
			} else if (d > dist2)
				dist2 = d;
		}
		clst->clusters[i].mdc = md;	/* save maximum distance. */

		clst->clusters[i].gcent[0] = gc.X;	/*! save cluster center. */
		clst->clusters[i].gcent[1] = gc.Y;
		clst->clusters[i].gcent[2] = gc.Z;
	}
	clst->marg = dist1 + dist2;	/* save a margine distance for a cluster set. */
}

void free_clset(struct clusterset *clst)
{
	int i;
	for (i = 0; i < clst->nclusters; ++i)
		free(clst->clusters[i].iatom);
	free(clst->clusters);
}

void gen_clset(int natoms, struct clusterset *clst, int nclust, int *clust)
{
	clst->nclusters = nclust;
	clst->clusters = calloc(nclust, sizeof(struct cluster));
	int i, j;
	for (i = 0; i < natoms; ++i)
		(clst->clusters[clust[i]].natoms)++;
	for (i = 0; i < nclust; ++i) {
		clst->clusters[i].iatom =
		    malloc((clst->clusters[i].natoms) * sizeof(int));
		clst->clusters[i].natoms = 0;
	}
	for (i = 0; i < natoms; ++i) {
		j = clst->clusters[clust[i]].natoms;
		clst->clusters[clust[i]].iatom[j] = i;
		clst->clusters[clust[i]].natoms++;
	}
}

//! Assign atoms to clusters based on three bonds connectivity.
/*! 1-4 clustering was performed the following way.
            1. All atoms were unassigned (flag -1) to any cluster.
            2. Loop through all atoms
            3. If atom is unassigned find all anassigned atoms connected to it through
               1-3 connection and save a list of all such connections (bonds)
            4. loop through the list of saved connections and for each calculate the
               number of unassigned atoms for which this connection is 2-3 connection.
               Connection with the maximum such number is the 2-3 connection of
               a new cluster.
            5. No restriction on carbon being 1 or 4 atom in the cluster was made
               (differ from the original published algorithm). */
void clust14(struct mol_atom_group *ag, int *nclust, int *clust)
{
	*nclust = 0;
	size_t npair;
	int ma, mapair, ipair;
	size_t *pairs = calloc(MAXPAIR * 2, sizeof(size_t));
	size_t k, m;

	/*All atoms were unassigned (flag -1) to any cluster. */
	for (size_t i = 0; i < ag->natoms; ++i) {
		clust[i] = -1;
	}
	/*Loop through all atoms. */
	for (size_t i = 0; i < ag->natoms; ++i) {
		if (clust[i] != -1)
			continue;

		npair = 0;

		/* If atom is unassigned find all anassigned atoms connected to it through
		   1-3 connection and save a list of all such connections (bonds) in an array
		   pairs. */
		for (size_t j = 0; j < ag->bond_lists[i].size; ++j) {
			// for each bond, add target to pairs
			// for each target, add its targets to pairs
			// exclude original

			k = mol_atom_group_get_neighbor(ag, i, j);
			if (clust[k] != -1)
				continue;

			addpair(i, k, &npair, pairs);
			for (size_t l = 0; l < ag->bond_lists[j].size; ++l) {
				m = mol_atom_group_get_neighbor(ag, j, l);
				if (m == i)
					continue;

				if (clust[m] == -1)
					addpair(k, m, &npair, pairs);
			}
		}

		if (npair == 0)
			clust[i] = *nclust;
		else {
			mapair = 2;
			ipair = 0;
			/* loop through the list of saved connections and for each calculate the
			   number of unassigned atoms for which this connection is 2-3 connection.
			   Connection with the maximum such number is the 2-3 connection of
			   a new cluster. */
			for (size_t j = 0; j < npair; ++j) {
				/* return the number of atoms in 1-4 cluster for a given 2-3 connection. */
				ma = natpair(ag, &pairs[2 * j], clust);
				if (ma > mapair) {
					ipair = j;
					mapair = ma;
				}
			}
			/* Assign cluster number to the atom indices in the clust array. */
			addclust(ag, &pairs[2 * ipair], clust, *nclust);
		}
		(*nclust)++;
	}

	free(pairs);

	for (size_t i = 0; i < ag->natoms; ++i) {
		// I'm not sure why, but this sometimes indeed happens when the coordinates are NaN or otherwise bad.
		if (clust[i] == -1) {
			_PRINTF_ERROR("Clustering for atom #%zu failed. Possible bad coordinates?", i);
			exit(EXIT_FAILURE);
		}
	}
}

//! Increase the list of atom pairs
/*! used in clust14 function. */
static void addpair(int i, int k, size_t *npair, size_t *pairs)
{
	size_t l = *npair;
	if (l + 1 > MAXPAIR) {
		_PRINTF_ERROR("Increase MAXPAIR in clust14 (currently: %d, need at least %zu)", MAXPAIR, l + 1);
		exit(EXIT_FAILURE);
	}
	pairs[2 * l] = i;
	pairs[2 * l + 1] = k;
	(*npair) += 1;
}

//! Return the number of atoms in 1-4 cluster for a given 2-3 connection.
/* used in clust14 function. */
static int natpair(const struct mol_atom_group *ag, const size_t *pair, const int *clust)
{
	int n = 0;
	size_t a1 = pair[0];
	size_t a2 = pair[1];
	size_t k;

	for (size_t j = 0; j < ag->bond_lists[a1].size; ++j) {
		k = mol_atom_group_get_neighbor(ag, a1, j);
		if (clust[k] == -1)
			n++;
	}
	for (size_t j = 0; j < ag->bond_lists[a2].size; ++j) {
		k = mol_atom_group_get_neighbor(ag, a2, j);
		if (clust[k] == -1)
			n++;
	}
	return n;
}

//! Assign cluster number to the atom indices in the clust array.
/* used in clust14 function. */

static void addclust(const struct mol_atom_group *ag, const size_t *pair, int *clust, int nclust)
{
	const size_t a1 = pair[0];
	const size_t a2 = pair[1];

	for (size_t j = 0; j < ag->bond_lists[a1].size; ++j) {
		size_t k = mol_atom_group_get_neighbor(ag, a1, j);
		if (clust[k] == -1)
			clust[k] = nclust;
	}
	for (size_t j = 0; j < ag->bond_lists[a2].size; ++j) {
		size_t k = mol_atom_group_get_neighbor(ag, a2, j);
		if (clust[k] == -1)
			clust[k] = nclust;
	}
}

//! Calculate a one dimensional bonded list list01 for the atomgroup ag.
/*! na01[i] - number of bonded atoms for the atom i
    list01 - sequential blocks of numbers of bonded atoms
    pna01[i] - pointer to the block of bonded atoms in list01 for the atom i */
void comp_list01(struct mol_atom_group *ag, int *list01, int *na01, int **pna01)
{
	for (size_t i = 0; i < ag->natoms; ++i) {
		na01[i] = 0;

		const size_t nb = ag->bond_lists[i].size;
		pna01[i] = list01;
		for (size_t j = 0; j < nb; ++j) {
			*list01 = mol_atom_group_get_neighbor(ag, i, j);
			na01[i]++;
			list01++;
		}
		if (na01[i] == 0)
			pna01[i] = NULL;
	}
}

//! Calculate a total number of bonded triplets of atoms n02 and quadruplets n03.
/*! Based on number of connections na01 and an array of pointers pna01 to the list01. */
void comp_n23(int natoms, int *na01, int **pna01, int *n02, int *n03)
{
	int i, j, k, l, m, *p;
	*n02 = 0;
	*n03 = 0;
	for (i = 0; i < natoms; ++i) {
		j = na01[i];
		if (j > 1) {
			(*n02) += j * (j - 1) / 2;
			p = pna01[i];
			for (k = 0; k < j; k++) {
				l = p[k];
				if (l > i) {
					m = na01[l];
					if (m > 1)
						(*n03) += (j - 1) * (m - 1);
				}
			}
		}
	}
}

int trim_comp(void *s1, void *s2)
{
	int *v1, *v2;
	v1 = (int *)s1;
	v2 = (int *)s2;

	int i11 = *v1;
	int i21 = *v2;
	if (i11 < i21)
		return -1;
	if (i11 > i21)
		return 1;

	int i12 = *(v1 + 1);
	int i22 = *(v2 + 1);
	if (i12 < i22)
		return -1;
	if (i12 > i22)
		return 1;
	return 0;
}

//! Calculate a list of atoms connected by two bonds list02.
/*! Based on number of connections na01 and an array of pointers pna01 to the list01.
    na02[i] number of atom pairs connected by two bonds with the smallest atom index i
    na02[i]=0 for some i
    pna02[i] pointer to the block in list02 associated with the smallest atom index i
    pna02[i]=NULL for some i.*/
void comp_list02(int natoms, int *na01, int **pna01,
		 int *na02, int **pna02, int *n02, int *list02)
{
	int i, j, k, l, m, n, *p;
	int *list = list02;

	// First, naively fill the list with all pairs of atoms that have two covalent bonds between them.
	for (i = 0; i < natoms; ++i) { // Loop over all atoms
		j = na01[i];
		if (j > 1) {
			p = pna01[i];
			for (k = 0; k < j - 1; k++) { // Loop over all neighbors of atom i
				n = p[k];
				for (l = k + 1; l < j; l++) { // Loop over all other neighbors of atom i
					m = p[l];
					// Since both n and m are neighbors of i, they are connected by two covalent bonds.
					if (m > n) { // Make each pair in list02 is ordered
						*list02 = n;
						list02++;
						*list02 = m;
						list02++;
					} else {
						*list02 = m;
						list02++;
						*list02 = n;
						list02++;
					}
				}
			}
		}
	}

	// Remove self-interactions and atoms that are already 0-1 neighbors
	// We set invalid pairs to "natoms" and later will "garbage collect" them.
	for (i = 0; i < *n02; ++i) {
		j = list[2 * i];
		k = list[2 * i + 1];
		if (j == k) {  // Self interaction
			// While it's unlikely that this happens, it's better to make sure we handle such things. Just in case.
			// The whole lists code is a bit messy and convoluted. Better be safe than sorry.
			list[2 * i] = natoms;
			list[2 * i + 1] = natoms;
			continue;
		}
		l = na01[j];
		if (l > 0) {
			p = pna01[j];
			for (m = 0; m < l; m++) {
				n = p[m];
				if (n == k) { // j and k are 0-1 neighbors
					list[2 * i] = natoms;
					list[2 * i + 1] = natoms;
					break;
				}
			}
		}
	}
	qsort(list, *n02, 2 * sizeof(list[0]), (void *)trim_comp);

	// Remove redundant pairs and then re-sort array
	for (i = 1; i < *n02; ++i) {
		j = i - 1;
		// Redundant pairs
		if (list[2 * i] == list[2 * j] && list[2 * i + 1] == list[2 * j + 1]) {
			// We process array from left to right, so we delete leftmost element
			list[2 * j] = natoms;
			list[2 * j + 1] = natoms;
		}
	}
	qsort(list, *n02, 2 * sizeof(list[0]), (void *)trim_comp);

	// Now, go from right to left and remove invalid pairs
	j = (*n02) - 1;
	for (i = j; i >= 0; i--) {
		if (list[2 * i] == natoms)
			(*n02)--;
		else
			break;
	}

	// Fill na02 and pna02 with per-atom 0-2 lists
	for (i = 0; i < natoms; ++i) {
		na02[i] = 0;
		pna02[i] = NULL;
	}
	for (i = 0; i < *n02; ++i) {
		j = list[2 * i];
		(na02[j])++;
		if (i == 0 || j > list[2 * i - 2])
			pna02[j] = &(list[2 * i]);
	}
}

//! Calculate list of atom pairs connected by three bonds list03.
/*! It is redundant so far. */
void comp_list03(int natoms, int *na01, int **pna01, int *n03, int *list03)
{
	int i, j, k, l, m, n, q, r, s, *p, *o;
	// We have to update n03 here, because rough calculation in comp_n23 does not account outright exclusions (q == i or r == k) done below
	int cur_n03 = 0;
	for (i = 0; i < natoms; ++i) {
		j = na01[i];
		if (j > 1) {
			p = pna01[i];
			for (k = 0; k < j; k++) {
				l = p[k];
				if (l > i) {
					m = na01[l];
					if (m > 1) {
						o = pna01[l];
						for (n = 0; n < m; n++) {
							q = o[n];
							if (q == i)
								continue;
							for (r = 0; r < j; r++) {
								if (r == k)
									continue;
								s = p[r];
								if (cur_n03 >= *n03) {
									// This should not happen, but a short sanity-check never hurts.
									_PRINTF_ERROR("Underestimated the size of 0-3 list: expected %d, found at least %d", *n03, cur_n03+1);
									exit(EXIT_FAILURE);
								}
								// There are possible redundancies here (including self-interaction).
								// They are later removed in trim_list03.
								if (q > s) {
									*list03 = s;
									list03++;
									*list03 = q;
									list03++;
								} else {
									*list03 = q;
									list03++;
									*list03 = s;
									list03++;
								}
								cur_n03 += 1;
							}
						}
					}
				}
			}
		}
	}
	*n03 = cur_n03;
}

//! Create list listf03 from list03 by removing pairs of "fixed" atoms
/*! nf03 is a number of nonfixed pairs */
void fix_list03(struct mol_atom_group *ag, int n03, int *list03, int *nf03,
		int *listf03)
{
	int i, i1, i2;
	int n = 0;

	for (i = 0; i < n03; ++i) {
		i1 = list03[2 * i];
		i2 = list03[2 * i + 1];

		if (ag->fixed[i1] && ag->fixed[i2])
			continue;

		listf03[2 * n] = i1;
		listf03[2 * n + 1] = i2;
		n++;
	}
	*nf03 = n;
}

//! Remove redundancies from list03.
/*! Reset the number n03 */
void trim_list03(int natoms,
		 int *na01, int **pna01,
		 int *na02, int **pna02, int *n03, int *list03)
{
	qsort(list03, *n03, 2 * sizeof(list03[0]), (void *)trim_comp);
	int i, j, k, l, m, n, o, *p;
	int *list = calloc(*n03, sizeof(int));

	for (i = 0; i < *n03; ++i) {
		j = 2 * i;
		k = list03[j];
		l = list03[j + 1];
		// Self-interacting
		if (k == l) {
			list[i] = 1;
			continue;
		}
		// Redundant pairs
		if (i > 0 && k == list03[j - 2] && l == list03[j - 1]) {
			list[i] = 1;
			continue;
		}
		// Directly bonded pairs
		m = na01[k];
		if (m > 0) {
			p = pna01[k];
			for (n = 0; n < m; n++) {
				o = p[n];
				if (l == o) {
					list[i] = 1;
					continue;
				}
			}
		}
		// Bonded through an angle pairs
		m = na02[k];
		if (m > 0) {
			p = pna02[k];
			for (n = 0; n < m; n++) {
				o = p[2 * n + 1];
				if (l == o) {
					list[i] = 1;
					continue;
				}
			}
		}
	}
	for (i = 0; i < *n03; ++i) {
		if (list[i] == 1) {
			j = 2 * i;
			list03[j] = natoms;
			list03[j + 1] = natoms;
		}
	}
	qsort(list03, *n03, 2 * sizeof(list03[0]), (void *)trim_comp);
	j = *n03 - 1;
	for (i = j; i >= 0; i--) {
		if (list03[2 * i] == natoms)
			(*n03)--;
		else
			break;
	}
	free(list);
}

//! Evaluate dimensions of the exclusion list.
/*! The dimensions of the exclusion
    list relate to how close the connected atoms are
    in terms of their numbers. */
void excl_dims(int natoms, int *na01, int **pna01,
	       int n02, int *list02, int n03, int *list03,
	       int *nd1, int *nd2, int *ndm, int *atmind)
{
	int i, j, k, l, *p;
	for (i = 0; i < 2 * natoms; ++i)
		atmind[i] = 0;
	*ndm = 20;
	*nd2 = 0;
	*nd1 = 0;
	for (i = 0; i < natoms; ++i) {
		p = pna01[i];
		for (k = 0; k < na01[i]; k++) {
			j = p[k];
			l = j - i;
			if (l > *ndm)
				*ndm = l;
			l--;
			if (l > 19 && atmind[2 * i + 1] == 0) {
				atmind[2 * i + 1] = 1;
				(*nd2)++;
			} else if (l > 9 && atmind[2 * i] == 0) {
				atmind[2 * i] = 1;
				(*nd1)++;
			}
		}
	}
	for (i = 0; i < n02; ++i) {
		j = list02[2 * i];
		k = list02[2 * i + 1];
		l = k - j;
		if (l > *ndm)
			*ndm = l;
		l--;
		if (l > 19 && atmind[2 * j + 1] == 0) {
			atmind[2 * j + 1] = 1;
			(*nd2)++;
		} else if (l > 9 && atmind[2 * j] == 0) {
			atmind[2 * j] = 1;
			(*nd1)++;
		}
	}
	for (i = 0; i < n03; ++i) {
		j = list03[2 * i];
		k = list03[2 * i + 1];
		l = k - j;
		if (l > *ndm) {
			*ndm = l;
//                   printf("ndm %d %d %d\n",j,k,*ndm);
		}
		l--;
		if (l > 19 && atmind[2 * j + 1] == 0) {
			atmind[2 * j + 1] = 1;
			(*nd2)++;
		} else if (l > 9 && atmind[2 * j] == 0) {
			atmind[2 * j] = 1;
			(*nd1)++;
		}
	}
}

//! Evaluate if a pair of atoms should be excluded from nonbonded list.
/*! Based on connectivity data in exclusion list excl_list/pd1/pd2. */
inline int exta(int a1, int a2, int *excl_list, int **pd1, int **pd2, int ndm)
{
	int i = a2 - a1 - 1;
	if (i < 0) {
		_PRINTF_ERROR("a1 <= a2: %d <= %d", a1, a2);
		exit(EXIT_FAILURE);
	}
	if (i >= ndm)
		return 0;
	if (i < 10)
		return excl_list[10 * a1 + i];
	int *p;
	if (i < 20) {
		p = pd1[a1];
		return p[i - 10];
	}
	p = pd2[a1];
	return p[i - 20];
}

/*
#ifdef _BGL_

// assuming ndm <= 20
int exta2( int a1, int a2, int *excl_list, int **pd1, int ndm )
{
    int i = a2 - a1 - 1;

    if ( i >= ndm ) return 0;

    if ( i < 10 ) return excl_list[ ( a1 << 3 ) + ( a1 << 1 ) + i ];
    else return pd1[ a1 ][ i - 10 ];
}

#endif
*/

//! Create an exclusion list (excl_list).
/*! Exclusion list is a compact array of numbers coding connected through 1,
    2, or 3 bonds pairs of atoms. It is used later by the function exta() to
    quickly evaluate if a pair is excluded. The compactness of the exclusion
    list relates to the convention, that connected atoms are generally close
    to each other by the numbering. */
void excl_tab(int natoms, int *na01, int **pna01,
	      int n02, int *list02, int n03, int *list03,
	      int nd1, int nd2, int ndm, int *atmind,
	      int *excl_list, int **pd1, int **pd2)
{
	int i, j, k, l, *p, *p1;
	int md1 = -1, md2 = -1;
// zero the exclusion list
	i = (natoms + nd1 + 1) * 10 + (nd2 + 1) * (ndm - 20);
	for (j = 0; j < i; ++j)
		excl_list[j] = 0;
// populate atom pointers to the exclusion list
	for (i = 0; i < natoms; ++i) {
		if (atmind[2 * i] == 1)
			pd1[i] = &(excl_list[10 * (++md1 + natoms)]);
		else
			pd1[i] = &(excl_list[10 * (nd1 + natoms)]);
		if (atmind[2 * i + 1] == 1)
			pd2[i] =
			    &(excl_list
			      [10 * (natoms + nd1 + 1) + (ndm - 20) * (++md2)]);
		else
			pd2[i] =
			    &(excl_list
			      [10 * (natoms + nd1 + 1) + (ndm - 20) * nd2]);
	}
	for (i = 0; i < natoms; ++i) {
		p = pna01[i];
		for (k = 0; k < na01[i]; k++) {
			j = p[k];
			l = j - i - 1;
			if (l > 19) {
				p1 = pd2[i];
				p1[l - 20] = 1;
			} else if (l > 9) {
				p1 = pd1[i];
				p1[l - 10] = 1;
			} else if (l >= 0) {
				excl_list[10 * i + l] = 1;
			}
		}
	}
	for (i = 0; i < n02; ++i) {
		j = list02[2 * i];
		k = list02[2 * i + 1];
		l = k - j - 1;
		if (l > 19) {
			p1 = pd2[j];
			p1[l - 20] = 2;
		} else if (l > 9) {
			p1 = pd1[j];
			p1[l - 10] = 2;
		} else if (l >= 0) {
			excl_list[10 * j + l] = 2;
		}
	}
	for (i = 0; i < n03; ++i) {
		j = list03[2 * i];
		k = list03[2 * i + 1];
		l = k - j - 1;
		if (l > 19) {
			p1 = pd2[j];
			p1[l - 20] = 3;
		} else if (l > 9) {
			p1 = pd1[j];
			p1[l - 10] = 3;
		} else if (l >= 0) {
			excl_list[10 * j + l] = 3;
		}
	}
}

//free nblist arrays in agsetup
void destroy_agsetup(struct agsetup *ags)
{
	free(ags->excl_list);
	free(ags->pd1);
	free(ags->pd2);
	free(ags->listf03);
	free(ags->list03);
	free(ags->list02);
	free_nblist(ags->nblst);
	free_clset(ags->clst);
	free(ags->clst);
}

void free_agsetup(struct agsetup *ags)
{
	destroy_agsetup(ags);
	free(ags);
}

//Setup nblist calculation
//! Initialize a nonbonded list data structure ags for the atomgroup ag.
/*! Nonbonded list generation involves initialization step performed once based on
    the atom connectivity in this function and also update step covered by update_nblst
    function using spatial positions of atoms. The algorithm is described at:
    Petrella et all, "An Improved Method For Nonbonded List Generation: Rapid Determination
    of Near-Neighbour Pairs" J. Comp. Chem. 2002, v.24 pp. 222-231.
    The algorithm involves assignement of atoms to clusters connected by at most 3 bonds and
    distribution of the clusters between cubes of nonbonded cutoff length with some padding.
    The speedup of the list generation is achieved by searching only neighbouring cubes and
    by using mostly cluster-cluster distances instead of atom-atom ones.
    */
void init_nblst(struct mol_atom_group *ag, struct agsetup *ags)
{
	if (ag == NULL || ag->natoms == 0) {
		_PRINTF_ERROR("Can not create nblist for uninitialized or empty atom group.");
		exit(EXIT_FAILURE);
	}
	if (ag->natoms >= INT32_MAX / 2) {
		// Because we certainly have linear list01 of up to 2*natoms indexed by int32
		_PRINTF_ERROR("Non-bonded lists can not be used for more than %d atoms: we have %zu.", INT32_MAX/2, ag->natoms);
		exit(EXIT_FAILURE);
	} else if (ag->natoms >= 46340) {
		// Approximately sqrt(INT32_MAX). We don't explicitly use n^2 arrays, but we might get there worst case.
		_PRINTF_WARNING("Atom group has too many atoms. Non-bonded lists may work, but are prone to overflows.");
	}
	int *list01 = calloc(2 * (ag->nbonds), sizeof(int));
	int *na01 = calloc((ag->natoms), sizeof(int));
	int **pna01 = calloc((ag->natoms), sizeof(int *));
	int *na02 = calloc((ag->natoms), sizeof(int));
	int **pna02 = calloc((ag->natoms), sizeof(int *));

	// Calculate a one dimensional bonded list list01 for the atomgroup ag.
	/* na01[i] - number of bonded atoms for the atom i
	   list01 - sequential blocks of numbers of bonded atoms
	   pna01[i] - pointer to the block of bonded atoms in list01 for the atom i. */
	comp_list01(ag, list01, na01, pna01);
	int i;

	int n02, n03, nf03;
	// Calculate a total number of bonded triplets of atoms n02 and quadruplets n03.
	/* Based on number of connections na01 and an array of pointers pna01 to the list01. */
	comp_n23(ag->natoms, na01, pna01, &n02, &n03);
	int *list02 = malloc(2 * n02 * sizeof(int));
	int *list03 = malloc(2 * n03 * sizeof(int));
	int *listf03 = malloc(2 * n03 * sizeof(int));
	// Calculate a list of atoms connected by two bonds list02.
	/* Based on number of connections na01 and an array of pointers pna01 to the list01.
	   na02[i] number of atom pairs connected by two bonds with the smallest atom index i
	   na02[i]=0 for some i
	   pna02[i] pointer to the block in list02 associated with the smallest atom index i
	   pna02[i]=NULL for some i. */
	comp_list02(ag->natoms, na01, pna01, na02, pna02, &n02, list02);
	// Calculate list of atom pairs connected by three bonds list03.
	/* It is redundant so far. */
	comp_list03(ag->natoms, na01, pna01, &n03, list03);
	//! Remove redundancies from list03.
	/*! Reset the number n03. */
	trim_list03(ag->natoms, na01, pna01, na02, pna02, &n03, list03);
	// Create list listf03 from list03 by removing pairs of "fixed" atoms
	/* nf03 is a number of nonfixed pairs. */
	fix_list03(ag, n03, list03, &nf03, listf03);
	if (nf03 > 0)
		listf03 = realloc(listf03, 2 * nf03 * sizeof(int));
	else {
		free(listf03);
		listf03 = NULL;
	}
	int nd1, nd2, ndm;
	int *atmind = malloc(2 * (ag->natoms) * sizeof(int));
	// Evaluate dimensions of the exclusion list.
	/* The dimensions of the exclusion
	   list relate to how close the connected atoms are
	   in terms of their numbers. */
	excl_dims(ag->natoms, na01, pna01,
		  n02, list02, n03, list03, &nd1, &nd2, &ndm, atmind);

	i = 10 * ((ag->natoms) + nd1 + 1) + (nd2 + 1) * (ndm - 20);
	int *excl_list = malloc(i * sizeof(int));
	int **pd1 = (int **)malloc((ag->natoms) * sizeof(int *));
	int **pd2 = (int **)malloc((ag->natoms) * sizeof(int *));
	// Create an exclusion list (excl_list).
	/* Exclusion list is a compact array of numbers coding connected through 1,
	   2, or 3 bonds pairs of atoms. It is used later by the function exta() to
	   quickly evaluate if a pair is excluded. The compactness of the exclusion
	   list relates to the convention, that connected atoms are generally close
	   to each other by the numbering. */
	excl_tab(ag->natoms, na01, pna01,
		 n02, list02, n03, list03,
		 nd1, nd2, ndm, atmind, excl_list, pd1, pd2);
	// Pack connectivity data into nonbonded list ags structure.
	/* Assemble all lists pointers and dimensions. */
	ags->list02 = list02;
	ags->list03 = list03;
	ags->listf03 = listf03;
	ags->n02 = n02;
	ags->n03 = n03;
	ags->nf03 = nf03;
	ags->ndm = ndm;
	ags->excl_list = excl_list;
	ags->pd1 = pd1;
	ags->pd2 = pd2;

	// Free temporal storage and data.
	/* 01 and 02 lists and temporal atmind go. */
	free(pna01);
	free(na01);
	free(list01);

	free(pna02);
	free(na02);
	free(atmind);
//***************Generating cluster------------------------
	int *clust = malloc((ag->natoms) * sizeof(int));
	int nclust;
	// Assign atoms to clusters based on three bonds connectivity.
	/* 1-4 clustering was performed the following way.
	   1. All atoms were unassigned (flag -1) to any cluster.
	   2. Loop through all atoms
	   3. If atom is unassigned find all anassigned atoms connected to it through
	   1-3 connection and save a list of all such connections (bonds)
	   4. loop through the list of saved connections and for each calculate the
	   number of unassigned atoms for which this connection is 2-3 connection.
	   Connection with the maximum such number is the 2-3 connection of
	   a new cluster.
	   5. No restriction on carbon being 1 or 4 atom in the cluster was made
	   (differ from the original published algorithm). */

	clust14(ag, &nclust, clust);

	struct clusterset *clst = malloc(sizeof(struct clusterset));

	// Convert an array clust to a structure clusterset clst.
	gen_clset(ag->natoms, clst, nclust, clust);
	ags->clst = clst;
//**********************free****************************************
	free(clust);

	struct nblist *nblst = malloc(sizeof(struct nblist));
	double nbcut = 13.0;
	double nbcof = 12.0;
	nblst->nbcut = nbcut;
	nblst->nbcof = nbcof;
	nblst->crds = malloc(3 * (ag->natoms) * sizeof(float));
	nblst->nfat = 0;
	nblst->ifat = malloc((ag->natoms) * sizeof(int));
	nblst->nsat = malloc((ag->natoms) * sizeof(int));
	for (size_t j = 0; j < ag->natoms; ++j)
		nblst->nsat[j] = 0;
	nblst->isat = malloc((ag->natoms) * sizeof(int *));
	ags->nblst = nblst;
}

//! Spatial part of the nblist generation altorithm
/*! See comments to init_nblst function. This part is performed
    periodically when atom coordinates change sufficiently.
    The steps of the function are (1) Save snapshot of coordinates at the update stage.
    (2) Calculate the margin size of future cubes. (3) Generate a set of cubes cust based
    on the clusterset ags->clst (4) Generate a nonbonded list nblst from cluster and cube
    subdivisions and exclusion list arrays. */
void update_nblst(struct mol_atom_group *ag, struct agsetup *ags)
{
	size_t i;
	/* save snapshot of coordinates at the update stage. */
	for (i = 0; i < ag->natoms; ++i) {
		ags->nblst->crds[3 * i] = ag->coords[i].X;
		ags->nblst->crds[3 * i + 1] = ag->coords[i].Y;
		ags->nblst->crds[3 * i + 2] = ag->coords[i].Z;
	}
	// Access margin size of future cubes.
	/* sum of nblist cutoff and margin size gives the cubesize. */
	findmarg(ag, ags->clst);

	struct cubeset *cust = malloc(sizeof(struct cubeset));
	// Generate a set of cubes cust based on the clusterset ags->clst.
	/* 0. sum of nblist cutoff and margin size gives the cubesize.
	   1. calculate coordinate span of cluster centers
	   2. number of cubes in each direction is span over cubesize plus 1
	   3. for each cluster find a cube it belongs to.
	   4. each cube knows last cluster contained in it.
	   5. each cluster knows another cluster belongin to the same cube (linked list)
	   6. each cube knows its occupied neighbouring cubes. */
	gen_cubeset(ags->nblst->nbcut, ags->clst, cust);
	// Generate a nonbonded list nblst from cluster and cube subdivisions and exclusion list arrays.
	/* To generate nblist
	   1. loop over the filled cubes
	   2. loop over linked clusters in the first cube
	   3. loop over neighboring cubes including first cube.
	   4. loop through the linked list clusters of the second cube.
	   5. go only for pairs of clusters which are closer than an nbcut plus lagest distance from any atom to the
	   geometry center of cluster 1 plus the largest distance of the cluster 2
	   6. for the pair of clusters icl1 and icl2 go through all pairs of their atoms
	   7. write the pair to the list if the distance between clusters is less than nbcut minus largest dist of
	   cluster one minus largest dist of cluster two.
	   8. check the atomic pair distance  if the condition in the step 7 is not true
	   9. write the pair if distance less than nonbonded cutoff.

	   The structure of the nblist is:
	   number of first atoms
	   array of first atom indices
	   array of numbers of second atoms for each first one
	   array of pointers to arrays of all second atoms
	   arrays of all second atoms. */
	gen_nblist(ag, cust, ags->clst, ags->excl_list, ags->pd1, ags->pd2,
		   ags->ndm, ags->nblst);
	free_cubeset(cust);
	free(cust);

}


void mol_nblst_update_fixed(struct mol_atom_group *ag, struct agsetup *ags)
{
	update_nblst(ag, ags);
	ags->listf03 = realloc(ags->listf03, 2 * ags->n03 * sizeof(int));
	fix_list03(ag, ags->n03, ags->list03, &ags->nf03, ags->listf03);
	if (ags->nf03 > 0)
		ags->listf03 = realloc(ags->listf03, 2 * ags->nf03 * sizeof(int));
	else {
		free(ags->listf03);
		ags->listf03 = NULL;
	}
}

//Checks wether cluster needs update
bool check_clusterupdate(struct mol_atom_group *ag, struct agsetup *ags)
{
	size_t j;
	bool flag = false;
	double delta0, delta;
	delta0 = fabs(ags->nblst->nbcut - ags->nblst->nbcof) / 2.0;
	delta0 = delta0 * delta0;
	for (size_t i = 0; i < ag->active_atoms->size; ++i) {
		j = ag->active_atoms->members[i];
		delta =
		    (ags->nblst->crds[3 * j] -
		     ag->coords[j].X) * (ags->nblst->crds[3 * j] -
					ag->coords[j].X) +
		    (ags->nblst->crds[3 * j + 1] -
		     ag->coords[j].Y) * (ags->nblst->crds[3 * j + 1] -
					ag->coords[j].Y) +
		    (ags->nblst->crds[3 * j + 2] -
		     ag->coords[j].Z) * (ags->nblst->crds[3 * j + 2] -
					ag->coords[j].Z);
		delta = delta * delta;
		if (delta > delta0) {
			flag = true;
			break;
		}
	}
	if (flag) {
		update_nblst(ag, ags);
		return true;
	} else {
		return false;
	}
}

void give_012(struct mol_atom_group *ag, struct agsetup *ags,
	      int *na012, int **la012, int *nf012, int **lf012)
{
	size_t a0, a1;
	int ia0, ia1;
	int n01 = ag->nbonds;
	int n02 = ags->n02;
	int n = n01 + n02;
	int na = 0;
	int nf = 0;

	if (n > 0) {
		*la012 = malloc(2 * n * sizeof(int));
		*lf012 = malloc(2 * n * sizeof(int));
		int i;
		for (i = 0; i < n01; ++i) {
			a0 = ag->bonds[i].ai;
			a1 = ag->bonds[i].aj;

			if (!ag->fixed[a0] || !ag->fixed[a1]) {
				*la012[2 * na] = a0;
				*la012[2 * na + 1] = a1;
				na++;
			} else {
				*lf012[2 * nf] = a0;
				*lf012[2 * nf + 1] = a1;
				nf++;
			}
		}
		for (i = 0; i < n02; ++i) {
			ia0 = ags->list02[2 * i];
			ia1 = ags->list02[2 * i + 1];
			if (!ag->fixed[ia0] || !ag->fixed[ia1]) {
				*la012[2 * na] = ia0;
				*la012[2 * na + 1] = ia1;
				na++;
			} else {
				*lf012[2 * nf] = ia0;
				*lf012[2 * nf + 1] = ia1;
				nf++;
			}
		}
		if (na > 0)
			*la012 = realloc(*la012, 2 * na * sizeof(int));
		else {
			free(*la012);
			*la012 = NULL;
		}
		if (nf > 0)
			*lf012 = realloc(*lf012, 2 * nf * sizeof(int));
		else {
			free(*lf012);
			*lf012 = NULL;
		}
	}
	*na012 = na;
	*nf012 = nf;
}

/* void springeng(struct springset *sprst, double *spren) */
/* { */
/* 	int i, j, nat, lj; */
/* 	double xtot, ytot, ztot, fk; */
/* 	struct mol_atom_group *ag; */
/* 	for (i = 0; i < sprst->nsprings; ++i) { */
/* 		nat = sprst->springs[i].naspr; */
/* 		if (nat > 0) { */
/* 			xtot = 0.0; */
/* 			ytot = 0.0; */
/* 			ztot = 0.0; */
/* 			ag = sprst->springs[i].agp; */
/* 			for (j = 0; j < nat; ++j) { */
/* 				lj = sprst->springs[i].laspr[j]; */
/* 				xtot += ag->atoms[lj].X; */
/* 				ytot += ag->atoms[lj].Y; */
/* 				ztot += ag->atoms[lj].Z; */
/* 			} */
/* 			xtot = xtot / nat - sprst->springs[i].X0; */
/* 			ytot = ytot / nat - sprst->springs[i].Y0; */
/* 			ztot = ztot / nat - sprst->springs[i].Z0; */
/* 			fk = sprst->springs[i].fkspr; */
/* 			*spren += */
/* 			    fk * (xtot * xtot + ytot * ytot + ztot * ztot); */
/* 			fk = 2 * fk / nat; */
/* 			for (j = 0; j < nat; ++j) { */
/* 				lj = sprst->springs[i].laspr[j]; */
/* 				ag->atoms[lj].GX -= xtot * fk; */
/* 				ag->atoms[lj].GY -= ytot * fk; */
/* 				ag->atoms[lj].GZ -= ztot * fk; */
/* 			} */
/* 		} */
/* 	} */
/* } */

void modify_vdw_save(struct mol_atom_group *ag, int nmod, int *mod, double *enew,
		     double *rnew, double *eold, double *rold)
{
	int i;
	for (i = 0; i < nmod; ++i) {
		eold[i] = ag->eps[mod[i]];
		if (enew[i] <= 0)
			ag->eps[mod[i]] = sqrt(-enew[i]);
		rold[i] = ag->vdw_radius[mod[i]];
		ag->vdw_radius[mod[i]] = rnew[i];
	}

}

void modify_vdw_back(struct mol_atom_group *ag, int nmod, int *mod, double *eold,
		     double *rold)
{
	int i;
	for (i = 0; i < nmod; ++i) {
		ag->eps[mod[i]] = eold[i];
		ag->vdw_radius[mod[i]] = rold[i];
	}

}

void get_mod_vdw_all(double lambe, double lambr, struct mol_atom_group *ag, int *nmod,
		     int **mod, double **modeps, double **modrminh)
{
	int i;
	*nmod = ag->natoms;
	*mod = malloc(*nmod * sizeof(int));
	*modeps = malloc(*nmod * sizeof(double));
	*modrminh = malloc(*nmod * sizeof(double));
	for (i = 0; i < *nmod; ++i) {
		(*mod)[i] = i;
		(*modeps)[i] = lambe * (ag->eps[i]);
		(*modrminh)[i] = lambr * (ag->vdw_radius[i]);
	}
}
