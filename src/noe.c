#include "noe.h"
#include "private_utils.h"

#define __NOE_DMIN__ 1.0E-8
#define __NOE_DMIN2__ (__NOE_DMIN__ * __NOE_DMIN__)
#define __NOE_PEAK_MIN__ 1.0E-8


static int _number_of_atoms_in_lines(const char *path)
{
	FILE *f = fopen(path, "r");
	if (f == NULL) {
		_PRINTF_ERROR("File %s not found", path);
		return -1;
	}

	char str[100];
	int natoms = 0;
	while (fscanf(f, "%99s", str) == 1) natoms++;

	fclose(f);
	return natoms;
}


static int _number_of_lines(const char *path)
{
	FILE *f = fopen(path, "r");
	if (f == NULL) {
		_PRINTF_ERROR("File %s not found", path);
		return -1;
	}
	char str[1000];
	int i = 0;
	while (fgets(str, 1000, f) != NULL) i++;
	fclose(f);

	return i;
}


bool mol_noe_calc_relaxation_matrix(struct mol_noe *spect, const struct mol_atom_group *ag)
{
	struct mol_noe_group_list *grps = spect->groups;
	double *rx = spect->rx;
	const double omega = spect->omega;
	const double t_cor = spect->t_cor;

	if (rx == NULL || grps == NULL) {
		_PRINTF_ERROR("NULL pointer");
		return false;
	}

	size_t loc, loc1;
	const size_t size = grps->ngroups;
	const size_t natoms = grps->natoms;

	struct mol_vector3* grad = NULL;
	if (spect->rx_grad != NULL) {
		memset(spect->rx_grad->grad, 0, spect->rx_grad->natoms * size * size * sizeof(struct mol_vector3));
		grad = spect->rx_grad->grad;
	}

	const double J_0 = t_cor;
	const double J_1 = t_cor / (1.0 + 4.0 * (M_PI * M_PI) * omega * omega * t_cor * t_cor);  // *10^(-9) s
	const double J_2 = t_cor / (1.0 + 16.0 * (M_PI * M_PI) * omega * omega * t_cor * t_cor);  // *10^(-9) s

	const double gyr = 2.6751965; // e+8
	const double hbar = 1.0545919; // e-34;
	const double coef = pow(gyr, 4) * pow(hbar, 2);

	const double S0 = J_0 * coef; // 1/s
	const double S1 = 1.5 * J_1 * coef; // 1/s
	const double S2 = 6.0 * J_2 * coef; // 1/s

	struct mol_noe_group *grpi, *grpj;
	struct mol_vector3 buf;

	size_t atomi = 0, atomj;
	double *r2_ptr = spect->r2;

	for (size_t i = 0; i < size; i++) {
		grpi = &grps->groups[i];
		atomj = atomi;
		for (size_t j = i; j < size; j++) {
			grpj = &grps->groups[j];

			double r2;
			size_t counter = 0;
			double rx_val = 0.0;
			struct mol_vector3 *coordi, *coordj;
			size_t ptr1;

			if (i == j) {
				// Self relaxation for unresolved groups if i == j
				if (grpi->size > 1) {
					for (size_t i1 = 0; i1 < grpi->size; i1++) {
						ptr1 = natoms * (atomi + i1);
						for (size_t j1 = i1 + 1; j1 < grpj->size; j1++) {
							r2 = r2_ptr[ptr1 + atomj + j1];

							if (r2 > __NOE_DMIN2__) {
								rx_val += 1.0 / (r2 * r2 * r2);
								counter += 1;

								// accumulate gradient
								if (grad != NULL) {
									double one_over_r8 = 1.0 / (r2 * r2 * r2 * r2);

									coordi = &ag->coords[grpi->atoms[i1]];
									coordj = &ag->coords[grpj->atoms[j1]];

									loc = (atomi + i1) * size * size +
									      (i * size + j);
									MOL_VEC_SUB(buf, *coordi, *coordj);
									MOL_VEC_MULT_SCALAR(buf, buf, 6 * one_over_r8);
									MOL_VEC_SUB(grad[loc], grad[loc], buf);

									loc = (atomj + j1) * size * size +
									      (i * size + j);
									MOL_VEC_ADD(grad[loc], grad[loc], buf);
								}
							}
						}
					}

					rx_val *= 2 * (grpi->size - 1) * (S1 + S2) / counter;

					// times self relaxation
					if (grad != NULL) {
						for (size_t i1 = 0; i1 < grpi->size; i1++) {
							loc = (atomi + i1) * size * size + (i * size + j);
							MOL_VEC_MULT_SCALAR(
								grad[loc],
								grad[loc],
								2 * (grpi->size - 1) * (S1 + S2) / counter
								);

						}
					}
				}
			} else {
				// 6 average if i != j
				for (size_t i1 = 0; i1 < grpi->size; i1++) {
					ptr1 = natoms * (atomi + i1);
					for (size_t j1 = 0; j1 < grpj->size; j1++) {
						r2 = r2_ptr[ptr1 + atomj + j1];
						counter += 1;

						if ((r2 > __NOE_DMIN2__) && (r2 <= spect->cutoff * spect->cutoff)) {
							rx_val += 1.0 / (r2 * r2 * r2);

							// accumulate gradient
							if (grad != NULL) {
								coordi = &ag->coords[grpi->atoms[i1]];
								coordj = &ag->coords[grpj->atoms[j1]];

								double one_over_r8 = 1.0 / (r2 * r2 * r2 * r2);

								loc = (atomi + i1) * size * size + (i * size + j);
								MOL_VEC_SUB(buf, *coordi, *coordj);
								MOL_VEC_MULT_SCALAR(buf, buf, 6 * one_over_r8);
								MOL_VEC_SUB(grad[loc], grad[loc], buf);

								loc = (atomj + j1) * size * size + (i * size + j);
								MOL_VEC_ADD(grad[loc], grad[loc], buf);
							}
						}
					}
				}

				rx_val /= counter;

				if (grad != NULL) {
					for (size_t i1 = 0; i1 < grpi->size; i1++) {
						loc = (atomi + i1) * size * size + (i * size + j);
						MOL_VEC_DIV_SCALAR(grad[loc], grad[loc], counter);
					}

					for (size_t j1 = 0; j1 < grpj->size; j1++) {
						loc = (atomj + j1) * size * size + (i * size + j);
						MOL_VEC_DIV_SCALAR(grad[loc], grad[loc], counter);
					}
				}
			}
			rx[size * i + j] = rx_val;
			rx[size * j + i] = rx_val;

			atomj += grpj->size;
		}

		atomi += grpi->size;
	}

	// Fill diagonal elements in R
	double mult = S0 + 2 * S1 + S2;

	for (size_t i = 0; i < size; i++) {
		double diag_val = 0.0;
		for (size_t j = 0; j < size; j++) {
			if (i != j) {
				diag_val += mult * grps->groups[j].size * rx[i * size + j];
			}
		}
		rx[i * size + i] += diag_val;
	}

	// Gradient
	if (grad != NULL) {
		for (size_t k = 0; k < grps->natoms; k++) {
			loc = k * size * size;
			for (size_t i = 0; i < size; i++) {
				for (size_t j = 0; j < size; j++) {
					if (i != j) {
						double mult1 = mult * grps->groups[j].size;

						if (i < j) {
							loc1 = (i * size + j);
						} else {
							loc1 = (j * size + i);
						}

						MOL_VEC_MULT_SCALAR(buf, grad[loc + loc1], mult1);
						MOL_VEC_ADD(grad[loc + (i * size + i)], grad[loc + (i * size + i)], buf);
					}
				}
			}
		}
	}


	// Fill the rest of R
	mult = S2 - S0;
	for (size_t i = 0; i < size - 1; i++) {
		for (size_t j = i + 1; j < size; j++) {
			rx[i * size + j] = mult * sqrt(grps->groups[i].size * grps->groups[j].size) * rx[i * size + j];
			rx[j * size + i] = rx[i * size + j];
		}
	}

	// Gradient
	if (grad != NULL) {
		int loc2;
		for (size_t k = 0; k < grps->natoms; k++) {
			loc = k * size * size;
			for (size_t i = 0; i < size - 1; i++) {
				for (size_t j = i + 1; j < size; j++) {
					double mult1 = mult * sqrt(grps->groups[i].size * grps->groups[j].size);

					loc1 = i * size + j;
					loc2 = j * size + i;
					MOL_VEC_MULT_SCALAR(grad[loc + loc1], grad[loc + loc1], mult1);
					grad[loc + loc2] = grad[loc + loc1];
				}
			}
		}
	}

	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			if (isnan(rx[i * size + j])) {
				_PRINTF_ERROR("Relaxation matrix contains NaN at (%zu, %zu)", i, j);
				return false;
			}
		}
	}
	return true;
}


static void _mol_noe_gsl_workspace_init(struct mol_noe_gsl_workspace *space, size_t size)
{
	space->size = size;
	space->eval = gsl_vector_alloc(size);
	space->evec = gsl_matrix_alloc(size, size);
	space->w = gsl_eigen_symmv_alloc(size);
	space->expeval = calloc(size, sizeof(double));
	space->pks2 = calloc(size * size, sizeof(double));
}


static struct mol_noe_gsl_workspace *_mol_noe_gsl_workspace_create(size_t size)
{
	struct mol_noe_gsl_workspace *space = calloc(1, sizeof(struct mol_noe_gsl_workspace));
	_mol_noe_gsl_workspace_init(space, size);
	return space;
}


static void _mol_noe_gsl_workspace_destroy(struct mol_noe_gsl_workspace *space)
{
	if (space != NULL) {
		if (space->eval != NULL) {
			gsl_vector_free(space->eval);
		}
		space->eval = NULL;

		if (space->evec != NULL) {
			gsl_matrix_free(space->evec);
		}
		space->evec = NULL;

		if (space->w != NULL) {
			gsl_eigen_symmv_free(space->w);
		}
		space->w = NULL;

		free_if_not_null(space->expeval);
		space->expeval = NULL;
		free_if_not_null(space->pks2);
		space->pks2 = NULL;
	}
}


static void _mol_noe_gsl_workspace_free(struct mol_noe_gsl_workspace *space)
{
	_mol_noe_gsl_workspace_destroy(space);
	free_if_not_null(space);
}


bool mol_noe_calc_matrix_exponential(struct mol_noe *spect)
{
	double *in = spect->in;
	double *rx = spect->rx;
	bool *mask = spect->_mask;

	struct mol_noe_grad *in_grad = spect->in_grad;
	struct mol_noe_grad *rx_grad = spect->rx_grad;

	const size_t size = spect->size;
	const double t_mix = spect->t_mix;

	if (in == NULL || rx == NULL) {
		_PRINTF_ERROR("Matrices are uninitialized");
		return false;
	}

	gsl_matrix_view m = gsl_matrix_view_array(rx, size, size);
	gsl_vector *eval = spect->space->eval;
	gsl_matrix *evec = spect->space->evec;
	gsl_eigen_symmv_workspace *w = spect->space->w;
	gsl_eigen_symmv(&m.matrix, eval, evec, w);

	double *expeval = spect->space->expeval;
	double *evals = eval->data;
	for (size_t i = 0; i < size; i++) {
		expeval[i] = exp(-t_mix * evals[i]);
		evals[i] *= -t_mix;
	}

	double *evecs = evec->data;
	double in_val;
	for (size_t i = 0; i < size; i++) {
		for (size_t j = i; j < size; j++) {
			in_val = 0.0;

			if (mask == NULL || mask[i * size + j]) {
				for (size_t k = 0; k < size; k++) {
					in_val += expeval[k] * evecs[i * size + k] * evecs[j * size + k];
				}
			}

			in[i * size + j] = in_val;
			in[j * size + i] = in_val;

			if (isnan(in_val)) {
				_PRINTF_ERROR("Matrix exponential contains NaN (%zu, %zu)", i, j);
				return false;
			}
		}
	}

	if (rx_grad != NULL && in_grad != NULL) {
		double *helperx = calloc(size * size, sizeof(double));
		double *helpery = calloc(size * size, sizeof(double));
		double *helperz = calloc(size * size, sizeof(double));

		double ijx, ijy, ijz, c1, c2, c3;
		struct mol_vector3 ru, buf;
		size_t loc;

		size_t atm = 0;
		for (size_t grp_i = 0; grp_i < spect->groups->ngroups; grp_i++) {
			for (size_t grp_j = 0; grp_j < spect->groups->groups[grp_i].size; grp_j++) {
				size_t atm_size_size = atm * size * size, grad_id2, grad_id;

				for (size_t r = 0; r < size; r++) {
					for (size_t u = r; u < size; u++) {
						ru.X = 0;
						ru.Y = 0;
						ru.Z = 0;

						if (r != u) {
							c1 = -(expeval[r] - expeval[u]) / (evals[r] - evals[u]) * t_mix;
						} else {
							c1 = -expeval[r] * t_mix;
						}

						// Gradient of relaxation matrix is sparse. E.g. group_id = 2:
						//
						//     0. 0. x  0. 0. 0. 0. 0.
						//     0. 0. x  0. 0. 0. 0. 0.
						//     x  x  x  x  x  x  x  x
						//     0. 0. x  0. 0. 0. 0. 0.
						//     0. 0. x  0. 0. 0. 0. 0.
						//     0. 0. x  0. 0. 0. 0. 0.
						//     0. 0. x  0. 0. 0. 0. 0.
						//     0. 0. x  0. 0. 0. 0. 0.
						//
						// Therefore using only non-zero values here
						for (size_t s = 0; s < size; s++) {
							c2 = c1 * evec->data[s * size + r];
							grad_id2 = atm_size_size + s * size;
							size_t t;
							if (s != grp_i) {
								t = grp_i;
								c3 = c2 * evec->data[t * size + u];
								grad_id = grad_id2 + t;
								MOL_VEC_MULT_SCALAR(buf, rx_grad->grad[grad_id], c3);
								MOL_VEC_ADD(ru, ru, buf);

								t = s;
								c3 = c2 * evec->data[t * size + u];
								grad_id = grad_id2 + t;
								MOL_VEC_MULT_SCALAR(buf, rx_grad->grad[grad_id], c3);
								MOL_VEC_ADD(ru, ru, buf);
							} else {
								for (t = 0; t < size; t++) {
									c3 = c2 * evec->data[t * size + u];
									grad_id = grad_id2 + t;
									MOL_VEC_MULT_SCALAR(buf, rx_grad->grad[grad_id], c3);
									MOL_VEC_ADD(ru, ru, buf);
								}
							}
						}

						// We are going to use only the upper triangle of helper,
						// since its symmetrical
						loc = r * size + u;
						helperx[loc] = ru.X;
						helpery[loc] = ru.Y;
						helperz[loc] = ru.Z;

					}
				}

				size_t r_size_plus_u;
				double evec_i_size_plus_r, evec_j_size_plus_u, evec_i_size_plus_u, evec_j_size_plus_r, evec_sum;
				for (size_t i = 0; i < size; i++) {
					for (size_t j = i; j < size; j++) {
						ijx = 0.0;
						ijy = 0.0;
						ijz = 0.0;

						if (mask == NULL || mask[i * size + j]) {
							for (size_t r = 0; r < size; r++) {
								evec_i_size_plus_r = evec->data[i * size + r];
								evec_j_size_plus_r = evec->data[j * size + r];

								// Start u with r, because of the helper matrix symmetry
								for (size_t u = r; u < size; u++) {
									evec_i_size_plus_u = evec->data[i * size + u];
									evec_j_size_plus_u = evec->data[j * size + u];

									if (u == r) {
										evec_sum = evec_i_size_plus_r *
										           evec_j_size_plus_u;
									} else {
										evec_sum = (evec_i_size_plus_r *
										            evec_j_size_plus_u +
										            evec_j_size_plus_r *
										            evec_i_size_plus_u);
									}

									r_size_plus_u = r * size + u;
									ijx += helperx[r_size_plus_u] * evec_sum;
									ijy += helpery[r_size_plus_u] * evec_sum;
									ijz += helperz[r_size_plus_u] * evec_sum;
								}
							}
						}

						loc = atm_size_size + i * size + j;
						in_grad->grad[loc].X = ijx;
						in_grad->grad[loc].Y = ijy;
						in_grad->grad[loc].Z = ijz;

						loc = atm_size_size + j * size + i;
						in_grad->grad[loc].X = ijx;
						in_grad->grad[loc].Y = ijy;
						in_grad->grad[loc].Z = ijz;
					}
				}
				atm++;
			}
		}

		free(helperx);
		free(helpery);
		free(helperz);
	}
	return true;
}


static void _calc_mask_from_experiment(struct mol_noe* spect)
{
	bool *mask = calloc(spect->size * spect->size, sizeof(bool));
	for (size_t pi = 0; pi < spect->exp->npeaks; pi++) {
		struct mol_noe_experiment_peak peak = spect->exp->peaks[pi];
		for (size_t i = 0; i < peak.group1_size; i++) {
			for (size_t j = 0; j < peak.group2_size; j++) {
				mask[peak.group1[i] * spect->size + peak.group2[j]] = true;
			}
		}
	}
	spect->_mask = mask;
}


bool mol_noe_calc_peaks(
	struct mol_noe *spect,
	const struct mol_atom_group *ag,
	const bool calc_dist_matrix,
	const bool exp_peaks_only)
{
	double *in = spect->in;
	struct mol_noe_group_list *grps = spect->groups;

	const size_t size = grps->ngroups;

	// Calculate mask
	if ((exp_peaks_only) && (spect->exp != NULL)) {
		_calc_mask_from_experiment(spect);
	}

	// Calculate distance matrix
	if (calc_dist_matrix) {
		if (!mol_noe_calc_r2_matrix(spect->r2, ag, spect->groups)) {
			_PRINTF_ERROR("Failed to compute distance matrix");
			return false;
		}
	}

	// Relaxation matrix
	if (!mol_noe_calc_relaxation_matrix(spect, ag)) {
		_PRINTF_ERROR("Failed to compute relaxation matrix");
		return false;
	}

	// EXP(matrix)
	if (!mol_noe_calc_matrix_exponential(spect)) {
		_PRINTF_ERROR("Failed to compute matrix exponential");
		return false;
	}

	// Normalization
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			in[i * size + j] *= sqrt(grps->groups[i].size * grps->groups[j].size);
		}
	}

	if (spect->in_grad != NULL) {
		size_t idx = 0;
		struct mol_vector3 *grad = spect->in_grad->grad;
		double coef;

		for (size_t k = 0; k < grps->natoms; k++) {
			for (size_t i = 0; i < size; i++) {
				for (size_t j = 0; j < size; j++) {
					coef = sqrt(grps->groups[i].size * grps->groups[j].size);
					MOL_VEC_MULT_SCALAR(grad[idx], grad[idx], coef);
					idx++;
				}
			}
		}
	}

	// If mask was used, remove it
	free_if_not_null(spect->_mask);
	spect->_mask = NULL;

	return true;
}


int mol_noe_calc_peaks_no_grad(struct mol_noe *spect, const struct mol_atom_group *ag)
{
	struct mol_noe_grad *rx_ana = spect->rx_grad;
	struct mol_noe_grad *in_ana = spect->in_grad;
	spect->rx_grad = NULL;
	spect->in_grad = NULL;

	int result = mol_noe_calc_peaks(spect, ag, true, false);

	spect->rx_grad = rx_ana;
	spect->in_grad = in_ana;

	return result;
}


bool mol_noe_calc_r2_matrix(double *r2, const struct mol_atom_group *ag, const struct mol_noe_group_list *grps)
{
	const size_t size = grps->natoms;
	struct mol_vector3 *crdi, *crdj;

	for (size_t i = 0; i < size; i++) {
		crdi = &ag->coords[grps->atoms[i]];

		for (size_t j = i; j < size; j++) {
			crdj = &ag->coords[grps->atoms[j]];

			r2[i * size + j] = MOL_VEC_EUCLIDEAN_DIST_SQ(*crdi, *crdj);

			r2[j * size + i] = r2[i * size + j];
		}
	}

	return true;
}


static void _r2_mat_recalc(double *r2, struct mol_atom_group *ag, struct mol_noe_group_list *grps, int atom)
{
	const size_t size = grps->natoms;

	struct mol_vector3 *crdi, *crdj;
	crdi = &ag->coords[grps->atoms[atom]];
	for (size_t j = 0; j < size; j++) {
		crdj = &ag->coords[grps->atoms[j]];

		r2[atom * size + j] = MOL_VEC_EUCLIDEAN_DIST_SQ(*crdi, *crdj);

		r2[j * size + atom] = r2[atom * size + j];
	}
}

bool mol_noe_calc_peaks_grad_numeric(struct mol_noe *spec, struct mol_atom_group *ag, const double step)
{
	struct mol_noe_group_list *grps = spec->groups;
	const size_t size = grps->ngroups;
	const size_t msize = size * size;

	// save gradients pointers
	struct mol_noe_grad *in_grad = spec->in_grad;
	struct mol_noe_grad *rx_grad = spec->rx_grad;

	// set to NULL so the gradients computed analytically
	spec->in_grad = NULL;
	spec->rx_grad = NULL;

	if (!mol_noe_calc_peaks(spec, ag, true, 0)) {
		_PRINTF_ERROR("Could not compute NOE peaks");
		return false;
	}

	double *pks_orig = spec->in;

	spec->in = spec->space->pks2;
	const double one_over_step = 1.0 / step;
	struct mol_vector3 *grad = in_grad->grad;

	size_t grad_atom_1 = 0;
	size_t grad_atom_2 = 0;
	double oldcrd;

	for (size_t i = 0; i < size; i++) {
		struct mol_noe_group grp = grps->groups[i];

		for (size_t j = 0; j < grp.size; j++) {
			size_t k;
			double *pks_pert;
			size_t atom = grp.atoms[j];
			struct mol_vector3 *coords = &ag->coords[atom];

			// fill grad for X
			oldcrd = coords->X;
			coords->X += step;
			_r2_mat_recalc(spec->r2, ag, grps, grad_atom_2);
			if (!mol_noe_calc_peaks(spec, ag, false, 0)) {
				_PRINTF_ERROR("Could not compute NOE peaks");
				return false;
			}
			pks_pert = spec->in;

			coords->X = oldcrd;

			for (k = 0; k < msize; k++) {
				grad[grad_atom_1 + k].X = (pks_pert[k] - pks_orig[k]) * one_over_step;
			}

			// fill grad for Y
			oldcrd = coords->Y;
			coords->Y += step;
			_r2_mat_recalc(spec->r2, ag, grps, grad_atom_2);
			if (!mol_noe_calc_peaks(spec, ag, false, 0)) {
				_PRINTF_ERROR("Could not compute NOE peaks");
				return false;
			}

			coords->Y = oldcrd;

			for (k = 0; k < msize; k++) {
				grad[grad_atom_1 + k].Y = (pks_pert[k] - pks_orig[k]) * one_over_step;
			}

			// fill grad for Z
			oldcrd = coords->Z;
			coords->Z += step;
			_r2_mat_recalc(spec->r2, ag, grps, grad_atom_2);
			if (!mol_noe_calc_peaks(spec, ag, false, 0)) {
				_PRINTF_ERROR("Could not compute NOE peaks");
				return false;
			}

			coords->Z = oldcrd;
			_r2_mat_recalc(spec->r2, ag, grps, grad_atom_2);

			for (k = 0; k < msize; k++) {
				grad[grad_atom_1 + k].Z = (pks_pert[k] - pks_orig[k]) * one_over_step;
			}

			grad_atom_1 += msize;
			grad_atom_2++;
		}
	}

	spec->in = pks_orig;
	spec->in_grad = in_grad;
	spec->rx_grad = rx_grad;

	return true;
}


bool mol_noe_calc_energy_grad_numeric(
	struct mol_noe *spect,
	struct mol_atom_group *ag,
	const double step,
	const double weight,
	const double power)
{
	struct mol_noe_group_list *grps = spect->groups;
	const size_t size = grps->ngroups;

	// save gradients pointers
	struct mol_noe_grad *in_grad = spect->in_grad;
	struct mol_noe_grad *rx_grad = spect->rx_grad;

	// make NULL so they will not be computed analytically
	spect->in_grad = NULL;
	spect->rx_grad = NULL;

	if (!mol_noe_calc_peaks(spect, ag, true, true)) {
		_PRINTF_ERROR("Could not compute NOE peaks");
		return false;
	}
	if (!mol_noe_calc_energy(spect, NULL, weight, power)) {
		_PRINTF_ERROR("Could not compute NOE energy");
		return false;
	}

	const double init_energy = spect->energy;
	const double one_over_step = 1.0 / step;

	struct mol_noe_group grp;
	struct mol_vector3 *coords;
	size_t atom, i, j;
	size_t atomi = 0;
	double oldcrd;

	for (i = 0; i < size; i++) {
		grp = grps->groups[i];
		for (j = 0; j < grp.size; j++) {
			atom = grp.atoms[j];
			coords = &ag->coords[atom];

			// fill grad for X
			oldcrd = coords->X;
			coords->X += step;
			_r2_mat_recalc(spect->r2, ag, grps, atomi);
			if (!mol_noe_calc_peaks(spect, ag, false, true)) {
				_PRINTF_ERROR("Could not compute NOE peaks");
				return false;
			}
			if (!mol_noe_calc_energy(spect, NULL, weight, power)) {
				_PRINTF_ERROR("Could not compute NOE energy");
				return false;
			}
			double new_energy = spect->energy;

			coords->X = oldcrd;
			_r2_mat_recalc(spect->r2, ag, grps, atomi);
			ag->gradients[atom].X = (new_energy - init_energy) * one_over_step;

			// fill grad for Y
			oldcrd = coords->Y;
			coords->Y += step;
			_r2_mat_recalc(spect->r2, ag, grps, atomi);
			if (!mol_noe_calc_peaks(spect, ag, false, true)) {
				_PRINTF_ERROR("Could not compute NOE peaks");
				return false;
			}
			if (!mol_noe_calc_energy(spect, NULL, weight, power)) {
				_PRINTF_ERROR("Could not compute NOE energy");
				return false;
			}
			new_energy = spect->energy;

			coords->Y = oldcrd;
			_r2_mat_recalc(spect->r2, ag, grps, atomi);
			ag->gradients[atom].Y = (new_energy - init_energy) * one_over_step;

			// fill grad for Z
			oldcrd = coords->Z;
			coords->Z += step;
			_r2_mat_recalc(spect->r2, ag, grps, atomi);
			if (!mol_noe_calc_peaks(spect, ag, false, true)) {
				_PRINTF_ERROR("Could not compute NOE peaks");
				return false;
			}
			if (!mol_noe_calc_energy(spect, NULL, weight, power)) {
				_PRINTF_ERROR("Could not compute NOE energy");
				return false;
			}
			new_energy = spect->energy;

			coords->Z = oldcrd;
			_r2_mat_recalc(spect->r2, ag, grps, atomi);
			ag->gradients[atom].Z = (new_energy - init_energy) * one_over_step;

			atomi++;
		}
	}

	spect->in_grad = in_grad;
	spect->rx_grad = rx_grad;

	return true;
}


double *mol_noe_matrix_read_txt_stacked(const char *path, const size_t size, int *mask)
{
	FILE *f = fopen(path, "r");
	double *mat = calloc(size * size, sizeof(double));

	size_t i, j;
	double val;
	int nchar;
	while ((nchar = fscanf(f, "%zu %zu %lf", &i, &j, &val)) != EOF) {
		bool correct = true;
		if (nchar != 3) {
			_PRINTF_ERROR("Wrong line format");
			correct = false;
		}

		if (i >= size) {
			_PRINTF_ERROR("Index exceeds the original matrix size (%zu >= %zu)", i, size);
			correct = false;
		}

		if (j >= size) {
			_PRINTF_ERROR("Index exceeds the original matrix size (%zu >= %zu)", j, size);
			correct = false;
		}

		if (!correct) {
			fclose(f);
			free(mat);
			return NULL;
		}

		mat[i * size + j] = val;

		if (mask != NULL) {
			mask[i * size + j] = 1;
		}
	}

	fclose(f);

	return mat;
}


json_t *mol_noe_matrix_to_json_object(const double *m, const size_t size)
{
	json_t *m_json = json_array();
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			json_t *peak = json_object();
			json_object_set_new(peak, "group1", json_integer(i));
			json_object_set_new(peak, "group2", json_integer(j));
			json_object_set_new(peak, "value", json_real(m[i * size + j]));
			json_array_append_new(m_json, peak);
		}
	}
	return m_json;
}


bool mol_noe_calc_energy(struct mol_noe *spect, struct mol_vector3 *mol_grad, const double weight, const double power)
{
	const size_t natoms = spect->groups->natoms;
	const size_t ngroups = spect->size;
	struct mol_noe_experiment *exp = spect->exp;
	double *cmp = spect->in;
	size_t *atoms = NULL;
	struct mol_vector3 grd;
	struct mol_vector3 *k_derivs = NULL;
	struct mol_vector3 *noe_grad = NULL;

	if (exp == NULL || cmp == NULL) {
		_PRINTF_ERROR("NULL pointer");
		return false;
	}

	if (mol_grad != NULL) {
		noe_grad = spect->in_grad->grad;
		if (noe_grad == NULL) {
			_PRINTF_ERROR("NOE gradients are not initialized. Allocate them using "
			              "`mol_noe_grad_create` and recompute peaks");
			return false;
		}

		atoms = spect->groups->atoms;
		k_derivs = calloc(natoms, sizeof(struct mol_vector3));
	}

	double in_o, in_o_abs, in_o_pow, in_c_pow, in_c, in_c_fabs, delta, coef, err;
	double k_num = 0.0, k_den = 0.0;

	for (size_t peak_i = 0; peak_i < exp->npeaks; peak_i++) {
		struct mol_noe_experiment_peak peak = exp->peaks[peak_i];

		// sum values of all overlapping peaks
		in_c = 0;
		for (size_t _i = 0; _i < peak.group1_size; _i++) {
			for (size_t _j = 0; _j < peak.group2_size; _j++) {
				in_c += cmp[peak.group1[_i] * ngroups + peak.group2[_j]];
			}
		}
		in_c_fabs = fabs(in_c);
		in_c_pow = pow(in_c_fabs, power);

		in_o = peak.volume;
		in_o_abs = fabs(in_o);
		err = peak.error;
		if (in_c_fabs < in_o_abs - err) {
			in_o_pow = pow(in_o_abs - err, power);
		} else if (in_c_fabs > in_o_abs + err) {
			in_o_pow = pow(in_o_abs + err, power);
		} else {
			in_o_pow = pow(in_o_abs, power);
		}

		k_num += in_c_pow * in_o_pow;
		k_den += in_o_pow * in_o_pow;

		if (mol_grad != NULL && in_c != 0. && in_o != 0.) {
			coef = power * in_o_pow * in_c_pow / in_c;
			for (size_t atm = 0; atm < natoms; atm++) {
				// get cumulative peak gradient as a sum of gradients of overlapping peaks
				size_t index = atm * ngroups * ngroups;
				struct mol_vector3 peak_grad = {0};
				for (size_t _i = 0; _i < peak.group1_size; _i++) {
					for (size_t _j = 0; _j < peak.group2_size; _j++) {
						MOL_VEC_ADD(peak_grad, peak_grad, noe_grad[index + peak.group1[_i] * ngroups + peak.group2[_j]]);
					}
				}

				MOL_VEC_MULT_SCALAR(peak_grad, peak_grad, coef);
				MOL_VEC_ADD(k_derivs[atm], k_derivs[atm], peak_grad);
			}
		}
	}

	if (k_den == 0.) {
		_PRINTF_ERROR("All observed intensities are zero");
		free_if_not_null(k_derivs);
		return false;
	}

	double ene = 0.0;
	const double best_k = k_num / k_den;
	double c1, c2;

	for (size_t peak_i = 0; peak_i < exp->npeaks; peak_i++) {
		struct mol_noe_experiment_peak peak = exp->peaks[peak_i];

		// sum values of all overlapping peaks
		in_c = 0;
		for (size_t _i = 0; _i < peak.group1_size; _i++) {
			for (size_t _j = 0; _j < peak.group2_size; _j++) {
				in_c += cmp[peak.group1[_i] * ngroups + peak.group2[_j]];
			}
		}
		in_c_fabs = fabs(in_c);
		in_c_pow = pow(in_c_fabs, power);

		in_o = exp->peaks[peak_i].volume;
		in_o_abs = fabs(in_o);
		err = exp->peaks[peak_i].error;
		if (in_c_fabs < in_o_abs - err) {
			in_o_pow = pow(in_o_abs - err, power);
		} else if (in_c_fabs > in_o_abs + err) {
			in_o_pow = pow(in_o_abs + err, power);
		} else {
			in_o_pow = pow(in_o_abs, power);
		}

		delta = (in_c_pow - best_k * in_o_pow);
		ene += weight * delta * delta;

		if (mol_grad != NULL && delta != 0.0) {
			c1 = weight * 2. * delta;
			c2 = (in_c != 0.) ? (power * in_c_pow / in_c) : 0.;
			for (size_t atm = 0; atm < natoms; atm++) {
				// gradient
				struct mol_vector3 buf1, buf2;

				// get cumulative peak gradient as a sum of gradients of overlapping peaks
				size_t index = atm * ngroups * ngroups;
				struct mol_vector3 peak_grad = {0};
				for (size_t _i = 0; _i < peak.group1_size; _i++) {
					for (size_t _j = 0; _j < peak.group2_size; _j++) {
						MOL_VEC_ADD(peak_grad, peak_grad, noe_grad[index + peak.group1[_i] * ngroups + peak.group2[_j]]);
					}
				}

				MOL_VEC_MULT_SCALAR(buf1, peak_grad, c2);
				MOL_VEC_MULT_SCALAR(buf2, k_derivs[atm], in_o_pow / k_den);
				MOL_VEC_SUB(buf1, buf1, buf2);
				MOL_VEC_MULT_SCALAR(grd, buf1, c1);
				MOL_VEC_SUB(mol_grad[atoms[atm]], mol_grad[atoms[atm]], grd);
			}
		}
	}

	free_if_not_null(k_derivs);

	spect->energy = ene;
	spect->scale = best_k;

	return true;
}


int *get_mask(double *exp, size_t size)
{
	int *mask = calloc(size * size, sizeof(int));

	for (size_t i = 0; i < size; i++) {
		for (size_t j = i; j < size; j++) {
			if (fabs(exp[i * size + j]) > __NOE_PEAK_MIN__) {
				mask[i * size + j] = 1;
				mask[j * size + i] = 1;
			} else {
				mask[i * size + j] = 0;
				mask[j * size + i] = 0;
			}
		}
	}

	return mask;
}


void ones(int *matrix, size_t size)
{
	for (size_t i = 0; i < size; i++) {
		for (size_t j = i; j < size; j++) {
			matrix[i * size + j] = 1;
			matrix[j * size + i] = 1;
		}
	}
}


struct mol_noe *mol_noe_create(
	struct mol_noe_group_list *groups,
	const double omega,
	const double t_cor,
	const double t_mix,
	const double cutoff)
{
	struct mol_noe *spec = calloc(1, sizeof(struct mol_noe));
	mol_noe_init(spec, groups, omega, t_cor, t_mix, cutoff);
	return spec;
}


void mol_noe_init(
	struct mol_noe *spec,
	struct mol_noe_group_list *groups,
	const double omega,
	const double t_cor,
	const double t_mix,
	const double cutoff)
{
	// Read proton groups
	if (groups == NULL) {
		_PRINTF_ERROR("Groups cannot be NULL");
	}
	spec->groups = groups;

	const size_t size = spec->groups->ngroups;
	spec->size = size;
	spec->in = calloc(size * size, sizeof(double));
	spec->rx = calloc(size * size, sizeof(double));
	spec->r2 = calloc(spec->groups->natoms * spec->groups->natoms, sizeof(double));
	spec->space = _mol_noe_gsl_workspace_create(size);

	spec->omega = omega; // GHz
	spec->t_cor = t_cor; // ns
	spec->t_mix = t_mix; // s
	spec->cutoff = cutoff;

	spec->_mask = NULL;
	spec->exp = NULL;
	spec->in_grad = NULL;
	spec->rx_grad = NULL;
}


void mol_noe_destroy(struct mol_noe *spec)
{
	if (spec != NULL) {
		mol_noe_group_list_free(spec->groups);
		spec->groups = NULL;
		_mol_noe_gsl_workspace_free(spec->space);
		spec->space = NULL;
		mol_noe_grad_free(spec->in_grad);
		spec->in_grad = NULL;
		mol_noe_grad_free(spec->rx_grad);
		spec->rx_grad = NULL;

		free_if_not_null(spec->in);
		spec->in = NULL;
		free_if_not_null(spec->rx);
		spec->rx = NULL;
		free_if_not_null(spec->r2);
		spec->r2 = NULL;
		mol_noe_experiment_free(spec->exp);
		spec->exp = NULL;
		free_if_not_null(spec->_mask);
		spec->_mask = NULL;
	}
}


void mol_noe_free(struct mol_noe *spec)
{
	mol_noe_destroy(spec);
	free_if_not_null(spec);
}


void mol_noe_alloc_grad(struct mol_noe *spec)
{
	spec->rx_grad = mol_noe_grad_create(spec->groups->natoms, spec->groups->ngroups);
	spec->in_grad = mol_noe_grad_create(spec->groups->natoms, spec->groups->ngroups);
}


struct mol_noe *mol_noe_from_json_object(json_t *root)
{
	json_error_t j_error;
	int j_code;

	// Read atom groups
	json_t *groups_json = json_object_get(root, "groups");
	if (!groups_json) {
		_PRINTF_ERROR("Input json doesn't have `groups`");
		return NULL;
	}

	struct mol_noe_group_list *groups = mol_noe_group_list_from_json_object(groups_json);
	if (!groups) {
		_PRINTF_ERROR("Groups reading failed");
		mol_noe_group_list_free(groups);
		return NULL;
	}

	// Read in NOESY parameters
	double frequency, t_cor, t_mix, cutoff;

	j_code = json_unpack_ex(
		root, &j_error, 0,
		"{s:F, s:F, s:F, s:F}",
		"frequency", &frequency,
		"t_cor", &t_cor,
		"t_mix", &t_mix,
		"cutoff", &cutoff);

	if (j_code != 0) {
		_PRINTF_ERROR(
			"Couldn't parse json NOE: %s (line: %i, column: %i)",
			j_error.text,
			j_error.line,
			j_error.column);
		mol_noe_group_list_free(groups);
		return NULL;
	}

	struct mol_noe *noe = mol_noe_create(groups, frequency, t_cor, t_mix, cutoff);

	// Read experimental matrix if provided
	json_t *matrix_json = json_object_get(root, "experiment");
	if (matrix_json) {
		noe->exp = mol_noe_experiment_from_json_object(matrix_json);
		if (noe->exp == NULL) {
			_PRINTF_ERROR("Could not parse 'experiment'");
			mol_noe_free(noe);
			return NULL;
		}

		// Check if group IDs in the experiment are in range
		for (size_t peak_i = 0; peak_i < noe->exp->npeaks; peak_i++) {
			for (size_t j = 0; j < noe->exp->peaks[peak_i].group1_size; j++) {
				if (noe->exp->peaks[peak_i].group1[j] >= noe->size) {
					_PRINTF_ERROR("Group ID is out of range in peak %zu", peak_i);
					mol_noe_free(noe);
					return NULL;
				}
			}
			for (size_t j = 0; j < noe->exp->peaks[peak_i].group2_size; j++) {
				if (noe->exp->peaks[peak_i].group2[j] >= noe->size) {
					_PRINTF_ERROR("Group ID is out of range in peak %zu", peak_i);
					mol_noe_free(noe);
					return NULL;
				}
			}
		}
	}

	return noe;
}


struct mol_noe *mol_noe_read_json(const char *path)
{
	json_t *root;
	json_error_t error;
	root = json_load_file(path, 0, &error);
	if (!root) {
		_PRINTF_ERROR("Couldn't load %s", path);
		return NULL;
	}
	struct mol_noe *noe = mol_noe_from_json_object(root);
	json_decref(root);
	if (!noe) {
		_PRINTF_ERROR("Couldn't parse %s", path);
		return NULL;
	}
	return noe;
}


json_t *mol_noe_to_json_object(const struct mol_noe *spec)
{
	json_t *root = json_object();

	json_object_set_new(root, "groups", mol_noe_group_list_to_json_object(spec->groups));
	json_object_set_new(root, "frequency", json_real(spec->omega));
	json_object_set_new(root, "t_cor", json_real(spec->t_cor));
	json_object_set_new(root, "t_mix", json_real(spec->t_mix));
	json_object_set_new(root, "cutoff", json_real(spec->cutoff));
	json_object_set_new(root, "energy", json_real(spec->energy));
	json_object_set_new(root, "scale", json_real(spec->scale));

	//if (spec->exp != NULL) {
	//	json_object_set_new(root, "experiment", mol_noe_matrix_to_json_object(spec->exp, spec->size));
	//}

	if (spec->in != NULL) {
		json_object_set_new(root, "prediction", mol_noe_matrix_to_json_object(spec->in, spec->size));
	}

	return root;
}


void mol_noe_fwrite_json(FILE *f, const struct mol_noe *spec)
{
	json_t *root = mol_noe_to_json_object(spec);
	json_dumpf(root, f, JSON_INDENT(4));
	json_decref(root);
}


bool mol_noe_write_json(const char *path, const struct mol_noe *spec)
{
	FILE *f = fopen(path, "w");
	if (f == NULL) {
		_PRINTF_ERROR("Can't open file %s for writing", path);
		return false;
	}
	mol_noe_fwrite_json(f, spec);
	fclose(f);
	return true;
}


struct mol_noe_group_list *mol_noe_group_list_create(const size_t ngroups, const size_t natoms)
{
	struct mol_noe_group_list *grps = calloc(1, sizeof(struct mol_noe_group_list));
	mol_noe_group_list_init(grps, ngroups, natoms);
	return grps;
}


void mol_noe_group_list_init(struct mol_noe_group_list *space, const size_t ngroups, const size_t natoms)
{
	space->ngroups = ngroups;
	space->groups = calloc(ngroups, sizeof(struct mol_noe_group));

	space->natoms = natoms;
	space->atoms = calloc(natoms, sizeof(size_t));
}


void mol_noe_group_list_destroy(struct mol_noe_group_list *groups)
{
	if (groups != NULL) {
		free_if_not_null(groups->groups);
		groups->groups = NULL;
		free_if_not_null(groups->atoms);
		groups->atoms = NULL;
	}
}


void mol_noe_group_list_free(struct mol_noe_group_list *groups)
{
	mol_noe_group_list_destroy(groups);
	free_if_not_null(groups);
}


struct mol_noe_group_list *mol_noe_group_list_read_txt(const char *path)
{
	const int n_atoms_lines = _number_of_atoms_in_lines(path);
	if (n_atoms_lines < 1) {
		_PRINTF_ERROR("Couldn't parse %s", path);
		return NULL;
	}
	const int n_lines = _number_of_lines(path);
	if (n_lines < 1) {
		_PRINTF_ERROR("Couldn't parse %s", path);
		return NULL;
	}
	struct mol_noe_group_list *grps = mol_noe_group_list_create(n_lines, n_atoms_lines);

	FILE *grp_file = fopen(path, "r");
	char str[100];
	char *chunk;
	int group_id = 0;
	int natoms = 0;
	while (fgets(str, 100, grp_file) != NULL) {
		int grp_size = 0;
		grps->groups[group_id].id = group_id;
		chunk = strtok(str, " \t");
		while (chunk != NULL) {
			if (grp_size == 6) {
				_PRINTF_ERROR("Group can't me larger than 6");
				fclose(grp_file);
				mol_noe_group_list_free(grps);
				return NULL;
			}

			int atom_id = atoi(chunk);
			grps->groups[group_id].atoms[grp_size] = atom_id;
			grps->atoms[natoms] = atom_id;
			grp_size++;
			natoms++;
			chunk = strtok(NULL, " \t");
		}

		grps->groups[group_id].size = grp_size;
		group_id++;
	}

	fclose(grp_file);
	return grps;
}


struct mol_noe_group_list *mol_noe_group_list_from_json_object(const json_t *root)
{
	if (!json_is_array(root)) {
		_PRINTF_ERROR("`groups` must be an array");
		return NULL;
	}

	const size_t ngroups = json_array_size(root);
	size_t natoms = 0;
	for (size_t i = 0; i < ngroups; i++) {
		json_t *group = json_array_get(root, i);
		if (!json_is_array(group)) {
			_PRINTF_ERROR("Each entry in `groups` must be an array");
			return NULL;
		}
		natoms += json_array_size(group);
	}

	struct mol_noe_group_list *groups = mol_noe_group_list_create(ngroups, natoms);
	size_t counter = 0;

	for (size_t i = 0; i < ngroups; i++) {
		json_t *group_json = json_array_get(root, i);
		struct mol_noe_group *group = &groups->groups[i];
		group->id = i;
		group->size = json_array_size(group_json);
		if (group->size > 6) {
			_PRINTF_ERROR("Group can't be larger than 6");
			mol_noe_group_list_free(groups);
			return NULL;
		}

		for (size_t j = 0; j < group->size; j++) {
			json_t *atom_id = json_array_get(group_json, j);
			if (!json_is_integer(atom_id)) {
				_PRINTF_ERROR("Atom ID must be an integer");
				mol_noe_group_list_free(groups);
				return NULL;
			}
			group->atoms[j] = json_integer_value(atom_id);
			groups->atoms[counter++] = group->atoms[j];
		}
	}
	return groups;
}


struct mol_noe_group_list *mol_noe_group_list_read_json(const char *path)
{
	json_t *root;
	json_error_t error;
	root = json_load_file(path, 0, &error);
	if (!root) {
		_PRINTF_ERROR("Couldn't load %s", path);
		return NULL;
	}
	struct mol_noe_group_list *groups = mol_noe_group_list_from_json_object(root);
	json_decref(root);
	if (!groups) {
		_PRINTF_ERROR("Couldn't parse %s", path);
	}
	return groups;
}


json_t *mol_noe_group_list_to_json_object(const struct mol_noe_group_list *groups)
{
	json_t *root = json_array();
	for (size_t i = 0; i < groups->ngroups; i++) {
		struct mol_noe_group group = groups->groups[i];
		json_t *group_json = json_array();
		for (size_t j = 0; j < group.size; j++) {
			json_array_append_new(group_json, json_integer(group.atoms[j]));
		}
		json_array_append_new(root, group_json);
	}
	return root;
}


bool mol_noe_group_list_write_txt(const char *path, const struct mol_noe_group_list *groups)
{
	FILE *f = fopen(path, "w");
	if (f == NULL) {
		_PRINTF_ERROR("Can't open file %s for writing", path);
		return false;
	}
	mol_noe_group_list_fwrite_txt(f, groups);
	fclose(f);
	return true;
}


void mol_noe_group_list_fwrite_txt(FILE *f, const struct mol_noe_group_list *groups)
{
	for (size_t i = 0; i < groups->ngroups; i++) {
		fprintf(f, "group %zu of npeaks %zu: ", groups->groups[i].id, groups->groups[i].size);
		for (size_t j = 0; j < groups->groups[i].size; j++) {
			fprintf(f, "%zu\t", groups->groups[i].atoms[j]);
		}
		fprintf(f, "\n");
	}
}


struct mol_noe_grad *mol_noe_grad_create(const size_t natoms, const size_t npeaks)
{
	struct mol_noe_grad *grad = calloc(1, sizeof(struct mol_noe_grad));
	mol_noe_grad_init(grad, natoms, npeaks);
	return grad;
}


void mol_noe_grad_init(struct mol_noe_grad *grad, const size_t natoms, const size_t npeaks)
{
	grad->size = natoms * npeaks * npeaks;
	grad->natoms = natoms;
	grad->npeaks = npeaks;
	grad->grad = calloc(grad->size, sizeof(struct mol_vector3));
}


void mol_noe_grad_destroy(struct mol_noe_grad *grad)
{
	if (grad != NULL) {
		free_if_not_null(grad->grad);
		grad->grad = NULL;
	}
}


void mol_noe_grad_free(struct mol_noe_grad *grad)
{
	mol_noe_grad_destroy(grad);
	free_if_not_null(grad);
}


void mol_noe_matrix_fwrite_txt(FILE *f, const double *m, const size_t size)
{
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size - 1; j++)
			fprintf(f, "%.6f\t", m[i * size + j]);
		fprintf(f, "%.6f\n", m[i * size + size - 1]);
	}
}

void mol_noe_matrix_fwrite_txt_stacked(FILE *f, const double *m, const size_t size)
{
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++) {
			fprintf(f, "%zu\t%zu\t%.6f\n", i, j, m[i * size + j]);
		}
	}
}

bool mol_noe_matrix_write_txt(const char *path, const double *m, const size_t size)
{
	FILE *f = fopen(path, "w");
	if (f == NULL) {
		_PRINTF_ERROR("Can't open file %s for writing", path);
		return false;
	}
	mol_noe_matrix_fwrite_txt(f, m, size);
	fclose(f);
	return true;
}


struct mol_noe_experiment *mol_noe_experiment_create(const size_t npeaks)
{
	struct mol_noe_experiment *exp = calloc(1, sizeof(struct mol_noe_experiment));
	mol_noe_experiment_init(exp, npeaks);
	return exp;
}


void mol_noe_experiment_init(struct mol_noe_experiment *exp, const size_t npeaks)
{
	exp->npeaks = npeaks;
	exp->peaks = calloc(npeaks, sizeof(struct mol_noe_experiment_peak));
}


void mol_noe_experiment_destroy(struct mol_noe_experiment *exp)
{
	if (exp != NULL) {
		free_if_not_null(exp->peaks);
		exp->peaks = NULL;
	}
}


void mol_noe_experiment_free(struct mol_noe_experiment *exp)
{
	mol_noe_experiment_destroy(exp);
	free_if_not_null(exp);
}


bool _parse_experiment_peak(size_t* members, size_t* nmembers, json_t* group, size_t peak_i)
{
	if (json_is_integer(group)) {
		members[0] = json_integer_value(group);
		*nmembers = 1;
	} else if (json_is_array(group)) {
		if (json_array_size(group) > __NOE_OVERLAP_GROUP_SIZE__) {
			_PRINTF_ERROR("NOE group in peak %zu is larger than __NOE_OVERLAP_GROUP_SIZE__=%i", peak_i, __NOE_OVERLAP_GROUP_SIZE__);
			return false;
		}
		json_t* member;
		size_t member_i;
		json_array_foreach(group, member_i, member) {
			if (!json_is_integer(member)) {
				_PRINTF_ERROR("NOE group in peak %zu contains non-integer values", peak_i);
				return false;
			}
			members[member_i] = json_integer_value(member);
		}
		*nmembers = json_array_size(group);
	} else {
		_PRINTF_ERROR("NOE group in peak %zu is not an integer or an array", peak_i);
		return false;
	}
	return true;
}


struct mol_noe_experiment *mol_noe_experiment_from_json_object(json_t* root)
{
	if (!root) {
		return NULL;
	}

	if (!json_is_array(root)) {
		_PRINTF_ERROR("`experiment` must be an array");
		return NULL;
	}

	json_error_t j_error;
	int j_code;
	size_t peak_i;
	json_t *peak;
	struct mol_noe_experiment* exp = mol_noe_experiment_create(json_array_size(root));

	json_array_foreach(root, peak_i, peak) {
		double value, error = 0.0;

		j_code = json_unpack_ex(
			peak, &j_error, 0,
			"{s:F, s?:F}",
			"value", &value,
			"error", &error);

		exp->peaks[peak_i].volume = value;
		exp->peaks[peak_i].error = error;

		if (j_code != 0) {
			_PRINTF_ERROR(
				"Couldn't parse peak %zu in json NOE: %s (line: %i, column: %i)",
				peak_i,
				j_error.text,
				j_error.line,
				j_error.column);
			mol_noe_experiment_free(exp);
			return NULL;
		}

		json_t* group1 = json_object_get(peak, "group1");
		if (!_parse_experiment_peak(
			exp->peaks[peak_i].group1,
			&exp->peaks[peak_i].group1_size,
			group1, peak_i)) {
			_PRINTF_ERROR("Could not parse peak %zu", peak_i);
			mol_noe_experiment_free(exp);
			return NULL;
		}

		json_t* group2 = json_object_get(peak, "group2");
		if (!_parse_experiment_peak(
			exp->peaks[peak_i].group2,
			&exp->peaks[peak_i].group2_size,
			group2, peak_i)) {
			_PRINTF_ERROR("Could not parse peak %zu", peak_i);
			mol_noe_experiment_free(exp);
			return NULL;
		}
	}
	return exp;
}


struct mol_noe_experiment *mol_noe_experiment_from_txt_stacked(const char *path, const size_t size, int *mask)
{
	double *mat = mol_noe_matrix_read_txt_stacked(path, size, mask);
	if (mat == NULL) {
		_PRINTF_ERROR("Could not parse %s", path);
		return NULL;
	}

	struct mol_noe_experiment* exp = mol_noe_experiment_create(size * (size-1) / 2);
	size_t peak_i = 0;
	for (size_t i = 0; i < size; i++) {
		for (size_t j = i + 1; j < size; j++) {
			exp->peaks[peak_i].volume= mat[i * size + j];
			exp->peaks[peak_i].error = 0;
			exp->peaks[peak_i].group1_size = 1;
			exp->peaks[peak_i].group1[0] = i;
			exp->peaks[peak_i].group2_size = 1;
			exp->peaks[peak_i].group2[0] = j;
			peak_i += 1;
		}
	}

	free(mat);
	return exp;
}