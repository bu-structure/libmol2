#include <math.h>
#include "pwpot.h"
#include "private_utils.h"

static const double PWPOT_DELTA = 0.5; //!< Angstrom

// Calculate pwpot energy and gradient multiplier in near-cutoff regions
static void _pwpot_around_cutoff(
	double *energy,
	double *gradient_mult,
	const double cutoff_min,
	const double weight,
	const double eps_ij,
	const double r2);


void mol_pwpot_eng(
		struct mol_atom_group *ag,
		double *energy,
		const struct mol_prms *atomprm,
		const struct nblist *nblst,
		const double weight)
{
	if (ag->pwpot_id == NULL) {
		_PRINTF_ERROR("Can not use pwpot: atom_group must have pwpot_id field! Have you called mol_atom_group_add_prms?");
		exit(EXIT_FAILURE);
	}
	if (atomprm->num_pwpot != 1) {
		_PRINTF_ERROR("Can not use pwpot: only single bin supported for proper forcefield calculation. You can try scoring mode instead (mol_pwpot_score)");
		exit(EXIT_FAILURE);
	}
	const double cutoff = atomprm->pwpot->r2;
	const double cutoff_min = cutoff - PWPOT_DELTA;
	const double cutoff_max = cutoff + PWPOT_DELTA;
	const double cutoff_min_sq = cutoff_min * cutoff_min;
	const double cutoff_max_sq = cutoff_max * cutoff_max;
	double energy_pwpot = 0.0;

	for (int i = 0; i < nblst->nfat; i++) {
		int atom_i = nblst->ifat[i];
		int subid_i = ag->pwpot_id[atom_i];

		if (subid_i < 0) // Skip atoms with unknown types
			continue;

		for (int j = 0; j < nblst->nsat[i]; j++) {
			int atom_j = nblst->isat[i][j];
			int subid_j = ag->pwpot_id[atom_j];

			if (subid_j < 0)
				continue;

			double eps_ij = atomprm->pwpot->eng[subid_i][subid_j];

			struct mol_vector3 dist;
			MOL_VEC_SUB(dist, ag->coords[atom_i], ag->coords[atom_j]);
			double r2 = MOL_VEC_SQ_NORM(dist);

			if (r2 < cutoff_min_sq) {
				energy_pwpot += eps_ij;
			} else if (r2 < cutoff_max_sq) {
				double energy_ij, dven_r;
				_pwpot_around_cutoff(&energy_ij, &dven_r, cutoff_min, weight, eps_ij, r2);
				energy_pwpot += energy_ij;

				// Be careful with the signs, GX is force, i.e., negative of derivative
				struct mol_vector3 g;
				MOL_VEC_MULT_SCALAR(g, dist, dven_r);
				MOL_VEC_ADD(ag->gradients[atom_i], ag->gradients[atom_i], g);
				MOL_VEC_SUB(ag->gradients[atom_j], ag->gradients[atom_j], g);
			}
		}
	}
	// Apply the weight
	(*energy) += energy_pwpot * weight;
}


double mol_pwpot_score(
		const struct mol_atom_group *ag1,
		const struct mol_atom_group *ag2,
		const struct mol_prms *atomprm)
{
	if (ag1->pwpot_id == NULL) {
		_PRINTF_ERROR("Can not use pwpot: atom_group must have pwpot_id field! Have you called mol_atom_group_add_prms?");
		exit(EXIT_FAILURE);
	}
	if (ag2->pwpot_id == NULL) {
		_PRINTF_ERROR("Can not use pwpot: atom_group must have pwpot_id field! Have you called mol_atom_group_add_prms?");
		exit(EXIT_FAILURE);
	}
	double cutoff = 0;
	for (size_t k = 0; k < atomprm->num_pwpot; k++) {
		cutoff = MAX(cutoff, atomprm->pwpot[k].r2);
	}
	const double cutoff_sq = cutoff * cutoff;

	double energy_pwpot = 0.0;

	for (size_t i = 0; i < ag1->natoms; i++) {
		int subid_i = ag1->pwpot_id[i];

		if (subid_i < 0) // Skip atoms with unknown types
			continue;

		for (size_t j = 0; j < ag2->natoms; j++) {
			int subid_j = ag2->pwpot_id[j];

			if (subid_j < 0)
				continue;

			struct mol_vector3 dist;
			MOL_VEC_SUB(dist, ag1->coords[i], ag2->coords[j]);
			const double r2 = MOL_VEC_SQ_NORM(dist);

			if (r2 < cutoff_sq) {
				const double r = sqrt(r2);
				for (size_t k = 0; k < atomprm->num_pwpot; k++) {
					if (atomprm->pwpot[k].r1 <= r && r < atomprm->pwpot[k].r2) {
						double eps_ij = atomprm->pwpot[k].eng[subid_i][subid_j];
						energy_pwpot += eps_ij;
					}
				}
			}
		}
	}
	return energy_pwpot;
}


void _pwpot_around_cutoff(
		double *energy,
		double *gradient_mult,
		const double cutoff_min,
		const double weight,
		const double eps_ij,
		const double r2)
{
	double r = sqrt(r2);
	double p = (r - cutoff_min) / (2 * PWPOT_DELTA);
	double p_sq = p * p;
	double f = 1 - 6 * p_sq * p_sq * p + 15 * p_sq * p_sq - 10 * p_sq * p;
	(*energy) = eps_ij * f;

	// dven = weight * eps_ij * df / dr = weight * eps_ij * (df/dp) / (2 * PWPOT_DELTA)
	double dven = 30 * weight * eps_ij * (p_sq - 2 * p + 1) * p_sq / (2 * PWPOT_DELTA);
	(*gradient_mult) = dven / r;
}
