#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include <stdlib.h>
#ifndef _WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "icharmm.h"
#include "utils.h"
#include "private_utils.h"


enum prm_read_mode {
	VDW_MODE,
	ACE_VOLUME_MODE,
	BOND_MODE,
	ANGLE_MODE,
	DIHEDRAL_MODE,
	IMPROPER_MODE,
	NON_BONDED_MODE,

	HBOND_MODE,
	NO_MODE
};

enum match_mode {
	MATCH_EXACT = 0,
	MATCH_AXXD = 1,
	MATCH_XBCD = 2,
	MATCH_XBCX = 3,
	MATCH_XXCD = 4
};

struct mol_dihedral_param {
	char tag0[16], tag1[16], tag2[16], tag3[16];
	double k;
	double d;
	size_t n;
};

/**
 * Parse lines from a prm file and apply the parameters to
 * the fields in an atom group
 */
static void parse_prm_vdw(const char *line, struct mol_atom_group *ag);
static void parse_prm_ace_volume(const char *line, struct mol_atom_group *ag);
static void parse_prm_bond(const char *line, struct mol_atom_group *ag);
static void parse_prm_angle(const char *line, struct mol_atom_group *ag);
static bool parse_prm_dihedral(const char *line, struct mol_dihedral_param *dp);
static bool parse_prm_improper(const char *line, struct mol_atom_group *ag);

static bool mol_atom_group_read_rtf(struct mol_atom_group *ag,
				const char *rtffile);
static bool mol_atom_group_read_prm(struct mol_atom_group *ag,
				const char *prmfile);

bool mol_atom_group_read_psf(struct mol_atom_group *ag, const char *psffile)
{
	bool has_error = false;
	FILE *fp = fopen(psffile, "r");
	if (fp == NULL) {
		perror(__func__);
		_PRINTF_ERROR("Unable to open %s", psffile);
		return false;
	}

	char *line = NULL;
	size_t len = 0;
	int scanned;
	char resname[4];
	char atom_name[5];

	size_t ndonors;
	size_t nacceptors;

	mol_atom_group_init_atomic_field(ag, charge);
	mol_atom_group_init_atomic_field(ag, ftypen);
	mol_atom_group_init_atomic_field(ag, bond_lists);
	mol_atom_group_init_atomic_field(ag, angle_lists);
	mol_atom_group_init_atomic_field(ag, dihedral_lists);
	mol_atom_group_init_atomic_field(ag, improper_lists);
	mol_atom_group_init_atomic_field(ag, hbond_prop);
	mol_atom_group_init_atomic_field(ag, hbond_base_atom_id);

	while ((getline(&line, &len, fp)) != -1) {
		if (strstr(line, "NATOM") != NULL) {
			if (ag->natoms != strtoul(line, NULL, 10)) {
				goto error_out;
			}

			for (size_t i = 0; i < ag->natoms; i++) {
				if(getline(&line, &len, fp) == -1) {
					_PRINTF_ERROR("Unexpected end of file when parsing atoms");
					goto error_out;
				}

				scanned = sscanf(line, "%*s %*s %*s %3s %4s %zu %lf",
						 resname, atom_name, &ag->ftypen[i], &ag->charge[i]);
				if (scanned != 4) {
					_PRINTF_ERROR("Failed to parse PSF atom %zu", i);
					goto error_out;
				}
			}
		} else if (strstr(line, "NBOND") != NULL) {
			ag->nbonds = strtoul(line, NULL, 10);
			ag->bonds = calloc(ag->nbonds, sizeof(struct mol_bond));

			for (size_t i = 0; i < ag->nbonds; i++) {
				struct mol_bond *bond = &ag->bonds[i];

				if (fscanf(fp, "%zu %zu", &bond->ai, &bond->aj) != 2) {
					_PRINTF_ERROR("Failed to parse PSF bond %zu", i);
					goto error_out;
				}
				bond->ai -= 1;
				bond->aj -= 1;
				ag->bond_lists[bond->ai].size++;
				ag->bond_lists[bond->aj].size++;
			}
		} else if (strstr(line, "NTHETA") != NULL) {
			ag->nangles = strtoul(line, NULL, 10);
			ag->angles = calloc(ag->nangles, sizeof(struct mol_angle));

			for (size_t i = 0; i < ag->nangles; i++) {
				struct mol_angle *angle = &ag->angles[i];

				if (fscanf(fp, "%zu %zu %zu",
					   &angle->a0, &angle->a1, &angle->a2) != 3) {
					_PRINTF_ERROR("Failed to parse PSF angle %zu", i);
					goto error_out;
				}
				angle->a0 -= 1;
				angle->a1 -= 1;
				angle->a2 -= 1;
				ag->angle_lists[angle->a0].size++;
				ag->angle_lists[angle->a1].size++;
				ag->angle_lists[angle->a2].size++;
			}
		} else if (strstr(line, "NPHI") != NULL) {
			ag->ndihedrals = strtoul(line, NULL, 10);
			ag->dihedrals = calloc(ag->ndihedrals, sizeof(struct mol_dihedral));

			for (size_t i = 0; i < ag->ndihedrals; i++) {
				struct mol_dihedral *dihedral = &ag->dihedrals[i];

				if (fscanf(fp, "%zu %zu %zu %zu",
					   &dihedral->a0, &dihedral->a1,
					   &dihedral->a2, &dihedral->a3) != 4) {
					_PRINTF_ERROR("Error: failed to parse PSF dihedral %zu", i);
					goto error_out;
				}
				dihedral->a0 -= 1;
				dihedral->a1 -= 1;
				dihedral->a2 -= 1;
				dihedral->a3 -= 1;
				ag->dihedral_lists[dihedral->a0].size++;
				ag->dihedral_lists[dihedral->a1].size++;
				ag->dihedral_lists[dihedral->a2].size++;
				ag->dihedral_lists[dihedral->a3].size++;
			}
		} else if (strstr(line, "NIMPHI") != NULL) {
			ag->nimpropers = strtoul(line, NULL, 10);
			ag->impropers = calloc(ag->nimpropers, sizeof(struct mol_improper));

			for (size_t i = 0; i < ag->nimpropers; i++) {
				struct mol_improper *improper = &ag->impropers[i];

				if (fscanf(fp, "%zu %zu %zu %zu",
					   &improper->a0, &improper->a1,
					   &improper->a2, &improper->a3) != 4) {
					_PRINTF_ERROR("Failed to parse PSF improper %zu", i);
					goto error_out;
				}
				improper->a0 -= 1;
				improper->a1 -= 1;
				improper->a2 -= 1;
				improper->a3 -= 1;
				ag->improper_lists[improper->a0].size++;
				ag->improper_lists[improper->a1].size++;
				ag->improper_lists[improper->a2].size++;
				ag->improper_lists[improper->a3].size++;
			}
		} else if (strstr(line, "NDON") != NULL) {
			ndonors = strtoul(line, NULL, 10);
			size_t donor, hydro;

			for (size_t i = 0; i < ndonors; i++) {
				if (fscanf(fp, "%zu %zu", &donor, &hydro) != 2) {
					goto error_out;
				}
				donor -= 1;
				hydro -= 1;
				MOL_ATOM_HBOND_PROP_ADD(ag, donor, HBOND_DONOR);
				MOL_ATOM_HBOND_PROP_ADD(ag, hydro, DONATABLE_HYDROGEN);
				ag->hbond_base_atom_id[hydro] = donor;
			}
		} else if (strstr(line, "NACC") != NULL) {
			nacceptors = strtoul(line, NULL, 10);
			size_t acceptor, base;

			for (size_t i = 0; i < nacceptors; i++) {
				if (fscanf(fp, "%zu %zu", &acceptor, &base) != 2) {
					goto error_out;
				}
				acceptor -= 1;
				base -= 1;
				MOL_ATOM_HBOND_PROP_ADD(ag, acceptor, HBOND_ACCEPTOR);
				ag->hbond_base_atom_id[acceptor] = base;
			}
		}
	}

	// Fill in geometry lists
	mol_atom_group_fill_geometry_lists(ag);

	goto cleanup;

error_out:
	has_error = true;
cleanup:
	if (fp != NULL)
		fclose(fp);
	free_if_not_null(line);
	return !has_error;
}

static bool mol_atom_group_read_rtf(struct mol_atom_group *ag,
				const char *rtffile)
{
	bool has_error = false;

	FILE *fp = fopen(rtffile, "r");
	if (fp == NULL) {
		return false;
	}

	mol_atom_group_init_atomic_field(ag, ftype_name);
	ag->num_atom_types = 0;

	char *line = NULL;
	size_t len;
	size_t ftypen;
	char ftype_name[17];
	size_t max_ftypen = 128;
	char **ftype_names = calloc(max_ftypen, sizeof(char *));

	while (getline(&line, &len, fp) != -1) {
		if (strncmp(line, "MASS", 4) == 0) {
			long ftypen_temp;
			if (sscanf(line, "%*s %ld %16s",
					&ftypen_temp, ftype_name) != 2) {
				goto read_rtf_error;
			}

			if (ftypen_temp < 0) {
				/* https://www.charmm.org/charmm/documentation/by-version/c42b1/params/doc/io/
				 * MASS atom-type-code atom-type-name mass
				 * As of c40a1 the atom-type-code can be input as a -1 and this
				 * generates atom-type-codes internally in a sequential manner
				 * defining the atom-type-code for the current mass declaration
				 * as that which increments NATC by one. One can mix both explicitly
				 * specified atom-type-codes and -1 values, the atom-type-codes are
				 * generated internally based on the atom-type-name.
				 */
				ftypen = ag->num_atom_types + 1;
			} else {
				ftypen = ftypen_temp;
			}

			if (ftypen >= max_ftypen) {
				// We if we are trying to insert a new element, we at least double the list size.
				// If the ftype is even larger, we do our best to accommodate it.
				const size_t new_max_ftypen = MAX(ftypen + 1, 2*max_ftypen);
				ftype_names = realloc(ftype_names,
				                      new_max_ftypen * sizeof(char *));
				memset(ftype_names + max_ftypen, 0, (new_max_ftypen - max_ftypen)*sizeof(char *));
				max_ftypen = new_max_ftypen;
			}

			if (ftype_names[ftypen] != NULL) {
				_PRINTF_ERROR("Duplicate ftypen in RTF: %zu (%s)", ftypen, ftype_names[ftypen]);
				goto read_rtf_error;
			}

			ftype_names[ftypen] = strndup(ftype_name, 16);

			ag->num_atom_types = MAX(ag->num_atom_types, ftypen);
		}
	}

	for (size_t i = 0; i < ag->natoms; i++) {
		ag->ftype_name[i] = strndup(ftype_names[ag->ftypen[i]], 16);
	}

	goto read_rtf_cleanup;

read_rtf_error:
	has_error = true;
read_rtf_cleanup:
	if (ftype_names != NULL) {
		for (size_t i = 0; i < max_ftypen; i++) {
			free_if_not_null(ftype_names[i]);
		}
		free(ftype_names);
	}
	if (fp != NULL)
		fclose(fp);
	free_if_not_null(line);
	return !has_error;
}

static bool __matching_tag(size_t a0, size_t a1, size_t a2, size_t a3,
			   const struct mol_atom_group *ag,
			   const struct mol_dihedral_param *dp,
			   enum match_mode mode)
{
	// the mask says which positions are allowed to be wildcard
	const bool masks[][4] = {
		{false, false, false, false},
		{false, true, true, false},
		{true, false, false, false},
		{true, false, false, true},
		{true, true, false, false}
	};

	const bool *mask = masks[mode];

	bool is_wildcard[4] = {false, false, false, false};
	is_wildcard[0] = !strcmp(dp->tag0, "X") && mask[0];
	is_wildcard[1] = !strcmp(dp->tag1, "X") && mask[1];
	is_wildcard[2] = !strcmp(dp->tag2, "X") && mask[2];
	is_wildcard[3] = !strcmp(dp->tag3, "X") && mask[3];

	return (((is_wildcard[0] || !strcmp(ag->ftype_name[a0], dp->tag0)) &&
		 (is_wildcard[1] || !strcmp(ag->ftype_name[a1], dp->tag1)) &&
		 (is_wildcard[2] || !strcmp(ag->ftype_name[a2], dp->tag2)) &&
		 (is_wildcard[3] || !strcmp(ag->ftype_name[a3], dp->tag3))) ||
		((is_wildcard[3] || !strcmp(ag->ftype_name[a0], dp->tag3)) &&
		 (is_wildcard[2] || !strcmp(ag->ftype_name[a1], dp->tag2)) &&
		 (is_wildcard[1] || !strcmp(ag->ftype_name[a2], dp->tag1)) &&
		 (is_wildcard[0] || !strcmp(ag->ftype_name[a3], dp->tag0))));
}

#define matching_tag(X, AG, DP, MODE)		\
	__matching_tag(X->a0, X->a1, X->a2, X->a3, AG, DP, MODE)

static void _add_index_to_dihedral_list(struct mol_index_list *l, const size_t dihedral_idx)
{
	l->size++;
	l->members = realloc(l->members, l->size * sizeof(size_t));
	l->members[l->size - 1] = dihedral_idx;
}

static bool mol_atom_group_read_prm(struct mol_atom_group *ag,
				const char *prmfile)
{
	bool has_error = false;

	FILE *fp = fopen(prmfile, "r");
	if (fp == NULL) {
		return false;
	}

	mol_atom_group_init_atomic_field(ag, vdw_radius);
	mol_atom_group_init_atomic_field(ag, vdw_radius03);
	mol_atom_group_init_atomic_field(ag, eps);
	mol_atom_group_init_atomic_field(ag, eps03);

	char *line = NULL;
	size_t len;
	enum prm_read_mode read_mode = NO_MODE;
	char *p;

	size_t ndihedral_params = 0;
	size_t max_dihedral_params = 256;
	struct mol_dihedral_param *dihedral_params =
		calloc(max_dihedral_params, sizeof(struct mol_dihedral_param));
	struct mol_dihedral_param *dp;

	while (getline(&line, &len, fp) != -1) {
		// Ignore comment portions of the line
		if ((p = strchr(line, '!')))
			*p = '\0';

		if (is_whitespace_line(line)) {
			continue;
		}

		// These need to be in this order
		// The less specific prefixes need to be lower
		if (strstr(line, "NBON") != NULL ||
		    strstr(line, "NONB") != NULL) {
			read_mode = VDW_MODE;
			continue;
		} else if (strstr(line, "VOLUME") != NULL) {
			read_mode = ACE_VOLUME_MODE;
			mol_atom_group_init_atomic_field(ag, ace_volume);
			continue;
		} else if (strstr(line, "HBOND") != NULL) {
			read_mode = HBOND_MODE;
			continue;
		} else if (strstr(line, "BOND") != NULL) {
			read_mode = BOND_MODE;
			continue;
		} else if (strstr(line, "ANGL") != NULL ||
			strstr(line, "THETA") != NULL) {
			read_mode = ANGLE_MODE;
			continue;
		} else if (strstr(line, "IMPR") != NULL ||
			strstr(line, "IMPHI") != NULL) {
			read_mode = IMPROPER_MODE;
			continue;
		} else if (strstr(line, "DIHE") != NULL ||
			strstr(line, "PHI") != NULL) {
			read_mode = DIHEDRAL_MODE;
			continue;
		} else if (strstr(line, "NBFIX") != NULL) {
			read_mode = NON_BONDED_MODE;
			continue;
		}

		/* We still need to scan through all the
		 * atoms/bonds/angles/dihedrals/impropers for each line
		 * since we are not guaranteed that each line of the
		 * parameter file matches only one geometry entity.
		 *
		 * This also means that you can set some "default" parameters
		 * by using *, and then refine specific atom types by putting a
		 * more specific type later in the file.
		 */
		switch (read_mode) {
		case VDW_MODE:
			parse_prm_vdw(line, ag);
			break;
		case ACE_VOLUME_MODE:
			parse_prm_ace_volume(line, ag);
			break;
		case BOND_MODE:
			parse_prm_bond(line, ag);
			break;
		case ANGLE_MODE:
			parse_prm_angle(line, ag);
			break;
		case DIHEDRAL_MODE:
			/* Dihedrals need to be handled very differently, since
			 * each dihedral may match more than one parameter line.
			 *
			 * The rules are:
			 *  * If an existing dihedral has an exact match, ignore
			 *    all wildcard matches
			 *  * If there are multiple exact matches, use them all
			 *  * If there are no exact matches, use as many
			 *    wildcards as possible
			 */
			if (ndihedral_params == max_dihedral_params) {
				max_dihedral_params *= 2;
				dihedral_params =
					realloc(dihedral_params,
						max_dihedral_params*sizeof(struct mol_dihedral_param));
			}
			dp = &dihedral_params[ndihedral_params++];
			parse_prm_dihedral(line, dp);
			break;
		case IMPROPER_MODE:
			parse_prm_improper(line, ag);
			break;
		case NON_BONDED_MODE:
			break;
		case HBOND_MODE:
			break;
		case NO_MODE:
		default:
			continue;
		}
	}
	fclose(fp);

	// Now we apply all the dihedrals we read earlier
	size_t ndihedrals = ag->ndihedrals;
	size_t max_dihedrals = ndihedrals;

	for (size_t i = 0; i < ag->ndihedrals; ++i) {
		struct mol_dihedral *dihedral = &ag->dihedrals[i];
		bool found_exact = false;

		// Find exact matches
		for (size_t j = 0; j < ndihedral_params; ++j) {
			dp = &dihedral_params[j];

			if (matching_tag(dihedral, ag, dp, MATCH_EXACT)) {
				// If we already found an exact match, put new dihedral at the end
				if (found_exact) {
					if (ndihedrals == max_dihedrals) {
						max_dihedrals *= 2;
						ag->dihedrals =
							realloc(ag->dihedrals,
								max_dihedrals * sizeof(struct mol_dihedral));
						dihedral = &ag->dihedrals[i];
					}

					// Copy the parameters to the new dihedral
					ag->dihedrals[ndihedrals] = *dihedral;
					dihedral = &ag->dihedrals[ndihedrals];

					// Add dihedral to lists
					_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a0], ndihedrals);
					_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a1], ndihedrals);
					_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a2], ndihedrals);
					_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a3], ndihedrals);

					ndihedrals++;
				}

				dihedral->k = dp->k;
				dihedral->n = dp->n;
				dihedral->d = dp->d;

				found_exact = true;
			}
		}

		if (!found_exact) {
			// Find wildcard matches
			bool found_wildcard = false;

			for (size_t j = 0; j < ndihedral_params; ++j) {
				dp = &dihedral_params[j];

				if (matching_tag(dihedral, ag, dp, MATCH_XBCX)) {
					// If we already found an exact match, put new dihedral at the end
					if (found_wildcard) {
						if (ndihedrals == max_dihedrals) {
							max_dihedrals *= 2;
							ag->dihedrals =
								realloc(ag->dihedrals,
									max_dihedrals * sizeof(struct mol_dihedral));
							dihedral = &ag->dihedrals[i];
						}

						// Copy the parameters to the new dihedral
						ag->dihedrals[ndihedrals] = *dihedral;
						dihedral = &ag->dihedrals[ndihedrals];

						// Add dihedral to lists
						_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a0],
						                            ndihedrals);
						_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a1],
						                            ndihedrals);
						_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a2],
						                            ndihedrals);
						_add_index_to_dihedral_list(&ag->dihedral_lists[dihedral->a3],
						                            ndihedrals);

						ndihedrals++;
					}

					dihedral->k = dp->k;
					dihedral->n = dp->n;
					dihedral->d = dp->d;

					found_wildcard = true;
				}
			}

			 if (!found_wildcard) {
				 _PRINTF_WARNING("No parameters for the torsion (%zu %zu %zu %zu)",
					 dihedral->a0, dihedral->a1, dihedral->a2, dihedral->a3);
				 dihedral->k = -1.0;
				 dihedral->n = -1;
				 dihedral->d = -1.0;
			 }
		}
	}
	ag->ndihedrals = ndihedrals;
	ag->dihedrals = realloc(ag->dihedrals, ndihedrals*sizeof(struct mol_dihedral));

	free_if_not_null(dihedral_params);
	free_if_not_null(line);
	return !has_error;
}

bool mol_atom_group_read_geometry(struct mol_atom_group *ag,
				const char *psffile,
				const char *prmfile,
				const char *rtffile)
{
	if (!mol_atom_group_read_psf(ag, psffile)) {
		return false;
	}

	if (!mol_atom_group_read_rtf(ag, rtffile)) {
		return false;
	}

	return mol_atom_group_read_prm(ag, prmfile);
}

static void parse_prm_vdw(const char *line, struct mol_atom_group *ag)
{
	char tag[17];
	double eps = 1.0, radius = -1.0, eps03 = 1.0, radius03 = -1.0;

	int scanned = sscanf(line, "%16s %*s %lf %lf %*s %lf %lf",
			tag, &eps, &radius, &eps03, &radius03);
	if (scanned < 3)
		return;

	if (scanned < 5 || eps03 > 0) {
		eps03 = 1.0;
		radius03 = -1.0;
	}

	if (eps <= 0)
		eps = sqrt(-eps);

	if (eps03 <= 0) {
		eps03 = sqrt(-eps03);
	} else {
		eps03 = eps;
		radius03 = radius;
	}

	size_t tag_len = strcspn(tag, "*");

	for (size_t i = 0; i < ag->natoms; ++i) {
		if (strncmp(tag, ag->ftype_name[i], tag_len) == 0) {
			ag->eps[i] = eps;
			ag->eps03[i] = eps03;
			ag->vdw_radius[i] = radius;
			ag->vdw_radius03[i] = radius03;
		}
	}
}

static void parse_prm_ace_volume(const char *line, struct mol_atom_group *ag)
{
	char tag[17];
	double volume = -1.0;

	int scanned = sscanf(line, "%16s %lf",
			tag, &volume);
	if (scanned != 2) {
		return;
	}
	if (volume < 0.0) {
		volume = 18.0;
	}

	size_t tag_len = strcspn(tag, "*");

	for (size_t i = 0; i < ag->natoms; ++i) {
		if (strncmp(tag, ag->ftype_name[i], tag_len) == 0) {
			ag->ace_volume[i] = volume;
		}
	}
}

static void parse_prm_bond(const char *line, struct mol_atom_group *ag)
{
	char tag0[17], tag1[17];
	double k = -1.0, l0 = -1.0;

	int scanned = sscanf(line, "%16s %16s %lf %lf",
			tag0, tag1, &k , &l0);
	if (scanned != 4) {
		return;
	}

	// Type names in a bond line are not expected to have a '*'
	for (size_t i = 0; i < ag->nbonds; ++i) {
		struct mol_bond *bond = &ag->bonds[i];
		if ((strncmp(tag0, ag->ftype_name[bond->ai], 16) == 0 &&
		     strncmp(tag1, ag->ftype_name[bond->aj], 16) == 0) ||
		    (strncmp(tag1, ag->ftype_name[bond->ai], 16) == 0 &&
		     strncmp(tag0, ag->ftype_name[bond->aj], 16) == 0)) {
			bond->k = k;
			bond->l0 = l0;
		}
	}
}

static void parse_prm_angle(const char *line, struct mol_atom_group *ag)
{
	char tag0[17], tag1[17], tag2[17];
	double k, th0;

	int scanned = sscanf(line, "%16s %16s %16s %lf %lf",
			tag0, tag1, tag2, &k , &th0);
	if (scanned != 5) {
		return;
	}

	for (size_t i = 0; i < ag->nangles; ++i) {
		struct mol_angle *angle = &ag->angles[i];

		if ((strncmp(tag0, ag->ftype_name[angle->a0], 16) == 0 &&
		     strncmp(tag1, ag->ftype_name[angle->a1], 16) == 0 &&
		     strncmp(tag2, ag->ftype_name[angle->a2], 16) == 0) ||
		    (strncmp(tag2, ag->ftype_name[angle->a0], 16) == 0 &&
		     strncmp(tag1, ag->ftype_name[angle->a1], 16) == 0 &&
		     strncmp(tag0, ag->ftype_name[angle->a2], 16) == 0)) {
			angle->k = k;
			angle->th0 = th0;
		}
	}
}

static bool parse_prm_dihedral(const char *line, struct mol_dihedral_param *dp)
{
	int scanned = sscanf(line, "%16s %16s %16s %16s %lf %zu %lf",
			     dp->tag0, dp->tag1, dp->tag2, dp->tag3, &dp->k, &dp->n, &dp->d);
	return scanned == 7;
}

static bool parse_prm_improper(const char *line, struct mol_atom_group *ag)
{
	struct mol_dihedral_param dp;
	double k, psi0;
	size_t n;

	int scanned = sscanf(line, "%16s %16s %16s %16s %lf %zu %lf",
			     dp.tag0, dp.tag1, dp.tag2, dp.tag3, &k, &n, &psi0);
	if (scanned != 7) {
		return false;
	}

	for (size_t i = 0; i < ag->nimpropers; ++i) {
		struct mol_improper *improper = &ag->impropers[i];

		if (matching_tag(improper, ag, &dp, MATCH_EXACT) ||
		    matching_tag(improper, ag, &dp, MATCH_AXXD) ||
		    matching_tag(improper, ag, &dp, MATCH_XBCD) ||
		    matching_tag(improper, ag, &dp, MATCH_XBCX) ||
		    matching_tag(improper, ag, &dp, MATCH_XXCD))
		{
			improper->k = k;
			improper->psi0 = psi0;
			improper->n = n;
		}
	}

	return true;
}
