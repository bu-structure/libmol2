#ifndef _MOL_MS_H_
#define _MOL_MS_H_

#include "atom_group.h"

#include <stdio.h>

struct mol_atom_group *mol_read_ms(const char *ms_filename);
struct mol_atom_group *mol_fread_ms(FILE *fp);
struct mol_atom_group_list *mol_read_ms_models(const char *ms_filename);

void mol_fwrite_ms(FILE *ofp, const struct mol_atom_group *ag);
bool mol_write_ms(const char *ms_filename, const struct mol_atom_group *ag);

#endif /* _MOL_MS_H_ */
