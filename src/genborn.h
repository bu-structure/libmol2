#ifndef _MOL2_GENBORN_H
#define _MOL2_GENBORN_H
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>

#include "atom_group.h"
#include "nbenergy.h"

/**
 * Calculate the Generalized Born (OBC) Energy
 *
 * There are two cut-off values: r_cut is used in calculating effective radius of an atom (default value is 25.0); nblist cutoff is used in calculating
 * GB energy and no less than r_cut + 2. GB energy consider all nb pairs with distances no greater than the nblist cutoff, 0-0, 0-1, 1-2, and 0-3 pairs.
 *
 * Bondii values, factor scales, effective bondii, descreening, differentiation of energy over effective bondii, tanh values,
 * and differentiation of descreening over pair distance are stored in metadata.
 *
 *[1]Onufriev A, Bashford D, Case DA. Exploring protein native states and large-scale conformational changes with a modified generalized born model. Proteins. 2004;55(2):383-394. doi:10.1002/prot.20033
 *[2]https://www.ks.uiuc.edu/Research/namd/2.9/ug/node29.html
 */

/**
 * Struct to store alpha, beta, gamma constant for effective radius calculation
 */
struct mol_genborn_obc_type { double alpha, beta, gamma; };
static const struct mol_genborn_obc_type MOL_GENBORN_OBC_1 = { .alpha = 0.8, .beta = 0.0, .gamma = 2.91 };
static const struct mol_genborn_obc_type MOL_GENBORN_OBC_2 = { .alpha = 1.0, .beta = 0.8, .gamma = 4.85 };

/**
 * Assign the memory locations for bondii, atom factor scale, effective bondii, descreening, tanh, differentiation of energy,
 * and differentiation of descreening then add them to metadata.
 * \param ag atom group instance
 * \return true if the initiation process is successful
 */
bool mol_init_gb(struct mol_atom_group *ag);

/**
 * Calculate OBC GB energy
 * mol_init_gb must be called once prior to any energy calculation.
 * \param ag atom group
 * \param r_cut cutoff value for descreening and effective bondii calculation. The default value is 25.0 in Amber
 * \param ags Wrapper of nonbonded list (\ref nbenergy)
 * \return GB energy
 */
double mol_gb_energy(struct mol_atom_group *ag, const double r_cut, struct agsetup *ags, const struct mol_genborn_obc_type type);

/**
 * Calculate SA energy
 * mol_init_gb must be called once prior to any energy calculation.
 * \param ag atom group
 * \param r_cut cutoff value for descreening and effective bondii calculation. The default value is 25.0 in Amber
 * \param ags Wrapper of nonbonded list (\ref nbenergy)
 * \return GB energy
 */
double mol_sa_energy(struct mol_atom_group *ag, const double r_cut, struct agsetup *ags, const struct mol_genborn_obc_type type);

/**
 * Calculate GB OBC+SA energy
 * mol_init_gb must be called once prior to any energy calculation.
 * \param ag atom group
 * \param r_cut cutoff value for descreening and effective bondii calculation. The default value is 25.0 in Amber
 * \param ags Wrapper of nonbonded list (\ref nbenergy)
 * \return GB energy
 */
double mol_gbsa_energy(struct mol_atom_group *ag, const double r_cut, struct agsetup *ags, const struct mol_genborn_obc_type type);

/**
 * Delete metadata
 * \param ag Atom group
 * \return True if the deletion is successful
 */
bool mol_gb_delete_metadata(struct mol_atom_group *ag);

#endif //MOL2_GENBORN_H