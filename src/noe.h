/** \file noe.h
 *  \brief Structures and functions for NOESY spectrum calculation and fitting.
 *
 *  This module contains routines for NOESY spectrum back-calculation. Briefly, in a molecule proton
 *  spins interact with each other, and NOESY is a type of NMR measurement, that identifies such interaction
 *  by taking advantage of Nuclear Overhauser Effect (NOE). It is observed as a 2D image with
 *  peaks corresponding to interacting pairs of protons or proton groups.
 *
 *  After the peak assignment is completed, the spectrum can be represented as a square matrix
 *  of size N x N, where N is the number of interacting proton groups. The purpose of this module is
 *  to compute simulated spectrum given a molecular structure. The implementation is similar to
 *  [Xplor](https://nmr.cit.nih.gov/xplor-nih/doc/current/xplor/node486.html)
 *
 *  Note, that throughout the module we operate with proton groups instead of isolated protons.
 *  Although, most of such groups will contain a single proton, sometimes several protons can act as a single spin,
 *  if they are magnetically equivalent. An example of such group can be the three protons of a methyl group.
 *
 *  In this module NOE spectrum is simply a list of pairs of proton groups plus the peak volume they produce.
 *  It is kept in struct mol_noe.
 *
 *  To compute a NOE spectrum you need:
 *
 *      1. A list of proton groups
 *      2. A molecule with coordinates
 *      3. (optional) Experimental spectrum to perform fitting
 *
 *  If the fitting is done, the gradients can also be computed and added to the gradients in struct mol_atom_group.
 *
 *  Usage:
 *
 *      // Read in NOE parameters. Mandatory fields are "groups", "frequency", "t_cor", "t_mix" and "cutoff"
 *      // One optional field is "experiment", which provides experimental peaks for comparison
 *      struct mol_noe* noe = mol_noe_read_json("noe_setup.json");
 *
 *      // Read in atom group
 *      struct mol_atom_group* ag = mol_read_pdb("mol.pdb");
 *
 *      // Calculate NOE cross-peaks (square matrix, which is stored in noe->in)
 *      // This is the main function in the module
 *      assert(mol_noe_calc_peaks(noe, ag, true, true/false) == 0);
 *
 *      // Save the computed NOE matrix as a Json file
 *      mol_noe_write_json("noe.json", noe);
 *
 *      // Calculate the fitting score (energy) of predicted peaks to the experimental peaks
 *      // The result is written to noe->energy and noe->scale
 *      double weight = 1000;  // This is just an example, what weight to use in each case is a big question
 *      double power = 1. / 6.;  // Read Xplor manual about the recommended values of this parameter (1/3 or 1/6 usually)
 *      assert(mol_noe_calc_energy(noe, NULL, weight, power) == 0);
 *
 *      // If you wish to compute the gradients as well, allocate them before computing the peaks
 *      // The energy gradients will be added to ag->gradient, if you provide it instead of NULL.
 *      // If the last flag is set to "true", than the peaks not present in the experiment will be ignored.
 *      // Otherwise all possible pairwise peaks will be included and their experimental values will be set to zero.
 *      mol_noe_alloc_grad(noe);
 *      assert(mol_noe_calc_peaks(noe, ag, true, true/false) == 0);
 *      assert(mol_noe_calc_energy(noe, ag->gradient, weight, power) == 0);
 *
 *      // The results can be dumped to Json file
 *      // It will contain parameters, calculated and experimental matrices, energy and best scaling coefficient
 *      mol_noe_write_json("noe.json", noe);
 *
 */
#ifndef _NMRGRAD_NOE_H_
#define _NMRGRAD_NOE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <jansson.h>

#include "pdb.h"

#define __NOE_OVERLAP_GROUP_SIZE__ 6

/**
 * This structure represents a magnetically equivalent group of protons -
 * basic unit in NOE back-calculation. NOESY peak volume is computed for a pair
 * of such groups (not separate atoms, although a group often contains a single atom)
 */
struct mol_noe_group {
	size_t id;       ///< Zero-based group ID is assigned according to the order in the input file
	size_t size;     ///< Number of atoms in the group
	size_t atoms[__NOE_OVERLAP_GROUP_SIZE__]; ///< Zero-based mol_atom_group atom IDs
};


/**
 * List of proton groups
 */
struct mol_noe_group_list {
	size_t ngroups;
	struct mol_noe_group *groups;

	size_t natoms;  ///< Total number of atoms in all groups
	size_t *atoms;  ///< Zero-based atom IDs in all groups, gradients are laid out in this order
};


/**
 * Single experimental peak
 * TODO: add weights for individual peaks instead of "weight" parameter in mol_noe_calc_energy
 */
struct mol_noe_experiment_peak {
	size_t group1[6];
	size_t group2[6];
	size_t group1_size;
	size_t group2_size;
	double volume;
	double error;
};


/**
 * Experimental spectrum
 */
struct mol_noe_experiment {
	size_t npeaks;
	struct mol_noe_experiment_peak* peaks;
};


/**
 * Derivatives for the calculated cross-peaks wrt. X, Y and Z of each atom are kept here
 */
struct mol_noe_grad {
	size_t size;   ///< Size of each grad<n> (natoms * npeaks * npeaks)
	size_t natoms; ///< Number of protons
	size_t npeaks; ///< Number of peaks (ngroups * ngroups)
	struct mol_vector3* grad; ///< Gradient values
};


/**
 * Workspace for matrix exponential calculation
 */
struct mol_noe_gsl_workspace {
	size_t size;
	gsl_vector *eval;
	gsl_matrix *evec;
	gsl_eigen_symmv_workspace *w;
	double *expeval;
	double *pks2;
};


/**
 * Main structure used for NOE back-calculation
 */
struct mol_noe {
	size_t size;      ///< Number of groups, same as groups->ngroups
	double omega;     ///< Frequency in GHz
	double t_cor;     ///< Correlation time in nanoseconds
	double t_mix;     ///< Mixing time is seconds
	double cutoff;    ///< Cutoff for peak calculation in Angstroms

	double *in;       ///< Back-calculated peak volumes (size * size)
	double *rx;       ///< Relaxation matrix (size * size)
	double *r2;       ///< Squared inter-proton distances (groups->natoms * groups->natoms)

	struct mol_noe_gsl_workspace *space;   ///< Workspace for matrix exponential calculation
	struct mol_noe_group_list *groups; ///< Proton groups

	double energy;    ///< Calculated fitting score of the predicted NOE matrix to the experimental one
	double scale;     ///< Best scaling factor for experimental spectrum (is calculated in mol_noe_calc_energy)
	struct mol_noe_experiment *exp;      ///< Experimental NOE matrix
	bool *_mask;        ///< Mask can be used to select certain peaks for calculation and ignore the rest

	struct mol_noe_grad *in_grad;   ///< Gradient of peak volume matrix
	struct mol_noe_grad *rx_grad;   ///< Gradient of relaxation matrix
};


/* **************************
 * Most important functions *
 ****************************/


/**
 * Compute squared pairwise distances for all protons
 *
 * \param r2 Stores the result
 * \param ag Atom group
 * \param grps Proton groups
 * \return 0 on success, 1 on failure
 */
bool mol_noe_calc_r2_matrix(double *r2, const struct mol_atom_group *ag, const struct mol_noe_group_list *grps);

/**
 * Compute relaxation matrix as in [here](https://nmr.cit.nih.gov/xplor-nih/doc/current/xplor/node491.html)
 *
 * \param spect Stores the result
 * \param ag Atom group
 * \return 0 on success, 1 on failure
 */
bool mol_noe_calc_relaxation_matrix(struct mol_noe *spect, const struct mol_atom_group *ag);

/**
 * Computes matrix exponential of spect->rx and computes the gradients
 *
 * \param spect Stores the result
 * \return 0 on success, 1 on failure
 */
bool mol_noe_calc_matrix_exponential(struct mol_noe *spect);

/**
 * Master function of the module
 *
 * Compute peaks and write them in spect->in. Gradients are computed,
 * if spect->in_grad and spect->rx_grad are allocated. This can be done by running
 * mol_noe_alloc_grad(spect).
 *
 * \param spect Stores the result
 * \param ag Atom group
 * \param calc_dist_matrix Must be true, unless you know what you're doing
 * \param exp_peaks_only Only the peaks and gradients present in experiment will be computed (spect->exp must be set)
 * \return 0 on success, 1 on failure
 */
bool mol_noe_calc_peaks(
	struct mol_noe *spect,
	const struct mol_atom_group *ag,
	const bool calc_dist_matrix,
	const bool exp_peaks_only);

/**
 * A convenience wrapper around mol_noe_calc_peaks to only compute peaks without gradient, if
 * spect->in_grad and spect->rx_grad have already been allocated and you're feeling lazy.
 *
 * \param spect Stores the result
 * \param ag Atom group
 * \return 0 on success, 1 on failure
 */
int mol_noe_calc_peaks_no_grad(struct mol_noe *spect, const struct mol_atom_group *ag);

/**
 * Calculate fitting energy of experimental matrix in spect->exp to simulated matrix in spect->in.
 * Energy value and best scaling factor for the simulated matrix are stored in spect->energy and spect->scale.
 * Gradients are computed if mol_grad is not NULL and spect->in_grad and spect->rx_grad are allocated.
 * This can be done by running mol_noe_alloc_grad(spect).
 *
 * The energy is calculated as described in https://nmr.cit.nih.gov/xplor-nih/doc/current/xplor/node493.html.
 * The parameter correspondence is the following: n = power, W_N = weight, m is fixed to 2. Unlike in Xplor,
 * the gradients of the calibration factor k_S are fully accounted for due to m being fixed. Calibration factor
 * is computed as k_S = sum(I_comp * I_exp / I_exp^2)
 *
 * \param spect Stores the result
 * \param mol_grad Atom group gradient
 * \param weight Energy and gradient are multiplied by this number
 * \param power Usually 1/6 or 1/3.
 * \return 0 on success, 1 on failure
 */
bool mol_noe_calc_energy(struct mol_noe *spect, struct mol_vector3 *mol_grad, const double weight, const double power);


/* ****************************************
 * Functions to manipulate struct mol_noe *
 ******************************************/


struct mol_noe *mol_noe_create(
	struct mol_noe_group_list *groups,
	const double omega,
	const double t_cor,
	const double t_mix,
	const double cutoff);

void mol_noe_init(
	struct mol_noe *spec,
	struct mol_noe_group_list *groups,
	const double omega,
	const double t_cor,
	const double t_mix,
	const double cutoff);

void mol_noe_destroy(struct mol_noe *spec);

void mol_noe_free(struct mol_noe *spec);

/**
 * Allocate gradients in struct mol_noe to enable gradient calculation in mol_noe_calc_peaks and mol_noe_calc_energy
 * \param spec Stores the result
 */
void mol_noe_alloc_grad(struct mol_noe *spec);

struct mol_noe *mol_noe_from_json_object(json_t *root);

json_t *mol_noe_to_json_object(const struct mol_noe *spec);

/**
 * Read back-calculation setup from json. The file must contain a json dictionary
 * with the following fields
 *
 * 1. "groups" - array of atom ID (IDs from mol_atom_group) groups in the following format
 *    [[0, 1, 2], [4, 5], [6, 9, 8]]
 * 2. "frequency" - Frequency in GHz
 * 3. "t_cor" - Correlation time in nanoseconds
 * 4. "t_mix" - Mixing time in seconds
 * 5. "cutoff" - Distance cutoff for peak calculation in Angstroms
 *
 * Optional fields are
 * 6. "experiment" - Experimental matrix in the following format ("error" is optional)
 *    [{"group1": 0, "group2": 1, "value": 100.0, "error": 10.0}, {"group1": 2, "group2": 4, "value": 1000.0}, ...]
 *    The groups are numbered according to their order in "groups" field
 *
 *
 * \param path
 * \return struct mol_noe* on success, NULL on failure
 */
struct mol_noe *mol_noe_read_json(const char *path);

void mol_noe_fwrite_json(FILE *f, const struct mol_noe *spec);

bool mol_noe_write_json(const char *path, const struct mol_noe *spec);


/* ***************************************************
 * Functions to manipulate struct mol_noe_group_list *
 *****************************************************/


struct mol_noe_group_list *mol_noe_group_list_create(const size_t ngroups, const size_t natoms);

void mol_noe_group_list_init(struct mol_noe_group_list *space, const size_t ngroups, const size_t natoms);

void mol_noe_group_list_destroy(struct mol_noe_group_list *groups);

void mol_noe_group_list_free(struct mol_noe_group_list *groups);

struct mol_noe_group_list *mol_noe_group_list_read_txt(const char *path);

bool mol_noe_group_list_write_txt(const char *path, const struct mol_noe_group_list *groups);

void mol_noe_group_list_fwrite_txt(FILE *f, const struct mol_noe_group_list *groups);

struct mol_noe_group_list *mol_noe_group_list_from_json_object(const json_t *root);

json_t *mol_noe_group_list_to_json_object(const struct mol_noe_group_list *groups);

struct mol_noe_group_list *mol_noe_group_list_read_json(const char *path);


/* *********************************************
 * Functions to manipulate struct mol_noe_grad *
 ***********************************************/


struct mol_noe_grad *mol_noe_grad_create(const size_t natoms, const size_t npeaks);

void mol_noe_grad_init(struct mol_noe_grad *grad, const size_t natoms, const size_t npeaks);

void mol_noe_grad_destroy(struct mol_noe_grad *grad);

void mol_noe_grad_free(struct mol_noe_grad *grad);


/* **********************************
 * Functions to manipulate matrices *
 ************************************/


void mol_noe_matrix_fwrite_txt(FILE *f, const double *m, const size_t size);

void mol_noe_matrix_fwrite_txt_stacked(FILE *f, const double *m, const size_t size);

bool mol_noe_matrix_write_txt(const char *path, const double *m, const size_t size);

double *mol_noe_matrix_read_txt_stacked(const char *path, const size_t size, int *mask);

json_t *mol_noe_matrix_to_json_object(const double *m, const size_t size);


/* ********************************************
 * Functions to manipulate mol_noe_experiment *
 **********************************************/


struct mol_noe_experiment *mol_noe_experiment_create(const size_t npeaks);

void mol_noe_experiment_init(struct mol_noe_experiment *exp, const size_t npeaks);

void mol_noe_experiment_destroy(struct mol_noe_experiment *exp);

void mol_noe_experiment_free(struct mol_noe_experiment *exp);

struct mol_noe_experiment *mol_noe_experiment_from_json_object(json_t* root);

json_t *mol_noe_experiment_to_json_object(struct mol_noe_experiment *exp);

struct mol_noe_experiment *mol_noe_experiment_from_txt_stacked(const char *path, const size_t size, int *mask);


/* **********************************************************************
 * Gradient calculation using finite differences (mostly used in tests) *
 ************************************************************************/


bool mol_noe_calc_peaks_grad_numeric(struct mol_noe *spec, struct mol_atom_group *ag, const double step);

bool mol_noe_calc_energy_grad_numeric(
	struct mol_noe *spect,
	struct mol_atom_group *ag,
	const double step,
	const double weight,
	const double power);

#endif /* _NMRGRAD_NOE_H_ */
