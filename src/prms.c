#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include "atom_group.h"
#include "prms.h"
#include "private_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#define VERSION_KEY "version"
#define ATOM_KEY "atom"
#define PWPOT_KEY "pwpot"
#define PWPOT_R_KEY "pwpot.r"

static mol_version mol_prms_version(FILE *fp);
static void mol_read_atoms_prms(struct mol_prms *prms, FILE *fp);
static void mol_read_pwpot_prms(struct mol_prms *prms, FILE *fp);


static bool _starts_with_key(const char *line, const char *key)
{
	const size_t l = strlen(key);
	const bool same_prefix = (strncmp(line, key, l) == 0);
	if (same_prefix) {
		// We do this check only if same_prefix is true, so line[l] is at least '\0'
		const bool space_after_prefix = (isspace(line[l]) != 0);
		return space_after_prefix;
	}
	return false;
}


/**
 * Starting with current position in \p fp, skip to the line starting with \p line_key, and return it.
 *
 * Don't forget to \c free the returned value afterwards!
 */
static size_t _count_lines(FILE *fp, const char *line_key)
{
	size_t count = 0;
	char *line = NULL;
	size_t len = 0;

	while (getline(&line, &len, fp) != -1) {
		if (_starts_with_key(line, line_key)) {
			count++;
		}
	}

	free_if_not_null(line);
	return count;
}


/**
 * Count the number of subatom types in pwpot potential line, which is the number of components of eigenvector.
 * \param pwpot_line Line from parameters file in the format "pwpot <eigenvalue> <eigenvector[0]> <eigenvector[1]> ... <eigenvector[n-1]>"
 * \return Size of eignevector, i.e. "n" from the example above.
 */
static int _count_nsubatoms(const char *pwpot_line)
{
	int nsubatoms = 0;
	int n;

	// Skip eigenvalue
	if (sscanf(pwpot_line, "%*s %*f %n", &n) != 0) {
		_PRINTF_ERROR("Can not parse pwpot line");
		exit(EXIT_FAILURE);
	}

	pwpot_line += n; // Moves pointer locally, does not do anything outside this function

	// Count number of columns in line (not counting key and eigenvalue)
	while (sscanf(pwpot_line, "%*f %n", &n) == 0) {
		nsubatoms += 1;
		pwpot_line += n;
	}

	return nsubatoms;
}


static int comp_mol_atom_prm(const void *a1, const void *a2)
{
	const struct mol_atom_prm *atom1 = (const struct mol_atom_prm *) a1;
	const struct mol_atom_prm *atom2 = (const struct mol_atom_prm *) a2;

	int cmp1 = strcmp(atom1->typemaj, atom2->typemaj);

	return (cmp1 != 0) ? cmp1 : strcmp(atom1->typemin, atom2->typemin);
}


static int atomid(const struct mol_prms *prms, const char *typemaj, const char *typemin)
{
	struct mol_atom_prm atomkey;
	struct mol_atom_prm *atomres;

	while (typemaj[0] == ' ') {
		typemaj++;
	}
	while (typemin[0] == ' ') {
		typemin++;
	}

	strncpy(atomkey.typemaj, typemaj, 5);
	strncpy(atomkey.typemin, typemin, 5);

	while (atomkey.typemaj[strlen(atomkey.typemaj)-1] == ' ') {
		atomkey.typemaj[strlen(atomkey.typemaj)-1] = '\0';
	}

	while (atomkey.typemin[strlen(atomkey.typemin)-1] == ' ') {
		atomkey.typemin[strlen(atomkey.typemin)-1] = '\0';
	}

	atomres = bsearch(&atomkey, prms->atoms, prms->natoms,
			sizeof(struct mol_atom_prm), comp_mol_atom_prm);

	if (atomres == NULL) {
		_PRINTF_ERROR("Unable to find atom (typemaj %s, typemin %s)", atomkey.typemaj, atomkey.typemin);
		return -1;
	} else {
		return atomres->id;
	}
}

struct mol_prms *mol_prms_read(const char *path)
{
	FILE *fp = fopen(path, "r");

	if (fp == NULL) {
		perror(__func__);
		_PRINTF_ERROR("Unable to open %s", path);
		return NULL;
	}

	struct mol_prms *prms = calloc(1, sizeof(struct mol_prms));
	if (prms == NULL) {
		goto close_file_out;
	}

	prms->version = mol_prms_version(fp);

	mol_read_atoms_prms(prms, fp);
	mol_read_pwpot_prms(prms, fp);

close_file_out:
	fclose(fp);

	return prms;
}

static mol_version mol_prms_version(FILE *fp)
{
	if (fp == NULL) {
		return NULL;
	}

	mol_version version_str = calloc(128, sizeof(char));

	char *line = NULL;
	size_t len = 0;
	bool found_version = false;

	while (getline(&line, &len, fp) != -1) {
		// relying on C string constant concatenation
		if (sscanf(line, VERSION_KEY " %80s", version_str) < 1) {
			continue;
		} else {
			found_version = true;
			break;
		}
	}

	free_if_not_null(line);

	if (!found_version) {
		free_if_not_null(version_str);
		return NULL;
	}

	version_str = realloc(version_str, (strlen(version_str)+1) * sizeof(char));
	rewind(fp);

	return version_str;
}

static void mol_read_atoms_prms(struct mol_prms *prms, FILE *fp)
{
	// See the file format description at https://bitbucket.org/bu-structure/mol-prms/src/master/atom/README.md
	// In particular, "atom" section
	if (fp == NULL) {
		return;
	}

	char *line = NULL;
	size_t len = 0;

	rewind(fp);
	prms->natoms = _count_lines(fp, ATOM_KEY);
	rewind(fp);

	prms->atoms = calloc(prms->natoms, sizeof(struct mol_atom_prm));

	size_t atomsi = 0;
	while (getline(&line, &len, fp) != -1) {
		if (is_whitespace_line(line))
			continue;

		if (_starts_with_key(line, ATOM_KEY)) {
			struct mol_atom_prm *atom_prm = &prms->atoms[atomsi];

			if (sscanf(line, "%*s %d %7s %7s %d %lf %lf",
					&atom_prm->id,
					atom_prm->typemaj,
					atom_prm->typemin,
					&atom_prm->subid,
					&atom_prm->r,
					&atom_prm->q) < 6) {
				return;
			}

			atomsi++;
		}
		if (atomsi == prms->natoms) {
			break;
		}
	}

	free_if_not_null(line);
	rewind(fp);

	qsort(prms->atoms, prms->natoms, sizeof(struct mol_atom_prm),
		comp_mol_atom_prm);
	for (size_t i = 0; i < prms->natoms; i++) {
		prms->atoms[i].id = i;
	}
}

static void mol_read_pwpot_prms(struct mol_prms *prms, FILE *fp)
{
	// See the file format description at https://bitbucket.org/bu-structure/mol-prms/src/master/atom/README.md
	// In particular, "pwpot" and "pwpot.r" sections
	if (fp == NULL) {
		prms->num_pwpot = 0;
		prms->pwpot = NULL;
	}

	rewind(fp);
	const size_t num_pwpot = _count_lines(fp, PWPOT_R_KEY);
	rewind(fp);

	prms->num_pwpot = num_pwpot;
	prms->pwpot = calloc(num_pwpot, sizeof(struct mol_pwpot_prm));

	rewind(fp);

	char *line = NULL;
	size_t len = 0;

	prms->nsubatoms = 0;
	int pwpot_i = -1;
	struct mol_pwpot_prm *pwpot = NULL;
	while (getline(&line, &len, fp) != -1) {
		if (_starts_with_key(line, PWPOT_R_KEY)) {
			pwpot_i++;
			if ((size_t) pwpot_i >= num_pwpot) {
				_PRINTF_ERROR("Somehow got more pwpot.r records than expected");
				exit(EXIT_FAILURE);
			}
			pwpot = &prms->pwpot[pwpot_i];
			pwpot->k = 0;
			if (sscanf(line, "%*s %f %f", &pwpot->r1, &pwpot->r2) != 2) {
				_PRINTF_ERROR("Error parsing radii in pwpot.r");
				exit(EXIT_FAILURE);
			}
		} else if (_starts_with_key(line, PWPOT_KEY)) {
			if (pwpot == NULL) {
				_PRINTF_ERROR("pwpot directive before pwpot.r");
				exit(EXIT_FAILURE);
			}
			// Preliminary parsing and allocation
			pwpot->lambdas = realloc(pwpot->lambdas, (pwpot->k + 1) * sizeof(float));
			// Length of each eigenvector is equal to the number of atom types (prms->nsubatoms)
			if (prms->nsubatoms == 0) {
				prms->nsubatoms = _count_nsubatoms(line);
				if (prms->nsubatoms == 0) {
					_PRINTF_ERROR("Malformed " PWPOT_KEY " line in parameters file");
					exit(EXIT_FAILURE);
				}
			} else if (prms->nsubatoms != _count_nsubatoms(line)) {
				_PRINTF_ERROR("Different number of subatoms in different pwpot lines. They should be the same even for different pwpot.r sections");
				exit(EXIT_FAILURE);
			}
			if (pwpot->k == 0) {
				// First pwpot in section, must allocate arrays
				pwpot->eng = calloc(prms->nsubatoms, sizeof(float *));
				pwpot->eng_data = calloc(prms->nsubatoms * prms->nsubatoms, sizeof(float));
				for (int i = 0; i < prms->nsubatoms; i++) {
					pwpot->eng[i] = &pwpot->eng_data[i * prms->nsubatoms];
				}
			}
			pwpot->Xs = realloc(pwpot->Xs, prms->nsubatoms * (pwpot->k + 1) * sizeof(float));

			const char *tmp_line = line;
			int n;
			if (sscanf(tmp_line, "%*s %f %n", &pwpot->lambdas[pwpot->k], &n) != 1) {
				_PRINTF_ERROR("Error parsing eigenvalue in pwpot");
				exit(EXIT_FAILURE);
			}
			tmp_line += n;

			for (int j = 0; j < prms->nsubatoms; j++) {
				if (sscanf(tmp_line, "%f %n", &pwpot->Xs[(pwpot->k * prms->nsubatoms) + j], &n) != 1) {
					_PRINTF_ERROR("Error parsing eigenvector in pwpot");
					exit(EXIT_FAILURE);
				}
				tmp_line += n;
			}
			pwpot->k++;
		}
	}

	for (pwpot_i = 0; pwpot_i < (int) num_pwpot; pwpot_i++) {
		pwpot = &prms->pwpot[pwpot_i];
		for (int i = 0; i < prms->nsubatoms; i++) {
			for (int j = 0; j < prms->nsubatoms; j++) {
				double s = 0;
				for (size_t k = 0; k < pwpot->k; k++) {
					s +=
						(double) pwpot->lambdas[k] *
						pwpot->Xs[(k * prms->nsubatoms) + i] *
						pwpot->Xs[(k * prms->nsubatoms) + j];
				}
				pwpot->eng[i][j] = s;
			}
		}
	}

	free_if_not_null(line);
}

void mol_prms_destroy(struct mol_prms *prms)
{
	if (prms != NULL) {
		free_if_not_null(prms->atoms);
		if (prms->pwpot != NULL) {
			for (size_t pwpot_i = 0; pwpot_i < prms->num_pwpot; pwpot_i++) {
				free_if_not_null(prms->pwpot[pwpot_i].lambdas);
				free_if_not_null(prms->pwpot[pwpot_i].Xs);
				free_if_not_null(prms->pwpot[pwpot_i].eng);
				free_if_not_null(prms->pwpot[pwpot_i].eng_data);
			}
			free(prms->pwpot);
		}
		free_if_not_null(prms->version);
	}
}

void mol_prms_free(struct mol_prms *prms)
{
	mol_prms_destroy(prms);
	free_if_not_null(prms);
}

void print_prms(const struct mol_prms *prms)
{
	// We call this pragma's to temporarily disable deprecation warning here.
	// This function and fprint_prms are "twins", and there's nothing bad that one
	// calls the other.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
	fprint_prms(stdout, prms);         /* no diagnostic for this one */
#pragma GCC diagnostic pop
}

void fprint_prms(FILE *fp, const struct mol_prms *prms)
{
	if (fp == NULL)
		return;

	fprintf(fp, "version: %s\n", prms->version);
	fprintf(fp, "== atoms ==\n");
	fprintf(fp, "natoms: %zu\n", prms->natoms);
	fprintf(fp, "nsubatoms: %zu\n", prms->pwpot->k);
	fprintf(fp, "\n");

	fprintf(fp, "== pwpot ==\n");
	fprintf(fp, "r1: %f\t r2: %f\n", prms->pwpot->r1, prms->pwpot->r2);
	fprintf(fp, "k: %zu\n", prms->pwpot->k);
	fprintf(fp, "\n");
}

void mol_atom_group_add_prms(struct mol_atom_group *ag, const struct mol_prms *prms)
{
	if (ag->charge == NULL) {
		ag->charge = malloc(ag->natoms * sizeof(double));
	}
	if (ag->vdw_radius == NULL) {
		ag->vdw_radius = malloc(ag->natoms * sizeof(double));
	}
	if (ag->pwpot_id == NULL) {
		ag->pwpot_id = malloc(ag->natoms * sizeof(int));
	}

	for (size_t i = 0; i < ag->natoms; i++) {
		int id = atomid(prms, ag->residue_name[i], ag->atom_name[i]);
		if (id == -1) {
			_PRINTF_WARNING("prm type not found for atom %zu", i);
			ag->charge[i] = 0.0;
			ag->vdw_radius[i] = 0.0;
			ag->pwpot_id[i] = -1;
			continue;
		}

		ag->charge[i] = prms->atoms[id].q;
		ag->vdw_radius[i] = prms->atoms[id].r;
		ag->pwpot_id[i] = prms->atoms[id].subid;
	}
}


struct mol_prms *read_prms(const char *path)
{
	_PRINTF_DEPRECATION("mol_prms_read");
	return mol_prms_read(path);
}


void destroy_prms(struct mol_prms *prms)
{
	_PRINTF_DEPRECATION("mol_prms_destroy");
	mol_prms_destroy(prms);
}

void free_prms(struct mol_prms *prms)
{
	_PRINTF_DEPRECATION("mol_prms_free");
	mol_prms_free(prms);
}