#define _POSIX_C_SOURCE 200809L
#include "rmsd.h"

#include <math.h>
#include <float.h>

double rmsd(struct mol_vector3 *la, struct mol_vector3 *lb, size_t len)
{
	double rmsd_sq = 0.0;

	for (size_t i = 0; i < len; ++i) {
		struct mol_vector3 *a = &la[i];
		struct mol_vector3 *b = &lb[i];
		rmsd_sq += pow(a->X - b->X, 2.0) +
			pow(a->Y - b->Y, 2.0) +
			pow(a->Z - b->Z, 2.0);
	}

	return sqrt(rmsd_sq / len);
}

double atom_group_rmsd(struct mol_atom_group *pa, struct mol_atom_group *pb)
{
	if (pa->natoms != pb->natoms)
		return NAN;

	return rmsd(pa->coords, pb->coords, pa->natoms);
}

double index_rmsd(struct mol_vector3 *la, struct mol_vector3 *lb,
                  struct mol_index_list *indices)
{
	double rmsd_sq = 0.0;

	for (size_t i = 0; i < indices->size; ++i) {
		size_t ii = indices->members[i];

		struct mol_vector3 *a = &la[ii];
		struct mol_vector3 *b = &lb[ii];
		rmsd_sq += pow(a->X - b->X, 2.0) +
			pow(a->Y - b->Y, 2.0) +
			pow(a->Z - b->Z, 2.0);
	}
	return sqrt(rmsd_sq / indices->size);
}

struct mol_index_list *atom_group_all_indices(struct mol_atom_group *ag)
{
	struct mol_index_list *indices = calloc(1, sizeof(struct mol_index_list));
        indices->size = ag->natoms;

        indices->members = malloc(ag->natoms * sizeof(size_t));
	for (size_t i = 0; i < ag->natoms; ++i)
		indices->members[i] = i;

	return indices;
}

/*Recursive algorithm for detecting symmetry based on bonds and atom types of the molecule.
Basic idea is in the fact that algorithm on depth k try to find atom in the potential permutation which behaves exactly like
atom k in original numbering this is achieved by comparing bonds of potential candidate with all atoms to the bonds of atom k
. When all atoms are arranged reuired permutation is obtained, however extra check is need to make sure that atoms are connected the same way as in original arrnagement
 */
static void detectsymmetry_recursive(struct mol_index_list *molecule,
				     size_t nftypes, struct mol_atom_group *ag, struct mol_symmetry_list *symmetry)
{
	struct mol_bool_list in_symmetry;
	mol_list_init(in_symmetry, ag->natoms);
	for (size_t i = 0; i < molecule->size; i++) {
		in_symmetry.members[molecule->members[i]] = true;
	}
	//Describe all bonds at level n (molecule->n) for original order molecule in bondstem
	//Check for all atoms not used yet, as feasible for level n
	for (size_t i = 0; i < ag->natoms; i++) {
		//We check whether atom is already used in the symmetry, whether it has the
		//same type as we need, and the same number of bonds
		if ((in_symmetry.members[i] == 0)
		    && (ag->ftypen[i] == ag->ftypen[molecule->size])
		    && (ag->bond_lists[i].size == ag->bond_lists[molecule->size].size)) {
			bool match = true;
			struct mol_int_fast8_t_list bonded_ftypes;
			mol_list_init(bonded_ftypes, nftypes);
			//Here we check that it is bonded to the same atom type as the nth level atom
			for (size_t j = 0; j < ag->bond_lists[molecule->size].size; j++) {
				size_t ai = mol_atom_group_get_neighbor(ag, molecule->size, j);
				bonded_ftypes.members[ag->ftypen[ai]]++;
			}
			for (size_t j = 0; j < ag->bond_lists[i].size; j++) {
				size_t ai = mol_atom_group_get_neighbor(ag, i, j);
				bonded_ftypes.members[ag->ftypen[ai]]--;
			}
			for (size_t k = 0; k < nftypes; k++) {
				if (bonded_ftypes.members[k] != 0) {
					match = false;
				}
			}
			if (match) {
				molecule->members[molecule->size] = i;
				if (molecule->size == (ag->natoms - 1)) {
					//We have good permutation where all atoms has the same bonding
					//pattern as original, it could happen that they are not connected the same way
					//We check that atoms bonded in original are also bonded in permutation
					struct mol_index_list chklist;
					chklist.members = calloc(ag->natoms, sizeof(size_t));
					for (size_t j = 0; j < ag->natoms; j++) {
						for (size_t k = 0; k < ag->bond_lists[j].size; k++) {
							size_t ai = mol_atom_group_get_neighbor(ag, j, k);
							chklist.members[molecule->members[ai]]++;
						}
						for (size_t k = 0; k < ag->bond_lists[molecule->members[j]].size; k++) {
							size_t ai = mol_atom_group_get_neighbor(ag, molecule->members[j], k);
							chklist.members[ai]--;
						}
						for (size_t k = 0; k < ag->natoms; k++) {
							if (chklist.members[k] != 0) {
								match = false;
							}
						}
					}
					mol_list_destroy(&chklist);
					if (match) {
						//offset by number of solutions already stored
						size_t offset = (symmetry->size * ag->natoms);
						for (size_t j = 0; j < ag->natoms; j++) {
							symmetry->members[j + offset] = molecule->members[j];
						}
						symmetry->size++;
					}
				} else {
					molecule->size++;
					detectsymmetry_recursive(molecule, nftypes, ag, symmetry);
					molecule->size--;
				}
			}
			mol_list_destroy(&bonded_ftypes);
		}
	}
	mol_list_destroy(&in_symmetry);
}

struct mol_symmetry_list *mol_detect_symmetry(struct mol_atom_group *ag)
{
	struct mol_symmetry_list *symmetry = calloc(1, sizeof(struct mol_symmetry_list));
	symmetry->ag = ag;
	struct mol_index_list molecule;
	size_t nftypes = 0;
	symmetry->members = calloc(ag->natoms * ag->natoms * ag->natoms, sizeof(size_t));
	symmetry->size = 0;
	molecule.size = 0;
	molecule.members = calloc(ag->natoms, sizeof(size_t));
	for (size_t i = 0; i < ag->natoms; i++) {
		if (ag->ftypen[i] > nftypes) {
			nftypes = ag->ftypen[i];
		}
	}
	//due to zero indexing and typical iteration, add to the nftypes
	nftypes++;

	if (ag->natoms > 1) {
		for (size_t i = 0; i < ag->natoms; i++) {
			if (ag->ftypen[i] == ag->ftypen[0]) {
				molecule.members[molecule.size++] = i;
				detectsymmetry_recursive(&molecule, nftypes, ag, symmetry);
				molecule.size--;
			}
		}
	} else {
//Default solution
		symmetry->members[0] = 0;
		symmetry->size = 1;
	}
	symmetry->members = realloc(symmetry->members, symmetry->size * ag->natoms * sizeof(size_t));

	mol_list_destroy(&molecule);
	return symmetry;
}

double mol_atom_group_rmsd_sym(struct mol_atom_group *pa, struct mol_atom_group *pb,
		struct mol_symmetry_list *symmetry)
{
	if (pa->natoms != pb->natoms || symmetry->ag->natoms != pa->natoms) {
		return NAN;
	}

	double rmsd_min = DBL_MAX;
	for (size_t s = 0; s < symmetry->size; s++) {
		double sum = 0.0;
		for (size_t pa_i = 0; pa_i < pa->natoms; pa_i++) {
			size_t pb_i = symmetry->members[pa_i + (s*pa->natoms)];
			sum += MOL_VEC_EUCLIDEAN_DIST_SQ(pa->coords[pa_i], pb->coords[pb_i]);
		}
		sum /= pa->natoms;
		rmsd_min = fmin(sum, rmsd_min);
	}

	return sqrt(rmsd_min);
}
