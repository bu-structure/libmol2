#include <math.h>

#include "energy_vina.h"
#include "atom_group.h"
#include "pdbqt.h"
#include "private_utils.h"

static const struct mol_vina_components def_weights = {
	.gauss1 = -0.035579,
	.gauss2 = -0.005156,
	.repuls = 0.840245,
	.hydrph = -0.035069,
	.hydrbn = -0.587439,
	.branch = 0.05846
};

// Paragraph after Eq. (8) in [1]
static const double VINA_CUTOFF_NB = 8.0; ///< Cutoff for non-bonded interactions, Angstrom

static const double VINA_CUTOFF_BOND = 1.7; ///< Cutoff for covalent bonds, Angstroms
static const double VINA_CUTOFF_HBOND = 1.2; ///< Cutoff for hydrogen bonds, Angstroms

enum atom_element {
	ELEMENT_H = 0, // H, HD
	ELEMENT_C = 1, // C, A
	ELEMENT_N = 2, // N, NA
	ELEMENT_O = 3, // O, OA
	ELEMENT_S = 4, // S, SA
	ELEMENT_P = 5, // P
	ELEMENT_F = 6, // F
	ELEMENT_Cl = 7, // Cl
	ELEMENT_Br = 8, // Br
	ELEMENT_I = 9, // I
	ELEMENT_METAL = 10, // Mg, Mn, Zn, Ca, Fe
	ELEMENT_X = 11,
};

static const enum atom_element atom_element_from_autodock_type[] = {
	ELEMENT_C,
	ELEMENT_C, // A
	ELEMENT_N,
	ELEMENT_O,
	ELEMENT_P,
	ELEMENT_S,
	ELEMENT_H,
	ELEMENT_F,
	ELEMENT_I,
	ELEMENT_N, // NA
	ELEMENT_O, // OA
	ELEMENT_S, // SA
	ELEMENT_H, // HD
	ELEMENT_METAL, // Mg
	ELEMENT_METAL, // Mn
	ELEMENT_METAL, // Zn
	ELEMENT_METAL, // Ca
	ELEMENT_METAL, // Fe
	ELEMENT_Cl,
	ELEMENT_Br,
	ELEMENT_X,
};

static const double xs_atom_radii[] = {1.9, 1.9, 1.8, 1.8, 1.8, 1.8, 1.7, 1.7, 1.7, 1.7, 2.0, 2.1, 1.5, 1.8, 2.0, 2.2, 1.2};

static enum mol_vina_atom_type _deduce_vina_atom_type(enum mol_autodock_atom_type t, const bool phb, const bool cth)
{
	const enum atom_element elem = atom_element_from_autodock_type[t];
	bool acc = (t == AUTODOCK_ATOM_OA || t == AUTODOCK_ATOM_NA);
	bool don = (elem == ELEMENT_METAL || phb);
	if (elem == ELEMENT_C) {
		if (cth)
			return XS_ATOM_C_HET;
		else
			return XS_ATOM_C;
	}
	if (elem == ELEMENT_N) {
		if (don && acc)
			return XS_ATOM_N_AD;
		else if (acc)
			return XS_ATOM_N_A;
		else if (don)
			return XS_ATOM_N_D;
		else
			return XS_ATOM_N;
	}
	if (elem == ELEMENT_O) {
		if (don && acc)
			return XS_ATOM_O_AD;
		else if (acc)
			return XS_ATOM_O_A;
		else if (don)
			return XS_ATOM_O_D;
		else
			return XS_ATOM_O;
	}
	if (elem >= 4 && elem <= 10) // S, P, F, Cl, Br, I, METAL
		return elem + 6;
	return XS_ATOM_X;
}


/// Eq. (6)
static double _gauss1(struct mol_vector3 *grad_total, double *en_total, const struct mol_vector3 *r, const double d, const double weight)
{
	double d1 = 2 * d;
	double en = exp(-d1 * d1);
	double enw = en * weight;

	double g = -4 * enw * d1;
	struct mol_vector3 grad;
	MOL_VEC_MULT_SCALAR(grad, *r, -g);

	MOL_VEC_ADD(*grad_total, *grad_total, grad);
	*en_total += en * weight;
	return en;
}

/// Eq. (7)
static double _gauss2(struct mol_vector3 *grad_total, double *en_total, const struct mol_vector3 *r, const double d, const double weight)
{
	double d1 = 0.5 * d - 1.5;
	double en = exp(-d1 * d1);
	double enw = en * weight;
	double g = -enw * d1;
	struct mol_vector3 grad;
	MOL_VEC_MULT_SCALAR(grad, *r, -g);

	MOL_VEC_ADD(*grad_total, *grad_total, grad);
	*en_total += en * weight;
	return en;
}

/// Eq. (8)
static double _repuls(struct mol_vector3 *grad_total, double *en_total, const struct mol_vector3 *r, const double d, const double weight)
{
	double en;
	if (d < 0) {
		en = d * d;
		double g = 2 * d * weight;
		struct mol_vector3 grad;
		MOL_VEC_MULT_SCALAR(grad, *r, -g);
		MOL_VEC_ADD(*grad_total, *grad_total, grad);

	} else
		en = 0;

	*en_total += en * weight;
	return en;
}

/// Paragraph after Eq. (8) in [1]
static double _hydrph(struct mol_vector3 *grad_total, double *en_total, const struct mol_vector3 *r, const double d, const double weight)
{
	double en;
	if (d < 0.5) {
		en = 1;
	} else if (d <= 1.5) {
		en = 1.5 - d;
		double g = -weight;
		struct mol_vector3 grad;
		MOL_VEC_MULT_SCALAR(grad, *r, -g);
		MOL_VEC_ADD(*grad_total, *grad_total, grad);
	} else
		en = 0;

	*en_total += en * weight;
	return en;
}

/// Paragraph after Eq. (8) in [1]
static double _hydrbn(struct mol_vector3 *grad_total, double *en_total, const struct mol_vector3 *r, const double d, const double weight)
{
	double en;
	if (d < -0.7) {
		en = 1;
	} else if (d <= 0.0) {
		en = -d / 0.7;
		double g = -weight / 0.7;
		struct mol_vector3 grad;
		MOL_VEC_MULT_SCALAR(grad, *r, -g);
		MOL_VEC_ADD(*grad_total, *grad_total, grad);
	} else
		en = 0;

	*en_total += en * weight;
	return en;
}


static bool _is_heteroatom(const enum mol_autodock_atom_type t)
{
	const enum atom_element elem = atom_element_from_autodock_type[t];
	return (elem != ELEMENT_C && elem != ELEMENT_H);
}


static bool _is_polar_h(const enum mol_autodock_atom_type t)
{
	return (t == AUTODOCK_ATOM_HD);
}

static bool _is_bound_to_atom_from_topology(
		const struct mol_atom_group *ag,
		const enum mol_autodock_atom_type *adt,
		const size_t i,
		bool (*filter)(const enum mol_autodock_atom_type))
{
	for (size_t bond_j = 0; bond_j < ag->bond_lists[i].size; bond_j++) {
		const size_t j = mol_atom_group_get_neighbor(ag, i, bond_j);
		if (filter(adt[j]))
			return true;
	}
	return false;
}

static bool _is_bound_to_atom_from_coords(
		const struct mol_atom_group *ag,
		const enum mol_autodock_atom_type *adt,
		const size_t i,
		const double cutoff_sq,
		bool (*filter)(const enum mol_autodock_atom_type))
{
	for (size_t j = 0; j < ag->natoms; j++) {
		if (filter(adt[j])) {
			const double dist_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[i], ag->coords[j]);
			if (dist_sq < cutoff_sq)
				return true;
		}
	}
	return false;
}

/**
 * Check if atom \p i is carbon with a bond to a heteroatom (neither carbon nor hydrogen).
 * \param ag Atomgroup.
 * \param adt List of Autodock atom types for atoms. See \ref mol_pdbqt_read.
 * \param i Atom index.
 * \return True if if atom \p i is carbon and has at least one bond to a heteroatom; false otherwise.
 */
static bool _is_c_bound_to_het(const struct mol_atom_group *ag, const enum mol_autodock_atom_type *adt, const size_t i, const bool ignore_topology)
{
	const enum atom_element elem_i = atom_element_from_autodock_type[adt[i]];
	if (elem_i != ELEMENT_C)
		return false;

	if (ignore_topology) {
		const double cutoff_sq = VINA_CUTOFF_BOND * VINA_CUTOFF_BOND;
		return _is_bound_to_atom_from_coords(ag, adt, i, cutoff_sq, _is_heteroatom);
	} else {
		return _is_bound_to_atom_from_topology(ag, adt, i, _is_heteroatom);
	}
}

static bool _is_bound_to_polar_h(const struct mol_atom_group *ag, const enum mol_autodock_atom_type *adt, const size_t i, const bool ignore_topology)
{
	if (adt[i] == AUTODOCK_ATOM_HD || adt[i] == AUTODOCK_ATOM_H)
		return false;

	if (ignore_topology) {
		const double cutoff_sq = VINA_CUTOFF_HBOND * VINA_CUTOFF_HBOND;
		return _is_bound_to_atom_from_coords(ag, adt, i, cutoff_sq, _is_polar_h);
	} else {
		return _is_bound_to_atom_from_topology(ag, adt, i, _is_polar_h);
	}
}


static enum mol_vina_atom_type *_deduce_vina_atom_types(const struct mol_atom_group *ag, const bool ignore_topology)
{
	const enum mol_autodock_atom_type *adt = \
		(const enum mol_autodock_atom_type *) \
			mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY);
	if (adt == NULL) {
		_PRINTF_ERROR("To use Vina you must have PDBQT atom types");
		return NULL;
	}
	enum mol_vina_atom_type *ret = calloc(ag->natoms, sizeof(enum mol_vina_atom_type));
	for (size_t i = 0; i < ag->natoms; i++)
		ret[i] = _deduce_vina_atom_type(adt[i],
		                                _is_bound_to_polar_h(ag, adt, i, ignore_topology),
		                                _is_c_bound_to_het(ag, adt, i, ignore_topology));
	return ret;
}

inline static bool _don(const enum mol_vina_atom_type t)
{
	switch (t) {
		case XS_ATOM_N_D:
		case XS_ATOM_N_AD:
		case XS_ATOM_O_D:
		case XS_ATOM_O_AD:
		case XS_ATOM_METAL:
			return true;
		default:
			return false;
	}
}

inline static bool _acc(const enum mol_vina_atom_type t)
{
	switch (t) {
		case XS_ATOM_N_A:
		case XS_ATOM_N_AD:
		case XS_ATOM_O_A:
		case XS_ATOM_O_AD:
			return true;
		default:
			return false;
	}
}

inline static bool _hyd(const enum mol_vina_atom_type t)
{
	switch (t) {
		case XS_ATOM_C:
		case XS_ATOM_F:
		case XS_ATOM_Cl:
		case XS_ATOM_Br:
		case XS_ATOM_I:
			return true;
		default:
			return false;
	}
}

///! Return false if the atom type does not participate in any calculations (e.g., Hydrogen)
inline static bool _valid(const enum mol_vina_atom_type t)
{
	return (t >= 0 && t < XS_ATOM_X);
}


static void _en_vina(
		struct mol_vector3 *gv,
		double *energy,
		struct mol_vina_components *energies,
		const struct mol_vector3 *r,
		const enum mol_vina_atom_type type_i,
		const enum mol_vina_atom_type type_j,
		const struct mol_vina_components *weights,
		const double vcut_sq)
{
	if (!_valid(type_i) || !_valid(type_j))
		return;

	const double r_sq = MOL_VEC_SQ_NORM(*r);
	if (r_sq > vcut_sq)
		return;

	const double riat = xs_atom_radii[type_i];
	const double rjat = xs_atom_radii[type_j];

	const double dist = sqrt(r_sq);
	struct mol_vector3 r_norm;
	MOL_VEC_DIV_SCALAR(r_norm, *r, dist);
	const double d = dist - riat - rjat;

	energies->gauss1 += _gauss1(gv, energy, &r_norm, d, weights->gauss1);
	energies->gauss2 += _gauss2(gv, energy, &r_norm, d, weights->gauss2);
	energies->repuls += _repuls(gv, energy, &r_norm, d, weights->repuls);

	if (_hyd(type_i) && _hyd(type_j))
		energies->hydrph += _hydrph(gv, energy, &r_norm, d, weights->hydrph);

	if ((_don(type_i) && _acc(type_j)) || (_don(type_j) && _acc(type_i)))
		energies->hydrbn += _hydrbn(gv, energy, &r_norm, d, weights->hydrbn);
}


struct mol_index_list _ligand_bonds_from_coords(
		const struct mol_atom_group *ag,
		const size_t lig_index_first,
		const size_t lig_index_last)
{
	struct mol_index_list bonds;
	const size_t natoms = lig_index_last - lig_index_first + 1;
	size_t nbonds = 0;
	bonds.members = calloc(natoms * (natoms - 1), sizeof(size_t));
	const double ab2 = VINA_CUTOFF_BOND * VINA_CUTOFF_BOND;
	for (size_t i = lig_index_first; i < lig_index_last; i++) {
		for (size_t j = i + 1; j <= lig_index_last; j++) {
			double d = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[i], ag->coords[j]);
			if (d < ab2) {
				size_t k = 2*nbonds;
				bonds.members[k] = i;
				bonds.members[k + 1] = j;
				nbonds++;
			}
		}
	}
	bonds.members = realloc(bonds.members, 2 * nbonds * sizeof(size_t));
	bonds.size = nbonds;
	return bonds;
}

struct mol_index_list _ligand_bonds_from_topology(
		const struct mol_atom_group *ag,
		const size_t lig_index_first,
		const size_t lig_index_last)
{
	struct mol_index_list bonds;
	size_t nbonds = 0;
	struct mol_bond *b;
	bonds.members = calloc(ag->nbonds, sizeof(size_t));
	for (size_t i = 0; i < ag->nbonds; ++i) {
		b = &(ag->bonds[i]);
		if ((lig_index_first <= b->ai) && (b->ai <= lig_index_last) && \
				(lig_index_first <= b->aj) && (b->aj <= lig_index_last)) {
			size_t k = 2 * nbonds;
			bonds.members[k] = b->ai;
			bonds.members[k + 1] = b->aj;
			nbonds++;
		}
	}
	bonds.members = realloc(bonds.members, 2 * nbonds * sizeof(size_t));
	bonds.size = nbonds;
	return bonds;
}

bool *_build_excl_1_4(
		const struct mol_index_list bonds,
		const size_t lig_index_first,
		const size_t lig_index_last)
{
	const size_t natoms = lig_index_last - lig_index_first + 1;
	bool *exclmat = calloc(natoms * natoms, sizeof(bool));

	// Exclude self interactions
	for (size_t i = 0; i < natoms; i++) {
		exclmat[natoms * i + i] = true;
	}

	// Exclude 0-1 bonds
	for (size_t k = 0; k < bonds.size; k++) {
		size_t bi = bonds.members[2 * k] - lig_index_first;
		size_t bj = bonds.members[2 * k + 1] - lig_index_first;
		exclmat[natoms * bi + bj] = true;
		exclmat[natoms * bj + bi] = true;
	}

	// Exclude 0-2 and 0-3 bonds
	if (bonds.size > 1) {
		// The check (bonds.size > 0) is needed to avoid (0 - 1) of unsigned variable.
		// Since we're already doing this check, might as well test that we have at least two bonds.
		size_t nexcl = 0;
		// During this cycle we use exclmat to quickly look-up covalent bonds, and we record the 0-2 and 0-3 to the
		// excl array. Then we copy the content of excl array to the matrix.
		size_t *excl = calloc(bonds.size * (bonds.size - 1) * 4, sizeof(size_t));
		for (size_t i = 0; i < bonds.size - 1; i++) {
			size_t ai = bonds.members[2 * i] - lig_index_first;
			size_t bi = bonds.members[2 * i + 1] - lig_index_first;
			for (size_t j = i + 1; j < bonds.size; j++) {
				size_t aj = bonds.members[2 * j] - lig_index_first;
				size_t bj = bonds.members[2 * j + 1] - lig_index_first;
				// exclmat[ai, aj] means that bi and bj are either
				//      0-3 neighbors (if ai and aj are covalently linked), or
				//      0-2 neighbors (if ai == aj).
				if (exclmat[natoms * ai + aj]) {
					excl[2 * nexcl] = bi;
					excl[2 * nexcl + 1] = bj;
					nexcl++;
				}
				if (exclmat[natoms * ai + bj]) {
					excl[2 * nexcl] = bi;
					excl[2 * nexcl + 1] = aj;
					nexcl++;
				}
				if (exclmat[natoms * bi + aj]) {
					excl[2 * nexcl] = ai;
					excl[2 * nexcl + 1] = bj;
					nexcl++;
				}
				if (exclmat[natoms * bi + bj]) {
					excl[2 * nexcl] = ai;
					excl[2 * nexcl + 1] = aj;
					nexcl++;
				}
			}
		}
		for (size_t i = 0; i < nexcl; i++) {
			exclmat[natoms * excl[2 * i] + excl[2 * i + 1]] = true;
			exclmat[natoms * excl[2 * i + 1] + excl[2 * i]] = true;
		}
		free(excl);
	}
	return exclmat;
}

void mol_vina_update_inter_list(struct mol_vina_params *vp, const struct mol_atom_group *ag)
{
	if (vp->rec_list.members == NULL) {
		vp->rec_list.members = calloc(ag->natoms, sizeof(size_t));
	} else {
		vp->rec_list.members = realloc(vp->rec_list.members, ag->natoms * sizeof(size_t));
	}
	size_t size = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		if (i >= vp->lig_index_first && i <= vp->lig_index_last)
			continue;
		struct mol_vector3 dist_min, dist_max;
		MOL_VEC_SUB(dist_min, vp->box_min, ag->coords[i]);
		MOL_VEC_SUB(dist_max, ag->coords[i], vp->box_max);
		// Zero if the atom is inside the box. Vector to nearest edge/side if outside.
		struct mol_vector3 dist = { 0 };
		MOL_VEC_MAX(dist, dist, dist_min);
		MOL_VEC_MAX(dist, dist, dist_max);
		double d = MOL_VEC_SQ_NORM(dist);
		if (d <= vp->cutoff_sq) {
			vp->rec_list.members[size] = i;
			size++;
		}
	}
	vp->rec_list.members = realloc(vp->rec_list.members, sizeof(size_t) * size);
	vp->rec_list.size = size;
}


void mol_vina_energy_intra(
		struct mol_atom_group *ag,
		double *energy,
		struct mol_vina_components *energies_detailed,
		const struct mol_vina_params *vp)
{
	const size_t natoms = vp->lig_index_last - vp->lig_index_first + 1;
	struct mol_vina_components energies = { 0 };
	for (size_t i = vp->lig_index_first; i < vp->lig_index_last; i++) {
		const enum mol_vina_atom_type ixs = vp->atom_types[i];
		const size_t i_lig = i - vp->lig_index_first;

		for (size_t j = i + 1; j <= vp->lig_index_last; j++) {
			const size_t j_lig = j - vp->lig_index_first;
			if (vp->exclmat[natoms * i_lig + j_lig])
				continue;

			const enum mol_vina_atom_type jxs = vp->atom_types[j];

			struct mol_vector3 d;
			MOL_VEC_SUB(d, ag->coords[i], ag->coords[j]);

			struct mol_vector3 grad = { 0 };
			_en_vina(&grad, energy, &energies, &d, ixs, jxs, &(vp->weights), vp->cutoff_sq);
			MOL_VEC_ADD(ag->gradients[i], ag->gradients[i], grad);
			MOL_VEC_SUB(ag->gradients[j], ag->gradients[j], grad);
		}
	}
	if (energies_detailed)
		*energies_detailed = energies;
}

void mol_vina_energy_inter(
		struct mol_atom_group *ag,
		double *energy,
		struct mol_vina_components *energies_detailed,
		const struct mol_vina_params *vp)
{
	struct mol_vina_components energies = { 0 };
	for (size_t i = vp->lig_index_first; i <= vp->lig_index_last; i++) {
		const enum mol_vina_atom_type ixs = vp->atom_types[i];

		for (size_t jl = 0; jl < vp->rec_list.size; jl++) {
			size_t j = vp->rec_list.members[jl];
			const enum mol_vina_atom_type jxs = vp->atom_types[j];

			struct mol_vector3 d;
			MOL_VEC_SUB(d, ag->coords[i], ag->coords[j]);

			struct mol_vector3 grad = { 0 };
			_en_vina(&grad, energy, &energies, &d, ixs, jxs, &(vp->weights), vp->cutoff_sq);
			MOL_VEC_ADD(ag->gradients[i], ag->gradients[i], grad);
			MOL_VEC_SUB(ag->gradients[j], ag->gradients[j], grad);
		}
	}

	if (energies_detailed)
		*energies_detailed = energies;
}

#define PDBQT_BUFFER_SIZE 91
size_t mol_vina_get_num_branches_from_pdbqt(const char *pdbqt_filename)
{
	char buffer[PDBQT_BUFFER_SIZE];
	size_t nbranch = 0;
	FILE *fp = fopen(pdbqt_filename, "r");
	if (fp == NULL)
		return 0;
	while (fgets(buffer, PDBQT_BUFFER_SIZE - 1, fp) != NULL) {
		if (!strncmp(buffer, "BRANCH", 6)) {
			nbranch++;
		}
	}
	fclose(fp);
	return nbranch;
}
#undef PDBQT_BUFFER_SIZE

double mol_vina_free_energy(const double energy, const size_t nbranch, const struct mol_vina_params *vp)
{
	double weight = def_weights.branch;
	if (vp != NULL)
		weight = vp->weights.branch;
	return energy / (1.0 + weight * nbranch);
}

bool mol_vina_update_box(
		struct mol_vina_params *vp,
		const struct mol_atom_group *ag,
		const double buffer,
		const bool lazy)
{
	struct mol_vector3 lig_min, lig_max;
	lig_min = ag->coords[vp->lig_index_first];
	lig_max = ag->coords[vp->lig_index_first];
	for (size_t i = vp->lig_index_first + 1; i <= vp->lig_index_last; i++) {
		MOL_VEC_MIN(lig_min, lig_min, ag->coords[i]);
		MOL_VEC_MAX(lig_max, lig_max, ag->coords[i]);
	}
	if (lazy) {
		// If lazy update, and ligand is still withing the old box, don't update anything
		if (lig_min.X >= vp->box_min.X && lig_min.Y >= vp->box_min.Y && lig_min.Z >= vp->box_min.Z &&
				lig_max.X <= vp->box_max.X && lig_max.Y <= vp->box_max.Y && lig_max.Z <= vp->box_max.Z)
			return false;
	}

	MOL_VEC_SUB_SCALAR(vp->box_min, lig_min, buffer);
	MOL_VEC_ADD_SCALAR(vp->box_max, lig_max, buffer);
	return true;
}

struct mol_vina_params mol_vina_params_init(
		const struct mol_atom_group *ag,
		const size_t lig_index_first,
		const size_t lig_index_last,
		const bool ignore_topology,
		const double box_buffer)
{
	struct mol_vina_params vp;

	vp.weights = def_weights;
	vp.cutoff_sq = VINA_CUTOFF_NB * VINA_CUTOFF_NB; // Angstrom
	vp.lig_index_first = lig_index_first;
	vp.lig_index_last = lig_index_last;

	// Build exclusion matrix for the ligand
	struct mol_index_list ligand_bonds;
	if (ignore_topology) {
		// Vina uses some really smart algorithm to deduce whether covalent bond exists between atoms
		// We use plain cutoff criterion
		ligand_bonds = _ligand_bonds_from_coords(ag, lig_index_first, lig_index_last);
	} else {
		// Or if we have topology for the ligand we can use it, because why not?
		ligand_bonds = _ligand_bonds_from_topology(ag, lig_index_first, lig_index_last);
	}
	vp.exclmat = _build_excl_1_4(ligand_bonds, lig_index_first, lig_index_last);
	mol_list_destroy(&ligand_bonds);

	if (box_buffer < 0) {
		const double big = 999999999999.9;
		MOL_VEC_SET_SCALAR(vp.box_min, -big);
		MOL_VEC_SET_SCALAR(vp.box_max, big);
	} else {
		mol_vina_update_box(&vp, ag, box_buffer, false);
	}

	vp.rec_list.size = 0;
	vp.rec_list.members = NULL;
	mol_vina_update_inter_list(&vp, ag);

	vp.atom_types = _deduce_vina_atom_types(ag, ignore_topology);

	return vp;
}


void mol_vina_params_destroy(struct mol_vina_params *vp)
{
	free(vp->exclmat);
	free(vp->atom_types);
	mol_list_destroy(&(vp->rec_list));
}


void mol_vina_components_scale(struct mol_vina_components *vc, const double scale)
{
	vc->gauss1 *= scale;
	vc->gauss2 *= scale;
	vc->repuls *= scale;
	vc->hydrph *= scale;
	vc->hydrbn *= scale;
}
