#ifndef _MOL_ICHARMM_H_
#define _MOL_ICHARMM_H_

#include "atom_group.h"

/**
	routines for reading charmm forcefield parameters for the
	particular atom structure
*/

bool mol_atom_group_read_geometry(struct mol_atom_group *ag,
				const char *psffile,
				const char *prmfile,
				const char *rtffile);

bool mol_atom_group_read_psf(struct mol_atom_group *ag, const char *psffile);

#endif /* _MOL_ICHARMM_H_ */
