#ifndef _MOL_MATRIX_H_
#define _MOL_MATRIX_H_

#include "lists.h"

struct mol_matrix3 {
	double m11;
	double m12;
	double m13;
	double m21;
	double m22;
	double m23;
	double m31;
	double m32;
	double m33;
};

struct mol_matrix3f {
	float m11;
	float m12;
	float m13;
	float m21;
	float m22;
	float m23;
	float m31;
	float m32;
	float m33;
};

struct mol_matrix3l {
	long double m11;
	long double m12;
	long double m13;
	long double m21;
	long double m22;
	long double m23;
	long double m31;
	long double m32;
	long double m33;
};

struct mol_matrix3_list *mol_matrix3_list_from_file(const char *filename);
struct mol_matrix3f_list *mol_matrix3f_list_from_file(const char *filename);
struct mol_matrix3l_list *mol_matrix3l_list_from_file(const char *filename);

/**
 * Rotate a matrix \p r1 using matrix \p r2.
 * The name is a bit confusing: the operator order is backwards compared to what's expected of matrix multiplication!
 * If we use [row, col] indexing of elements, out = r2 * r1.
 */
void mol_matrix3_multiply(struct mol_matrix3 *out, const struct mol_matrix3 *r1,
                          const struct mol_matrix3 *r2);

#ifndef MOL_MATRIX_COPY
#define MOL_MATRIX_COPY(DST, U) do               \
	{					\
		(DST).m11 = (U).m11;	\
		(DST).m12 = (U).m12;	\
		(DST).m13 = (U).m13;	\
		(DST).m21 = (U).m21;	\
		(DST).m22 = (U).m22;	\
		(DST).m23 = (U).m23;	\
		(DST).m31 = (U).m31;	\
		(DST).m32 = (U).m32;	\
		(DST).m33 = (U).m33;	\
	} while(0)
#endif

#ifndef MOL_MATRIX_TRANSPOSE
#define MOL_MATRIX_TRANSPOSE(DST, U) do               \
	{					\
		(DST).m11 = (U).m11;	\
		(DST).m12 = (U).m21;	\
		(DST).m13 = (U).m31;	\
		(DST).m21 = (U).m12;	\
		(DST).m22 = (U).m22;	\
		(DST).m23 = (U).m32;	\
		(DST).m31 = (U).m13;	\
		(DST).m32 = (U).m23;	\
		(DST).m33 = (U).m33;	\
	} while(0)
#endif

#endif /* _MOL_MATRIX_H_ */
