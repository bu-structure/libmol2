#include <errno.h>
#include <math.h>
#include <string.h>

#include "transform.h"

/* Macro designed to allow transform functions to handle null pointer cases.
 *
 * Assumes that the ARG is a pointer and tests whether or not it is null, and
 * logs the result in errno global variable.
 *
 * */
#define __MOL_NULL_ARG_CHECK(ARG) do			\
	{						\
		if (ARG == NULL) {			\
			errno = EFAULT;			\
			return;				\
		}					\
	} while(0)

void mol_vector3_translate(struct mol_vector3 *v, const struct mol_vector3 *tv)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(tv);
	MOL_VEC_ADD((*v), (*tv), (*v));
}

void mol_vector3f_translate(struct mol_vector3f *v, const struct mol_vector3f *tv)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(tv);
	MOL_VEC_ADD((*v), (*tv), (*v));
}

void mol_vector3l_translate(struct mol_vector3l *v, const struct mol_vector3l *tv)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(tv);
	MOL_VEC_ADD((*v), (*tv), (*v));
}

void mol_vector3_rotate(struct mol_vector3 *v, const struct mol_matrix3 *r)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(r);
	double X = v->X, Y = v->Y, Z = v->Z;
	v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
	v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
	v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
}

void mol_vector3f_rotate(struct mol_vector3f *v, const struct mol_matrix3f *r)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(r);
	float X = v->X, Y = v->Y, Z = v->Z;
	v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
	v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
	v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
}

void mol_vector3l_rotate(struct mol_vector3l *v, const struct mol_matrix3l *r)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(r);
	long double X = v->X, Y = v->Y, Z = v->Z;
	v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
	v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
	v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
}

void mol_vector3_rotate_by_quaternion(struct mol_vector3 *v, const struct mol_quaternion *q)
{
	struct mol_vector3 tmp1, tmp2, tmp3; // Sorry for non-descriptive names
	MOL_VEC_MULT_SCALAR(tmp1, *v, q->W);
	MOL_VEC_CROSS_PROD(tmp2, *q, *v);
	MOL_VEC_ADD(tmp2, tmp1, tmp2);
	MOL_VEC_CROSS_PROD(tmp3, *q, tmp2);
	MOL_VEC_MULT_SCALAR(tmp3, tmp3, 2);
	MOL_VEC_ADD(*v, *v, tmp3);
}

void mol_vector3_translate_list(struct mol_vector3 *v_list, size_t len,
				const struct mol_vector3 *tv)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(tv);
	for (size_t i = 0; i < len; ++i) {
		MOL_VEC_ADD(v_list[i], (*tv), v_list[i]);
	}
}

void mol_vector3f_translate_list(struct mol_vector3f *v_list, size_t len,
				 const struct mol_vector3f *tv)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(tv);
	for (size_t i = 0; i < len; ++i) {
		MOL_VEC_ADD(v_list[i], (*tv), v_list[i]);
	}
}

void mol_vector3l_translate_list(struct mol_vector3l *v_list, size_t len,
				 const struct mol_vector3l *tv)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(tv);
	for (size_t i = 0; i < len; ++i) {
		MOL_VEC_ADD(v_list[i], (*tv), v_list[i]);
	}
}

void mol_vector3_rotate_list(struct mol_vector3 *v_list, size_t len,
			     const struct mol_matrix3 *r)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(r);
	double X, Y, Z;
	struct mol_vector3 *v;
	for (size_t i = 0; i < len; ++i) {
		v = &v_list[i];
		X = v->X; Y = v->Y; Z = v->Z;
		v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
		v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
		v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
	}
}

void mol_vector3f_rotate_list(struct mol_vector3f *v_list, size_t len,
			      const struct mol_matrix3f *r)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(r);
	float X, Y, Z;
	struct mol_vector3f *v;
	for (size_t i = 0; i < len; ++i) {
		v = &v_list[i];
		X = v->X; Y = v->Y; Z = v->Z;
		v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
		v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
		v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
	}
}

void mol_vector3l_rotate_list(struct mol_vector3l *v_list, size_t len,
			      const struct mol_matrix3l *r)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(r);
	long double X, Y, Z;
	struct mol_vector3l *v;
	for (size_t i = 0; i < len; ++i) {
		v = &v_list[i];
		X = v->X; Y = v->Y; Z = v->Z;
		v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
		v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
		v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
	}
}

void mol_vector3_move(struct mol_vector3 *v,
		      const struct mol_matrix3 *r, const struct mol_vector3 *tv)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(r);
	__MOL_NULL_ARG_CHECK(tv);
	double X = v->X, Y = v->Y, Z = v->Z;
	v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
	v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
	v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
	MOL_VEC_ADD((*v), (*tv), (*v));
}

void mol_vector3f_move(struct mol_vector3f *v,
		       const struct mol_matrix3f *r,
		       const struct mol_vector3f *tv)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(r);
	__MOL_NULL_ARG_CHECK(tv);
	float X = v->X, Y = v->Y, Z = v->Z;
	v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
	v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
	v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
	MOL_VEC_ADD((*v), (*tv), (*v));
}

void mol_vector3l_move(struct mol_vector3l *v,
		       const struct mol_matrix3l *r,
		       const struct mol_vector3l *tv)
{
	__MOL_NULL_ARG_CHECK(v);
	__MOL_NULL_ARG_CHECK(r);
	__MOL_NULL_ARG_CHECK(tv);
	long double X = v->X, Y = v->Y, Z = v->Z;
	v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
	v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
	v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
	MOL_VEC_ADD((*v), (*tv), (*v));
}

void mol_vector3_move_list(struct mol_vector3 *v_list, size_t len,
			   const struct mol_matrix3 *r,
			   const struct mol_vector3 *tv)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(r);
	__MOL_NULL_ARG_CHECK(tv);
	double X, Y, Z;
	struct mol_vector3 *v;
	for (size_t i = 0; i < len; ++i) {
		v = &v_list[i];
		X = v->X; Y = v->Y; Z = v->Z;
		v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
		v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
		v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
		MOL_VEC_ADD((*v), (*tv), (*v));
	}
}

void mol_vector3f_move_list(struct mol_vector3f *v_list, size_t len,
			    const struct mol_matrix3f *r,
			    const struct mol_vector3f *tv)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(r);
	__MOL_NULL_ARG_CHECK(tv);
	float X, Y, Z;
	struct mol_vector3f *v;
	for (size_t i = 0; i < len; ++i) {
		v = &v_list[i];
		X = v->X; Y = v->Y; Z = v->Z;
		v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
		v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
		v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
		MOL_VEC_ADD((*v), (*tv), (*v));
	}
}

void mol_vector3l_move_list(struct mol_vector3l *v_list, size_t len,
			    const struct mol_matrix3l *r,
			    const struct mol_vector3l *tv)
{
	__MOL_NULL_ARG_CHECK(v_list);
	__MOL_NULL_ARG_CHECK(r);
	__MOL_NULL_ARG_CHECK(tv);
	long double X, Y, Z;
	struct mol_vector3l *v;
	for (size_t i = 0; i < len; ++i) {
		v = &v_list[i];
		X = v->X; Y = v->Y; Z = v->Z;
		v->X = X * r->m11 + Y * r->m12 + Z * r->m13;
		v->Y = X * r->m21 + Y * r->m22 + Z * r->m23;
		v->Z = X * r->m31 + Y * r->m32 + Z * r->m33;
		MOL_VEC_ADD((*v), (*tv), (*v));
	}
}

bool mol_get_transform_to_residue(
	struct mol_vector3 *translation, struct mol_matrix3 *rotation,
	const struct mol_atom_group *ag, const struct mol_residue *residue)
{
	if (translation == NULL || rotation == NULL ||
	    ag == NULL || residue == NULL) {
		errno = EFAULT;
		return false;
	}

	size_t N_index = ag->natoms, CA_index = ag->natoms, C_index = ag->natoms;

	for (size_t i = residue->atom_start; i <= residue->atom_end; i++) {
		if (strcmp(ag->atom_name[i], " N  ") == 0) {
			N_index = i;
		} else if (strcmp(ag->atom_name[i], " CA ") == 0) {
			CA_index = i;
		} else if (strcmp(ag->atom_name[i], " C  ") == 0) {
			C_index = i;
		}
	}

	if ((N_index == ag->natoms) ||
	    (CA_index == ag->natoms) ||
	    (C_index == ag->natoms)) {
		return false;
	}

	mol_get_transform_to_atoms(
		translation, rotation,
		ag, N_index, CA_index, C_index);

	return true;
}

void mol_get_transform_to_atoms(
	struct mol_vector3 *translation, struct mol_matrix3 *rotation,
	const struct mol_atom_group *ag,
	size_t N_index, size_t CA_index, size_t C_index)
{
	__MOL_NULL_ARG_CHECK(translation);
	__MOL_NULL_ARG_CHECK(rotation);
	__MOL_NULL_ARG_CHECK(ag);

	struct mol_vector3 e1 = {0}, e2 = {0}, e3 = {0};
	double d;

	MOL_VEC_SUB(e1, ag->coords[N_index], ag->coords[CA_index]);

	// normalize e1
	d = sqrt(MOL_VEC_SQ_NORM(e1));
	MOL_VEC_DIV_SCALAR(e1, e1, d);

	MOL_VEC_SUB(e2, ag->coords[C_index], ag->coords[CA_index]);

	//dot product to get component of e1 along e2
	d = MOL_VEC_DOT_PROD(e1, e2);

	// subtract component
	MOL_VEC_MULT_SCALAR(e3, e1, d);
	MOL_VEC_SUB(e2, e2, e3);

	// normalize e2
	d = sqrt(MOL_VEC_SQ_NORM(e2));
	MOL_VEC_DIV_SCALAR(e2, e2, d);

	// generate third orthonormal component as cross product
	MOL_VEC_CROSS_PROD(e3, e1, e2);

	rotation->m11 = e1.X;
	rotation->m12 = e2.X;
	rotation->m13 = e3.X;
	rotation->m21 = e1.Y;
	rotation->m22 = e2.Y;
	rotation->m23 = e3.Y;
	rotation->m31 = e1.Z;
	rotation->m32 = e2.Z;
	rotation->m33 = e3.Z;

	*translation = ag->coords[CA_index];
}

void mol_get_symmetry_transform(
	struct mol_vector3 *out_translation, struct mol_matrix3 *out_rotation,
	const struct mol_vector3 *in_translation,
	const struct mol_matrix3 *in_rotation,
	const unsigned int subunit)
{
	__MOL_NULL_ARG_CHECK(out_translation);
	__MOL_NULL_ARG_CHECK(out_rotation);
	__MOL_NULL_ARG_CHECK(in_translation);
	__MOL_NULL_ARG_CHECK(in_rotation);

	MOL_VEC_COPY(*out_translation, *in_translation);
	MOL_MATRIX_COPY(*out_rotation, *in_rotation);
/*
	To construct subunits for C-n symmetry, the translation is calculated by 
	rotating the translation vector and adding it to the previous translation.  
	A way to visualize this is drawing a polygon. We have the first side of the 
	polygon as our initial translation. We then rotate that vector and add it to 
	the initial vector to draw the next side and get to the position of the next 
	subunit. And so on.

	How do we rotate it? We use the same rotation matrix we would apply to the initial 
	subunit. E.g. for a trimer, that rotation is 120, so we are rotating the vector 
	according to the interior angle of an equilateral polygon.
*/

	struct mol_vector3 tmp_translation;
	MOL_VEC_COPY(tmp_translation, *in_translation);
	for (unsigned int i = 1; i < subunit; i++) {
		mol_vector3_rotate(&tmp_translation, in_rotation);
		MOL_VEC_ADD(*out_translation, *out_translation, tmp_translation);
	}
/*
	The rotation is constructed by multiplying the rotation matrix by itself the number
	of times necessary for a subunit.
*/
	for (unsigned int i = 1; i < subunit; i++) {
		mol_matrix3_multiply(out_rotation, out_rotation, in_rotation);
	}
}

#undef __MOL_NULL_ARG_CHECK
